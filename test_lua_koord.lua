ret=0
i=0
dofile("./lua/visp_anten_info.lua")
dofile("./lua/visp_bitwise_work.lua")
dofile("./lua/visp_get_koord_prl.lua")
dofile("./lua/visp_get_location_time.lua")
dofile("./lua/visp_get_package.lua")
dofile("./lua/visp_koord_info.lua")
dofile("./lua/visp_meteo_info.lua")
dofile("./lua/visp_trass_info.lua")
dofile("./lua/visp_get_package_from_koord_prl.lua")
dofile("./lua/visp_string.lua")
dofile("./lua/visp_global_data_for_test.lua")


my_ki={}

my_ki.n_sender=SENDER_NUM
my_ki.n_receiver=RECEIVER_NUM
my_ki.kg_prl=true
my_ki.kk_prl=true
my_ki.type_i=KOORD_INFO_CONST
my_ki.psk=0

my_ki.ok_gradus=3.3
my_ki.og_gradus=2.2
my_ki.d_km=23.45
my_ki.a_gr=321.09
my_ki.h_km=0.246



fbs=get_koord_package(my_ki)

print(get_string("fbs",fbs))


my_ki2=read_koord_info(fbs)

print (get_string("my_ki", my_ki))

if my_ki2 then
	
	print (get_string("my_ki2", my_ki2))
else

end



