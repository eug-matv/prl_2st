ret=0
i=0
dofile("./lua/visp_anten_info.lua")
dofile("./lua/visp_bitwise_work.lua")
dofile("./lua/visp_get_koord_prl.lua")
dofile("./lua/visp_get_location_time.lua")
dofile("./lua/visp_get_package.lua")
dofile("./lua/visp_koord_info.lua")
dofile("./lua/visp_meteo_info.lua")
dofile("./lua/visp_trass_info.lua")
dofile("./lua/visp_get_package_from_koord_prl.lua")
dofile("./lua/visp_string.lua")
dofile("./lua/visp_global_data_for_test.lua")


my_prl={}
my_ki={}

my_ki.n_sender=SENDER_NUM
my_ki.n_receiver=RECEIVER_NUM
my_ki.kg_prl=true
my_ki.kk_prl=true
my_ki.type_i=KOORD_INFO_CONST
my_ki.psk=0

my_ki.ok_gradus=3.3
my_ki.og_gradus=2.2
my_ki.d_km=0
my_ki.a_gr=0
my_ki.h_km=0



my_prl.x_km=50.0
my_prl.y_km=0.15
my_prl.z_km=20*math.tan(2.56/180*math.pi)
print(get_string("my_prl",my_prl))

my_ki=make_emul_koord_info(my_ki,my_prl)

print(get_string("t_pkg",my_ki))

my_prl2=get_data_prl(my_ki)
print("my_prl2=",my_prl2)

print(get_string("my_prl2",my_prl2))



