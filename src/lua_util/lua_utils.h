/*Модуль: lua_utils
Автор: Матвеенко Е.А. ЧРЗ Полет ОКБ

Реализовны процедуры упрощающих работу с файлами LUA, которы используются для анализа и
разбора текста.

*/



#ifndef LOAD_LUA_FROM_DIR
#define LOAD_LUA_FROM_DIR


#define KOORD_INFO_CONST 0x0A

#define TRASS_INFO_CONST 0x02

#define METEO_INFO_CONST 0x0D

#define ANTEN_INFO_CONST 0x1D

#define CONTR_INFO_CONST 0x01




#define LUA_ERR_ISNOT_FUNCT (-1000)

#define LUA_ERR_ISNOT_TABLE (-2000)

#define LUA_ERR_PCALL       (-1001)

#define LUA_ERR_REZ_IS_NOT_INT (-1002)




#define LUA_ERR_DIRECTORY_NOT_EXISTS  (-10000)


#define LUA_ERR_ALREADY_NEWSTATE (-10001)

#define LUA_ERR_LUAL_NEWSTATE  (-10002)

#define LUA_ERR_ISNOT_CORRECT_DATA (-9999)


#include <QString>
#include "lua.hpp"
#include "OtmetkaDannye.h"






/*Функция загрузки всех модулей lua, размещенных в каталоге dir.
 * Возвращает значение число загруженных файлов, 0 - если каталог есть,
 * а файлов нет. -1 -- если проблемы с каталогом
 * загружает все файы каталога с расшируением lua*/
int luLoadLuaFilesFromDirectory(const QString &dir);


/*Добавление байта b в LUA.
Основная функция разбора текста, реализовання в Lua.
Возращает значения:
 LUA_ERR_ISNOT_FUNCT - ошибка, требуемая функция add_new_byte не определена в LUA
 LUA_ERR_PCALL - ошибка вызова функции add_new_byte в LUA
 LUA_ERR_REZ_IS_NOT_INT - результат вызова процедуры addn_new_byte в LUA - некорректный
 KOORD_INFO_CONST - распознан пакет с "координатной информацией", надо вызвать
       функцию luGetPRLInfo для получения координатных данных отметки из LUA
 TRASS_INFO_CONST - распознан пакет с "трассовой информацией", надо вызвать
       функцию luGetPRLInfo для получения координатных данных отметки из LUA
 1 байт успешно передан
*/
int luAddByteToFIFOInLUA(unsigned char b);

/*Передача опций в lua контекст
Возвращает:
 LUA_ERR_ISNOT_TABLE --- если не подготовлена внутри LUA таблица для приема данных
 1 успешно

*/
int luSetNewParametersForLUA(double min_ugol_kurs,
                             double max_ugol_kurs,
                             double min_ugol_glis,
                             double max_ugol_glis,
                             double min_ugol_scan_glis_in_kurs,
                             double max_ugol_scan_glis_in_kurs,
                             double min_ugol_scan_kurs_in_glis,
                             double max_ugol_scan_kurs_in_glis,
                             double x_prl_km, double y_prl_km,
                             int lLevelOfVPP,
                             double magnit_vpp,
                             double max_d_km
                             );


/*Передача информации о магнитном курсе ВПП
Возвращает:
 LUA_ERR_ISNOT_TABLE  --- если не подготовлена внутри LUA таблица для приема данных
 1 --- успех*/
int luSetNewMagnitVPP(double magnit_vpp
                     );

/*Заполнения данных о цели по последним полученному в ЛУА пакету с данными.
Данная процедура  должна вызываться сразу после того как luAddByteToFIFOInLUA
вернет значения KOORD_INFO_CONST или TRASS_INFO_CONST

Возвращаемые значения:
LUA_ERR_ISNOT_FUNCT - ошибка, требуемая функция add_new_byte не определена в LUA
LUA_ERR_ISNOT_CORRECT_DATA -- не был распознан правильный пакет
1 - успех, данные занесены в doc
*/
int luGetPRLInfo(struct TDannyeObCeli &doc);

/*Используется в программе test_udp. Отправляет байты на отправку*/
int luTestGetBytes(unsigned char *bytes);


/*Используется в программе test_udp. Формирует строку с данными*/
QString luTestGetPackageInfo();


/*Используется в программе test_udp. Формирует строку с данными*/
QString luTestGetBytesOut();


/*Выполнить программу LUA, заданную в str
Возвращает:
1 - успех
0 - ошибка

*/
int luDoString(const QString &str);

/*Закрытие всего и вся в LUA*/
int luCloseLua(void);


/*Получение последнего кода ошибки в виде текста*/
const char * luGetErrorMessage(void);


#endif // LOAD_LUA_FROM_DIR

