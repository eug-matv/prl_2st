#include <QDir>
#include <QStringList>
#include <QFileInfo>
#include <QFileInfoList>
#include <string.h>

#include "AnyTools.h"
#include "lua_utils.h"
#include "safe_function.h"

static lua_State *L=NULL;

static char lua_error_mes[1000];


static int luNewState()
{
    if(L)return LUA_ERR_ALREADY_NEWSTATE;
    L=luaL_newstate();
    if(!L)
    {

        return LUA_ERR_LUAL_NEWSTATE;
    }
    luaL_openlibs(L);
    return 1;
}




/*Возвращает значение число загруженных файлов, 0 - если каталог есть,
 * а файлов нет. -1 -- если проблемы с каталогом
 * загружает все файы каталога с расшируением lua*/
int luLoadLuaFilesFromDirectory(const QString &dir)
{
   QStringList filter_fn;
   QFileInfoList fil;
   QString fn;
   QDir d(dir);
   int i;
   int iRet;
   int n_of_f=0;
   QString debugStr;


   iRet=luNewState();
   if(iRet<=0)return iRet;


   debugStr=QDir::currentPath();

   if(!d.exists())
   {
       lua_close(L);
       L=NULL;

       sfSTRCPY(lua_error_mes,"Directory with lua-files do not exist!");
       return LUA_ERR_DIRECTORY_NOT_EXISTS;
   }

   filter_fn<<"*.lua";
   fil=d.entryInfoList(filter_fn);

   for(i=0; i<fil.size(); i++)
   {
        QFileInfo fileInfo = fil.at(i);
        fn=fileInfo.absoluteFilePath();
        if(!luaL_dofile(L,fn.toUtf8().data()))
        {
            n_of_f++;
            //Всё нормально
        }
   }

   if(n_of_f!=fil.size())
   {
       lua_close(L);
       L=NULL;

       sfSPRINTF(lua_error_mes ,"Only %d lua-file(s) of %d lua-files was did", n_of_f, fil.size());

       return (fil.size()-n_of_f)*1000+n_of_f;
   }
   lua_error_mes[0]=0;
   return n_of_f;
}


/*Добавление байт в LUA*/
int luAddByteToFIFOInLUA(unsigned char b)
{
    int iRet;
    lua_getglobal(L,"add_new_byte");
    if(!lua_isfunction(L,-1))
    {
        sfSTRCPY(lua_error_mes,"Matveenko: add_new_byte is not function");
        return LUA_ERR_ISNOT_FUNCT;
    }
    lua_pushinteger(L,(int)b);
    iRet=lua_pcall(L,1,1,0);
    if(iRet!=LUA_OK)
    {

        strncpy(lua_error_mes,lua_tostring(L,-1),sizeof(lua_error_mes));
        lua_error_mes[sizeof(lua_error_mes)]=0;
        return LUA_ERR_PCALL;
    }

    if(lua_isinteger(L,-1))
    {
        iRet=lua_tointeger(L,-1);
        lua_error_mes[0]=0;
    }else{
        iRet=LUA_ERR_REZ_IS_NOT_INT;
        sfSTRCPY(lua_error_mes,"Matveenko: function add_new_byte returns no integer");
    }

    lua_pop(L,1);
    return iRet;
}

/*Передача опций в lua контекст*/
int luSetNewParametersForLUA(double min_ugol_kurs,
                             double max_ugol_kurs,
                             double min_ugol_glis,
                             double max_ugol_glis,
                             double min_ugol_scan_glis_in_kurs,
                             double max_ugol_scan_glis_in_kurs,
                             double min_ugol_scan_kurs_in_glis,
                             double max_ugol_scan_kurs_in_glis,
                             double x_prl_km, double y_prl_km,
                             int lLevelOfVPP,
                             double magnit_vpp,
                             double max_d_km
                             )
{
    if(!L)return 0;
    lua_getglobal(L,"option_prl");
    if(!lua_istable(L,-1))
    {
        lua_pop(L,1);

        sfSTRCPY(lua_error_mes,"Error of getting table: option_prl in function luSetNewParametersForLUA");
        return LUA_ERR_ISNOT_TABLE;
    }

    lua_pushnumber(L,min_ugol_kurs);
    lua_setfield(L,-2,"min_ugol_kurs");

    lua_pushnumber(L,max_ugol_kurs);
    lua_setfield(L,-2,"max_ugol_kurs");

    lua_pushnumber(L,min_ugol_glis);
    lua_setfield(L,-2,"min_ugol_glis");

    lua_pushnumber(L,max_ugol_glis);
    lua_setfield(L,-2,"max_ugol_glis");


    lua_pushnumber(L,min_ugol_scan_glis_in_kurs);
    lua_setfield(L,-2,"min_ugol_scan_glis_in_kurs");

    lua_pushnumber(L, max_ugol_scan_glis_in_kurs);
    lua_setfield(L,-2,"max_ugol_scan_glis_in_kurs");

    lua_pushnumber(L, min_ugol_scan_kurs_in_glis);
    lua_setfield(L,-2,"min_ugol_scan_kurs_in_glis");

    lua_pushnumber(L, max_ugol_scan_kurs_in_glis);
    lua_setfield(L,-2,"max_ugol_scan_kurs_in_glis");

    lua_pushnumber(L,x_prl_km);
    lua_setfield(L,-2,"x_prl_km");

    lua_pushnumber(L, y_prl_km);
    lua_setfield(L,-2,"y_prl_km");

    lua_pushinteger(L, lLevelOfVPP);
    lua_setfield(L,-2,"lLevelOfVPP");

    lua_pushnumber(L, magnit_vpp);
    lua_setfield(L,-2,"magnit_vpp");

    lua_pushnumber(L, max_d_km);
    lua_setfield(L,-2,"max_d_km");
    lua_pop(L,1);

    return 1;
}


/*Передача опций в lua контекст*/
int luSetNewMagnitVPP(double magnit_vpp
                     )
{
    if(!L)return 0;
    lua_getglobal(L,"option_prl");
    if(!lua_istable(L,-1))
    {
        lua_pop(L,1);

        sfSTRCPY(lua_error_mes,"Error of getting table: option_prl in function luSetNewMagnitVPP()");
        return LUA_ERR_ISNOT_TABLE;
    }

    lua_pushnumber(L, magnit_vpp);
    lua_setfield(L,-2,"magnit_vpp");
    lua_pop(L,1);
    lua_error_mes[0]=0;
    return 1;
}


int luGetPRLInfo(struct TDannyeObCeli &doc)
{
    int iRet;
    if(!L)return 0;
    lua_getglobal(L,"visp_data_for_view");
    if(!lua_istable(L,-1))
    {
        lua_pop(L,1);
        return LUA_ERR_ISNOT_TABLE;
    }

    lua_getfield(L,-1,"tip");
    if(!lua_isinteger(L,-1))
    {
        lua_pop(L,2);
        return LUA_ERR_ISNOT_CORRECT_DATA;
    }

    iRet=lua_tointeger(L,-1);
    if(iRet<0)
    {
        lua_pop(L,2);
        return LUA_ERR_ISNOT_CORRECT_DATA;
    }

    doc.Tip=(unsigned short)iRet;
    lua_pop(L,1);



//Получим координату X
    lua_getfield(L,-1,"x_km");

    if(lua_isnumber(L,-1))
    {
        doc.X=Okrugl(lua_tonumber(L,-1)*1000.0);
    }else{
        doc.X=-10000000;
    }
    lua_pop(L,1);

//Получим координату Y
    lua_getfield(L,-1,"y_km");

    if(lua_isnumber(L,-1))
    {
        doc.Y=Okrugl(lua_tonumber(L,-1)*1000.0);
    }else{
        doc.Y=-10000000;
    }
    lua_pop(L,1);


//Получим координату Z
    lua_getfield(L,-1,"z_km");

    if(lua_isnumber(L,-1))
    {
        doc.Z=Okrugl(lua_tonumber(L,-1)*1000.0);
    }else{
        doc.Z=-10000000;
    }
    lua_pop(L,1);


//Получим скорость вдоль оси X
    lua_getfield(L,-1,"vx_m_s");
    if(lua_isinteger(L,-1))
    {
        doc.Vx=lua_tointeger(L,-1);
    }else
    if(lua_isnumber(L,-1))
    {
        doc.Vx=Okrugl(lua_tonumber(L,-1));
    }else{
        doc.Vx=-100000;
    }
    lua_pop(L,1);


//Получим скорость вдоль оси Y
    lua_getfield(L,-1,"vy_m_s");
    if(lua_isinteger(L,-1))
    {
        doc.Vy=lua_tointeger(L,-1);
    }else
    if(lua_isnumber(L,-1))
    {
        doc.Vy=Okrugl(lua_tonumber(L,-1));
    }else{
        doc.Vy=-100000;
    }
    lua_pop(L,1);

    //Получим скорость вдоль оси Z
    lua_getfield(L,-1,"vz_m_s");
    if(lua_isinteger(L,-1))
    {
        doc.Vz=lua_tointeger(L,-1);
    }else
    if(lua_isnumber(L,-1))
    {
        doc.Vz=Okrugl(lua_tonumber(L,-1));
    }else{
        doc.Vz=-100000;
    }
    lua_pop(L,1);


//Осталось получить номер и позывной - это тоже номер
    lua_getfield(L,-1,"t_num");
    if(lua_isinteger(L,-1))
    {
        doc.lNomerVozdushnogoSudna=lua_tointeger(L, -1);
        sfSPRINTF(doc.sPozyvnoy,"%04ld",(long)doc.lNomerVozdushnogoSudna);
    }else{
        doc.lNomerVozdushnogoSudna=-1;
        doc.sPozyvnoy[0]=0;
    }
    lua_pop(L,1);

    lua_pop(L,1);

    if(doc.Tip>5)
    {
        doc.timeOfLife1_ms=1200;
        doc.timeOfLife2_ms=100000;
        doc.timeOfLifeWithOut_ms=4500;
    }else{
        doc.timeOfLife1_ms=1000;
        doc.timeOfLife2_ms=1000;
        doc.timeOfLifeWithOut_ms=1000;
    }


    return 1;

}



/*Для test_udp*/

//Получить последовательность байт на отправку
//Возвращает:>0 --- успешно число байт
//-1 --- информация о пакетах не свормирована
int luTestGetBytes(unsigned char *bytes)
{
    int iRet, n_of_b;
    if(!L)return 0;
    lua_getglobal(L,"glbl_get_bytes");
    if(!lua_isfunction(L,-1))
    {
        sfSTRCPY(lua_error_mes,"Matveenko: glbl_get_bytes is not function");
        return LUA_ERR_ISNOT_FUNCT;
    }
    iRet=lua_pcall(L,0,1,0);
    if(iRet!=LUA_OK)
    {
        strncpy(lua_error_mes,lua_tostring(L,-1),sizeof(lua_error_mes));
        lua_error_mes[sizeof(lua_error_mes)]=0;
        return LUA_ERR_PCALL;
    }

    if(!lua_isinteger(L,-1))
    {
        lua_pop(L,1);
        return -2;
    }

    iRet=lua_tointeger(L,-1);
    lua_pop(L,1);

    if(iRet<1)
    {
        return iRet;
    }

//Получим последовательгость байт

    lua_getglobal(L,"t_bytes");
    if(!lua_istable(L,-1))
    {
        lua_pop(L,1);
        return LUA_ERR_ISNOT_TABLE;
    }

    lua_getfield(L,-1,"size");

    if(!lua_isinteger(L,-1))
    {
        lua_pop(L,2);
        return LUA_ERR_ISNOT_CORRECT_DATA;
    }


    n_of_b=lua_tointeger(L,-1);
    lua_pop(L,1);




    for(int i=1;i<=n_of_b;i++)
    {
        lua_pushinteger(L, i);
        lua_gettable(L,-2);
        if(!lua_isinteger(L,-1))
        {
            lua_pop(L,2);
            return LUA_ERR_ISNOT_CORRECT_DATA;
        }
        iRet=lua_tointeger(L,-1);
        lua_pop(L,1);
        if(iRet<0||iRet>255)
        {
            lua_pop(L,1);
            return LUA_ERR_ISNOT_CORRECT_DATA;
        }
        bytes[i-1]=(unsigned char)iRet;
    }
    lua_pop(L,1);
    return n_of_b;
}



QString luTestGetPackageInfo()
{
    int iRet;
    QString str1;
    if(!L)return 0;

    lua_getglobal(L,"glbl_get_string_pkg");
    if(!lua_isfunction(L,-1))
    {
        sfSTRCPY(lua_error_mes,"Matveenko: glbl_get_string_pkg is not function");
        return QString(lua_error_mes);
    }

    iRet=lua_pcall(L,0,1,0);
    if(iRet!=LUA_OK)
    {

        strncpy(lua_error_mes,lua_tostring(L,-1),sizeof(lua_error_mes));
        lua_error_mes[sizeof(lua_error_mes)]=0;
        lua_pop(L,1);
        return QString(lua_error_mes);
    }

    if(!lua_isstring(L,-1))
    {
        lua_pop(L,1);
        return QString("Error: rezult is no string");
    }

    str1=QString(lua_tostring(L,-1));
    lua_pop(L,1);
    return str1;
}



QString luTestGetBytesOut()
{
    int iRet;
    QString str1;
    if(!L)return 0;

    lua_getglobal(L,"glbl_get_string_bytes");
    if(!lua_isfunction(L,-1))
    {
        sfSTRCPY(lua_error_mes,"Matveenko: glbl_get_string_bytes is not function");
        return QString(lua_error_mes);
    }

    iRet=lua_pcall(L,0,1,0);
    if(iRet!=LUA_OK)
    {

        strncpy(lua_error_mes,lua_tostring(L,-1),sizeof(lua_error_mes));
        lua_error_mes[sizeof(lua_error_mes)]=0;
        lua_pop(L,1);
        return QString(lua_error_mes);
    }

    if(!lua_isstring(L,-1))
    {
        lua_pop(L,1);
        return QString("Error: rezult is no string");
    }

    str1=QString(lua_tostring(L,-1));
    lua_pop(L,1);
    return str1;

}

int luDoString(const QString &str)
{
    if(!L)return 0;
    if(luaL_dostring(L,str.toUtf8().data()))
    {
        return 0;
    }
    return 1;
}



int luCloseLua(void)
{
    if(!L)return 0;
    lua_close(L);
    L=NULL;
    return 1;
}




const char * luGetErrorMessage(void)
{
    return lua_error_mes;
}

