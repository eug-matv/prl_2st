﻿
#include "stdafx.h"
#include "vc_graphout.h"
#include "RZPLinGlaf.h"
#include "m_radiobuttonsgroup.h"


#define VCG_SIZE_TOOLPANEL 200

static TRZPDraw *rzpDraw=NULL;
static TRadioButtonsGroup *rbg_max_dalnost=NULL;


static int static_vcpMakeOrResizeMaxDalnost(HWND hwnd, HINSTANCE hInst)
{
	RECT rect;

	TMyRBGroupDescr mrbgd;
	TMyRadioButtonDescr mrbd;

	if(!hwnd||!hInst)return 0;
	if(rbg_max_dalnost)
	{
		rbg_max_dalnost->renew();
	}else{			
		mrbgd.code=VCG_MAX_DALNOST_CODE;
		mrbgd.dx=VCG_SIZE_TOOLPANEL-20;
		mrbgd.dy=160;
		mrbgd.hinstance=hInst;
		mrbgd.isFromBottom=0;
		mrbgd.isFromRight=1;
		mrbgd.parent_hwnd=hwnd;
		_tcscpy(mrbgd.text, _T("Макс.дальность"));
		mrbgd.x=VCG_SIZE_TOOLPANEL-10;
		mrbgd.y=10;
		
		rbg_max_dalnost=new TRadioButtonsGroup(mrbgd);
		if(!rbg_max_dalnost)return (-1);
		
		mrbd.code=VCG_MAX_DALNOST_CODE+1;
		mrbd.dx=mrbgd.dx-10;
		mrbd.dy=mrbgd.dy/5-5;
		mrbd.f=NULL;		//Пока ничего не будем вводить
		_tcscpy(mrbd.text, _T("5 км"));
		mrbd.x=5;
		mrbd.y=mrbgd.dy/6;
		rbg_max_dalnost->addRadioButton(mrbd);
		
		_tcscpy(mrbd.text, _T("10 км"));
		mrbd.code++;
		mrbd.y+=mrbgd.dy/5;
		mrbd.f=NULL;		//Пока ничего не будем вводить
		rbg_max_dalnost->addRadioButton(mrbd);

		_tcscpy(mrbd.text, _T("20 км"));
		mrbd.code++;
		mrbd.y+=mrbgd.dy/5;
		mrbd.f=NULL;		//Пока ничего не будем вводить
		rbg_max_dalnost->addRadioButton(mrbd);

		_tcscpy(mrbd.text, _T("40 км"));
		mrbd.code++;
		mrbd.y+=mrbgd.dy/5;
		mrbd.f=NULL;		//Пока ничего не будем вводить
		rbg_max_dalnost->addRadioButton(mrbd);

		_tcscpy(mrbd.text, _T("60 км"));
		mrbd.code++;
		mrbd.y+=mrbgd.dy/5;
		mrbd.f=NULL;		//Пока ничего не будем вводить
		rbg_max_dalnost->addRadioButton(mrbd);

	}
	return 1;
}




int vcpResizeRZP(HWND hwnd, HINSTANCE hInst)
{	RECT rect,
		rDraw;
	int iRet;
	if(!hwnd)return 0;
	GetClientRect(hwnd,&rect);
	rDraw.left=rect.left+5;
	rDraw.top=rect.top+5;
	rDraw.bottom=rect.bottom-10;
	rDraw.right=rect.right-VCG_SIZE_TOOLPANEL-5;

	if(!rzpDraw)	
	{
		rzpDraw=new TRZPDraw("c:\\work\\prl_2st\\log\\log.txt",
						 "c:\\work\\prl_2st\\log\\error_log.txt");	
	
		if(!rzpDraw)return (-1);
//Получим размер клиентской части окна области 		
		iRet=rzpDraw->BeginWork(hwnd,rDraw,
			"C:\\work\\prl_2st\\ini\\PreSetup.ini",
			"C:\\work\\prl_2st\\ini\\User.ini");
		
		if(!iRet)return (-2);
	}else{
		rzpDraw->UstanovitOblastVyvoda(rDraw);
	}
	static_vcpMakeOrResizeMaxDalnost(hwnd,hInst);
	return 1;

}