﻿#include "ci_package_base.h"
unsigned char TCIPackageBase::cip_types[5]={CIP_VOZDUSH_SUDNO_INFO, CIP_TRASSA_INFO, 
                           CIP_METEO_INFO, CIP_ORIENT_ANTENNA_INFO, CIP_CONTROL_INFO};

/*Конструктор*/
TCIPackageBase::TCIPackageBase(	unsigned char n_s,	//адрес отправителя 
				unsigned char n_r	//адрес получателя		
				)
{
	n_sender=n_s;
	n_receiver=n_r;
	n_of_bytes=0;
	last_cip_type=0xFF;	
	input_bytes=bytes[0];
	last_bytes=bytes[1];


}



//Возвращает: 0 - байт не принят. 1 - байт принят.
//2 - пакет распознан. Можно читать данные пакета
int TCIPackageBase::addByte(unsigned char b)
{
	if(n_of_bytes==0)
	{
		if(b!=n_sender)
		{
			return 0;
		}
		input_bytes[0]=b;
		n_of_bytes=1;
		return 1;
	}else
	if(n_of_bytes==1)
	{
		if(b!=n_receiver)
		{
			n_of_bytes=0;
			return 0;
		}
		input_bytes[1]=b;
		n_of_bytes=2;
		return 1;
	}else
	if(n_of_bytes==3)
	{
		unsigned char b1;
		b1=b>>2;
		for(int i=0;i<5;i++)
		{
			if(cip_types[i]==b1)
			{
				 cur_cip_type=b1;
  				 input_bytes[3]=b;
                 n_of_bytes++;
				 return 1;	
			}
		}
//Ни один тип пакета не определен
//Проверим первые 3 байта на наличие байт n_sender и n_receiver
		if(input_bytes[1]==n_sender&&input_bytes[2]==n_receiver)
		{
			input_bytes[0]=n_sender;
			input_bytes[1]=n_receiver;
			input_bytes[2]=b;
			cur_cip_type=0xFF;
			n_of_bytes=3;
			return 1;
		}else
		if(input_bytes[2]==n_sender&&b==n_receiver)
		{
			input_bytes[0]=n_sender;
			input_bytes[1]=n_receiver;
			cur_cip_type=0xFF;
			n_of_bytes=2;
			return 1;
			
		}else 
		if(b==n_sender)
		{
			input_bytes[0]=n_sender;
			cur_cip_type=0xFF;
			n_of_bytes=1;
			return 1;			
		}
		n_of_bytes=0;
		return 0;

	}else
	if(n_of_bytes==17)
	{
		unsigned char *tmp_bytes;
		input_bytes[n_of_bytes]=b;
		tmp_bytes=input_bytes;
		input_bytes=last_bytes;
		last_bytes=tmp_bytes;
		last_cip_type=cur_cip_type;
		cur_cip_type=0xFF;
		n_of_bytes=0;
		return 2;
		
	}
	input_bytes[n_of_bytes]=b;
	n_of_bytes++;
	return 1;
}
	
	//Получить тип последнего пакета
//Если значение известно, возвращает одно из значений CIP_...
//Если не известно то 0xFF	
unsigned char TCIPackageBase::getTypeOfLastPackage()
{
	return last_cip_type;
}




/*Получаемые значения*/

int TCIPackageBase::getChisloSoZnakomFromBytes(unsigned int four_bytes,
                                               int n_of_bits)
{
    int i;
    unsigned int _bit;
    union
    {
       unsigned int ui;
       int i;
    } b;

    if(n_of_bits>=32)
    {

#ifdef CIP_TYPE_PRYAMOY_KOD
        if((four_bytes&(1<<31))
        {
           b.ui=four_bytes&(~(1<<31));
           return (-b.i);
        }
        b.ui=four_bytes;
        return b.i;
#else
        b.ui=four_bytes;
        return b.i;
#endif
    }

    _bit=four_bytes&(1<<(n_of_bits-1));
    if(_bit)_bit=1;
    b.ui=four_bytes;
    if(_bit)
    {
        for(i=n_of_bits;i<32;i++)
        {
            b.ui=b.ui|(1<<i);
        }
    }
    return b.i;
}


int TCIPackageBase::isKoordKurs_PRL()
{
    if(last_bytes[2]&0x2)return 1;
    return 0;
}


int TCIPackageBase::isKoordGlissad_PRL()
{
    if(last_bytes[2]&0x1)return 1;
    return 0;
}


double TCIPackageBase::dfVO_OtklonenieKurs()
{
    unsigned int uiData;
    uiData=(((unsigned int)(last_bytes[4]))<<4)|((unsigned int)(last_bytes[5]>>4));
    return (0.01*getChisloSoZnakomFromBytes(uiData,12));
}

double TCIPackageBase::dfVO_OtklonenieGlissada()
{
    unsigned int uiData;
    uiData=(((unsigned int)(last_bytes[5]&0x0F))<<8)|((unsigned int)(last_bytes[17]));
    return (0.01*getChisloSoZnakomFromBytes(uiData,12));
}


//3) получение дальноти в км
double TCIPackageBase::dfVO_Dalnost_km()
{
    unsigned int uiData;
    uiData=((unsigned int)(last_bytes[7]))|((unsigned int)(last_bytes[6]<<8));
    return (0.001*uiData);
}

//4) азимут в градусах
double TCIPackageBase::dfVO_Azimuth()
{
    unsigned int uiData;
    uiData=((unsigned int)(last_bytes[9]))|((unsigned int)(last_bytes[8]<<8));
    return (0.01*uiData);
}


//5) Это контрольный предмет
int TCIPackageBase::isVO_KMP()
{
    if((last_bytes[10]&(1<<6)))
    {
        return 1;
    }
    return 0;
}


//6) вид высоты полета. пока возврашает только 0 - локационный
int TCIPackageBase::iVO_TipVysotyPolyota()
{
    return 0;
}


//7) Высота полета в км
double TCIPackageBase::dfVO_Vysota_km()
{
    unsigned int uiData;
    uiData=(((unsigned int)(last_bytes[10]&0x1F))<<8)|((unsigned int)(last_bytes[11]));
    return (0.001*uiData);
}


//8) Время локации в с
double TCIPackageBase::dfVO_Time()
{
    unsigned int uiData;
    uiData=((unsigned int)(last_bytes[15]))|((unsigned int)(last_bytes[14]<<8))|
            ((unsigned int)(last_bytes[16]<<16));
    return (0.01*uiData);
}

int TCIPackageBase::iVO_h_Time()
{

    return (int)(dfVO_Time()/3600.0);
}

int TCIPackageBase::iVO_m_Time()
{
    return (int)((dfVO_Time()-iVO_h_Time()*3600)/60.0);
}


double TCIPackageBase::dfVO_s_Time()
{
    return (dfVO_Time()-3600*iVO_h_Time()-60*iVO_m_Time());
}




/*Сообщения о трассе*/
//1) признак сопровождения.
//	1 - новая трасса
//      3 - координаты экстраполированные
//	4 - обновление X, Y
//	6 - обновление X,Y,H
//      7 - сброс трассы
int TCIPackageBase::iTRS_SOPR()
{
    return (int)(last_bytes[2]>>5);
}

//2) номер трассы 1-511
int TCIPackageBase::iTRS_NomerTrassy()
{
    unsigned int uiData;
    uiData=(((unsigned int)(last_bytes[4]))<<1)|(((unsigned int)(last_bytes[5]))>>7);
    return (int)uiData;
}


//3) прямоугольная координата X в км со знаком
double TCIPackageBase::dfTRS_X_km()
{
   unsigned int uiData;
   uiData=(((unsigned int)(last_bytes[5]&0x02))<<15)|
              (((unsigned int)(last_bytes[6]))<<8)|
              ((unsigned int)(last_bytes[7]));
   return (0.01*getChisloSoZnakomFromBytes(uiData,17));
}



//4) прямоугольная координата Y в км со знаком
double TCIPackageBase::dfTRS_Y_km()
{
    unsigned int uiData;
    uiData=(((unsigned int)(last_bytes[5]&0x01))<<16)|
               (((unsigned int)(last_bytes[8]))<<8)|
               ((unsigned int)(last_bytes[9]));
    return (0.01*getChisloSoZnakomFromBytes(uiData,17));
}

//5) время, когда был расчет координат трассы
double TCIPackageBase::dfTRS_Time()
{
    unsigned int uiData;
    uiData=((unsigned int)(last_bytes[15]))|((unsigned int)(last_bytes[14]<<8))|
            ((unsigned int)(last_bytes[16]<<16));
    return (0.01*uiData);
}


int TCIPackageBase::iTRS_h_Time()
{

    return (int)(dfTRS_Time()/3600.0);
}

int TCIPackageBase::iTRS_m_Time()
{
    return (int)((dfTRS_Time()-iTRS_h_Time()*3600)/60.0);
}


double TCIPackageBase::dfTRS_s_Time()
{
    return (dfTRS_Time()-3600*iTRS_h_Time()-60*iTRS_m_Time());
}




//6) высота полета в км
double TCIPackageBase::dfTRS_Vysota_km()
{
    unsigned int uiData;
    uiData=(((unsigned int)(last_bytes[10]&0x1F))<<8)|((unsigned int)(last_bytes[11]));
    return (0.001*uiData);
}


//7) Вид высота полета
int TCIPackageBase::iTRS_TipVysotyPolyota()
{
    return 0;
}


//8) скорость по оси X в м/с
double TCIPackageBase::dfTRS_Vx_m_s()
{
    unsigned int uiData, uiZnack;
    if(last_bytes[10]&(1<<6))
    {
        uiZnack=1;
    }else{
        uiZnack=0;
    }
    uiData=(uiZnack<<8)|((unsigned int)last_bytes[12]);
    return (5.0*getChisloSoZnakomFromBytes(uiData,9));
}



//9) скорость по оси Y в м/с
double TCIPackageBase::dfTRS_Vy_m_s()
{
    unsigned int uiData, uiZnack;
    if(last_bytes[10]&(1<<5))
    {
        uiZnack=1;
    }else{
        uiZnack=0;
    }
    uiData=(uiZnack<<8)|((unsigned int)last_bytes[13]);
    return (5.0*getChisloSoZnakomFromBytes(uiData,9));
}


//10) признаки маневра
//10)a)	маневр по курсу
int TCIPackageBase::isTRS_ManevrPoKursu()
{
    if(last_bytes[5]&(1<<4))
    {
        return 1;
    }
    return 0;
}

//10)б)
int TCIPackageBase::isTRS_ManevrPoVysote()
{
    if(last_bytes[5]&(1<<5))
    {
        return 1;
    }
    return 0;
}



//10)в)
int TCIPackageBase::isTRS_ManevrPoSkorosti()
{
    if(last_bytes[5]&(1<<6))
    {
        return 1;
    }
    return 0;
}


//11 скорость изменения высоты полета
double TCIPackageBase::dfTRS_Vh_m_s()
{
    return (double)getChisloSoZnakomFromBytes((unsigned int)(last_bytes[17]),8);
}


//12 скорость сообщение об ориентации антены ПРЛ
double TCIPackageBase::dfPRLA_AzmtCenterKurs()
{
         unsigned int uiData=0;
         uiData=(((unsigned int)last_bytes[6])<<2)|(last_bytes[7]>>6);
         return 0.5*(double)uiData;
}


//13 Азимут  положения максимума
 double TCIPackageBase::dfPRLA_AzmtMaxDiagNaprGlis()
 {
        unsigned int uiData=0;
        uiData=(((unsigned int)last_bytes[8])<<2)|(last_bytes[9]>>6);
        return 0.5*(double)uiData;
 }

