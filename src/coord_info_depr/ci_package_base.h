﻿/*
Автор: Матвеенко Е.А.
Базовый класс для описания пакетов
*/

#ifndef __CI_PACKAGE_BASE_H__
#define __CI_PACKAGE_BASE_H__

//Изменяемый параметр по умолчанию дополнительный, иначе прямой код
//#define CIP_TYPE_PRYAMOY_KOD





//ТИП ДАННЫХ - координатная информация о воздушном судне
#define CIP_VOZDUSH_SUDNO_INFO  0x0A

//ТИП ДАННЫХ - сообщение о трассе ВО
#define CIP_TRASSA_INFO	0x02

//ТИП ДАННЫХ - сообщение о метео. Пока не реализовано
#define CIP_METEO_INFO  0x0B	

//ТИП ДАННЫХ - ориентация антены
#define CIP_ORIENT_ANTENNA_INFO  0x1B

//ТИП ДАННЫХ - контрольное донесение 
#define CIP_CONTROL_INFO 0x01




class TCIPackageBase
{
private:
static 	unsigned char cip_types[5];	//Коды типов пакетов
	unsigned char n_sender;  	//Байты должны совпадать с байтом bytes[0]
	unsigned char n_receiver;	//Байты должны совпадать с байтом bytes[1]
	unsigned char bytes[2][18];	//Байты класса
	unsigned char *input_bytes;	//Вводимые байты
	unsigned char *last_bytes;
	int n_of_bytes;			//Число байт.
	unsigned char last_cip_type;	//Последний тип данных, которые были получены
	unsigned char cur_cip_type;	//текущий тип пакета


protected:

    static int getChisloSoZnakomFromBytes(unsigned int four_bytes,
                                          int n_of_bits);



public:
	TCIPackageBase(	unsigned char n_s,
			unsigned char n_r			
			);

//Возвращает: 0 - байт не принят. 1 - байт принят.
//2 - пакет распознан. Можно читать данные пакета
	int addByte(unsigned char b);	

	
//Получить тип последнего пакета
//Если значение известно, возвращает одно из значений CIP_...
//Если не известно то 0xFF	
	unsigned char getTypeOfLastPackage();
	
	
	
//Получение информации из пакета
//1) получение источников информации для ВО и для трассы
	int isKoordKurs_PRL(); //1 - есть координаты курса, 0 - нет координат курса
	int isKoordGlissad_PRL();	//1 - есть координаты глиссады, 0 - нет информации о координатых глиссады

/*Сообщения о воздушном объекта*/	
//1) 	Отклонение от осевой линии курса в градусах
	double dfVO_OtklonenieKurs();

//2) отклонение от осевой линии глиссады в градусах
	double dfVO_OtklonenieGlissada();

//3) получение дальноти в км
	double dfVO_Dalnost_km();
	
//4) азимут в градусах
	double dfVO_Azimuth();


//5) Это контрольный предмет
	int isVO_KMP();

//6) вид высоты полета. пока возврашает только 0 - локационный
	int iVO_TipVysotyPolyota();

//7) Высота полета в км
	double dfVO_Vysota_km();	


//8) Время локации в с
	double dfVO_Time();
    int iVO_h_Time();
    int iVO_m_Time();
    double dfVO_s_Time();


/*Сообщения о трассе*/
//1) признак сопровождения.
//	1 - новая трасса
//      3 - координаты экстраполированные
//	4 - обновление X, Y
//	6 - обновление X,Y,H
//      7 - сброс трассы
	int iTRS_SOPR();

//2) номер трассы 1-511
	int iTRS_NomerTrassy();

//3) прямоугольная координата X в км со знаком
	double dfTRS_X_km();

//4) прямоугольная координата Y в км со знаком
	double dfTRS_Y_km();

//5) время, когда был расчет координат трассы
	double dfTRS_Time();

    int iTRS_h_Time();
    int iTRS_m_Time();
    double dfTRS_s_Time();


//6) высота полета в км
	double dfTRS_Vysota_km();

//7) Вид высота полета
	int iTRS_TipVysotyPolyota();

//8) скорость по оси X в м/с
	double dfTRS_Vx_m_s();

//9) скорость по оси Y в м/с	
	double dfTRS_Vy_m_s();

//10) признаки маневра 
//10)a)	маневр по курсу
	int isTRS_ManevrPoKursu();

//10)б)
	int isTRS_ManevrPoVysote();

//10)в)
	int isTRS_ManevrPoSkorosti();

//11 скорость изменения высоты полета
    double dfTRS_Vh_m_s();

//12  сообщение об ориентации антены ПРЛ
    double dfPRLA_AzmtCenterKurs();


//13 Азимут  положения максимума
     double dfPRLA_AzmtMaxDiagNaprGlis();


};


#endif
