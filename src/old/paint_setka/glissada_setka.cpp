﻿#include <math.h>
#include <stdio.h>
#include "glissada_setka.h"
#include "bp_line.h"
#include "bp_figures.h"

#define M_PI       3.14159265358979323846

//Первичный расчет всех параметров	
int TGlissadaSetka::protected_initParameters()
{
	int i;
	double tmp;
	double max_vys,dx,dfH,dfH_km;
//Вычтем долю последнего коэффициента
	tmp=0;
	for(i=0;i<optns.n_of_Daln-1;i++)
	{
		tmp+=optns.dfDolyaDaln_km[i];
	}
		
	if(tmp>=1.0)
	{
		return -1;
	}

	optns.dfDolyaDaln_km[optns.n_of_Daln-1]=1.0-tmp;

//Определеим коэффициенты по высоте
//Линия идет из угла (optns.left, optns.top+optns.height-1) в угол (optns.left+optns.width-1, optns.top)
//	
	dKoef1=((double)height)/((double)width);
	for(i=0;i<optns.n_of_Daln;i++)
	{
		dfW=width*optns.dfDolyaDaln_km[i];
		dfH=dfW*dKoef1;
		if(i==0)
		{
			dfH_km=(optns.dfDalnostAntennaVPP_km+optns.iNextDaln_km[0])*tan(dfMaxUgol_grds/180.0*M_PI);
		}else{
			dfH_km=(optns.iNextDaln_km[i]-optns.iNextDaln_km[i-1])*tan(dfMaxUgol_grds/180.0*M_PI);
		}
		dfKoefH[i]=dfH/dfH_km;
	}

}


int TGlissadaSetka::protected_paintSectorGlissady(void *cntx)
{
	int x[10],y[10];
	int i,k=1;
	int iRet;
	double dfXkm, dfYkm,dfXkm1,dfYkm1;
	double ugl;
	  
//определим координаты самой первой точки - это точка места посадки судна
	dfXkm=optns.dfDalnostAntennaVPP_km+optns.dfDalnostPrizemlTorets_km;
	dfYkm=-optns.dfVysota_ant_km;
	if((iRet=getXYByXkmYkm(dfXkm,0,dfYkm,0,x[0],y[0]))<=0)
	{
		return iRet;
	}
	  
//определим координаты объекты	  
	ugl=optns.dfUgolGlissady_grds+optns.dfDUgolGlissadyPlus_grds;
	for(i=0;i<optns.n_of_Daln;i++)
	{
	    dfXkm1=dfXkm+optns.dfDalnostUgolsGlissady_km;
	    if(dfXkm1<optns.iNextDaln_km[i]+optns.dfDalnostAntennaVPP_km)
	    {
		dfYkm1=optns.dfDalnostUgolsGlissady_km*tan(ugl/180.0*M_PI)-optns.dfVysota_ant_km;
		if((iRet=getXYByXkmYkm(dfXkm1,0,dfYkm1,0,x[k],y[k]))<=0)
		{
			return iRet;
		}

		k++;
		break;
	    }else{
		dfYkm1=(optns.iNextDaln_km[i]-optns.dfDalnostPrizemlTorets_km)*tan(ugl/180.0*M_PI)-optns.dfVysota_ant_km;
		if((iRet=getXYByXkmYkm(optns.iNextDaln_km[i]+optns.dfDalnostAntennaVPP_km,0,dfYkm1,0,x[k],y[k]))<=0)
		{
			return iRet;
		}
		k++;
	    }	
	}

	ugl=optns.dfUgolGlissady_grds-optns.dfDUgolGlissadyMinus_grds;
        dfXkm1=dfXkm+optns.dfDalnostUgolsGlissady_km;
	dfYkm1=optns.dfDalnostUgolsGlissady_km*tan(ugl/180.0*M_PI)-optns.dfVysota_ant_km;
	if((iRet=getXYByXkmYkm(dfXkm1,0,dfYkm1,0,x[k],y[k]))<=0)
	{
			return iRet;
	}
	k++;
	i--;
	for(;i>=0;i--)
	{
		dfYkm1=(optns.iNextDaln_km[i]-optns.dfDalnostPrizemlTorets_km)*tan(ugl/180.0*M_PI)-optns.dfVysota_ant_km;
		if((iRet=getXYByXkmYkm(optns.iNextDaln_km[i]+optns.dfDalnostAntennaVPP_km,0,dfYkm1,0,x[k],y[k]))<=0)
		{
			return iRet;
		}
		k++;
	}	
	
//Точки расчитаны - надо нарисовать сектор допустимых отклонений
	if(pbDrawPolygon(cntx,x,y,k,BP_PS_SOLID,1,uiSectorGlissadyCol,0,uiSectorGlissadyCol)<=0)
	{
		return (-100);
	}
	return 1;
}


//Рисовать угловые линии сетки
int TGlissadaSetka::protected_paintUglovLinii(void *cntx)
{
	double ugl;
	double dfXkm, dfYkm;
	int x1,y1,x2,y2;
	void *font;
	char strk[50];	
	int sz_txt_x, sz_txt_y;
	font=bptCreateFont(&(optns.textOptUgolLines));
	if(!font)
	{
		return -1;
	}
	ugl=optns.dfMinUgolMer_grds;
	x1=left;
	y1=top-height+1;
	


	getXYByUgolAndDaln(ugl,optns.iNextDaln_km[optns.n_of_Daln-1],1,x2,y2);
	
	pblDrawSegment(cntx,x1,y1,x2,y2,optns.loUgolLines.uiPenStyle,optns.loUgolLines.uiWidth,optns.loUgolLines.uiColour);	
	
	sprintf(strk,"%1.1lf°",ugl);	

//Надо справа подписать
	bptGetTextSize(cntx,font,strk,&sz_txt_x, &sz_txt_y);
	bptTextOut(cntx,font,1,0,optns.loUgolLines.uiColour,x2+4,y2-sz_txt_y/2,strk);

	for(;ugl<optns.dfMaxUgolMer_grds-0.001;ugl+=dfStepUgolMer_grds)
	{	
		getXYByUgolAndDaln(ugl,optns.iMinDUgols_km,1,x1,y1);
		getXYByUgolAndDaln(ugl,optns.iNextDaln_km[optns.n_of_Daln-1],1,x2,y2);
		pblDrawSegment(cntx,x1,y1,x2,y2,optns.loUgolLines.uiPenStyle,optns.loUgolLines.uiWidth,optns.loUgolLines.uiColour);	
		sprintf(strk,"%1.1lf°",ugl);	
		bptGetTextSize(cntx,font,strk,&sz_txt_x, &sz_txt_y);
		bptTextOut(cntx,font,1,0,optns.loUgolLines.uiColour,x2+4,y2-sz_txt_y/2,strk);
	}	


	x1=left;
	y1=top-height+1;
	getXYByUgolAndDaln(ugl,optns.iNextDaln_km[optns.n_of_Daln-1],1,x2,y2);
	pblDrawSegment(cntx,x1,y1,x2,y2,optns.loUgolLines.uiPenStyle,optns.loUgolLines.uiWidth,optns.loUgolLines.uiColour);	
	sprintf(strk,"%1.1lf°",ugl);	
	bptGetTextSize(cntx,font,strk,&sz_txt_x, &sz_txt_y);
	bptTextOut(cntx,font,1,0,optns.loUgolLines.uiColour,x2+4,y2-sz_txt_y/2,strk);
	bptDestroyFont(font);
	return 1;
}



//Рисовать линии дальности
int TGlissadaSetka::protected_paintLiniiDaln(void *cntx)
{

	int i;
	void *font;
	int iRet;
	int x1,y1,x2, y2;
	int sz_txt_x, sz_txt_y;
	char strk[20];
	for(i=0;i<daln_lines.size();i++)
	{
		iRet=getXYByUgolAndDaln(dfMinUgol_grds,daln_lines[i].iDaln_km,1,x1,y1);
		if(iRet<=0)continue;
		iRet=getXYByUgolAndDaln(dfMaxUgol_grds,daln_lines[i].iDaln_km,1,x2,y2);
		if(iRet<=0)continue;		
		pblDrawSegment(cntx,x1,y1,x2,y2,daln_lines[i].lo.uiPenStyle,daln_lines[i].lo.uiWidth,
			daln_lines[i].lo.uiColour);
		if(daln_lines[i].isFigure)
		{
			font=bptCreateFont(&(daln_lines[i].textOpt));
			if(!font)continue;
			 sprintf(strk, "%d",daln_lines[i].iDaln_km);
			iRet=bptGetTextSize(cntx,font,strk,&sz_txt_x,&sz_txt_y);
			if(iRet<=0)continue;
			iRet=bptTextOut(cnxt,font,1,0,daln_lines[i].lo.uiColour,x1-sz_txt_x/2,y1+4,strk);
			bptDestroyFont(font);

		}		
	}


}



int TGlissadaSetka::getXByDaln(double daln_km, int isOtTorcaVPP)
{
	int i;
	int smesh;
	double dolya;
//

	if(isOtTorcaVPP)
	{
		return ::getXByDaln(optns.dfDalnostAntennaVPP_km+daln_km,0);
	}
	
//Определим, в какую часть попадает дальность
	dolya=0;
	sum_daln1=0;
	for(i=0;i<optns.n_of_Daln;i++)
	{
		if(optns.iNextDaln_km[i]+optns.dfDalnostAntennaVPP_km>daln_km)
		{
			break;
		}
		dolya+=optns.dfDolyaDaln_km[i];
		
	}	
	if(i==optns.n_of_Daln)
	{
		last_index_getXByDaln=-1;
		return -100000;
	}
	if(i==0)
	{
//Длина первого участка optns.iNextDaln_km[i]+optns.dfDalnostAntennaVPP_km
		smesh=OKRUGL_INT(daln_km/(optns.iNextDaln_km[0]+optns.dfDalnostAntennaVPP_km)*width*optns.dfDolyaDaln_km[0]);
	}else{
		smesh=OKRUGL_INT(dolya*width+
				(daln_km-(optns.iNextDaln_km[i-1]+optns.dfDalnostAntennaVPP_km))/(optns.iNextDaln_km[i]-optns.iNextDaln_km[i-1])*
				width*optns.dfDolyaDaln_km[0]);
	}			

	last_index_getXByDaln=i;	

	return optns.left+smesh;	
}



//Промежуточная процедура: возвращает значения X и Y координаты точки на окне в зависимости от дальности и угла глиссады
//Параметр isOtTorcaVPP указывает - считать ли дальность от торца ВПП или от антены
//Если значение выше она возвращает 100000, если ниже, то возвращает -100000
//Если значение левее -200000, если правее 200000
//Если всё нормально возврашает X
int TGlissadaSetka::getXYByUgolAndDaln(double ugl_grds, double daln_km, int isOtTorcaVPP,
				int &x, int &y)
{

	
	if(isOtTorcaVPP)
	{
		return getXYByUgolAndDaln(ugl_grds,daln_km+optns.dfDalnostAntennaVPP_km, 0,x,y);
	}		

	x=getXByDaln(daln_km, 0);
	if(x<=-100000)return -200000;
	if(x>=100000)return 200000;

	if(ugl_grds<dfMinUgol_grds)return -100000;
	if(ugl_grds>dfMaxUgol_grds)return 100000;
		
	if(last_index_getXByDaln<0)
	{
		return -200000;
	}

//Расчитаем высоту в км
	y=top+height-1-OKRUGL_INT(daln_km*tan(ugl_grds/180.0*M_PI)*dfKoefH[last_index_getXByDaln]);
	return 1;
}



int TGlissadaSetka::getXYByXkmYkm(double x_km, 	//Дальность 
			int isOtTorcaVPP,		//Дальность от торца ВПП иначе от антенны
			double y_km, 			//Высота в км
			int isOtVzlyotnoyoPolosy,	//Высота относительно взлетноё полосы. Если значение 0, то относительно антенны 
			int &x, int &y			//Экранные координаты
			)
{
	double dln_km, h_km;
	double ugol;
	if(isOtTorcaVPP)
	{
		dln_km=x_km+optns.dfDalnostAntennaVPP_km;
	}else{
		dln_km=x_km;
	}

	if(isOtVzlyotnoyoPolosy)
	{
		h_km=y_km-optns.dfVysota_ant_km;
	}else{
		h_km=y_km;
	}
			
	x=getXByDaln(dln_km,0);
	if(x<=-100000)return -200000;
	if(x>=100000)return 200000;

//Получим значение угла, чтобы определить попадает ли в область
	ugol=atan(h_km/dln_km)/M_PI*180.0;
	if(ugol<dfMinUgol_grds)
	{
		return -100000;
	}
	if(ugol>dfMaxUgol_grds)
	{
		return 100000;
	}
	
//Расчитаем y
	y=top+height-1-OKRUGL_INT(h_km*dfKoefH[last_index_getXByDaln]);
	return 1;	
}


