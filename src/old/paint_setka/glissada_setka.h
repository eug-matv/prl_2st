﻿#ifndef __GLISSADA_SETKA_H__
#define __GLISSADA_SETKA_H__

#include "line_options.h"
#include "bp_text.h"	

struct TGlissadaSetkaDalnLine
{
      int iDaln_km;		//Дальность от торца ВПП в км
      TLineOptions lo;		//Свойства линии
      int isFigure;		//Выводить ли цифры
      TBPTextOption textOpt;	
};



struct TGlissadaSetkaRavnayaVysotaLine
{
	int iVysota_m;
        TLineOptions lo;		//Свойства линии
  	TBPTextOption textOpt;	
};




	
using namespace std;


struct TGlissadaSetkaOptions
{
	int iNextDaln_km[5];	//Следующее значение дальности от торца ВПП
	double dfDolyaDaln_km[5];	//Значение меньше - доля пространства. Все значения должны быть меньше 1, а их сумма не должна превышать 1. 
				//Последнее значение не учитывается
	int n_of_Daln;		//Число элементов dfNextDaln и dfDolyaDaln

	double dfDalnostAntennaVPP_km;			//Расстояние от антенны до торца ВПП
	double dfUgolGlissady_grds;			//Значения угла глиссады в градусах
	TLineOptions loGlissada;			//Свойства рисования угла
	double dfDUgolGlissadyPlus_grds;		//Отклонение от глиссады в большую сторону в градусах
	double dfDUgolGlissadyMinus_grds;		//Отклонение от глиссады в меньшую сторону в градусах
	unsigned int uiSectorGlissadyCol;		//Цвет сектора допустимых отклонений глиссады
	double dfDalnostUgolsGlissady_km;	//Дальность в км до которой сектор глиссады распространяется	
	
	double dfDalnostPrizemlTorets_km;	//Дальность  от торца ближайшего к станции до точки приземления в км	


/*Свойства отображаемых угломерных линий*/
	double dfMinUgolMer_grds, dfMaxUgolMer_grds;	  //Значение минимального и максимального значений угла для угломерных линий
	double dfStepUgolMer_grds;			  //Шаг угла
	int iMinDUgols_km, iMaxDUgols_km;	  //Дальности, при которых отображаются угловые линии
	TLineOptions loUgolLines;		  //Свойства рисования угла
	TBPTextOption textOptUgolLines;		  //Свойства текста, который выводится
	

/*Высота антены на точками приземления в км*/
	double dfVysota_ant_km;

};

class TGlissadaSetka	
{
/*Основные параметры класса*/
	int left, top;		//Верхний левый угол
	int width, height;	//Размеры области отображения в пикселях
	double dfMinUgol_grds, dfMaxUgol_grds; 
	
	vector<TGlissadaSetkaDalnLine> daln_lines;		//линии удаления от торца ВПП в км. Задается также цвет, тип линии, толщина линии и ставить ли цифры

	struct TGlissadaSetkaOptions optns;			//Прочие опции кроме линий удаления. Определены в структуре TGlissadaSetkaOptions выше
			
	vector<TGlissadaSetkaRavnayaVysotaLine> ravn_vys_lines;		//линии удаления от торца ВПП в км. Задается также цвет, тип линии, толщина линии и ставить ли цифры


//Промежуточные значения
	double max_vysota_right_km;				//Максимальная высота в км над антеной - соответствует значению в верхнем правом углу
	double min_vysota_right_km;				//Максимальная высота в км над антеной - соответствует значению в верхнем правом углу
	double dfScreenUchastokX[5];				//Участки 


//Получение индекса последнего  участка используемого процедурой
	int last_index_getXByDaln;
protected:

//Первичный расчет всех параметров	
	int protected_initParameters();	



//Нарисовать сектор глиссады
	int protected_paintSectroGlissady(void *cntx);
	
//Рисовать угловые линии сетки
	int protected_paintUglovLinii(void *cntx);
	
//Рисовать линии дальности
	int protected_paintLiniiDaln(void *cntx);

//Рисовать линию глисспды
	int protected_paintLiniyuGlissady(void *cntx);

//Рисовать линии равных высот
	int protected_paintLiniiRavnyhVysot(void *cntx);

public:
	

//Промежуточная процедура: возвращает значение X координаты на окне в зависимости от дальности. 
//Если значение лежит за пределами области отображения, то возвращается значение -100000
//Если значение isOtTorcaVPP =1, то дальность от торца ВПП
	int getXByDaln(double daln_km, int isOtTorcaVPP);	

//Промежуточная процедура: возвращает значения X и Y координаты точки на окне в зависимости от дальности и угла глиссады
//Параметр isOtTorcaVPP указывает - считать ли дальность от торца ВПП или от антены
//Если значение выше она возвращает 100000, если ниже, то возвращает -100000
//Если значение левее -200000, если правее 200000
//Если всё нормально возврашает X
	int getXYByUgolAndDaln(double ugl_grds, double daln_km, int isOtTorcaVPP,
				int &x, int &y);



	int getXYByXkmYkm(double x_km, 			//Дальность 
			int isOtTorcaVPP,		//Дальность от торца ВПП иначе от антенны
			double y_km, 			//Высота в км
			int isOtVzlyotnoyoPolosy,	//Высота относительно взлетноё полосы. Если значение 0, то относительно антенны 
			int &x, int &y			//Экранные координаты
			);
	


//Добавление дальности	удаления от торца ВПП
	int addLineUdalOtTorcaVPP(
			 int iDaln_km,	//Дальность от торца
			 const TLineOptions &lo,	//Опции линии 
			 int isFigure,			//Выводить ли фигуры
			 const TBPTextOption &textOpt			 
			);



		
		
};

#endif