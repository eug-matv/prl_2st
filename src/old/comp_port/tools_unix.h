/*Вспомогательные функции для работы в linux
Автор: Матвеенко Е.А. нач КБ-29
Дата комментария: 01.01.2016.

Реализованы функции:


*/

#ifndef TOOLS_UNIX_H_INCLUDED
#define TOOLS_UNIX_H_INCLUDED

int findProcessesOpenFile(const char *file_name, int *proc_id, int max_n_of_proc_id);
int findProcessesOpenFile(const char *file_name);
#endif // TOOLS_UNIX_H_INCLUDED
