﻿
#include <windows.h>
#include "bp_line.h"





int pblDrawSegment(
			void *cntx,			//Контекст устройства
			int x1, int y1, 		//Координаты первой точки
			int x2, int y2,			//Координаты второй точки
			unsigned int uiPenStyle,		//Стиль - определено в файле pb_line.h, как BP_PS_*
			unsigned int uiWidth,		//Толшина линии
			unsigned int uiColour		//Цвет линии
		    )
{
	HPEN hp,oldHp;
	int oldBkMode;
	int stls[]={PS_SOLID, PS_DASH, PS_DOT, PS_DASHDOT, PS_DASHDOTDOT};
	

	hp=CreatePen(stls[uiPenStyle%5], (int)uiWidth, (COLORREF)(uiColour));
	if(!hp)
	{
		return (-1);
	}
	oldHp=(HPEN)SelectObject((HDC)cntx,hp);
	if(!oldHP)
	{
		DeleteObject(hp);
		return (-2);
	}
	
	oldBkMode=SetBkMode((HDC)cntx,TRANSPARENT);
	if(oldBkMode==0)
	{
		SelectObject((HDC)cntx,oldHP);
		DeleteObject(hp);
		return (-3);	
	}		

	
	if(!MoveToEx((HDC)cntx,x1,y1,NULL))
	{	
		SetBkMode((HDC)cntx,oldBkMode);
		SelectObject((HDC)cntx,oldHp);
		DeleteObject(hp);
		return (-11);
	}	
	
	if(!LineTo((HDC)cntx, x2, y2))
	{
		SetBkMode((HDC)cntx,oldBkMode);
		SelectObject((HDC)cntx,oldHp);
		DeleteObject(hp);
		return (-12);
	}		
	SetBkMode((HDC)cntx,oldBkMode);	
	SelectObject((HDC)cntx,oldHp);
	DeleteObject(hp);
	return 1;		
}
