﻿#ifndef __BP_LINE_H__
#define __BP_LINE_H__



/*Отобразить отрезок. На контекст устройства*/
int pblDrawSegment(
			void *cntx,			//Контекст устройства
			int x1, int y1, 		//Координаты первой точки
			int x2, int y2,			//Координаты второй точки
			unsigned int uiPenStyle,		//Стиль - определено в файле pb_line.h, как BP_PS_*
			unsigned int uiWidth,		//Толшина линии
			unsigned int uiColour		//Цвет линии
		    );	

#endif






