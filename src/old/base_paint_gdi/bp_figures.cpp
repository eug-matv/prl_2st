﻿#include <windows.h>
#include <stdlib.h>

#include "bp_figures.h"
#include "bp_line.h"


int pbfDrawRectangle(
			void *cntx,			//Контекст устройства
			int x1, int y1, 		//Координаты первой точки угла
			int x2, int y2,			//Координаты второй точки
			unsigned int uiPenStyleBrdr,	//Стиль - определено в файле pb_line.h, как BP_PS_*
			unsigned int uiWidthBrdr,	//Толшина линии
			unsigned int uiColourBrdr,	//Цвет линии
			int isTransparentArea,		//Рисование внутри
			unsigned int uiColourArea
		    )
{
	HPEN hp, oldHp;
	HBRUSH brsh, oldBrsh;
	LOGBRUSH logBrush;
	
	int stls[]={PS_SOLID, PS_DASH, PS_DOT, PS_DASHDOT, PS_DASHDOTDOT};	
	int oldBkMode;	
	
	int _x1, _x2, _y1,  _y2;
	int iRet;

	hp=CreatePen(stls[uiPenStyleBrdr%5], (int)uiWidthBrdr, (COLORREF)(uiColourBrdr));
	if(!hp)
	{
		return (-1);
	}

	logBrush.lbColor=(COLORREF)uiColourArea;
	logBrush.lbHatch=0;
	if(isTransparentArea)
	{
		logBrush.lbStyle=BS_HOLLOW;
		oldBkMode=SetBkMode((HDC)cntx,TRANSPARENT);
	}else{
		logBrush.lbStyle=BS_SOLID;
		oldBkMode=SetBkMode((HDC)cntx,OPAQUE);
	}
	brsh=CreateBrushIndirect(&logBrush);
	if(!brsh)
	{
		DeleteObject(hp);
		SetBkMode((HDC)cntx, oldBkMode);
		return (-2);
	}	

	oldHp=SelectObject((HDC)cntx,hp);
	if(!oldHp)
	{
		DeleteObject(brsh);
		DeleteObject(hp);
		SetBkMode((HDC)cntx, oldBkMode);
		return (-3);
	}
		
	
	oldBrsh=SelectObject((HDC)cntx, brsh);
	if(!oldBrsh)
	{
		SelectObject((HDC)cntx, oldHp);
		DeleteObject(brsh);
		DeleteObject(hp);
		SetBkMode((HDC)cntx, oldBkMode);
		return (-4);

	}

	if(x2<x1)
	{
		_x1=x2;
		_x2=x1;
	}else{
		_x1=x1;
		_x2=x2;
	}

	if(y2<y1)
	{
		_y1=y2;
		_y2=y1;
	}else{
		_y1=y1;
		_y2=y2;
	}
	
	if(Rectangle((HDC)cntx, _x1, _y1, _x2, _y2))
	{
		iRet=1;
	}else{
		iRet=-11;
	}
	
	SetBkMode((HDC)cntx, oldBkMode);
	SelectObject((HDC)cntx, oldBrsh);
	SelectObject((HDC)cntx, oldHp);
	DeleteObject(brsh);
	DeleteObject(hp);
	return iRet;
}


int pbDrawPolygon(void *cntx,
		int *x, int *y, int n_of_pixels,
		unsigned int uiPenStyleBrdr,	//Стиль - определено в файле pb_line.h, как BP_PS_*
		unsigned int uiWidthBrdr,	//Толшина линии
		unsigned int uiColourBrdr,	//Цвет линии
		int isTransparentArea,		//Рисование внутри
		unsigned int uiColourArea	//Цвет внутри		
		 )
{
	HPEN hp, oldHp;
	HBRUSH brsh, oldBrsh;
	LOGBRUSH logBrush;
	
	int stls[]={PS_SOLID, PS_DASH, PS_DOT, PS_DASHDOT, PS_DASHDOTDOT};	
	int oldBkMode;	
	
	int _x1, _x2, _y1,  _y2;
	int iRet;
	POINT *pnts;	
	int i;	

	if(n_of_pixels<=0||!x||!y||!cntx)return 0;


	hp=CreatePen(stls[uiPenStyleBrdr%5], (int)uiWidthBrdr, (COLORREF)(uiColourBrdr));
	if(!hp)
	{
		return (-1);
	}

	logBrush.lbColor=(COLORREF)uiColourArea;
	logBrush.lbHatch=0;
	if(isTransparentArea)
	{
		logBrush.lbStyle=BS_HOLLOW;
		oldBkMode=SetBkMode((HDC)cntx,TRANSPARENT);
	}else{
		logBrush.lbStyle=BS_SOLID;
		oldBkMode=SetBkMode((HDC)cntx,OPAQUE);
	}
	brsh=CreateBrushIndirect(&logBrush);
	if(!brsh)
	{
		DeleteObject(hp);
		SetBkMode((HDC)cntx, oldBkMode);
		return (-2);
	}	

	oldHp=SelectObject((HDC)cntx,hp);
	if(!oldHp)
	{
		DeleteObject(brsh);
		DeleteObject(hp);
		SetBkMode((HDC)cntx, oldBkMode);
		return (-3);
	}
		
	
	oldBrsh=SelectObject((HDC)cntx, brsh);
	if(!oldBrsh)
	{
		SelectObject((HDC)cntx, oldHp);
		DeleteObject(brsh);
		DeleteObject(hp);
		SetBkMode((HDC)cntx, oldBkMode);
		return (-4);

	}
	
	pnts=(POINT*)malloc(sizeof(POINT)*n_of_pixels);
	if(!pnts)
	{
		SetBkMode((HDC)cntx, oldBkMode);
		SelectObject((HDC)cntx, oldBrsh);
		SelectObject((HDC)cntx, oldHp);
		DeleteObject(brsh);
		DeleteObject(hp);
		return(-11);
	}	
	
	for(i=0;i<n_of_pixels;i++)
	{
		pnts[i].x=x[i];
		pnts[i].y=y[i];
	}		
	
	if(Polygone((HDC)cntx, pnts,n_of_pixels))
	{
		iRet=1;
	}else{
		iRet=-12;
	}
	free(pnts);		
	SetBkMode((HDC)cntx, oldBkMode);
	SelectObject((HDC)cntx, oldBrsh);
	SelectObject((HDC)cntx, oldHp);
	DeleteObject(brsh);
	DeleteObject(hp);
	return iRet;

}
