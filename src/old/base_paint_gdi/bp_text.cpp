﻿#include <windows.h>
#include <string.h>

#include "bp_text.h"


void *bptCreateFont(
	        const struct  TBPTextOption *textOpt
		)
{
	HFONT font;
	font=CreateFont(texpOpt->size,texpOpt->size*2/3,GM_COMPATIBLE,
    0,FW_REGULAR,texpOpt->isItalic,texpOpt->isUnderline,texpOpt->isBold,RUSSIAN_CHARSET,0,0,DEFAULT_QUALITY,
    DEFAULT_PITCH ,texpOpt->csFam);
	return (void*)font;	
}

int bptDestroyFont(
		void *font
		)
{
	if(!DeleteObject(font))
	{
		return 0;
	}
	return 1;
}

int bptTextOut(
		void  *cntx,  		//Контекст устройства
		void  *font,  		//Шрифт созданый функцией bptCreateFont	
		int isTransparent,	//Прозрачное или нет
		unsigned int uiColourBk, //W
		unsigned int uiColourText,	//Цвет текста
		int x, int y,	//Координаты левого верхнего левого угла
		const char *csText	//Отображаемый текст
		)
{
	HFONT oldFont;
	int prevBkMode;
	int iRet=1; 
	COLORREF oldBkColour, oldTextColour;
	oldFont=(HFONT)SelectObject((HDC)cntx, font);
	
	if(!oldFont)
	{
		return (-1);
	}
	if(isTransparent)
	{
		prevBkMode=SetBkMode((HDC)cntx,	TRANSPARENT);
	}else{
		prevBkMode=SetBkMode((HDC)cntx,	OPAQUE);
	}
	if(prevBkMode==0)
	{
		SelectObject((HDC)cntx, oldFont);
		return (-2);
	}	

	if(!isTransparent)
	{
		oldBkColour=SetBkColor((HDC)cntx, uiColourBk&0xFFFFFF);
		if(oldBkColour==CLR_INVALID)
		{
			SelectObject((HDC)cntx, oldFont);
			SetBkMode((HDC)cntx,prevBkMode);
			return (-3);
		}
	}
	
	oldTextColour=SetTextColor((HDC)cntx,uiColourText&0xFFFFFF);
	if(oldTextColour==CLR_INVALID)
	{
		SelectObject((HDC)cntx, oldFont);
		SetBkMode((HDC)cntx,prevBkMode);
		if(!isTransparent)SetBkColor((HDC)cntx, oldBkColour);	
		return (-4);	
	}	
	
	if(!TextOut((HDC)cntx, x,y,csText, strlen(csText)))
	{
		iRet=-11;
	}	 
	
	SelectObject((HDC)cntx, oldFont);
	SetBkMode((HDC)cntx,prevBkMode);
	if(!isTransparent)SetBkColor((HDC)cntx, oldBkColour);
	SetTextColor((HDC)cntx,oldTextColour);
	return iRet;
}


/*Получение размера текста в пикселях*/
int bptGetTextSize(
		void  *cntx,  		//Контекст устройства
		void  *font,  		//Шрифт созданый функцией bptCreateFont	
		const char *csText,	//Отображаемый текст
		int *sz_x, int *sz_y	//Получаемый размер текста
		)
{
	HFONT oldFont;
	SIZE sz;
	int iRet;
	if(!cntx||!font||!sz_x||!sz_y||!csText)return 0;

	oldFont=(HFONT)SelectObject((HDC)cntx, font);
	if(!oldFont)
	{
		return (-1);
	}
	if(GetTextExtentPoint32((HDC)cntx,csText, sizeof(csText), &sz))
	{
		iRet=1;
		*sz_x=sz.cx;
		*sz_y=sz.cy;
	}else{
		iRet=-11;
	}	
	SelectObject((HDC)cntx,oldFont);
	return iRet;
}