﻿#ifndef __LINE_OPTIONS_H__
#define __LINE_OPTIONS_H__
/*Тут задается цвет, толщина и тип линии*/	

#define BP_PS_SOLID	0
#define BP_PS_DASH	1
#define BP_PS_DOT 	2
#define BP_PS_DASHDOT 	3
#define PS_DASHDOTDOT	4



struct TLineOptions
{
	unsigned int uiColour;
	unsigned int uiWidth;
	unsigned int uiPenStyle;	//Определен в виде макросов в файле src\base_paint_gdi

};

#endif