#ifndef __M_RADIOBUTTONSGROUP_H__
#define __M_RADIOBUTTONSGROUP_H__

#include <vector>

using namespace std;

struct TMyRadioButtonDescr
{
	TCHAR text[100];	//�����
	int x,y;			//���������� ������ �������� ���� ������
	int dx,dy;			//������� ������� ������
	int code;			//��� ������������ �� ������� - ������ ���� ����������
	
//���� �� ����� ������������
	TCHAR font_name[100];	//����� ������
	int font_size;	

//��������� �� �������
	void (*f)(void);


//���������� ����, ������� �������������
	void* hwnd;


};



struct TMyRBGroupDescr
{
	TCHAR text[100];
	int x,y;
	int dx,dy;
	int code;
	int isFromRight;
	int isFromBottom;
	void *parent_hwnd;	//��������� �� ���������� ����
	void *hinstance;

//��������� �������� �� �����������, � �����������
	void *hwnd;
};

class TRadioButtonsGroup
{

public:

	TRadioButtonsGroup(				
	const struct TMyRBGroupDescr &_rbgd				
			);
	~TRadioButtonsGroup(void);


	int addRadioButton(const struct TMyRadioButtonDescr &_rb);
	
	int testAndMake(int _code);

	int renew(void);

	int setCheckNoFunction();
private:
	struct TMyRBGroupDescr rbgd;	//�������� ������
	vector<TMyRadioButtonDescr> rbs;
	int min_code, max_code;

};


#endif