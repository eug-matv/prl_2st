/*Автор: Матвеенко Е.А.   ЧРЗ "Полёт"
email: eug-matv@yandex.ru.
Реализованы обертки для возможности использования "безопасных" функций,
при сборке компилятором от Майкрософт в ОС Windows

В среде Linux и в компиляторе MinGW используются стандартные функции.
*/

#ifndef SAFE_FUNCTION_H
#define SAFE_FUNCTION_H
#include <stdio.h>


#define SF_SIZE_STR(s) ((sizeof(s)==sizeof(void*))?100:sizeof(s))


#ifdef _MSC_VER

#define sfSTRCPY(d,s) strcpy_s((d), strlen(s)+1, (s))
#define sfACCESS(s,d) _access((s),(d))
#define sfCLOSE(d)  _close((d))
#define sfWRITE(d, s, n) _write((d),(s),(n))
#define sfREAD(d, s, n) _read((d),(s),(n))
#define sfLSEEK(d,of,or)  _lseek((d),(of), (or))
#define sfSPRINTF(s,c,...) sprintf_s((s),SF_SIZE_STR(s),c, __VA_ARGS__)
#define sfFSCANF  fscanf_s
#define sfLOCALTIME(tm,tmr) (localtime_s((tm),(tmr))==0)
#define sfTELL(ind)		_tell(ind)

#else
#define sfSTRCPY(d,s) strcpy((d), (s))
#define sfACCESS(s,d) access((s),(d))
#define sfCLOSE(d)  close((d))
#define sfWRITE(d, s, n) write((d),(s),(n))
#define sfREAD(d, s, n) read((d),(s),(n))
#define sfLSEEK(d,of,o)  lseek((d),(of),(o))
#define sfSPRINTF   sprintf
#define sfFSCANF fscanf
int sfLOCALTIME(void *, const void*);
#define sfTELL(ind)		tell(ind)

#endif

int sfOpen(const char *fn, int oflag, int pmode=0);
FILE *sfFOpen(const char *fn, const char *mode);



#endif // SAFE_FUNCTION_H
