/*Модуль: time_tools
Автор: Матвеенко Е.А. ЧРЗ Полет ОКБ
Реализованы процедуры для замера временных интервалов.

*/
#ifndef __TIME_TOOLS_H__
#define __TIME_TOOLS_H__

/*Получение значения миллисекунд от какого-то периода.
*/
long ttGetMillySecondsCounts(void);

/*Получение значения интервала ms2-ms1. Учитывается переход через 0*/
long ttGetDTimeMS(long ms1, long ms2);

/*Получение значения  tm_ms+d_tm_ms. Учитывается переход через 0*/
long ttGetTimePlusDTime(long tm_ms, long d_tm_ms);

/*Определение, наступило ли событие wait_tm_ms в момент времени cur_tm_ms
Если наступило: возвр 1, иначе 0
*/
int ttIsTimeCame(long wait_tm_ms, long cur_tm_ms);
#endif
