
#ifdef _WIN32
#include <windows.h>
#else
#include <sys/time.h> 
#endif
#include "time_tools.h"


long ttGetMillySecondsCounts(void)
{
#ifdef _WIN32
	return (long)(GetTickCount()%2000000000L);
#else
	int iRet;
	struct timeval tv;
	iRet=gettimeofday(&tv,0);
	if(iRet!=0)return (-1);
	return ((long)(tv.tv_sec)%2000000)*1000+((long)(tv.tv_usec)/1000);
#endif
}


long ttGetDTimeMS(long ms1, long ms2)
{
	if(ms1>ms2)
	{
		if(ms1-ms2<1000000000L)
		{
			return (ms2-ms1);
		}
		return ms2-ms1+2000000000L;
	}	
	
	if(ms2-ms1<1000000000L)
	{
		return (ms2-ms1);
	}	
	
	return ms2-ms1-2000000000L;
}


long ttGetTimePlusDTime(long tm_ms, long d_tm_ms)
{
	if(d_tm_ms<=0)return tm_ms;
	return (tm_ms+d_tm_ms)%2000000000L;
}


int ttIsTimeCame(long wait_tm_ms, long cur_tm_ms)
{
	return (int)(ttGetDTimeMS(wait_tm_ms, cur_tm_ms)>=0);
}


