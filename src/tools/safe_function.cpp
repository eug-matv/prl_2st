#ifdef _MSC_VER
#include "my_stdafx.h"
#endif
#ifdef _WIN32
#include <io.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include "my_stdlib.h"
#include <string.h>
#ifdef _MSC_VER
#include <share.h>
#endif
#include <stdio.h>

#include "safe_function.h"




int sfOpen(const char *fn, int oflag, int pmode)
{
#ifdef _WIN32
#ifdef _MSC_VER
    int iRet, id_f, _oflag=0,_pmode=0;
    if(oflag&O_CREAT)_oflag|=_O_CREAT;
    if(oflag&O_APPEND)_oflag|=_O_APPEND;
    if(oflag&O_RDONLY)_oflag|=_O_RDONLY;
    if(oflag&O_RDONLY)_oflag|=_O_RDONLY;
    if(oflag&O_WRONLY)_oflag|=_O_WRONLY;
    if(oflag&O_RDWR)_oflag|=_O_RDWR;
    if(oflag&O_EXCL)_oflag|=_O_EXCL;
    if(oflag&O_BINARY)_oflag|=_O_BINARY;
    if(oflag&O_TEXT)_oflag|=_O_TEXT;
    if(pmode>0)
    {
        if(pmode&S_IWRITE)_pmode=_S_IWRITE;
        if(pmode&S_IREAD)_pmode=_S_IREAD;
    }

    iRet=_sopen_s(&id_f,fn,_oflag,_SH_DENYNO,_pmode);
    if(iRet)
    {
        return (-1);
    }
    return id_f;
#else
    return open(fn, oflag, pmode);
#endif
#else
    return open(fn,oflag,0666); //Пусть будет для всех
#endif

}



FILE *sfFOpen(const char *fn, const char *mode)
{
#ifdef _MSC_VER
    errno_t ret;
    FILE *fp;
    ret=fopen_s(&fp,fn,mode);
    if(ret)return NULL;
    return fp;
#else
    return fopen(fn,mode);
#endif

}


#ifndef _MSC_VER
int sfLOCALTIME(void *a, const void *b)
{
    struct tm* my_tm;
    my_tm=localtime((time_t*)b);
    memcpy(a,my_tm,sizeof(struct tm));
    return 1;
}
#endif
