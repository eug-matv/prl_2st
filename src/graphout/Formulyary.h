﻿//---------------------------------------------------------------------------
/*Модуль: Formulyary
Автор: Матвеенко Е.А. ЧРЗ Полет ОКБ

Отображение формуляров на границе между областями курса (модуль KursGraf) и
глиссады (GlissadaGraf).

*/


#ifndef FormulyaryH
#define FormulyaryH

#include <QPen>
#include <QBrush>
#include <QColor>
#include <QRect>
#include <QFont>
#include <QPainter>

//---------------------------------------------------------------------------
/*Класс TFormulyar используется в методе TRZPDraw::VyvestiOtschetyVPolyaKursaIGlissady
для формирования и отображения формуляров.

*/


class TFormulyar
{

  long Left,Top, Right, Bottom; //Координаты углов, которые рассчитываются
                                //по координатам и
  long X11,Y11,X12,Y12;         //Линия горизонтальняя от поля курса к формуляре
  long X21,Y21,X22,Y22;         //Вертикальная линия от поля глиссады к формуляре
  char Stroki[5][20];           //Строки
  long ChisloStrok;             //Число строк
  QPen lpOut;                 //Логическое перо
  QBrush lbOut;               //Логический браш
  long szTextOut;                  //размер текста
  QColor cTextOut;               //Цвет текста
  public:
  bool IsOut;                   //Отображать ли данный формуляр

/*Установить параметры формуляра.
Параметры:
in:  X1,Y1 --- экранные координаты объекта в поле курса.
in:  X2,Y2 --- экранные координаты объекта в поле глиссады.
in:  sTextOut --- массив строк текста формуляра
in:  N_of_sTextOut --- число строк текста формуляра
in: lp, lb - перо и область формуляра.
in: szText, cText ---- размер текста и цвет текста внутри формуляра
out: Rect --- координаты прямоугольной области формуляра
*/
  long UstanovitParametry(long X1, long Y1,
                          long X2, long Y2,
                          char **sTextOut,
                          long N_of_sTextOut,
                          QPen &lp, QBrush &lb,
                          long szText, QColor cText,
                          QRect *Rect
                          );

  bool Vyvod(QPainter &pntr);   //Вывести формуляр на pntr

//Проверка, что внутри рамки при нажатии мыши - в настоящий момент не используется
  bool VnutriFormulyara(long X, long Y);

};

#endif
