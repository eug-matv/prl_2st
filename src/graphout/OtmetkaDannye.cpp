﻿//---------------------------------------------------------------------------
#include "stdafx.h"
#pragma hdrstop

#include <math.h>
#include "OtmetkaDannye.h"
#include "AnyTools.h"

//---------------------------------------------------------------------------

//Определение с другой отметкой расстояние. И будут ли они находится в ограниченной области
//Возвращает: 0 - не будут,
//         старший байт для поля глиссады: 1 - есть опасность, что встретятся. 2 - уже на растоянии
//         младший - для поля курса: 1 - могут оказаться на расстоянии в этот период, 2 - уже на этом расстоянии
//         мигать соответственно должны в соответствующем поле
//
//Проверка на критическое положение или положение по дальности
long TDannyeObCeli::
        ControlForKrit_AvariaTwo(TDannyeObCeli *Dan,  //Данные о цели
                              double Period,       //Период в секундах
                              double lDopRast)       //Допустимое расстояние в метрах
{

  bool Yopredelena=false, Zopredelena=false;
  unsigned short Ret1=0,Ret2=0;  //Возвращаемые значения
  long Neopr=0;            //Оценка сколько неопределенностей по координатам
  double dX,dY,dZ;     //Значения разниц координат разных бортов
  double dVx,dVy,dVz;  //Значения разниц скоростей разных бортов (то есть скороти одного относительно другого)
  double kvRast,       //Квадрат расстояния
         kvDopRast;    //
  double kvSkor;       //Квадрат скорости одного относительно другого
  double T;




/*Рассчитаем разницы координат между сравниваемым и текущим отсчетом*/

  dX=(Dan->X-X)/1000.0;

//Рассмотрим случаи с Y-координатой
  if(Dan->Y!=-100000&&Y!=-100000)
  {
//Y координата определена у обоих отсчетов
     dY=(Dan->Y-Y)/1000.0;
     Ret1|=0x01;
     Ret2|=0x02;
     Yopredelena=true;
  }else{
//Y координата не определена у одного или обоих отметок
//Тогда рассмотрим случай будьто они в одно Y-плоскости
     dY=0;
  };

  if(Dan->Z!=-100000&&Z!=-100000)
  {
//Z координата определена у обоих отсчетов
     dZ=(Dan->Z-Z)/1000.0;
     Ret1|=0x0100;
     Ret2|=0x0200;
     Zopredelena=true;
  }else{
//Z координата не определена у одной или у двух отметок
//Тогда будем предполагать, что они в одной Z-плоскости
     dZ=0;
  }


  if(!Yopredelena && !Zopredelena)
  {
//Пусть не определить, что они на одной плоскости или одной отметки, тогда будем предпологать
//что они на одной прямой (м.б. этот вариант выкинуть)
    Ret1=0x1000;
    Ret2=0x2000;
    return 0;
  }



//Проверка - определим квадрат расстояний между отметками бортов
  kvDopRast=(lDopRast/1000.0)*(lDopRast/1000.0); //Расчитаем квадрат доп. растояний
              //в метрах

  kvRast=dX*dX+dY*dY+dZ*dZ; //Расчитаем квадрат расстояний по дальности

//Проверим а не приблизились ли ону друг к другу на расстояние,
//меньшее чем на на kvDopRast
  if(kvRast<=kvDopRast)
  {
    //Проверим lHazardTwo
    if((lHazardTwo&0xFF)<(Ret2&0xFF))
    {
      lHazardTwo=lHazardTwo&0xFF00|Ret2&0x00FF;
    }

    if((lHazardTwo&0xFF00)<(Ret2&0xFF00))
    {
      lHazardTwo=lHazardTwo&0x00FF|Ret2&0xFF00;
    }

    if((Dan->lHazardTwo&0xFF)<(Ret2&0xFF))
    {
      Dan->lHazardTwo=(Dan->lHazardTwo)&0xFF00|Ret2&0x00FF;
    }

    if((Dan->lHazardTwo&0xFF00)<(Ret2&0xFF00))
    {
      Dan->lHazardTwo=(Dan->lHazardTwo)&0x00FF|Ret2&0xFF00;
    }
    return Ret2; //Вернем опасную ситуацию
  }

//Разберемся со скоростью
  if(Dan->Vx!=-100000&&Vx!=-100000)
  {
     dVx=(Dan->Vx-Vx);
  }else{
     dVx=0;
  }

  if(Dan->Vy!=-100000&&Vy!=-100000)
  {
     dVy=(Dan->Vy-Vy);
  }else{
     dVy=0;
  }

  if(Dan->Vz!=-100000&&Vz!=-100000)
  {
    dVz=(Dan->Vz-Vz);
  }else{
    dVy=0;
  }

  if(dVx<0.1 &&dVx>-0.1 && dVy<0.1 &&dVy>-0.1&&
     dVz<0.1 && dVz>-0.1)
  {
//На всякий случай, вдруг они летят паралельно
     return 0;
  }
  dVx/=1000.0; dVy/=1000.0; dVz/=1000.0;

//Найдем квадрат скорости
  kvSkor=dVx*dVx+dVy*dVy+dVz*dVz;

//Расчитаем время наименьшей дальности
  T=-(dX*dVx+dY*dVy+dZ*dVz)/kvSkor;

  if(T<0)
  {
//Если время меньше 0, это значит, что минимальное сближение было
//бы раньше и рассматривать не стоит
    return 0;
  }

  if(T>Period)
  {
//Если время указанного сближения больше чем период
//Значит надо проверить расстояние между объектами в конце указанного периода
      dX=dX+dVx*Period;
      dY=dY+dVy*Period;
      dZ=dZ+dVz*Period;
      kvRast=dX*dX+dY*dY+dZ*dZ;
      if(kvRast<=kvDopRast)
      {
        if((lHazardTwo&0x00FF)<(Ret1&0x00FF))
        {
           lHazardTwo=(lHazardTwo&0xFF00)|(Ret1&0x00FF);
        }
        if((lHazardTwo&0xFF00)<(Ret1&0xFF00))
        {
           lHazardTwo=(lHazardTwo&0x00FF)|(Ret1&0xFF00);
        }

        if(((Dan->lHazardTwo)&0x00FF)<(Ret1&0x00FF))
        {
           Dan->lHazardTwo=((Dan->lHazardTwo)&0xFF00)|(Ret1&0x00FF);
        }
        if(((Dan->lHazardTwo)&0xFF00)<(Ret1&0xFF00))
        {
           Dan->lHazardTwo=((Dan->lHazardTwo)&0x00FF)|(Ret1&0xFF00);
        }
        return Ret1;
      }
      return 0;
  }

//А тут если T>=0 и T<=Period
//тогда надо рассчитать расстояние при времени T
  dX=dX+dVx*T;
  dY=dY+dVy*T;
  dZ=dZ+dVz*T;
  kvRast=dX*dX+dY*dY+dZ*dZ;
  if(kvRast<=kvDopRast)
  {
     if((lHazardTwo&0x00FF)<(Ret1&0x00FF))
     {
        lHazardTwo=(lHazardTwo&0xFF00)|(Ret1&0x00FF);
     }
     if((lHazardTwo&0xFF00)<(Ret1&0xFF00))
     {
        lHazardTwo=(lHazardTwo&0x00FF)|(Ret1&0xFF00);
     }

     if(((Dan->lHazardTwo)&0x00FF)<(Ret1&0x00FF))
     {
        Dan->lHazardTwo=((Dan->lHazardTwo)&0xFF00)|(Ret1&0x00FF);
     }
     if(((Dan->lHazardTwo)&0xFF00)<(Ret1&0xFF00))
     {
        Dan->lHazardTwo=((Dan->lHazardTwo)&0x00FF)|(Ret1&0xFF00);
     }
     return Ret1;
  }
  return 0;
}


/*Определяет, не лежит ли отсчет на курсе.
Возвращает 0, если борт лежит в секторе курса.
           1, если борт вне  сектора курса

Кроме того устанавливает значения отклонения от курса в метрах, проверяет

*/

long TDannyeObCeli::
  ControlForKrit_AvariaKurs(
                           long lPervDalnostVMetrah,  //Первая дальность в метрах
                           long lDalnostVzleta,       //Дальность места взлета
                           long lRastOtOsiVPPDoKursa, //Расстояние от ОСИ ВПП до курса
                           long lMaxDalnostOtRLS,     //Максимальная дальность относительно РЛС
                           double lfTan,       //Значение тангенса, но по идее должен быть равен 0
                           double lfTanMinus,  //Тангенс максим отклонения вниз
                           double lfTanPlus,   //Значение тангенса отклонения плюс
                           double lfTanMinus_3,//Значение тангенса угла отклонения угла/3
                           double lfTanPlus_3  //Значение тангенса угла/3
                            )
{
   long Y1,Ymin,Ymax,Ymin_3,Ymax_3;
//Не имеет смысла осуществлять проверку по курсу, если значение не известно
   if(Y==-100000)return 0;

//Проверка по дальности. Если дальность превышает lMaxDalnostOtRLS,
//то смысла проверять не умеет
   if(X>lMaxDalnostOtRLS+lPervDalnostVMetrah||X<lPervDalnostVMetrah)
   {
     VyhodOtklKurs_3=false;
     lHazardKurs=0;
     return 0;
   }
//Расчитаем координату Y курса, где борт
   Y1=Okrugl(lfTan*(X-lDalnostVzleta))+lRastOtOsiVPPDoKursa;

//Найдем крайную точку Y, где можно распологаться борту
   Ymin=
    Okrugl(lfTanMinus*(X-lDalnostVzleta))+lRastOtOsiVPPDoKursa;

//Найдем крайную точку Y, где можно распологаться борту    
   Ymax=
    Okrugl(lfTanPlus*(X-lDalnostVzleta))+lRastOtOsiVPPDoKursa;

//Найдем отклонение по Y
   lOtklKurs=Y-Y1;

//Отклонение на 1/3
   Ymin_3=Okrugl(lfTanMinus_3*(X-lDalnostVzleta))+lRastOtOsiVPPDoKursa;
   Ymax_3=Okrugl(lfTanPlus_3*(X-lDalnostVzleta))+lRastOtOsiVPPDoKursa;

   if (Y<=Ymin_3 || Y>=Ymax_3)
   {
     VyhodOtklKurs_3=true;
     if (Y<Ymin || Y>Ymax)
     {
        lHazardKurs=1;
        return 1;
     }
   }
   return 0;
}

/*Поиск критических ситуаций или аварийных ситуаций для глиссады:
Возвращает 0, если борт лежит в секторе курса.
           1, если борт вне  сектора курса
Кроме того устанавливает значения отклонения от курса в метрах, проверяет
 */
long TDannyeObCeli::
  ControlForKrit_AvariaGlis(
                           long lPervDalnostVMetrah,  //Первая дальность в метрах
                           long lDalnostVzleta,       //Дальность места взлета
                           long lDopVysotaGlis,       //Начальная высота глиссады
                           long lMaxDalnostOtRLS,     //Максимальная дальность относительно РЛС
                           double lfTan,      //Значение тангенса глиссалы
                           double lfTanMinus,  //Тангенс максим отклонения вниз
                           double lfTanPlus,   //Значение тангенса отклонения плюс
                           double lfTanMinus_3,//Значение тангенса угла отклонения угла/3
                           double lfTanPlus_3  //Значение тангенса угла/3
                            )
{
   long Z1,Zmin,Zmax,Zmin_3,Zmax_3;
//Проверим насчет глиссады
   if(Z==-100000)return 0;

   if(X>lMaxDalnostOtRLS+lPervDalnostVMetrah||X<lPervDalnostVMetrah)
   {
     VyhodOtklGlis_3=false;
     lHazardGlis=0;
     return 0;
   }

//НАйдем значение Z координаты при X
   Z1=Okrugl(lfTan*(X-lDalnostVzleta))+lDopVysotaGlis;
//Найдем минимальное значение Z, при данном X, чтобы было попадание в сектор глис
   Zmin=
    Okrugl(lfTanMinus*(X-lDalnostVzleta))+lDopVysotaGlis;

//Найдем максимальное значение Z, при данном X, чтобы было попадание в сектор глис
   Zmax=
    Okrugl(lfTanPlus*(X-lDalnostVzleta))+lDopVysotaGlis;

//Найдем отклонение по Z
   lOtklGlis=Z-Z1;      //Расчитаем значение отклонение

//Отклонение
   Zmin_3=Okrugl(lfTanMinus_3*(X-lDalnostVzleta))+lDopVysotaGlis;
   Zmax_3=Okrugl(lfTanPlus_3*(X-lDalnostVzleta))+lDopVysotaGlis;

   if (Z<=Zmin_3 || Z>=Zmax_3)
   {
     VyhodOtklGlis_3=true;
     if (Z<Zmin || Z>Zmax)
     {
        lHazardGlis=1;
        return 1;
     }
   }
   return 0;
}
//Получение информации находится ли точка клика внутри
bool TDannyeObCeli::isVnutryOtmetki(int x_scr, int y_scr, struct TVidCeli &vid_celi)
{
    double dx,dy;
    dx=x_scr-XvpGlis;
    dy=y_scr-YvpGlis;
    if(dx<=vid_celi.lRazmerCeliGlis/2.0&&
       dy<=vid_celi.lRazmerCeliGlis/2.0)
    {
        if((dx*dx+dy*dy)<=((vid_celi.lRazmerCeliGlis*vid_celi.lRazmerCeliGlis)/4.0))
        {
            return true;
        }
    }

    dx=x_scr-XvpKurs;
    dy=y_scr-YvpKurs;
    if(dx<=vid_celi.lRazmerCeliKurs/2.0&&
       dy<=vid_celi.lRazmerCeliKurs/2.0)
    {
        if((dx*dx+dy*dy)<=((vid_celi.lRazmerCeliKurs*vid_celi.lRazmerCeliKurs)/4.0))
        {
            return true;
        }
    }

    if(x_scr>=FormulyarRect.left()&&
       x_scr<=FormulyarRect.right()&&
       y_scr>=FormulyarRect.top()&&
       y_scr<=FormulyarRect.bottom())
    {
            return true;
    }

    return false;
}





QString TDannyeObCeli::getStringInfo()
{
    QString ret_str=QString("");

    if(lNomerVozdushnogoSudna<0)
    {
        ret_str=ret_str+QString("Б/Н-");
    }else{
        ret_str=ret_str+QString::number((int)lNomerVozdushnogoSudna)+QString("-");
    }

    if(lHazardKurs)
    {
        ret_str=ret_str+QString("ОПАСНОСТЬ: ВЫХОД С КУРСА!!! ");
    }

    if(lHazardGlis)
    {
        ret_str=ret_str+QString("ОПАСНОСТЬ: ВЫХОД С ГЛИССАДЫ!!! ");
    }


    if(lHazardTwo)
    {
        ret_str=ret_str+QString("ОПАСНОСТЬ С ДРУГИМ БОРТОМ!!! ");
    }

    if(lOtklKurs<=-10)
    {
        ret_str=ret_str+QString("Правее ")+QString::number((int)((-lOtklKurs)/10*10))+QString(" ");
    }else
    if(lOtklKurs>=10)
    {
        ret_str=ret_str+QString("Левее ")+QString::number((int)((lOtklKurs)/10*10))+QString(" ");
    }

    if(lOtklGlis<=-10)
    {
        ret_str=ret_str+QString("Ниже ")+QString::number((int)((-lOtklGlis)/10*10))+QString(" ");
    }else
    if(lOtklGlis>=10)
    {
        ret_str=ret_str+QString("Выше ")+QString::number((int)((lOtklGlis)/10*10))+QString(" ");
    }

    return ret_str;

}



bool odCompareByNomerVozdSudna(const TDannyeObCeli &doc1, const TDannyeObCeli &doc2)
{
    if(doc1.lNomerVozdushnogoSudna<0)return true;
    if(doc2.lNomerVozdushnogoSudna<0)return false;

    if(doc1.lNomerVozdushnogoSudna>doc2.lNomerVozdushnogoSudna)return true;
    return false;
}

