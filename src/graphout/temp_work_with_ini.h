/*Временный файл для работы с ини.
Имя объекта QSetting *stng

*/
#ifndef TEMP_WORK_WITH_INI_H
#define TEMP_WORK_WITH_INI_H



#include <QTextStream>

#include "safe_function.h"








#define RGB(R,G,B) (((unsigned)(R))|(((unsigned)(G))<<8)|(((unsigned)(B))<<16)|0xFF000000)

#define COLOR_TO_UINT(COL)  RGB(COL.red(), COL.green(),COL.blue())
#define UINT_TO_COLOR(UI)   QColor((UI)&0xFF, ((UI)>>8)&0xFF, ((UI)>>16)&0xFF)

#define STR_TO_COLOR(STR,COL)   \
{           \
    unsigned int ui_tmp;    \
    ui_tmp=QString(STR).toUInt(NULL,16);    \
    COL.setRgb((ui_tmp&0xFF),(ui_tmp>>8)&0xFF,(ui_tmp>>16)&0xFF);       \
}

#define COLOR_TO_STR(COL,STR)   \
{       \
    unsigned int tmp_us_col;    \
    tmp_us_col=COLOR_TO_UINT(COL);  \
    STR=QString::number(tmp_us_col,16); \
}



#define PEN_SET_STYLE(PN, STL) \
{                       \
    switch(STL) \
    { \
        case 0: \
            PN.setStyle(Qt::NoPen); \
        break; \
        \
        case 1: \
            PN.setStyle(Qt::SolidLine); \
        break;    \
        \
        case 2: \
            PN.setStyle(Qt::DashLine); \
        break;    \
            \
        case 3: \
            PN.setStyle(Qt::DotLine); \
        break; \
         \
        case 4: \
            PN.setStyle(Qt::DashDotLine); \
        break; \
         \
        case 5: \
            PN.setStyle(Qt::DashDotDotLine); \
        break;  \
        default:    \
            PN.setStyle(Qt::NoPen); \
        break;  \
    } \
}

#define PEN_GET_STYLE(PN, STL) \
{                       \
    if(PN.style()==Qt::NoPen) \
    { \
            STL=0; \
    } \
    else if(PN.style()==Qt::SolidLine)        \
    {     \
            STL=1; \
    }        \
    else if(PN.style()==Qt::DashLine)   \
    { \
            STL=2; \
    }  \
    else if(PN.style()==Qt::DotLine)        \
    { \
            STL=3; \
    } \
    else if(PN.style()==Qt::DashDotLine) \
    {     \
            STL=4; \
    } \
    else if(PN.style()==Qt::DashDotDotLine) \
    { \
            STL=5; \
    }else{ \
            STL=0; \
    }\
}


#define PEN_TO_STR(PEN, STR)  \
{\
    QString col_str111;    \
    int stl111; \
    PEN_GET_STYLE(PEN,stl111); \
    COLOR_TO_STR(PEN.color(), col_str111);  \
    STR=QString::number(stl111)+QString(" ")+   \
        QString::number(PEN.width())+QString(" ")+  \
        QString::number(PEN.width())+QString(" ")+  \
        col_str111; \
}


#define STR_TO_PEN(STR,PEN) \
{   \
    int stl111,w1,w2;  \
    QString col_str111; \
    QTextStream ts; \
    QString str111=STR; \
    QColor col111; \
    ts.setString(&str111);  \
    ts>>stl111>>w1>>w2>>col_str111; \
    STR_TO_COLOR(col_str111,col111);    \
    PEN_SET_STYLE(PEN,stl111);  \
    PEN.setWidth(w1);   \
    PEN.setColor(col111);   \
}


#define BRUSH_SET_STYLE(BR, STL) \
{       \
    if(STL) \
    {   \
       BR.setStyle(Qt::SolidPattern);   \
    }else{   \
       BR.setStyle(Qt::NoBrush);    \
    }   \
}

#define BRUSH_GET_STYLE(BR, STL) \
{       \
    if(BR.style()==Qt::SolidPattern) \
    {   \
        (STL)=1;   \
    }else{   \
       (STL)=0;    \
    }   \
}


#define BRUSH_TO_STR(BRSH, STR) \
{   \
    QString col_str111;    \
    int stl111; \
    BRUSH_GET_STYLE(BRSH, stl111);  \
    COLOR_TO_STR((BRSH.color()),col_str111);    \
    STR=QString::number(stl111)+QString(" ")+   \
        col_str111+QString(" 1"); \
}

#define STR_TO_BRUSH(STR,BRSH)  \
{   \
    int stl111;  \
    QString col_str111; \
    QTextStream ts; \
    QString str111=STR; \
    QColor col111; \
    ts.setString(&str111);  \
    ts>>stl111>>col_str111; \
    BRUSH_SET_STYLE(BRSH,stl111); \
    STR_TO_COLOR(col_str111,col111);    \
    BRSH.setColor(col111);  \
}

#define PS_SOLID   (Qt::SolidLine)

#define PS_DASH    (Qt::DashLine)

#define BS_SOLID   (Qt::SolidPattern)







#define SAVE_TO_INI(KEY, DATA) \
{   \
    QString tmp_str_key;  \
    tmp_str_key=QString(INI_GROUP)+QString("/")+QString(KEY);  \
    stng->setValue(tmp_str_key, (int)(DATA));  \
}

#define LOAD_FROM_INI(KEY, DATA, DEFAULT)   \
{   \
  int def;  \
  QString tmp_str_key;  \
  tmp_str_key=QString(INI_GROUP)+QString("/")+QString(KEY);  \
  def=atoi(DEFAULT);    \
  DATA=stng->value(tmp_str_key,def).toInt();    \
}


//Запись угла в файл
#define SAVE_UGOL_TO_INI(KEY, DATA) \
{    QString tmp_str_key;      \
    tmp_str_key=QString(INI_GROUP)+QString("/")+QString(KEY);  \
    stng->setValue(tmp_str_key,DATA);       \
}
//Чтение значение угла из файла
#define LOAD_UGOL_FROM_INI(KEY,DATA,DEFAULT) \
{ double DefaultPromDouble;                         \
  QTextStream ts; \
  QString tmp_str_key,str;     \
    tmp_str_key=QString(INI_GROUP)+QString("/")+QString(KEY);  \
     str=QString(DEFAULT);      \
   ts.setString(&str);  \
  if(ts.atEnd())    \
  { \
    DefaultPromDouble=0;    \
  }else{    \
        ts>>DefaultPromDouble;  \
  } \
  DATA=stng->value(tmp_str_key,DefaultPromDouble).toDouble();      \
}



#define SAVE_COLORREF_TO_INI(KEY, DATA) \
{   char tmp_str_char[100];                           \
    QString tmp_str_key;                      \
    sfSPRINTF(tmp_str_char,"%08X",((unsigned)DATA.red())| \
                                (((unsigned)DATA.green())<<8)|  \
                (((unsigned)DATA.blue())<<16)|0xFF000000); \
    tmp_str_key=QString(INI_GROUP)+QString("/")+QString(KEY);  \
    stng->setValue(tmp_str_key,QString(tmp_str_char));  \
}

#define LOAD_COLORREF_FROM_INI(KEY, DATA, U1) \
{	\
    bool ok;        \
    QString tmp_str_key,str;                                   \
    char tmp_str_char[100];             \
    unsigned long tmp_ul;			\
    sfSPRINTF(tmp_str_char,"%08X",(unsigned)U1); \
    tmp_str_key=QString(INI_GROUP)+QString("/")+QString(KEY);  \
    str=stng->value(tmp_str_key,QString(tmp_str_char)).toString();              \
    tmp_ul=str.toUInt(&ok,16);                                      \
    DATA.setRgb((int)(tmp_ul&0xFF),(int)((tmp_ul>>8)&0xFF),(int)(tmp_ul>>16)&0xFF);        \
}

//Сохранение в ини файл LOGPEN
#define SAVE_LOGPEN_TO_INI(KEY, DATA) \
{               \
  QPen pen222=(DATA);   \
  QString str222,tmp_str_key;       \
  tmp_str_key=QString(INI_GROUP)+QString("/")+QString(KEY); \
  PEN_TO_STR(pen222,str222);    \
  stng->setValue(tmp_str_key,str222); \
}

#define LOAD_LOGPEN_FROM_INI(KEY, DATA, U1, U2, U3, U4) \
{   \
    QString tmp_str_key,tmp_str,str222; \
    tmp_str_key=QString(INI_GROUP)+QString("/")+QString(KEY); \
    tmp_str=QString("0000"); \
    str222=stng->value(tmp_str_key,tmp_str).toString(); \
    if(str222==tmp_str) \
    {               \
        DATA.setStyle(U1);  \
        DATA.setWidth(U2);  \
        DATA.setColor(UINT_TO_COLOR(U4));  \
    }else{ \
        STR_TO_PEN(str222,(DATA));  \
    }   \
}
//Сохранение в ини файл LOGBRUSH

#define SAVE_LOGBRUSH_TO_INI(KEY, DATA) \
{   \
    QString tmp_str_key,str222;              \
    tmp_str_key=QString(INI_GROUP)+QString("/")+QString(KEY); \
    BRUSH_TO_STR((DATA),str222);    \
    stng->setValue(tmp_str_key,str222); \
}

#define LOAD_LOGBRUSH_FROM_INI(KEY, DATA, U1, U2, U3)\
{   \
    QString tmp_str_key,tmp_str,str222; \
    tmp_str_key=QString(INI_GROUP)+QString("/")+QString(KEY); \
    tmp_str=QString("0000"); \
    str222=stng->value(tmp_str_key,tmp_str).toString(); \
    if(str222==tmp_str) \
    {               \
        DATA.setStyle(U1);  \
        DATA.setColor(UINT_TO_COLOR(U2));  \
    }else{ \
        STR_TO_BRUSH(str222,(DATA));  \
    }   \
}

#define SAVE_BOOL_TO_INI(KEY, DATA) \
{   \
    int tmp_i;  \
    if(DATA)    \
    {       \
        tmp_i=1;    \
    }else{   \
        tmp_i=0;    \
    }   \
    SAVE_TO_INI(KEY,tmp_i); \
}


#define LOAD_BOOL_FROM_INI(KEY, DATA, U1)    \
{   \
    int tmp_i;  \
    if(U1)  \
    {   \
        LOAD_FROM_INI(KEY, tmp_i, "1")  \
    }else{ \
        LOAD_FROM_INI(KEY, tmp_i, "0")  \
    }   \
    DATA=(tmp_i!=0);    \
}


#define SAVE_STR_TO_INI(KEY, DATA)  \
{   \
    QString tmp_str_key,str222;              \
    str222=QString(DATA);   \
    tmp_str_key=QString(INI_GROUP)+QString("/")+QString(KEY); \
    stng->setValue(tmp_str_key, str222);    \
}

#define LOAD_STR_FROM_INI(KEY, DATA, DEFAULT)  \
{       \
    QString tmp_str_key;              \
    tmp_str_key=QString(INI_GROUP)+QString("/")+QString(KEY); \
    DATA=stng->value(tmp_str_key,QString(DEFAULT)).toString(); \
}

#define LOAD_PCHAR_FROM_INI(KEY, DATA, SZ_DATA, DEFAULT)  \
{   \
    QString str222; \
    LOAD_STR_FROM_INI(KEY, str222, DEFAULT)  \
    if(qstrlen(str222.toUtf8().data())<SZ_DATA) \
    {   \
        qstrcpy(DATA, str222.toUtf8().data());      \
    }else{   \
        qstrncpy(DATA,str222.toUtf8().data(), ((SZ_DATA)-1)); \
        (DATA)[(SZ_DATA)-1]=0;  \
    }   \
}

#endif // TEMP_WORK_WITH_INI_H
