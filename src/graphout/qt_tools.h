/*Модуль: qt_tools
Автор: Матвеенко Е.А. ЧРЗ Полет ОКБ

Реализованы вспомогательные процедуры отображения тескта средствами QT
*/

#ifndef QT_TOOLS_H
#define QT_TOOLS_H

#include <QString>
#include <QFont>
#include <QPainter>
#include <QPen>
#include <QColor>


#define DEFAULT_FAMILY_FONT QString("Times New Roman")

/*Определение положения текста относительно какого пикселя*/
enum QTT_GdeText
{
    QTT_levee=1,    //текст левее.
    QTT_pravee,     //текст правее
    QTT_vyshe,      //текст выше
    QTT_nije        //текст ниже
};


/*Вывод текста. Возвращает 1*/
int textOutScr(QPainter &pntr,          //Дескриптер области отображения
               int x_scr, int y_scr,    //Координаты в системе окна
               const QFont &font,       //фонте
               const QColor &col,       //цвет текста
               const QString &str,      //строка текста
               enum QTT_GdeText qtt,    //положение текста
               int otstup               //отступ текста от точки в количестве пикселей
              );

/*Определение высоты текста в зависимости отнастроек шрифта.
Возвращает высоту в количестве пикселей
*/
int heightOfTextScr(const QFont &font);

/*Определение ширину текста в зависимости отнастроек шрифта.
Возвращает ширину в количестве пикселей
*/
int widthOfTextScr(const QFont &font, const QString &str);


/*Упрощенная процедура создания Шрифта от имени шрифта (fam), размера (size),
жирности(bBold)
*/
QFont createFontFromFamalyAndSize(const QString &fam, int size, bool bBold);




#endif // QT_TOOLS_H
