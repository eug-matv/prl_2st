﻿//---------------------------------------------------------------------------

#include "stdafx.h"


#include <math.h>
#include <stdio.h>
#include <QMessageBox>
#include <QSettings>
#include <QDateTime>
#include <QDate>
#include <QTime>
#include <QTextStream>
#include <QCoreApplication>
#include <QDir>
#include <QFile>

#include "AnyTools.h"
#include "RZPLinGlaf.h"
#include "time_tools.h"
#include "temp_work_with_ini.h"
#include "lua_utils.h"


#define MAX(a, b)  (((a) > (b)) ? (a) : (b))
#define MIN(a, b)  (((a) < (b)) ? (a) : (b))


//---------------------------------------------------------------------------


/*Конструктор */
TRZPDraw::TRZPDraw(
           const char *LogFileOut,      //Лог-файл для журналирования
           const char *LogFileOutForErr//Лог-файл для записи ошибок
)
{
  long Dlina;
  Dlina=strlen(LogFileOut);
  LogFile=(char*)malloc((Dlina+1)*sizeof(char));

  Dlina=strlen(LogFileOutForErr);
  LogFileError=(char*)malloc((Dlina+1)*sizeof(char));

  sfSTRCPY(LogFile,LogFileOut);
  sfSTRCPY(LogFileError,LogFileOutForErr);

//Заполним критические секции

  lNapravlenieVPP=0;
  lHazardGlis=0;
  lHazardKurs=0;
  lHazardTwoBorts=0;
  isNeedPereraschet=0;

  ldoc.clear();
  ldoc_tail.clear();

  imgBack=NULL;
  imgSetka=NULL;

  bInit=false;	
  isReadyForOutput=0;
  bHazardSituation=false;


  dfHazardMaxUgolKurs=dfHazardMaxUgolGlis=dfHazardMinUgolGlis=0;


   snd_plr=NULL;        //Пока нечего играть

}



TRZPDraw::~TRZPDraw()
{

  if(bInit)
  {
        EndWork();
  }
  free(LogFile);
  free(LogFileError);

};


long TRZPDraw::BeginWork(
        const QRect &rDraw,            //Область отображения окна
        const char *PreSetupIni,     //Файл инициализации. Этот файл заполняется вручную - или отдельной программой
        const char *UserIni         //Файл инициализации.
                )
{
  FILE *fp;
  long Ret,DlinaStr;
  char Strka[200],Strka2[200];
  QPainter pntr;

  double magnit_otkl_napravlenie_grad;



  ErrorKod1=0;
  ErrorKod2=0;

//Проверим запущен ли вывод
  if(bInit)return 0;





//Проверка существования файла
  fp=fopen(PreSetupIni,"r"); //Откроем в режиме чтения
  if(fp==NULL)
  {
    ErrorKod1=-3;
    ErrorKod1=0;
    sfSPRINTF(Strka,"Error opening Admin ini-file: %s",PreSetupIni);
    DataToLogFileError(Strka,true);
    QMessageBox::critical(NULL,QString("ERROR"),QString(Strka));
    return -3;
  }
  fclose(fp);



//Открытие ини файла
  Ret=ZagruzitPreSetupIni(PreSetupIni,Strka2);
  if(Ret!=1)
  {
    ErrorKod1=-4;
    ErrorKod2=Ret;
    sfSPRINTF(Strka,"Error reading %s from Admin ini-file:  %s. Code error=%d",
       Strka2,
       PreSetupIni,Ret);
    DataToLogFileError(Strka,true);
    QMessageBox::critical(NULL,QString("ERROR"),QString(Strka));
    return -4;
  }


/*Надо получить значение направление из файла UserIni*/
  zagruzitNapravlenie(UserIni);


  /*А теперь установим все необходимые параметры в том классе, которые являются
  родителями текущего*/
   UstanovitParametry();



/*Добавлено 08.06.2015*/
//Проверка загрузки данных из каталога LUA
  Ret=luLoadLuaFilesFromDirectory(csLUA_Directory);
  if(Ret>99 || Ret<=0 )
  {
      QMessageBox::critical(NULL,QString("ERROR"),QString(luGetErrorMessage()));
      return -5;

  }




//Проверка существования
/*
  fp=fopen(UserIni,"r");
  if(fp==NULL)
  {
    ErrorKod1=-5;
    ErrorKod2=0;
    sprintf(Strka,"Error opening Users ini-file: %s",UserIni);
    DataToLogFileError(Strka,true);
    MessageBox(NULL,Strka,"Error",MB_OK);
    return -5;
  }
  fclose(fp);
*/
//Загрузим данные настроек
//Пока пропущено
//Установить все параметры из файла
  Ret=ZagruzitIUstanovitDannye(UserIni);
  DlinaStr=strlen(UserIni);
  IniFileForUser=(char*)malloc((DlinaStr+1)*sizeof(char));
  if(IniFileForUser)
  {
    sfSTRCPY(IniFileForUser,UserIni);
  }




  //Передача параметров в LUA

  if(lNapravlenieVPP)
  {
     magnit_otkl_napravlenie_grad=dMagnKursPosadkiNaVPP+180.0;
     if(dMagnKursPosadkiNaVPP>=360.0)
     {
         magnit_otkl_napravlenie_grad-360.0;
     }
  }else{
     magnit_otkl_napravlenie_grad=dMagnKursPosadkiNaVPP;
  }

    Ret=luSetNewParametersForLUA(dMinSektorSkanKurs,
                                 dMaxSektorSkanKurs,
                                 dMinSektorSkanGlis,
                                 dMaxSektorSkanGlis,
                                 dMinUgolDNGlisVKurs,
                                 dMaxUgolDNGlisVKurs,
                                 dMinUgolDNKursaVGlis,
                                 dMaxUgolDNKursaVGlis,
                                 dXofRSP_m/1000.0,dYofRSP_m/1000.0,
                                 lRaznicaVPPIAntena,
                                 magnit_otkl_napravlenie_grad,
                                 80.0
                                 );









//Теперь инициируем
  RectDraw=rDraw;


//Теперь создадим совместимый с ним
  imgSetka=NULL;

//Теперь зададим размеры
  imgBack=NULL;



//Зададим размеры области глиссады, исходя из данныъ rdGlissada
  RectGlissada.setLeft(0);
  RectGlissada.setRight(0);
  RectGlissada.setTop(0);
  RectGlissada.setBottom(0);

//Зададим размеры области курса, исходя из данныъ rdKurs
  RectKurs.setLeft(0);
  RectKurs.setRight(0);
  RectKurs.setTop(0);
  RectKurs.setBottom(0);


  if(QFile::exists(fileNameAlarmWAV))
  {
      snd_plr=new QSound(fileNameAlarmWAV);
  }else{
      snd_plr=NULL;
  }


  isPlayingSound=false;

  bOblastVydeleniya=false;
  bInit=true;




  return 1;
}


long TRZPDraw::EndWork(void)
{
  if(!bInit)return 0;

//Запишем данные в ини-файл
  if(IniFileForUser)
  {
//Запись данных в ини-файл
     ZapisatDannyeVIniFile(IniFileForUser);
     free(IniFileForUser);
  }
  bInit=false;
  if(imgSetka)delete imgSetka;
  if(imgBack)delete imgBack;

  if(snd_plr)
  {
      delete snd_plr;
      snd_plr=NULL;
  }

  luCloseLua();
  return 1;
}




/*Предварительный расчет координат и подготовка данных к выводу.
Если что-то поменялось, то вернуть 1, иначе вернуть 0*/
int TRZPDraw::RaschetKoordinatIPodgotovkaDannyhKVyvodu()
{

    QPainter pntr;
    TDannyeObCeli doc;
    int is_change_masshtab=0;

//Надо добавить все отметки в список
    mutex_dobav_cel.lock();


    while(!(ldoc_new.empty()))
    {
        doc=ldoc_new.front();
        ldoc_new.pop_front();
        DobavitNovuyuCelProtected(doc);
    }


    mutex_dobav_cel.unlock();


    mutex_napr.lock();

    removeDataFromLdocAndLdoc_tail();



    mutex_napr.unlock();


    if(isNeedPereraschet)
    {
        bHazardSituation=false;

        dfHazardMaxUgolGlis=dfHazardMaxUgolKurs=dfHazardMinUgolGlis=0.0;
        lHazardMaxDalnost=0;
        RaschetKoordinatXZYIOcenkaPar(isNeedPereraschet==2);

        if(trySetNewMasshtabWithHazard())
        {
            RaschetKoordinatXZYIOcenkaPar(isNeedPereraschet==2);
            is_change_masshtab=1;
        }

        if(bHazardSituation)
        {
            if(!isPlayingSound)
            {
                if(snd_plr)
                {
                   snd_plr->setLoops(1);
                   snd_plr->play();
                }
                isPlayingSound=true;
            }else{
                if(snd_plr)
                {
                    if(snd_plr->isFinished())
                    {
                        snd_plr->setLoops(1);
                        snd_plr->play();
                    }
                }
            }
        }else{
            if(isPlayingSound)
            {
                if(snd_plr)
                {
                    snd_plr->stop();
                }
                isPlayingSound=false;
            }
        }

        mutex_paint.lock();
        if(imgBack&&imgSetka)
        {
            pntr.begin(imgBack);
            pntr.drawImage(0,0,*imgSetka);
            pntr.end();
        }
      //Нарисуем отсчеты на imgBack

        VyvestiOtschetyVPolyaKursaIGlissady();
        isNeedPereraschet=0;
        mutex_paint.unlock();

        return 1+1000*is_change_masshtab;
    }

    return 0;

}



//Процедура отображения на экран при периодическом вызове
//Вызове
long TRZPDraw::VyvodNaEkranForPeriodDC(QPainter &pntr)
{

//Скопируем сетку в поле DCBack
    //Выведем сетку в поле DCSetka
    //Скопируем сетку в поле DCBack
  mutex_paint.lock();



  pntr.drawImage(0,0,*imgBack);
  mutex_paint.unlock();
  return 1;
}


void TRZPDraw::zagruzitNapravlenie(const char* IniFile)
{

    QSettings *stng;
    stng=new QSettings(QString(IniFile),QSettings::IniFormat);

    lNapravlenieVPP=stng->value(QString("COMMON/NapravlenieVPP"),0).toInt();

    delete stng;


}

long TRZPDraw::UstanovitParametry()
{
//Передадим значения максимальной дальности

  if(lNapravlenieVPP)
  {
    TGlissadaDraw::NovoeRasstoyanieRSPOtMestaPosadki(
       lROtTorcaVPPDoCentraAntKurNapr[1]-
       lROtTochkiPrizDoTorcaVPP[1]);
    TGlissadaDraw::NovoeRasstoyanieRSPOtTorcaVPP(lROtTorcaVPPDoCentraAntKurNapr[1]);


    TKursDraw::NovoeRasstoyanieRSPOtMestaPosadki(
       lROtTorcaVPPDoCentraAntKurNapr[1]-
       lROtTochkiPrizDoTorcaVPP[1]);
    TKursDraw::NovoeRasstoyanieRSPOtTorcaVPP(lROtTorcaVPPDoCentraAntKurNapr[1]);

//Передадим данные о курсе. Так как с другой стороны
    TKursDraw::NovayLiniyaPosadKursa(
              -lROtOsiVPPDoCentraAntKursa,
			  dUgolKursa[1],
              dOtklonPoKursuMinus,
              dOtklonPoKursuPlus,
              lUdalenie);


   }else{

    TGlissadaDraw::NovoeRasstoyanieRSPOtMestaPosadki(
       lROtTorcaVPPDoCentraAntKurNapr[0]-
       lROtTochkiPrizDoTorcaVPP[0]);
    TGlissadaDraw::NovoeRasstoyanieRSPOtTorcaVPP(lROtTorcaVPPDoCentraAntKurNapr[0]);


    TKursDraw::NovoeRasstoyanieRSPOtMestaPosadki(
       lROtTorcaVPPDoCentraAntKurNapr[0]-
       lROtTochkiPrizDoTorcaVPP[0]);
    TKursDraw::NovoeRasstoyanieRSPOtTorcaVPP(lROtTorcaVPPDoCentraAntKurNapr[0]);

//Передадим данные о курсе
    TKursDraw::NovayLiniyaPosadKursa(
              lROtOsiVPPDoCentraAntKursa,
              dUgolKursa[0],
              dOtklonPoKursuMinus,
              dOtklonPoKursuPlus,
              lUdalenie);

   }


//Передадим данные о расстоянии ВПП до антенны. Тут вне зависимости от направления
  TGlissadaDraw::NovayLiniyaGlissady(dUgolGlissady[lNapravlenieVPP],
                                      lUdalenie,
                                      lRaznicaVPPIAntena,
                                      dOtklonPoGlisMinus,
                                      dOtklonPoGlisPlus,
                                      lUdalenie);

//Установим данные о секторе сканирования в поле курса
  TKursDraw::NovyySektorSkanirovania(dMinSektorSkanKurs,
                                     dMaxSektorSkanKurs);

//Установим значение сектора диаграммы направленности антенны глиссады в поле
//курса
  TKursDraw::NovyyDNGlis(dMinUgolDNGlisVKurs,
                         dMaxUgolDNGlisVKurs);

//Установим значение сектора сканирования вполе глиссады
  TGlissadaDraw::NovyySektorSkanirovania(dMinSektorSkanGlis,
                                     dMaxSektorSkanGlis);

//Установим значение сектора диаграммы направленности антенны курса в поле
//глиссады
  TGlissadaDraw::NovyyDNKursa(dMinUgolDNKursaVGlis,
                              dMaxUgolDNKursaVGlis);


  return 1;
}


//Полный путь к имени файла
//Возвращает 1, если успешно
//0 - если не успешно
long TRZPDraw::ZagruzitPreSetupIni(const char *IniFile, const char *ErrorPar)
{
  int Ret;
  char Strka[90];
  double temp;
  QString str;
  QDir a_dir;

  QSettings *stng;


  stng=new QSettings(QString(IniFile),QSettings::IniFormat);






//Прочитать
//Расстояние от точки приземления до торца ВПП
//Должно быть записано два значения
  LOAD_FROM_INI("lROtTochkiPrizDoTorcaVPP0",lROtTochkiPrizDoTorcaVPP[0],"-10")

  if(lROtTochkiPrizDoTorcaVPP[0]<0)return (-1); //Ошибка


  LOAD_FROM_INI("lROtTochkiPrizDoTorcaVPP1",lROtTochkiPrizDoTorcaVPP[1],"-10")
  if(lROtTochkiPrizDoTorcaVPP[1]<0)return (-2); //Ошибка


//Расстояние от оси ВПП до центра полотна антенны курса
  LOAD_FROM_INI("lROtOsiVPPDoCentraAntKursa",lROtOsiVPPDoCentraAntKursa,"-100000")
  if(lROtOsiVPPDoCentraAntKursa==-100000)return (-3);


//Расстояние от торца ВПП до центра полотнп антенны курса
//в направлении паралельном оси ВПП
  LOAD_FROM_INI("lROtTorcaVPPDoCentraAntKurNapr0",lROtTorcaVPPDoCentraAntKurNapr[0],"-10")
  if(lROtTorcaVPPDoCentraAntKurNapr[0]<0)return (-4);
  LOAD_FROM_INI("lROtTorcaVPPDoCentraAntKurNapr1",lROtTorcaVPPDoCentraAntKurNapr[1],"-10")
  if(lROtTorcaVPPDoCentraAntKurNapr[1]<0)return (-5);

//Превышение и приуменьшение точки приземления на поверхности ВПП над центром полотна аннтены глиссады
//Значение может быть отрицательным
  LOAD_FROM_INI("lRaznicaVPPIAntena",lRaznicaVPPIAntena,"-100000")
  if(lRaznicaVPPIAntena==-100000)return (-6);


//Расстояние от торца ВПП до ДПРМ
  LOAD_FROM_INI("lDalnDoDPRMOtTorcaVPP0",lDalnDoDPRMOtTorcaVPP[0],"-10")
  if(lDalnDoDPRMOtTorcaVPP[0]<0)return (-7);
  LOAD_FROM_INI("lDalnDoDPRMOtTorcaVPP1",lDalnDoDPRMOtTorcaVPP[1],"-10")
  if(lDalnDoDPRMOtTorcaVPP[1]<0)return (-8);

//Расстояние от торца ВПП до БПРМ
  LOAD_FROM_INI("lDalnDoBPRMOtTorcaVPP0",lDalnDoBPRMOtTorcaVPP[0],"-10")

  if(lDalnDoBPRMOtTorcaVPP[0]<0)return (-7);
  LOAD_FROM_INI("lDalnDoBPRMOtTorcaVPP1",lDalnDoBPRMOtTorcaVPP[1],"-10")

  if(lDalnDoBPRMOtTorcaVPP[1]<0)return (-8);

//Максимальное удаление глиссады и курса
  LOAD_FROM_INI("lUdalenie",lUdalenie,"-10")
  if(lUdalenie<0)return (-9);


//Теперь разберемся с углами - это числа с пл. запятой.
//Угол наклона глиссады к горизонту
  LOAD_UGOL_FROM_INI("dUgolGlissady0",dUgolGlissady[0],"-1000.0")
  if(dUgolGlissady[0]<-10)return (-10);

//Теперь разберемся с углами - это числа с пл. запятой.
//Угол наклона глиссады к горизонту
  LOAD_UGOL_FROM_INI("dUgolGlissady1",dUgolGlissady[1],"-1000.0")
  if(dUgolGlissady[1]<-10)return (-1010);



//Углы сектора относительно глиссады. Который надо отнять от глиссады
  LOAD_UGOL_FROM_INI("dOtklonPoGlisMinus",dOtklonPoGlisMinus,"-1000.0")
  if(dOtklonPoGlisMinus<0)return (-11);

//Углы сектора относительно глиссады. Который надо приплюсовать  к глиссаде
  LOAD_UGOL_FROM_INI("dOtklonPoGlisPlus",dOtklonPoGlisPlus,"-1000.0")
  if(dOtklonPoGlisMinus<0)return (-12);



//Загрузим значение угла курса - скорее всего 0 - равный нулю антуна
  LOAD_UGOL_FROM_INI("dUgolKursa0",dUgolKursa[0],"-1000.0")
  if(dUgolKursa[0]<-10)return (-112);

//Теперь разберемся с углами - это числа с пл. запятой.
//Угол наклона глиссады к горизонту
  LOAD_UGOL_FROM_INI("dUgolKursa1",dUgolKursa[1],"-1000.0")
  if(dUgolKursa[1]<-10)return (-1112);



//Отклонение нижнее от линии курса
  LOAD_UGOL_FROM_INI("dOtklonPoKursuMinus",dOtklonPoKursuMinus,"-10")
  if(dOtklonPoKursuMinus<0)return (-13);

//Оклонение верхнее от линии курса
  LOAD_UGOL_FROM_INI("dOtklonPoKursuPlus",dOtklonPoKursuPlus,"-10")
  if(dOtklonPoKursuPlus<0)return (-14);

//нижнии и верхнии значения сектора сканирования курса
  LOAD_UGOL_FROM_INI("dMinSektorSkanKurs",dMinSektorSkanKurs,"10")
  if(dMinSektorSkanKurs>0)return (-15);

  LOAD_UGOL_FROM_INI("dMaxSektorSkanKurs",dMaxSektorSkanKurs,"-10")
  if(dMaxSektorSkanKurs<0)return (-16);

//нижнии и верхнии значения сектора сканирования глиссады
  LOAD_UGOL_FROM_INI("dMinSektorSkanGlis",dMinSektorSkanGlis,"100")
  if(dMinSektorSkanGlis>20)return (-16);


  LOAD_UGOL_FROM_INI("dMaxSektorSkanGlis",dMaxSektorSkanGlis,"-20")
  if(dMaxSektorSkanKurs<-10)return (-17);

//Диаграмма направленности курса в поле глиссады
  LOAD_UGOL_FROM_INI("dMinUgolDNKursaVGlis",dMinUgolDNKursaVGlis,"100")
  if(dMinUgolDNKursaVGlis>20)return (-18);

  LOAD_UGOL_FROM_INI("dMaxUgolDNKursaVGlis",dMaxUgolDNKursaVGlis,"-20")
  if(dMaxUgolDNKursaVGlis<-10)return (-19);

//Диаграмма направленности глиссады в поле курса
  LOAD_UGOL_FROM_INI("dMinUgolSkanGlisVKurs",dMinUgolDNGlisVKurs,"10")
  if(dMinUgolDNGlisVKurs>0)return (-20);

  LOAD_UGOL_FROM_INI("dMaxUgolSkanGlisVKurs",dMaxUgolDNGlisVKurs,"-10")
  if(dMaxUgolDNGlisVKurs<0)return (-21);

//Магнитный курс посадки
  LOAD_UGOL_FROM_INI("dMagnKursPosadkiNaVPP",dMagnKursPosadkiNaVPP,"-100")
  if(dMagnKursPosadkiNaVPP<-0.01||dMagnKursPosadkiNaVPP>=359.9999)return (-22);

/*Разберемся с расстояние и со временем, которе используется для анализа аварии*/
//Минимальное расстояние между целями
  LOAD_UGOL_FROM_INI("dRmin",dRmin,"-10")
  if(dRmin<-0.01)return (-23);

//Время за которое отметки могут сблизиться на допустимое расстояние
  LOAD_UGOL_FROM_INI("dTimeForRmin",dTimeForRmin,"0")
  if(dTimeForRmin<0.1)return (-24);


//Получение координаты X РСП в системе МПСТК в м
  LOAD_FROM_INI("dXofRSP_m", dXofRSP_m, "-2000000001");
  if(dXofRSP_m<-2000000000)return (-25);


  LOAD_FROM_INI("dYofRSP_m", dYofRSP_m, "-2000000001");
  if(dYofRSP_m<-2000000000)return (-26);



  LOAD_STR_FROM_INI("csLUA_Directory", csLUA_Directory, "");
  if(csLUA_Directory.trimmed().isEmpty())return (-27);



  QTextStream ts;

   str=stng->value(QString("COMMON/rdKurs"),QString("0.05 0.5 0.6 0.95")).toString();


  ts.setString(&str);

  if(ts.atEnd()==false)
  {
    ts>>rdKurs.left;
  }
  if(ts.atEnd()==false)
  {
        ts>>rdKurs.top;
  }
  if(ts.atEnd()==false)
  {
      ts>>rdKurs.right;
  }
  if(ts.atEnd()==false)
  {
     ts>>rdKurs.bottom;
  }




   if(rdKurs.left>rdKurs.right)
   {temp=rdKurs.right;rdKurs.right=rdKurs.left;rdKurs.right=temp;}
   if(rdKurs.top>rdKurs.bottom)
   {temp=rdKurs.bottom;rdKurs.bottom=rdKurs.top;rdKurs.top=temp;}
   if(rdKurs.left<0||rdKurs.right>1)return (-25);



   str=stng->value(QString("COMMON/rdGlissada"),QString("0.4 0.05 0.95 0.45")).toString();

   ts.setString(&str);
   if(ts.atEnd()==false)
   {
     ts>>rdGlissada.left;
   }
   if(ts.atEnd()==false)
   {
         ts>>rdGlissada.top;
   }
   if(ts.atEnd()==false)
   {
       ts>>rdGlissada.right;
   }
   if(ts.atEnd()==false)
   {
      ts>>rdGlissada.bottom;
   }

   if(rdGlissada.left>rdGlissada.right)
   {temp=rdGlissada.right;
    rdGlissada.right=rdGlissada.left;rdGlissada.right=temp;}
   if(rdGlissada.top>rdGlissada.bottom)
   {temp=rdGlissada.bottom;
    rdGlissada.bottom=rdGlissada.top;rdGlissada.top=temp;}
   if(rdGlissada.left<0||rdGlissada.right>1)return (-26);



   rdInfoText.top=0.05; rdInfoText.left=0.05; rdInfoText.right=0.4; rdInfoText.bottom=0.4;

   str=stng->value(QString("COMMON/rdInfoText"),QString("0.05 0.05 0.4 0.4")).toString();
   ts.setString(&str);
   if(ts.atEnd()==false)
   {
     ts>>rdInfoText.left;
   }
   if(ts.atEnd()==false)
   {
         ts>>rdInfoText.top;
   }
   if(ts.atEnd()==false)
   {
       ts>>rdInfoText.right;
   }
   if(ts.atEnd()==false)
   {
      ts>>rdInfoText.bottom;
   }

   if(rdInfoText.left>rdInfoText.right)
   {temp=rdInfoText.right;
    rdInfoText.right=rdInfoText.left;rdInfoText.right=temp;}
   if(rdInfoText.top>rdInfoText.bottom)
   {temp=rdInfoText.bottom;
    rdInfoText.bottom=rdInfoText.top;rdInfoText.top=temp;}
   if(rdInfoText.left<0||rdInfoText.right>1)return (-27);




    delete stng;
   return 1;
}


//Полный путь к имени файла
//Возвращает 1, если успешно
//0 - если не успешно
long TRZPDraw::ZagruzitIUstanovitDannye(const char *IniFile)
{
   int max_dalnost;
   int tmp_arr[10];

   QPen lps;
   QBrush lbs[3];
   long RazmerCeli,RazmerTexta;
   QColor Cvet;
   bool bTextGlis,bTextKurs; //Отображать текст курса, такст глиссады
   bool bVectGlis,bVectKurs;
   int i,j;
   int r,g,b;
   QSettings *stng;
   QDir a_dir;

   stng=new QSettings(QString(IniFile),QSettings::IniFormat);



   lpOblastVydeleniya.setStyle(Qt::SolidLine);
   lpOblastVydeleniya.setWidth(1);
   lpOblastVydeleniya.setColor(QColor(0,200,0));  //Зеленый цвет



//Загрузить данные из файла
   TKursDraw::LoadDataFromINI(IniFile);
   TGlissadaDraw::LoadDataFromINI(IniFile);

//Установим параметры отсчетов
//Загрузим описание для первого вида отсчтов если отсчет получен только по мос
   LOAD_LOGPEN_FROM_INI("LOGPEN_LabelMOS",
                        lps,PS_SOLID,1,1,RGB(80,180,10))


   LOAD_LOGBRUSH_FROM_INI("LOGBRUSH_LabelMOS0",
                          lbs[0],BS_SOLID,RGB(160,0,0),0)



   LOAD_LOGBRUSH_FROM_INI("LOGBRUSH_LabelMOS1",
                          lbs[1],BS_SOLID,RGB(200,100,0),0)


   LOAD_LOGBRUSH_FROM_INI("LOGBRUSH_LabelMOS2",
                          lbs[2],BS_SOLID,RGB(200,200,100),0)

   LOAD_FROM_INI("SizeOfLabelMOS",RazmerCeli,"6");    //Получим размер цели


   LOAD_BOOL_FROM_INI("VisibleTextForMOSInCourse",bTextKurs,true)
   LOAD_BOOL_FROM_INI("VisibleTextForMOSInGlissade",bTextGlis,true)
   LOAD_FROM_INI("SizeOfTextForMOS",RazmerTexta,"18")
   LOAD_COLORREF_FROM_INI("ColorOfTextForMOS",Cvet,RGB(160,0,0))

//А теперь присвоим все полученные данные различным видам отсчетов
   VidyCeley[1].lpCeliKurs[0][0]=lps;
   VidyCeley[1].lpCeliKurs[1][0]=lps;
   VidyCeley[1].lpCeliKurs[2][0]=lps;
   VidyCeley[1].lbCeliKurs[0][0]=lbs[0];
   VidyCeley[1].lbCeliKurs[1][0]=lbs[1];
   VidyCeley[1].lbCeliKurs[2][0]=lbs[2];

   VidyCeley[1].lRazmerCeliKurs=RazmerCeli;
   VidyCeley[1].IsVyvodTextKurs=bTextKurs;
   VidyCeley[1].szTextKurs=RazmerTexta;
   VidyCeley[1].cTextKurs=Cvet;

   VidyCeley[1].lpCeliGlis[0][0]=lps;
   VidyCeley[1].lpCeliGlis[1][0]=lps;
   VidyCeley[1].lpCeliGlis[2][0]=lps;
   VidyCeley[1].lbCeliGlis[0][0]=lbs[0];
   VidyCeley[1].lbCeliGlis[1][0]=lbs[1];
   VidyCeley[1].lbCeliGlis[2][0]=lbs[2];
   VidyCeley[1].lRazmerCeliGlis=RazmerCeli;
   VidyCeley[1].IsVyvodTextGlis=bTextGlis;
   VidyCeley[1].szTextGlis=RazmerTexta;
   VidyCeley[1].cTextGlis=Cvet;

   VidyCeley[3].lpCeliGlis[0][0]=lps;
   VidyCeley[3].lpCeliGlis[1][0]=lps;
   VidyCeley[3].lpCeliGlis[2][0]=lps;
   VidyCeley[3].lbCeliGlis[0][0]=lbs[0];
   VidyCeley[3].lbCeliGlis[1][0]=lbs[1];
   VidyCeley[3].lbCeliGlis[2][0]=lbs[2];


   VidyCeley[3].lRazmerCeliGlis=RazmerCeli;
   VidyCeley[3].IsVyvodTextGlis=bTextGlis;
   VidyCeley[3].szTextGlis=RazmerTexta;
   VidyCeley[3].cTextGlis=Cvet;

   VidyCeley[5].lpCeliKurs[0][0]=lps;
   VidyCeley[5].lpCeliKurs[1][0]=lps;
   VidyCeley[5].lpCeliKurs[2][0]=lps;
   VidyCeley[5].lbCeliKurs[0][0]=lbs[0];
   VidyCeley[5].lbCeliKurs[1][0]=lbs[1];
   VidyCeley[5].lbCeliKurs[2][0]=lbs[2];
   VidyCeley[5].lRazmerCeliKurs=RazmerCeli;
   VidyCeley[5].IsVyvodVektorSkorostiKurs=bTextKurs;
   VidyCeley[5].szTextKurs=RazmerTexta;
   VidyCeley[5].cTextKurs=Cvet;



//Теперь получим данные полученные по ПРЛФС, но не по МОС
   //Установим параметры отсчетов
//Загрузим описание для первого вида отсчтов если отсчет получен только по мос
   LOAD_LOGPEN_FROM_INI("LOGPEN_LabelPRLFS",
                        lps,PS_SOLID,1,1,RGB(0,0,0))

   LOAD_LOGBRUSH_FROM_INI("LOGBRUSH_LabelPRLFS0",
                          lbs[0],BS_SOLID,RGB(160,80,0),0)

   LOAD_LOGBRUSH_FROM_INI("LOGBRUSH_LabelPRLFS1",
                          lbs[1],BS_SOLID,RGB(200,180,0),0)

   LOAD_LOGBRUSH_FROM_INI("LOGBRUSH_LabelPRLFS2",
                          lbs[2],BS_SOLID,RGB(200,200,100),0)

   LOAD_FROM_INI("SizeOfLabelPRLFS",RazmerCeli,"6");    //Получим размер цели

   LOAD_BOOL_FROM_INI("VisibleTextForPRLFSInCourse",bTextKurs,true)
   LOAD_BOOL_FROM_INI("VisibleTextForPRLFSInGlissade",bTextGlis,true)
   LOAD_FROM_INI("SizeOfTextForPRLFS",RazmerTexta,"18")
   LOAD_COLORREF_FROM_INI("ColorOfTextForPRLFS",Cvet,RGB(160,80,0))


//А теперь присвоим все полученные данные различным видам отсчетов
   VidyCeley[6].lpCeliKurs[0][0]=lps;
   VidyCeley[6].lpCeliKurs[1][0]=lps;
   VidyCeley[6].lpCeliKurs[2][0]=lps;
   VidyCeley[6].lbCeliKurs[0][0]=lbs[0];
   VidyCeley[6].lbCeliKurs[1][0]=lbs[1];
   VidyCeley[6].lbCeliKurs[2][0]=lbs[2];

   VidyCeley[6].lRazmerCeliKurs=RazmerCeli;
   VidyCeley[6].IsVyvodTextKurs=bTextKurs;
   VidyCeley[6].szTextKurs=RazmerTexta;
   VidyCeley[6].cTextKurs=Cvet;


   VidyCeley[6].lpCeliGlis[0][0]=lps;
   VidyCeley[6].lpCeliGlis[1][0]=lps;
   VidyCeley[6].lpCeliGlis[2][0]=lps;
   VidyCeley[6].lbCeliGlis[0][0]=lbs[0];
   VidyCeley[6].lbCeliGlis[1][0]=lbs[1];
   VidyCeley[6].lbCeliGlis[2][0]=lbs[2];
   VidyCeley[6].lRazmerCeliGlis=RazmerCeli;
   VidyCeley[6].IsVyvodTextGlis=bTextGlis;
   VidyCeley[6].szTextGlis=RazmerTexta;
   VidyCeley[6].cTextGlis=Cvet;


   VidyCeley[2].lpCeliKurs[0][0]=lps;
   VidyCeley[2].lpCeliKurs[1][0]=lps;
   VidyCeley[2].lpCeliKurs[2][0]=lps;
   VidyCeley[2].lbCeliKurs[0][0]=lbs[0];
   VidyCeley[2].lbCeliKurs[1][0]=lbs[1];
   VidyCeley[2].lbCeliKurs[2][0]=lbs[2];
   VidyCeley[2].lRazmerCeliKurs=RazmerCeli;
   VidyCeley[2].IsVyvodTextKurs=bTextKurs;
   VidyCeley[2].szTextKurs=RazmerTexta;
   VidyCeley[2].cTextKurs=Cvet;



   VidyCeley[4].lpCeliGlis[0][0]=lps;
   VidyCeley[4].lpCeliGlis[1][0]=lps;
   VidyCeley[4].lpCeliGlis[2][0]=lps;
   VidyCeley[4].lbCeliGlis[0][0]=lbs[0];
   VidyCeley[4].lbCeliGlis[1][0]=lbs[1];
   VidyCeley[4].lbCeliGlis[2][0]=lbs[2];
   VidyCeley[4].lRazmerCeliGlis=RazmerCeli;
   VidyCeley[4].IsVyvodTextGlis=bTextGlis;
   VidyCeley[4].szTextGlis=RazmerTexta;
   VidyCeley[4].cTextGlis=Cvet;

//Теперь получим данные полученные по ПРЛФС и по МОС
//Установим параметры отсчетов
   LOAD_LOGPEN_FROM_INI("LOGPEN_LabelFull",
                        lps,PS_SOLID,1,1,RGB(0,0,0))

   LOAD_LOGBRUSH_FROM_INI("LOGBRUSH_LabelFull0",
                          lbs[0],BS_SOLID,RGB(0,80,160),0)


   LOAD_LOGBRUSH_FROM_INI("LOGBRUSH_LabelFull1",
                          lbs[1],BS_SOLID,RGB(200,100,0),0)


   LOAD_LOGBRUSH_FROM_INI("LOGBRUSH_LabelFull2",
                          lbs[2],BS_SOLID,RGB(200,200,100),0)

   LOAD_FROM_INI("SizeOfLabelFull",RazmerCeli,"6");    //Получим размер цели


   LOAD_BOOL_FROM_INI("VisibleTextForFullInCourse",bTextKurs,true)
   LOAD_BOOL_FROM_INI("VisibleTextForFullInGlissade",bTextGlis,true)
   LOAD_FROM_INI("SizeOfTextForFull",RazmerTexta,"18")
   LOAD_COLORREF_FROM_INI("ColorOfTextForFull",Cvet,RGB(0,80,160))

   VidyCeley[7].lpCeliKurs[0][0]=lps;
   VidyCeley[7].lpCeliKurs[1][0]=lps;
   VidyCeley[7].lpCeliKurs[2][0]=lps;
   VidyCeley[7].lbCeliKurs[0][0]=lbs[0];
   VidyCeley[7].lbCeliKurs[1][0]=lbs[1];
   VidyCeley[7].lbCeliKurs[2][0]=lbs[2];

   VidyCeley[7].lRazmerCeliKurs=RazmerCeli;
   VidyCeley[7].IsVyvodTextKurs=bTextKurs;
   VidyCeley[7].szTextKurs=RazmerTexta;
   VidyCeley[7].cTextKurs=Cvet;

   VidyCeley[7].lpCeliGlis[0][0]=lps;
   VidyCeley[7].lpCeliGlis[1][0]=lps;
   VidyCeley[7].lpCeliGlis[2][0]=lps;
   VidyCeley[7].lbCeliGlis[0][0]=lbs[0];
   VidyCeley[7].lbCeliGlis[1][0]=lbs[1];
   VidyCeley[7].lbCeliGlis[2][0]=lbs[2];
   VidyCeley[7].lRazmerCeliGlis=RazmerCeli;
   VidyCeley[7].IsVyvodTextGlis=bTextGlis;
   VidyCeley[7].szTextGlis=RazmerTexta;
   VidyCeley[7].cTextGlis=Cvet;



   VidyCeley[3].lpCeliKurs[0][0]=lps;
   VidyCeley[3].lpCeliKurs[1][0]=lps;
   VidyCeley[3].lpCeliKurs[2][0]=lps;
   VidyCeley[3].lbCeliKurs[0][0]=lbs[0];
   VidyCeley[3].lbCeliKurs[1][0]=lbs[1];
   VidyCeley[3].lbCeliKurs[2][0]=lbs[2];
   VidyCeley[3].lRazmerCeliKurs=RazmerCeli;
   VidyCeley[3].IsVyvodTextKurs=bTextKurs;
   VidyCeley[3].szTextKurs=RazmerTexta;
   VidyCeley[3].cTextKurs=Cvet;


   VidyCeley[5].lpCeliGlis[0][0]=lps;
   VidyCeley[5].lpCeliGlis[1][0]=lps;
   VidyCeley[5].lpCeliGlis[2][0]=lps;
   VidyCeley[5].lbCeliGlis[0][0]=lbs[0];
   VidyCeley[5].lbCeliGlis[1][0]=lbs[1];
   VidyCeley[5].lbCeliGlis[2][0]=lbs[2];
   VidyCeley[5].lRazmerCeliGlis=RazmerCeli;
   VidyCeley[5].IsVyvodTextGlis=bTextGlis;
   VidyCeley[5].szTextGlis=RazmerTexta;
   VidyCeley[5].cTextGlis=Cvet;

//Разберемся с отсчетом, который не видно
   LOAD_LOGPEN_FROM_INI("LOGPEN_LabelNoData",
                        lps,PS_SOLID,1,1,RGB(0,0,0))

   LOAD_LOGBRUSH_FROM_INI("LOGBRUSH_LabelNoData",
                          lbs[0],BS_SOLID,RGB(0,80,160),0)



   LOAD_FROM_INI("SizeOfLabelNoData",RazmerCeli,"6");    //Получим размер цели

   VidyCeley[2].lRazmerCeliGlis=
     VidyCeley[4].lRazmerCeliKurs=RazmerCeli;

   VidyCeley[4].lpCeliKurs[0][0]=lps;
   VidyCeley[4].lpCeliKurs[1][0]=lps;
   VidyCeley[4].lpCeliKurs[2][0]=lps;

   VidyCeley[4].lbCeliKurs[0][0]=lbs[0];
   VidyCeley[4].lbCeliKurs[1][0]=lbs[1];
   VidyCeley[4].lbCeliKurs[2][0]=lbs[2];


   VidyCeley[2].lpCeliGlis[0][0]=lps;
   VidyCeley[2].lpCeliGlis[1][0]=lps;
   VidyCeley[2].lpCeliGlis[2][0]=lps;

   VidyCeley[2].lbCeliGlis[0][0]=lbs[0];
   VidyCeley[2].lbCeliGlis[1][0]=lbs[1];
   VidyCeley[2].lbCeliGlis[2][0]=lbs[2];


//Разберемся с векторами скороторстей
   LOAD_BOOL_FROM_INI("VisibleSpeedVectorInCourse",
                      bVectKurs,true)

   LOAD_BOOL_FROM_INI("VisibleSpeedVectorInGlissade",
                      bVectGlis,true)

   LOAD_FROM_INI("SizeOfSpeedVector",RazmerCeli,"20")
   VidyCeley[1].IsVyvodVektorSkorostiKurs=bVectKurs;
   VidyCeley[1].IsVyvodVektorSkorostiGlis=bVectGlis;
   VidyCeley[1].lDlinaVektoraSkorosti=RazmerCeli;
   VidyCeley[2].IsVyvodVektorSkorostiKurs=bVectKurs;
   VidyCeley[2].IsVyvodVektorSkorostiGlis=false;
   VidyCeley[2].lDlinaVektoraSkorosti=RazmerCeli;
   VidyCeley[3].IsVyvodVektorSkorostiKurs=bVectKurs;
   VidyCeley[3].IsVyvodVektorSkorostiGlis=bVectGlis;
   VidyCeley[3].lDlinaVektoraSkorosti=RazmerCeli;
   VidyCeley[4].IsVyvodVektorSkorostiKurs=false;
   VidyCeley[4].IsVyvodVektorSkorostiGlis=bVectGlis;
   VidyCeley[4].lDlinaVektoraSkorosti=RazmerCeli;
   VidyCeley[5].IsVyvodVektorSkorostiKurs=bVectKurs;
   VidyCeley[5].IsVyvodVektorSkorostiGlis=bVectGlis;
   VidyCeley[5].lDlinaVektoraSkorosti=RazmerCeli;
   VidyCeley[6].IsVyvodVektorSkorostiKurs=bVectKurs;
   VidyCeley[6].IsVyvodVektorSkorostiGlis=bVectGlis;
   VidyCeley[6].lDlinaVektoraSkorosti=RazmerCeli;
   VidyCeley[7].IsVyvodVektorSkorostiKurs=bVectKurs;
   VidyCeley[7].IsVyvodVektorSkorostiGlis=bVectGlis;
   VidyCeley[7].lDlinaVektoraSkorosti=RazmerCeli;

//Загрузить данные о формулярах


   LOAD_FROM_INI("SizeOfFormulText",
                  szTextFormulyara,"16");

   LOAD_COLORREF_FROM_INI("ColorOfFormulText",
                 cTextFormulyara,RGB(0,0,0));


//Заполним параметры формуляра, что выводлить
   LOAD_FROM_INI("rfs_option1",tmp_arr[0],"1");
   LOAD_FROM_INI("rfs_option2",tmp_arr[1],"2");
   LOAD_FROM_INI("rfs_option3",tmp_arr[2],"3");
   LOAD_FROM_INI("rfs_option4",tmp_arr[3],"5");
   LOAD_FROM_INI("rfs_option5",tmp_arr[4],"0");
   LOAD_FROM_INI("rfs_option6",tmp_arr[5],"0");


   for(i=0;i<6;i++)
   {
       if(tmp_arr[i]==1)
       {
           rfs_options[i]=RFS_Nomer;
       }else
       if(tmp_arr[i]==2)
       {
           rfs_options[i]=RFS_VysM;
       }else
       if(tmp_arr[i]==3)
       {
           rfs_options[i]=RFS_SpeedGround;
       }else
       if(tmp_arr[i]==4)
       {
           rfs_options[i]=RFS_SpeedVertical;
       }else
       if(tmp_arr[i]==5)
       {
           rfs_options[i]=RFS_DalnostKM;
       }else{
           rfs_options[i]=RFS_None;
       }
   }


   LOAD_COLORREF_FROM_INI("cTextInfo",cTextInfo,RGB(0,0xEE,0))
   LOAD_FROM_INI("szTextInfo",szTextInfo,"12")
   LOAD_BOOL_FROM_INI("bBoldTextInfo",bBoldTextInfo,true)
   LOAD_STR_FROM_INI("sFamTextInfo",sFamTextInfo, QString(DEFAULT_FAMILY_FONT))



   for(i=0;i<=7;i++)
   {
       for(j=0;j<3;j++)
       {

           VidyCeley[i].lbCeliGlis[j][1]=VidyCeley[i].lbCeliGlis[j][0];
           r=Okrugl(VidyCeley[i].lbCeliGlis[j][0].color().red()/3.0);
           g=Okrugl(VidyCeley[i].lbCeliGlis[j][0].color().green()/3.0);
           b=Okrugl(VidyCeley[i].lbCeliGlis[j][0].color().blue()/3.0);
           VidyCeley[i].lbCeliGlis[j][1].setColor(QColor(r,g,b));

           VidyCeley[i].lbCeliKurs[j][1]=VidyCeley[i].lbCeliKurs[j][0];
           r=Okrugl(VidyCeley[i].lbCeliKurs[j][0].color().red()/3.0);
           g=Okrugl(VidyCeley[i].lbCeliKurs[j][0].color().green()/3.0);
           b=Okrugl(VidyCeley[i].lbCeliKurs[j][0].color().blue()/3.0);
           VidyCeley[i].lbCeliKurs[j][1].setColor(QColor(r,g,b));


           VidyCeley[i].lpCeliGlis[j][1]=VidyCeley[i].lpCeliGlis[j][0];
           r=Okrugl(VidyCeley[i].lpCeliGlis[j][0].color().red()/3.0);
           g=Okrugl(VidyCeley[i].lpCeliGlis[j][0].color().green()/3.0);
           b=Okrugl(VidyCeley[i].lpCeliGlis[j][0].color().blue()/3.0);
           VidyCeley[i].lpCeliGlis[j][1].setColor(QColor(r,g,b));

           VidyCeley[i].lpCeliKurs[j][1]=VidyCeley[i].lpCeliKurs[j][0];
           r=Okrugl(VidyCeley[i].lpCeliKurs[j][0].color().red()/3.0);
           g=Okrugl(VidyCeley[i].lpCeliKurs[j][0].color().green()/3.0);
           b=Okrugl(VidyCeley[i].lpCeliKurs[j][0].color().blue()/3.0);
           VidyCeley[i].lpCeliKurs[j][1].setColor(QColor(r,g,b));



       }
   }

   //Масштаб масимальный под дальности
   LOAD_FROM_INI("iMaxDalnost",max_dalnost,"60000");
   if(max_dalnost<10000)
   {
       UstanovitMaxVizDalnost(0);
   }else
   if(max_dalnost>80000)
   {
       UstanovitMaxVizDalnost(8);
   }else{
       UstanovitMaxVizDalnost(max_dalnost/10000);
   }







 //Скачать данные об аудио файле
   fileNameAlarmWAV=stng->value(QString("fileNameAlarmWAV"),QString("ahtung.wav")).toString();
   a_dir.setPath(QCoreApplication::applicationDirPath());
   fileNameAlarmWAV=a_dir.absoluteFilePath(fileNameAlarmWAV);
   fileNameAlarmWAV=QDir::cleanPath(fileNameAlarmWAV);



   delete stng;

   return 1;
}



//Сохранение файла в ини-файла
long TRZPDraw::ZapisatDannyeVIniFile(const char *IniFile)
{
   int tmp_arr[10];
   int max_dalnost;
   int i;
   QSettings *stng;


   TKursDraw::SaveDataToINI(IniFile);
   TGlissadaDraw::SaveDataToINI(IniFile);




   if(iTipDalnosti<=0)
   {
        max_dalnost=5000;
   }else
   if(iTipDalnosti>=8)
   {
       max_dalnost=80000;
   }else{
       max_dalnost=iTipDalnosti*10000;
   }

   stng=new QSettings(QString(IniFile),QSettings::IniFormat);


   SAVE_TO_INI("iMaxDalnost",max_dalnost)

   SAVE_LOGPEN_TO_INI("LOGPEN_LabelMOS",
                      VidyCeley[1].lpCeliKurs[0][0])

   SAVE_LOGBRUSH_TO_INI("LOGBRUSH_LabelMOS0",
                        VidyCeley[1].lbCeliKurs[0][0])

   SAVE_LOGBRUSH_TO_INI("LOGBRUSH_LabelMOS1",
                        VidyCeley[1].lbCeliKurs[1][0])

   SAVE_LOGBRUSH_TO_INI("LOGBRUSH_LabelMOS2",
                        VidyCeley[1].lbCeliKurs[2][0])

   SAVE_TO_INI("SizeOfLabelMOS",VidyCeley[1].lRazmerCeliKurs)

   SAVE_BOOL_TO_INI("VisibleTextForMOSInCourse",
                           VidyCeley[1].IsVyvodTextKurs)

   SAVE_BOOL_TO_INI("VisibleTextForMOSInGlissade",
                           VidyCeley[1].IsVyvodTextGlis)

   SAVE_TO_INI("SizeOfTextForMOS", VidyCeley[1].szTextKurs)

   SAVE_COLORREF_TO_INI("ColorOfTextForMOS",VidyCeley[1].cTextKurs)


   SAVE_LOGPEN_TO_INI("LOGPEN_LabelPRLFS",
                      VidyCeley[6].lpCeliKurs[0][0])

   SAVE_LOGBRUSH_TO_INI("LOGBRUSH_LabelPRLFS0",
                        VidyCeley[6].lbCeliKurs[0][0])

   SAVE_LOGBRUSH_TO_INI("LOGBRUSH_LabelPRLFS1",
                        VidyCeley[6].lbCeliKurs[1][0])

   SAVE_LOGBRUSH_TO_INI("LOGBRUSH_LabelPRLFS2",
                        VidyCeley[6].lbCeliKurs[2][0])

   SAVE_TO_INI("SizeOfLabelPRLFS",VidyCeley[6].lRazmerCeliKurs)

   SAVE_BOOL_TO_INI("VisibleTextForPRLFSInCourse",
                           VidyCeley[6].IsVyvodTextKurs)

   SAVE_BOOL_TO_INI("VisibleTextForPRLFSInGlissade",
                           VidyCeley[6].IsVyvodTextGlis)

   SAVE_TO_INI("SizeOfTextForPRLFS", VidyCeley[6].szTextKurs)

   SAVE_COLORREF_TO_INI("ColorOfTextForPRLFS",VidyCeley[6].cTextKurs)


   SAVE_LOGPEN_TO_INI("LOGPEN_LabelFull",
                      VidyCeley[7].lpCeliKurs[0][0])

   SAVE_LOGBRUSH_TO_INI("LOGBRUSH_LabelFull0",
                        VidyCeley[7].lbCeliKurs[0][0])

   SAVE_LOGBRUSH_TO_INI("LOGBRUSH_LabelFull1",
                        VidyCeley[7].lbCeliKurs[1][0])

   SAVE_LOGBRUSH_TO_INI("LOGBRUSH_LabelFull2",
                        VidyCeley[7].lbCeliKurs[2][0])

   SAVE_TO_INI("SizeOfLabelFull",VidyCeley[7].lRazmerCeliKurs)

   SAVE_BOOL_TO_INI("VisibleTextForFullInCourse",
                           VidyCeley[7].IsVyvodTextKurs)

   SAVE_BOOL_TO_INI("VisibleTextForFullInGlissade",
                           VidyCeley[7].IsVyvodTextGlis)

   SAVE_TO_INI("SizeOfTextForFull", VidyCeley[7].szTextKurs)

   SAVE_COLORREF_TO_INI("ColorOfTextForFull",VidyCeley[7].cTextKurs)

  //Разберемся с отсчетом, который не видно
           SAVE_LOGPEN_TO_INI("LOGPEN_LabelNoData", VidyCeley[4].lpCeliKurs[0][0])

   SAVE_LOGBRUSH_TO_INI("LOGBRUSH_LabelNoData",
                        VidyCeley[4].lbCeliKurs[0][0])


   SAVE_TO_INI("SizeOfLabelNoData",     //Получим размер цели
        VidyCeley[4].lRazmerCeliKurs)

 //Разберемся с векторами скороторстей
   SAVE_BOOL_TO_INI("VisibleSpeedVectorInCourse",
                      VidyCeley[7].IsVyvodVektorSkorostiKurs)

   SAVE_BOOL_TO_INI("VisibleSpeedVectorInGlissade",
                      VidyCeley[7].IsVyvodVektorSkorostiGlis)

   SAVE_TO_INI("SizeOfSpeedVector",VidyCeley[7].lDlinaVektoraSkorosti)


   //Сохранить данные о формулярах

   SAVE_TO_INI("SizeOfFormulText",
                  szTextFormulyara);

   SAVE_COLORREF_TO_INI("ColorOfFormulText",
                 cTextFormulyara)


   for(i=0;i<6;i++)
   {
        if(rfs_options[i]==RFS_Nomer)tmp_arr[i]=1;
        else
        if(rfs_options[i]==RFS_VysM)tmp_arr[i]=2;
        else
        if(rfs_options[i]==RFS_SpeedGround)tmp_arr[i]=3;
        else
        if(rfs_options[i]==RFS_SpeedVertical)tmp_arr[i]=4;
        else
        if(rfs_options[i]==RFS_DalnostKM)tmp_arr[i]=5;
        else tmp_arr[i]=0;
   }

   SAVE_TO_INI("rfs_option1",tmp_arr[0]);
   SAVE_TO_INI("rfs_option2",tmp_arr[1]);
   SAVE_TO_INI("rfs_option3",tmp_arr[2]);
   SAVE_TO_INI("rfs_option4",tmp_arr[3]);
   SAVE_TO_INI("rfs_option5",tmp_arr[4]);
   SAVE_TO_INI("rfs_option6",tmp_arr[5]);



   SAVE_COLORREF_TO_INI("cTextInfo",cTextInfo)
   SAVE_TO_INI("szTextInfo",szTextInfo)
   SAVE_BOOL_TO_INI("bBoldTextInfo",bBoldTextInfo)
   SAVE_STR_TO_INI("sFamTextInfo",sFamTextInfo)


//Сохраним направление
   SAVE_TO_INI("NapravlenieVPP",lNapravlenieVPP)



   delete stng;
   return 1;
}

//Промежуточная процедура - поиск элемента с таким же номером
//поиск элемента с номером в ldoc_tail
int TRZPDraw::addDataIntoLdoc_tail(const TDannyeObCeli &doc)
{
    list<TDannyeObCeli>::iterator it;
    if(!ldoc_tail.empty())
    {
        it=ldoc_tail.end();
        --it;
        while(1)
        {
            if(ttGetDTimeMS((*it).timeOfCel,doc.timeOfCel)>=0)
            {
                ++it;
                ldoc_tail.insert(it,doc);
                return 1;
            }
            if(it==ldoc_tail.begin())
            {
                ldoc_tail.insert(it,doc);
                return 1;
            }else{
                --it;
            }
        }
    }
    ldoc_tail.push_back(doc);

    return 1;
}

//Добавление данных в ldoc
int TRZPDraw::addDataIntoLdoc(const TDannyeObCeli &doc)
{
    ldoc.push_back(doc);
    return 1;
}



//Проредить по времени и    ldoc и ldoc_tail
int TRZPDraw::removeDataFromLdocAndLdoc_tail()
{
    unsigned int tstNomBorts[1024]; //  Тестирование номеров бортов
    list<TDannyeObCeli>::iterator it,it1;
    long lCurTime=ttGetMillySecondsCounts();
    bool bDeleted=false;

    memset(tstNomBorts,0,sizeof(tstNomBorts));
    it=ldoc.begin();
    while(it!=ldoc.end())
    {
        if((*it).lNomerVozdushnogoSudna>=0)
        {
//Зафиксируем присутствие отметки с данным номером борта
            tstNomBorts[(*it).lNomerVozdushnogoSudna%1024]=1;
        }

        if(ttGetDTimeMS((*it).timeForDie1_ms,lCurTime)>0)
        {
          it1=it;
          ++it;
          if(((*it1).timeForDie2_ms>=0)&&
             (ttGetDTimeMS((*it1).timeForDie2_ms,
                          lCurTime)<0))
          {
              addDataIntoLdoc_tail(*it1);
          }
          ldoc.erase(it1);
          if(isNeedPereraschet==0)isNeedPereraschet=1;
          continue;
        }
        ++it;
    }

//Удаление объекта из хвоста
    if(!(ldoc_tail.empty()))
    {
        it=ldoc_tail.end();
        --it;

        while(1)
        {
            bDeleted=false;

            if(ttGetDTimeMS((*it).timeForDie2_ms,lCurTime)>0)
            {
                bDeleted=true;
            }

            TDannyeObCeli debugDOC;
            if((*it).lNomerVozdushnogoSudna>=0)
            {
                if(tstNomBorts[(*it).lNomerVozdushnogoSudna%1024]==0)
                {
                    debugDOC=(*it);

                    if(ttGetDTimeMS((*it).timeOfCel, lCurTime)>=
                     (*it).timeOfLifeWithOut_ms)
                    {
                        bDeleted=true;
                        tstNomBorts[(*it).lNomerVozdushnogoSudna%1024]=2;
                    }else{
                        tstNomBorts[(*it).lNomerVozdushnogoSudna%1024]=3;
                    }
                }else
                if(tstNomBorts[(*it).lNomerVozdushnogoSudna%1024]==2)
                {
                    bDeleted=true;
                }
            }

//Удалим теперь отметку из списка
            if(bDeleted)
            {
            it1=it;
            if(it==ldoc_tail.begin())
            {
                  ldoc_tail.erase(it1);
                if(isNeedPereraschet==0)isNeedPereraschet=1;
                break;
            }else{
                  --it;
                ldoc_tail.erase(it1);
                if(isNeedPereraschet==0)isNeedPereraschet=1;
                continue;
            }
            }
            if(it==ldoc_tail.begin())
            {
                break;
            }
            --it;
        }
    }
    return 1;
}



//Добавить новый элемент
//Возвращает 1, если все успешно
//2 - если уже введен элемент.
//0, если не правильный номер >10 или <1
long TRZPDraw::DobavitNovuyuCelProtected(const TDannyeObCeli &NovayCel)
{

  int fnd=0;

  TDannyeObCeli doc,NovayCel1;

  list<TDannyeObCeli>::iterator it;

  NovayCel1=NovayCel;

  if(NovayCel1.timeOfCel<0)
  {
    NovayCel1.timeOfCel=ttGetMillySecondsCounts();
  }
  NovayCel1.timeForDie1_ms=ttGetTimePlusDTime(NovayCel1.timeOfCel,NovayCel1.timeOfLife1_ms);
  NovayCel1.timeForDie2_ms=ttGetTimePlusDTime(NovayCel1.timeOfCel,NovayCel1.timeOfLife2_ms);


  if(NovayCel1.lNomerVozdushnogoSudna>0)
  {
        it=ldoc.begin();
        while(it!=ldoc.end())
        {
            if((*it).lNomerVozdushnogoSudna==NovayCel1.lNomerVozdushnogoSudna)
            {
                doc=(*it);
                ldoc.erase(it);
                addDataIntoLdoc_tail(doc);
                ldoc.push_back(NovayCel1);
                fnd=1;
                if(isNeedPereraschet==0)isNeedPereraschet=1;
                break;
            }
            ++it;
        }
  }

  if(!fnd)
  {
      ldoc.push_back(NovayCel1);
      if(isNeedPereraschet==0)isNeedPereraschet=1;
  }
  return 1;
}

long TRZPDraw::DobavitNovuyuCel(const TDannyeObCeli &NovayCel)
{
    TDannyeObCeli NovayCel1;
    mutex_dobav_cel.lock();
    NovayCel1=NovayCel;
    NovayCel1.timeOfCel=ttGetMillySecondsCounts();
    ldoc_new.push_back(NovayCel1);
    mutex_dobav_cel.unlock();
    return 1;
}



//Создание строки для соответствующей цели
QString TRZPDraw::getStringInfo()
{
    QString ret_str=QString("");

    list<TDannyeObCeli>::iterator it;

    //Массив отметок, которые описаны в модуле OtmetkaDannye
    list<TDannyeObCeli> sort_ldoc;


    ret_str=ret_str+QString("ДАТА: ")+QDateTime::currentDateTime().toString(QString("dd.MM.yyyy"))+
            QString("\n");

    ret_str=ret_str+QString("ВРЕМЯ: ")+QDateTime::currentDateTime().toString(QString("hh:mm:ss"))+
            QString("\n");


    sort_ldoc=ldoc;

    sort_ldoc.sort(odCompareByNomerVozdSudna);

    it=sort_ldoc.begin();
    while(it!=sort_ldoc.end())
    {
        ret_str=ret_str+(*it).getStringInfo()+QString("\n");
        it++;
    }


    return ret_str;
}



void TRZPDraw::testParametersWithHazard(const TDannyeObCeli &doc)
{
    double ugl, diap_ugl,diap_ugl1,diap_ugl2;
//Проверим в поле курса
    if(doc.Y>-10000)
    {
//Определим угол
        ugl=atan2(doc.Y,doc.X)/M_PI*180.0;
        PoluchitVizDiapazonUglKurs(&diap_ugl);
        if(diap_ugl<(fabs(ugl)+1))
        {
            if(dfHazardMaxUgolKurs<(fabs(ugl)+1.0))
            {
                dfHazardMaxUgolKurs=fabs(ugl)+1.0;
            }
        }
    }

//Проверим в поле глиссады
    if(doc.Z>-10000)
    {
        ugl=atan2(doc.Z,doc.X)/M_PI*180.0;
        PoluchitVizDiapazonGlis(&diap_ugl1,&diap_ugl2);
        if(((ugl-1.0)<diap_ugl1)||((ugl+1.0)>diap_ugl2))
        {
            if((ugl-1.0)<dfHazardMinUgolGlis)
            {
                dfHazardMinUgolGlis=ugl-1.0;
            }

            if((ugl+1.0)>dfHazardMaxUgolGlis)
            {
                dfHazardMaxUgolGlis=ugl+1.0;
            }
        }

    }

//Теперь проверим по дальности
    if(doc.X>PoluchitMaxVizDalnost())
    {
        if(doc.X>lHazardMaxDalnost)
        {
            lHazardMaxDalnost=doc.X;
        }
    }



}


//Следующая процедура устанавливает новый масштаб, если были отметки с признаками аварии, не попадающие
//  в диапазон. Возвращает 0, если масштаб не  изменился. 1 - если изменился
int  TRZPDraw::trySetNewMasshtabWithHazard()
{
    double diap_ugl, diap_ugl1, diap_ugl2;
    int isChange=0,isChange1=0;
    long curD,D;
    int TipDalnosti;
    PoluchitVizDiapazonUglKurs(&diap_ugl);
    if(dfHazardMaxUgolKurs>diap_ugl)
    {
        TKursDraw::NovyyMasshtabAzimuta(dfHazardMaxUgolKurs);
        isChange=1;
    }

    PoluchitVizDiapazonGlis(&diap_ugl1,&diap_ugl2);
    if(dfHazardMinUgolGlis<diap_ugl1)
    {
        diap_ugl1=dfHazardMinUgolGlis;
        isChange1=1;
    }

    if(dfHazardMaxUgolGlis>diap_ugl2)
    {
        diap_ugl2=dfHazardMaxUgolGlis;
        isChange1=1;
    }

    if(isChange1)
    {
        TGlissadaDraw::NovyyMasshtabPoVertikali(diap_ugl1,diap_ugl2);
        isChange=1;
    }
    curD=PoluchitMaxVizDalnost();
    if(lHazardMaxDalnost>curD)
    {
        TipDalnosti=lHazardMaxDalnost/10000+1;
        if (TipDalnosti>8)TipDalnosti=8;
        D=TipDalnosti*10000;
        TKursDraw::NovyyMasshtabDalnosti(D);  //Установим дальность
        TGlissadaDraw::NovyyMasshtabDalnosti(D);
        iTipDalnosti=TipDalnosti;
        isChange=1;
    }

    if(isChange)
    {
        RisovatSetku();
        isNeedPereraschet=2;
        return 1;
    }
    return 0;
}


/*Очистить всю информацию об отметках, которые только были добавлены*/
int  TRZPDraw::clearAllData()
{

}



//Рисовать сетку в imgSkreen
long TRZPDraw::RisovatSetku(void)
{
  QPainter pntr;
  QPen lp(QColor(0,0,0));
  QBrush lb(QColor(0,0,0));
  if(!imgSetka)
  {
      return 0;
  }

  lp.setStyle(Qt::SolidLine);

   pntr.begin(imgSetka);
   pntr.setPen(lp);
   pntr.setBrush(lb);

//Зарисуем все, что было до этого
  pntr.drawRect(0,0,RectDraw.width(), RectDraw.height());


//Теперь нарисуем и сетку глиссады и сетку курса
  PaintKurs(pntr,RectKurs);
  PaintGlis(pntr, RectGlissada);
  pntr.end();

  return 1;
}








//Добавить новый элемент
//Возвращает 1, если все успешно
//2 - если успешно но пришлось вытолкнуть элемент, так как элементов  200
//0, если не успешно
long TRZPDraw::RaschetKoordinatXZYIOcenkaPar(
                            bool isTail     //Для хвоста тоже расчитывать
                                            )
{
  long i,j,k;  //Индексы для счетстчиков

  long Ret;
  long lROtTorcaVPPDoCentraAntKurNapr1,
       lROtOsiVPPDoCentraAntKursa1,
       lROtTochkiPrizDoTorcaVPP1;



  list<TDannyeObCeli>::iterator it, it1;


  if(!imgSetka||!imgBack)
  {
      return (long)0;
  }


  if(lNapravlenieVPP)
  {
    lROtTorcaVPPDoCentraAntKurNapr1=lROtTorcaVPPDoCentraAntKurNapr[1];
    lROtOsiVPPDoCentraAntKursa1=-lROtOsiVPPDoCentraAntKursa;
    lROtTochkiPrizDoTorcaVPP1=lROtTochkiPrizDoTorcaVPP[1];
  }else{
    lROtTorcaVPPDoCentraAntKurNapr1=lROtTorcaVPPDoCentraAntKurNapr[0];
    lROtOsiVPPDoCentraAntKursa1=lROtOsiVPPDoCentraAntKursa;
    lROtTochkiPrizDoTorcaVPP1=lROtTochkiPrizDoTorcaVPP[0];
  }


  for(it=ldoc.begin();it!=ldoc.end();++it)
  {
      it1=it;
      ++it1;
      for(;it1!=ldoc.end();++it1)
      {
          Ret=(*it).ControlForKrit_AvariaTwo(&(*it1),dTimeForRmin, dRmin);
          if((Ret&0xFF00)==0x0200||(Ret&0x00FF)==0x0002)
          {
             lHazardTwoBorts=2;
             testParametersWithHazard((*it));
             testParametersWithHazard((*it1));
             bHazardSituation=true;
             continue;
          }
          if(((Ret&0xFF00)==0x0100||(Ret&0x00FF)==0x0001)&&
              lHazardTwoBorts<1)
          {
             lHazardTwoBorts=1;
             testParametersWithHazard((*it));
             testParametersWithHazard((*it1));
             bHazardSituation=true;
             continue;
          }
      }
      Ret=(*it).ControlForKrit_AvariaKurs(
                                 lROtTorcaVPPDoCentraAntKurNapr1,
               lROtTorcaVPPDoCentraAntKurNapr1-lROtTochkiPrizDoTorcaVPP1,
                                 lROtOsiVPPDoCentraAntKursa1,
                                 lUdalenie,
                                 tan(dUgolKursa[lNapravlenieVPP]),
                                 tan((dUgolKursa[lNapravlenieVPP]-dOtklonPoKursuMinus)/180.0*M_PI),
                                 tan((dUgolKursa[lNapravlenieVPP]+dOtklonPoKursuPlus)/180.0*M_PI),
                                 tan((dUgolKursa[lNapravlenieVPP]-dOtklonPoKursuMinus/3.0)/180.0*M_PI),
                                 tan((dUgolKursa[lNapravlenieVPP]+dOtklonPoKursuPlus/3.0)/180.0*M_PI));

      if(Ret==1)
      {
          lHazardKurs=1;
          testParametersWithHazard((*it));
          bHazardSituation=true;
      }
      Ret=(*it).ControlForKrit_AvariaGlis(
                                 lROtTorcaVPPDoCentraAntKurNapr1,
               lROtTorcaVPPDoCentraAntKurNapr1-lROtTochkiPrizDoTorcaVPP1,
                                 lRaznicaVPPIAntena,
                                 lUdalenie,
                                 tan(dUgolGlissady[lNapravlenieVPP]/180.0*M_PI),
                                 tan((dUgolGlissady[lNapravlenieVPP]-dOtklonPoGlisMinus)/180.0*M_PI),
                                 tan((dUgolGlissady[lNapravlenieVPP]+dOtklonPoGlisPlus)/180.0*M_PI),
                                 tan((dUgolGlissady[lNapravlenieVPP]-dOtklonPoGlisMinus/3.0)/180.0*M_PI),
                                 tan((dUgolGlissady[lNapravlenieVPP]+dOtklonPoGlisPlus/3.0)/180.0*M_PI));
      if(Ret)
      {
          lHazardGlis=1;
          testParametersWithHazard((*it));
          bHazardSituation=true;
      }
  }

//Тут же оценим попарно отсчеты на попадание в область, близкую друг от друга
/*Пока закоментируем
  for(i=k;i<szDannyeObCelyah;i++)
  {
     for(j=i+1;j<szDannyeObCelyah;j++)
     {
        Ret=DannyeObCelyah[i].ControlForKrit_AvariaTwo(
            DannyeObCelyah+j, dTimeForRmin, dRmin);
//Проверим, а не было ли где-то критической ситуации
        if((Ret&0xFF00)==0x0200||(Ret&0x00FF)==0x0002)
        {
           lHazardTwoBorts=2;
           continue;
        }
        if(((Ret&0xFF00)==0x0100||(Ret&0x00FF)==0x0001)&&
            lHazardTwoBorts<1)
        {
           lHazardTwoBorts=1;
           continue;
        }
     }
//Тут же осуществим проверки отклонений по глиссаде и курсу
     Ret=DannyeObCelyah[i].ControlForKrit_AvariaKurs(
                                lROtTorcaVPPDoCentraAntKurNapr1,
              lROtTorcaVPPDoCentraAntKurNapr1-lROtTochkiPrizDoTorcaVPP1,
                                lROtOsiVPPDoCentraAntKursa1,
                                lUdalenie,
                                0.0,
                                tan((-dOtklonPoKursuMinus)/180.0*M_PI),
                                tan((dOtklonPoKursuPlus)/180.0*M_PI),
                                tan((-dOtklonPoKursuMinus/3.0)/180.0*M_PI),
                                tan((dOtklonPoKursuPlus/3.0)/180.0*M_PI));

     if(Ret==1)lHazardKurs=1;

     Ret=DannyeObCelyah[i].ControlForKrit_AvariaGlis(
                                lROtTorcaVPPDoCentraAntKurNapr1,
              lROtTorcaVPPDoCentraAntKurNapr1-lROtTochkiPrizDoTorcaVPP1,                                
                                lRaznicaVPPIAntena,
                                lUdalenie,
                                tan(dUgolGlissady[lNapravlenieVPP]/180.0*M_PI),
                                tan((dUgolGlissady[lNapravlenieVPP]-dOtklonPoGlisMinus)/180.0*M_PI),
                                tan((dUgolGlissady[lNapravlenieVPP]+dOtklonPoGlisPlus)/180.0*M_PI),
                                tan((dUgolGlissady[lNapravlenieVPP]-dOtklonPoGlisMinus/3.0)/180.0*M_PI),
                                tan((dUgolGlissady[lNapravlenieVPP]+dOtklonPoGlisPlus/3.0)/180.0*M_PI));
     if(Ret)lHazardGlis=1;
  }
  */
  return 1;
}


//Вывести отсчеты в поле курса
long TRZPDraw::VyvestiOtschetyVPolyaKursaIGlissady(void)
{
  QPainter pntr;
  int Ret;
  long i,nform;
  TFormulyar Formulyar;
  QPen Lp;
  QBrush Lb;
  char **TextStroka;
  double Skor,Skor1;
  double Daln;
  unsigned char r,g,b;


  QRect Rect;   //Возвращаемые координаты прямоугольника
  list<TDannyeObCeli>::iterator it;


  QFont oldFont, font;
  QPen oldPen;
  Qt::BGMode oldBkMode;


  QString info_str;

  if(!imgSetka||!imgBack)
  {
      return (long)0;
  }



  TextStroka=new char*[5];
  for(i=0;i<5;i++)
  {
      TextStroka[i]=new char[30];
  }


  pntr.begin(imgBack);
//Выведем отсчеты на отсчеты на DCBack
//Выведем данные курса
//Нарисовать цели

  Ret=TKursDraw::RisovatCeliKurs(pntr,
                                 RectKurs,ldoc,ldoc_tail,VidyCeley,8);




  if(!Ret)
  {
//Подумать над ошибкой
  }


  Ret=TGlissadaDraw::RisovatCeliGlis(pntr,
                                     RectGlissada,ldoc,ldoc_tail,VidyCeley,8);

  if(Ret)
  {
//Подумать, что сделать с ощибкой
  }



  Lp.setStyle(Qt::SolidLine);
  Lp.setWidth(1);
  Lb.setStyle(Qt::SolidPattern);

  it=ldoc.begin();



  int isWasOutDebug=0;

  while(it!=ldoc.end())
  {
      TDannyeObCeli debug_doc;
      debug_doc=(*it);

      if((*it).X>=lROtTorcaVPPDoCentraAntKurNapr[lNapravlenieVPP]+
          PoluchitMaxVizDalnost())
      {
          it++;
          continue;
      }
      r=b=g=0;

//Заполним формуляр
      if((((*it).lHazardTwo&0x0202)||
         (*it).lHazardKurs)&&
         getFlashStateGlis())
      {
        r|=(unsigned char)(VidyCeley[(*it).Tip].lbCeliKurs[2][0].color().red());
        g|=(unsigned char)(VidyCeley[(*it).Tip].lbCeliKurs[2][0].color().green());
        b|=(unsigned char)(VidyCeley[(*it).Tip].lbCeliKurs[2][0].color().blue());
      }
      else if((((*it).lHazardTwo)&0x0101)&&
               getFlashStateGlis())
      {
        r|=(unsigned char)(VidyCeley[(*it).Tip].lbCeliKurs[1][0].color().red());
        g|=(unsigned char)(VidyCeley[(*it).Tip].lbCeliKurs[1][0].color().green());
        b|=(unsigned char)(VidyCeley[(*it).Tip].lbCeliKurs[1][0].color().blue());
      }else{
        r|=(unsigned char)(VidyCeley[(*it).Tip].lbCeliKurs[0][0].color().red());
        g|=(unsigned char)(VidyCeley[(*it).Tip].lbCeliKurs[0][0].color().green());
        b|=(unsigned char)(VidyCeley[(*it).Tip].lbCeliKurs[0][0].color().blue());
      }


      if((((*it).lHazardTwo&0x0202)||
         (*it).lHazardGlis)&&
         getFlashStateGlis())
      {        
        r|=(unsigned char)(VidyCeley[(*it).Tip].lbCeliGlis[2][0].color().red());
        g|=(unsigned char)(VidyCeley[(*it).Tip].lbCeliGlis[2][0].color().green());
        b|=(unsigned char)(VidyCeley[(*it).Tip].lbCeliGlis[2][0].color().blue());
      }
      else if(((*it).lHazardTwo&0101)&&
              getFlashStateGlis())
      {
        r|=(unsigned char)(VidyCeley[(*it).Tip].lbCeliGlis[1][0].color().red());
        g|=(unsigned char)(VidyCeley[(*it).Tip].lbCeliGlis[1][0].color().green());
        b|=(unsigned char)(VidyCeley[(*it).Tip].lbCeliGlis[1][0].color().blue());
      }else{
        r|=(unsigned char)(VidyCeley[(*it).Tip].lbCeliGlis[0][0].color().red());
        g|=(unsigned char)(VidyCeley[(*it).Tip].lbCeliGlis[0][0].color().green());
        b|=(unsigned char)(VidyCeley[(*it).Tip].lbCeliGlis[0][0].color().blue());
      }

      Lp.setColor(QColor((int)r, (int)g, (int)b));
      Lb.setColor(QColor((int)r/4, (int)g/4, (int)b/4));


//Пока скопирем позывной
      nform=0;
      for(i=0;i<sizeof(rfs_options)/sizeof(rfs_options[0]);i++)
      {

            if(rfs_options[i]==RFS_Nomer)
            {

                if((*it).sPozyvnoy[0]!=0)
                {
                    sfSTRCPY(TextStroka[nform],(*it).sPozyvnoy);
                    nform++;
                }
            }else
            if(rfs_options[i]==RFS_VysM)
            {
//Введем высоту
                if((*it).Z>0)
                {
                    sfSPRINTF(TextStroka[nform],"H%04d",(*it).Z);
                }else{
                    sfSPRINTF(TextStroka[nform],"H");
                }
                nform++;
            }else
            if(rfs_options[i]==RFS_SpeedGround)
            {
//Выведем значение скоросто
                if((*it).Tip==6||(*it).Tip==7)
                {
                    Skor1=(*it).Vx/1000.0*3600.0;
                    Skor=Skor1*Skor1;
                    Skor1=(*it).Vy/1000.0*3600.0;
                    Skor+=Skor1*Skor1;
                    Skor1=(*it).Vz/1000.0*3600.0;
                    Skor+=Skor1*Skor1;
                    Skor=sqrt(Skor);  //Расчитаем скорость и выведем ее
                    sfSPRINTF(TextStroka[nform],"S%04.0lf",Skor);
                }else{
                    sfSPRINTF(TextStroka[nform],"S");
                }
                nform++;
            }else
            if(rfs_options[i]==RFS_DalnostKM)
            {
//Расчитаем дальность
                Daln=0;
                if((*it).X>-100000)
                {
                    Daln+=(*it).X/1000.0*(*it).X/1000.0;
                }

                if((*it).Y>-100000)
                {
                    Daln+=(*it).Y/1000.0*(*it).Y/1000.0;
                }

                if((*it).Z>-100000)
                {
                    Daln+=(*it).Z/1000.0*(*it).Z/1000.0;
                }
                Daln=sqrt(Daln);

//Выведем строку
                sfSPRINTF(TextStroka[nform],"D%5.1lf",Daln);
                nform++;
            }
      }

            Formulyar.UstanovitParametry(
  (*it).XvpKurs,  (*it).YvpKurs,
  (*it).XvpGlis,  (*it).YvpGlis,
         TextStroka,nform, Lp, Lb,szTextFormulyara,cTextFormulyara,
         &Rect );

      Formulyar.Vyvod(pntr);

      isWasOutDebug=1;

      (*it).FormulyarRect=Rect;
      ++it;
  }


  for(i=0;i<5;i++)
  {
      delete[] TextStroka[i];
  }
  delete[] TextStroka;

//Теперь выведем в области информации
  font=createFontFromFamalyAndSize(sFamTextInfo,szTextInfo,bBoldTextInfo);
  oldBkMode=pntr.backgroundMode(); pntr.setBackgroundMode(Qt::TransparentMode);
  oldFont=pntr.font();  pntr.setFont(font);
  oldPen=pntr.pen(); pntr.setPen(Qt::SolidLine); pntr.setPen(cTextInfo);
  info_str=getStringInfo();
  pntr.drawText(RectInfoText,Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,info_str);

  pntr.setBackgroundMode(oldBkMode);
  pntr.setPen(oldPen);
  pntr.setFont(oldFont);

  pntr.end();

  return 1;
}




/*Методы, которые реализуют действия текущего оператора,
не требуюшие прав администратора*/

long TRZPDraw::UstanovitMaxVizDalnost(long TipDalnost)  //Установка максимальной отображаемой дальности дальности
                                       //TipDalnost=0, -> 5000 м
                                       //TipDalnost=1, -> 10000 м

//TipDalnost=2, -> 20000 м
                                       //3 - > 30000 м
                                       //4 -> 40000 м
                                       //5 -> 50000 м
                                       //6 -> 60000
                                       //7 -> 70000
                                       //8 -> 80000
{
  long D;
  if (TipDalnost>8 || TipDalnost<0)return 0;

  if(TipDalnost==0)
  {
      D=5000;
  }else{
      D=TipDalnost*10000;
  }

  TKursDraw::NovyyMasshtabDalnosti(D);  //Установим дальность
  TGlissadaDraw::NovyyMasshtabDalnosti(D);

  RisovatSetku();
  isNeedPereraschet=2;

  iTipDalnosti=TipDalnost;
  return 1;
}


 //Установить массштаб по вертикали Glissady
 //Должны лежать в прелеоах угла
//Возвращает 1, если успешно установлено.
//0 если нет
//Максимально допустимое
long TRZPDraw::UstanovitVizDiapazonUglKurs(
                       double dUgol
                       )
{


  NovyyMasshtabAzimuta(dUgol);


 //Перерисуем полностью всю сетку
  RisovatSetku();
  isNeedPereraschet=2;
  return 1;
}

//Установить масштаб по вертикали в поле глиссады
//Возращает 1, если нормально
//0, если нет
long TRZPDraw::UstanovitVizDiapazonGlis(
                                 double Ugol1,
                                 double Ugol2)
{
  double DopUgol1,DopUgol2;
  if(Ugol1>0|| Ugol2<0)return 0;  //Не удача
  DopUgol1=dUgolGlissady[lNapravlenieVPP]-dOtklonPoGlisMinus-
      dOtklonPoGlisMinus/3.0;
  DopUgol2=dUgolGlissady[lNapravlenieVPP]+dOtklonPoGlisPlus+
      dOtklonPoGlisPlus/3.0;
  DopUgol1=MIN(DopUgol1,Ugol1);
  DopUgol2=MAX(DopUgol2,Ugol2);



  NovyyMasshtabPoVertikali(DopUgol1,DopUgol2);

//Перерисуем полностью всю сетку
  RisovatSetku();
  isNeedPereraschet=2;
  return 1;
}



long TRZPDraw::UstanovitVisibleSetkaKurs(
                              bool VisAzmt,
                              bool VisAzmtText,
                              bool VisUdal,
                              bool VisUdalText)
{
   if(VisAzmt)
   {
     TKursDraw::bVyvodAzmt=true;
     TKursDraw::bVyvodAzmtText=VisAzmtText;
   }else{
     TKursDraw::bVyvodAzmt=false;
     TKursDraw::bVyvodAzmtText=false;
   }
   if(VisUdal)
   {
     TKursDraw::bVyvodGorUdal=true;
     TKursDraw::bVyvodGorUdalText=VisUdalText;
   }else{
     TKursDraw::bVyvodGorUdal=false;
     TKursDraw::bVyvodGorUdalText=false;
   }
   RisovatSetku();
   return 1;
}

//Установить свойства отображения сетки глиссады
long TRZPDraw::UstanovitVisibleSetkaGlis(bool VisUgol,
                                  bool VisUgolText,
                                  bool VisUdal,
                                  bool VisUdalText
                                 )
{
  if(VisUgol)
   {
     TGlissadaDraw::bVyvodUglomer=true;
     TGlissadaDraw::bVyvodUglomerText=VisUgolText;
   }else{
     TGlissadaDraw::bVyvodUglomer=false;
     TGlissadaDraw::bVyvodUglomerText=false;
   }
   if(VisUdal)
   {
     TGlissadaDraw::bVyvodLiniiVertUdal=true;
     TGlissadaDraw::bVyvodTextVertUdal=VisUdalText;
   }else{
     TGlissadaDraw::bVyvodLiniiVertUdal=false;
     TGlissadaDraw::bVyvodTextVertUdal=false;
   }
   RisovatSetku();
   return 1;
}


//Отображать линии равных высот
long TRZPDraw::UstanovitVisibleLiniiRavnyhVysot(
                                bool VyvodLinii,
                                bool VyvodLiniiText)
{
  if(VyvodLinii)
  {
    TGlissadaDraw::bVyvodRavnyhVysot=true;
    TGlissadaDraw::bVyvodTextRavnyhVysot=VyvodLiniiText;
  }else{
    TGlissadaDraw::bVyvodRavnyhVysot=false;

    TGlissadaDraw::bVyvodTextRavnyhVysot=false;
  }
   RisovatSetku();
  return 1;
}



//Изменить размер и положение отображаемой области
long TRZPDraw::UstanovitOblastVyvoda(const QRect &rNewOblst)
{
  long Ret;
//Проверим. 1) Если ничего не поменялось, то и не меняем
   if(rNewOblst==RectDraw)
   {
       Ret=0;
   }else{
       Ret=1;
   }

   if(Ret==0)return 1; //Нечего менять
   if(rNewOblst.left()>=rNewOblst.right()||
      rNewOblst.top()>=rNewOblst.bottom())return 0; //Какая-то ошибка

//2) Только смещена но размеры не поменялись      
   if(rNewOblst.right()-rNewOblst.left()==
      RectDraw.right()-RectDraw.left()&&
      rNewOblst.bottom()-rNewOblst.top()==
      RectDraw.bottom()-RectDraw.top())
   {
//Выйдем и посчитаем новое

      RectDraw=rNewOblst;
      return 1;
   }
      RectDraw=rNewOblst;
      if(RectDraw.width()<=0||RectDraw.height()<=0||RectDraw.x()<0||RectDraw.y()<0)
      {
          //Зададим размеры области глиссады, исходя из данныъ rdGlissada
            RectGlissada.setLeft(0);
            RectGlissada.setRight(0);
            RectGlissada.setTop(0);
            RectGlissada.setBottom(0);

          //Зададим размеры области курса, исходя из данныъ rdKurs
            RectKurs.setLeft(0);
            RectKurs.setRight(0);
            RectKurs.setTop(0);
            RectKurs.setBottom(0);
            if(imgSetka)delete imgSetka;
            if(imgBack)delete imgBack;
            imgSetka=NULL;
            imgBack=NULL;
      }else{

    //Зададим размеры области глиссады, исходя из данныъ rdGlissada
            RectGlissada.setLeft(Okrugl(rdGlissada.left*(RectDraw.right()-RectDraw.left()+1)));
            RectGlissada.setRight(Okrugl(rdGlissada.right*(RectDraw.right()-RectDraw.left()+1)));
            RectGlissada.setTop(Okrugl(rdGlissada.top*(RectDraw.bottom()-RectDraw.top()+1)));
            RectGlissada.setBottom(Okrugl(rdGlissada.bottom*(RectDraw.bottom()-RectDraw.top()+1)));

    //Зададим размеры области курса, исходя из данныъ rdKurs
            RectKurs.setLeft(Okrugl(rdKurs.left*(RectDraw.right()-RectDraw.left()+1)));
            RectKurs.setRight(Okrugl(rdKurs.right*(RectDraw.right()-RectDraw.left()+1)));
            RectKurs.setTop(Okrugl(rdKurs.top*(RectDraw.bottom()-RectDraw.top()+1)));
            RectKurs.setBottom(Okrugl(rdKurs.bottom*(RectDraw.bottom()-RectDraw.top()+1)));


            RectInfoText.setLeft(Okrugl(rdInfoText.left*(RectDraw.right()-RectDraw.left()+1)));
            RectInfoText.setRight(Okrugl(rdInfoText.right*(RectDraw.right()-RectDraw.left()+1)));
            RectInfoText.setTop(Okrugl(rdInfoText.top*(RectDraw.bottom()-RectDraw.top()+1)));
            RectInfoText.setBottom(Okrugl(rdInfoText.bottom*(RectDraw.bottom()-RectDraw.top()+1)));



            if(imgSetka)delete imgSetka;
            if(imgBack)delete imgBack;


      //Теперь создадим совместимый с ним
            imgSetka=new QImage(RectDraw.width(),RectDraw.height(),QImage::Format_RGB32);

      //Теперь зададим размеры
            imgBack=new QImage(RectDraw.width(),RectDraw.height(),QImage::Format_RGB32);
    }




//Выведем тут все на экран
   RisovatSetku();
   isNeedPereraschet=2;
   return 1;
}



long TRZPDraw::DataToLogFile(char *Strka,bool MustOutData)
{
  FILE *fp;
  QDateTime SysTime;
  char Stroka[30];

//Получим системное время
  SysTime=QDateTime::currentDateTime();

  if(MustOutData)
  {
     sfSPRINTF(Stroka,"%02d//%02d//%04d  %02d:%02d:%02d  ",
           SysTime.date().day(),SysTime.date().month(),SysTime.date().year(),
           SysTime.time().hour(),SysTime.time().minute(),SysTime.time().second());
  }
  csLogFile.lock();
  fp=fopen(LogFile,"a+");
  if(!fp)
  {
    csLogFile.unlock();
    return 0;
  }
  if(MustOutData)fputs(Stroka,fp);

  fputs(Strka,fp);
  fputs("\n",fp);
  fclose(fp);
  csLogFile.unlock();
  return 1;
}


long TRZPDraw::DataToLogFileError(const char *Strka,bool MustOutData)
{
  FILE *fp;
  QDateTime SysTime;
  char Stroka[30];


//Получим системное время
   SysTime=QDateTime::currentDateTime();


  if(MustOutData)
  {
     sfSPRINTF(Stroka,"%02d//%02d//%04d  %02d:%02d:%02d  ",
           SysTime.date().day(),SysTime.date().month(),SysTime.date().year(),
           SysTime.time().hour(),SysTime.time().minute(),SysTime.time().second());
  }
  csLogFileError.lock();
  fp=fopen(LogFile,"a+");
  if(!fp)
  {
    csLogFileError.unlock();
    return 0;
  }
  if(MustOutData)fputs(Stroka,fp);
  fputs(Strka,fp);
  fputs("\n",fp);
  fclose(fp);
  csLogFileError.unlock();
  return 1;
}


/*Вывод отметки на передний план*/
long TRZPDraw::VyvodOtmetkiNaPeredniyPlan(list<TDannyeObCeli>::iterator &it)
{

    return 1;
}


/*Проверка: был ли выбран отсчет по координатам в пикселях, относительно области вывода*/
//Возвращает истинное значение, если отсчет был выбран
//
bool TRZPDraw::VyborOtschetaPoTochke(long X, long Y) //точки
{
   bool bRet;
   list<TDannyeObCeli>::iterator it;
   if(ldoc.empty())
   {
       return false;
   }
   it=ldoc.end();
   --it;
   while(1)
   {
       if((*it).lNomerVozdushnogoSudna>0)
       {
           bRet=(*it).isVnutryOtmetki((int)X, (int)Y, VidyCeley[(*it).Tip]);
           if(bRet)
           {
               VyvodOtmetkiNaPeredniyPlan(it);
               return true;
           }
       }
       if(it==ldoc.begin())
       {
            break;
       }
       --it;
   };
   return false;
}


/*Обновить данные сетки, после изменения параметров*/
int TRZPDraw::updateForRepaint()
{
    UstanovitMaxVizDalnost(iTipDalnosti);
    return 1;
}


/*Задать второе направление*/
int TRZPDraw::ustanovitNapravlenie(int napr)
{
  int Ret;
  double   magnit_otkl_napravlenie_grad;


  if(napr!=0&&lNapravlenieVPP==0)
  {
      lNapravlenieVPP=1;
  }else
  if(napr==0&&lNapravlenieVPP!=0)
  {
      lNapravlenieVPP=0;
  }else{

      return 0;
  }

//Очистим все очереди
  ldoc_new.clear();
  ldoc.clear();
  ldoc_tail.clear();

//Закроем LUA
  luCloseLua();



/*А теперь установим все необходимые параметры в том классе, которые являются
  родителями текущего с учетом нового направления*/
   UstanovitParametry();




   /*Добавлено 08.06.2015*/
   //Проверка загрузки данных из каталога LUA
     Ret=luLoadLuaFilesFromDirectory(csLUA_Directory);
     if(Ret>99 || Ret<=0 )
     {
         QMessageBox::critical(NULL,QString("ERROR"),QString(luGetErrorMessage()));
         return -1;

     }

     //Передача параметров в LUA

     if(lNapravlenieVPP)
     {
        magnit_otkl_napravlenie_grad=dMagnKursPosadkiNaVPP+180.0;
        if(dMagnKursPosadkiNaVPP>=360.0)
        {
            magnit_otkl_napravlenie_grad-360.0;
        }
     }else{
        magnit_otkl_napravlenie_grad=dMagnKursPosadkiNaVPP;
     }

       Ret=luSetNewParametersForLUA(dMinSektorSkanKurs,
                                    dMaxSektorSkanKurs,
                                    dMinSektorSkanGlis,
                                    dMaxSektorSkanGlis,
                                    dMinUgolDNGlisVKurs,
                                    dMaxUgolDNGlisVKurs,
                                    dMinUgolDNKursaVGlis,
                                    dMaxUgolDNKursaVGlis,
                                    dXofRSP_m/1000.0,dYofRSP_m/1000.0,
                                    lRaznicaVPPIAntena,
                                    magnit_otkl_napravlenie_grad,
                                    80.0
                                    );





   RisovatSetku();
   isNeedPereraschet=2;



    return 1;

}



/*ДЛЯ ТЕСТИРОВАНИЯ РЗП*/

long TRZPDraw::GetKoordinatyForTestNaKurseIGlissade(
  //Входной параметр
                            long X,
  //Выходные паараметры
                            double *Azmt,
                            long *Dlnst,
                            long *H,
                            long *Y,
                            long *Z,
                            long *MinY,
                            long *MaxY,
                            long *MinZ,
                            long *MaxZ,
                            long *MinY_3,
                            long *MaxY_3,
                            long *MinZ_3,
                            long *MaxZ_3)
{
  long Otstup;
  double TempUgol;
  double MagKurs;
  double KvDaln;

//Оценим азимут и дальность в координатах относительно севера
  if(lNapravlenieVPP)
  {
    Otstup=-lROtOsiVPPDoCentraAntKursa;
    MagKurs=dMagnKursPosadkiNaVPP-180.0;
    if(MagKurs<0)MagKurs+=360.0;
  }else{
    Otstup=lROtOsiVPPDoCentraAntKursa;
    MagKurs=dMagnKursPosadkiNaVPP;
  }

//Найдем  Y, Z
  *Y=Otstup;
  *Z=lRaznicaVPPIAntena+
    Okrugl((X-lROtTorcaVPPDoCentraAntKurNapr[lNapravlenieVPP]+
        lROtTochkiPrizDoTorcaVPP[lNapravlenieVPP])*tan(dUgolGlissady[lNapravlenieVPP]/180.0*M_PI));

//Рассчитаем угол
   TempUgol=atan2((double)X,*Y)/M_PI*180;

//Оценим азмут
   *Azmt=TempUgol-MagKurs;
   while(*Azmt<0)(*Azmt)+=360.0;

//Оценим дальность
    KvDaln=(X/1000.0)*(X/1000.0)+((*Y)/1000.0)*((*Y)/1000.0)+
           ((*Z)/1000.0)*((*Z)/1000.0);
    *Dlnst=Okrugl(sqrt(KvDaln)*1000);

    *H=*Z;  //Оценим высоту

//Оценим диапазон допустимых изменения для Y
    *MaxY=Otstup+Okrugl(
     (X-lROtTorcaVPPDoCentraAntKurNapr[lNapravlenieVPP]+
        lROtTochkiPrizDoTorcaVPP[lNapravlenieVPP])*
        tan(dOtklonPoKursuMinus/180.0*M_PI));

    *MinY=Otstup-Okrugl((X-lROtTorcaVPPDoCentraAntKurNapr[lNapravlenieVPP]+
        lROtTochkiPrizDoTorcaVPP[lNapravlenieVPP])*
         tan(dOtklonPoKursuPlus/180.0*M_PI));

    *MinZ=
          lRaznicaVPPIAntena+Okrugl(
    (X-lROtTorcaVPPDoCentraAntKurNapr[lNapravlenieVPP]+
        lROtTochkiPrizDoTorcaVPP[lNapravlenieVPP])*
            tan(dUgolGlissady[lNapravlenieVPP]/180.0*M_PI-
                            dOtklonPoGlisMinus/180.0*M_PI));

    *MaxZ=
        lRaznicaVPPIAntena+Okrugl(
    (X-lROtTorcaVPPDoCentraAntKurNapr[lNapravlenieVPP]+
        lROtTochkiPrizDoTorcaVPP[lNapravlenieVPP])*
        tan(dUgolGlissady[lNapravlenieVPP]/180.0*M_PI+
                            dOtklonPoGlisMinus/180.0*M_PI));

    *MaxY_3=Otstup+Okrugl((X-lROtTorcaVPPDoCentraAntKurNapr[lNapravlenieVPP]+
        lROtTochkiPrizDoTorcaVPP[lNapravlenieVPP])*
        tan(dOtklonPoKursuMinus/540.0*M_PI));

    *MinY_3=Otstup-Okrugl((X-lROtTorcaVPPDoCentraAntKurNapr[lNapravlenieVPP]+
        lROtTochkiPrizDoTorcaVPP[lNapravlenieVPP])*
         tan(dOtklonPoKursuPlus/540.0*M_PI));

    *MinZ_3=
       lRaznicaVPPIAntena+Okrugl(
                      (X-lROtTorcaVPPDoCentraAntKurNapr[lNapravlenieVPP]+
        lROtTochkiPrizDoTorcaVPP[lNapravlenieVPP])*
        tan(dUgolGlissady[lNapravlenieVPP]/180.0*M_PI-
                            dOtklonPoGlisMinus/540.0*M_PI));

    *MaxZ_3=
       lRaznicaVPPIAntena+Okrugl(
 (X-lROtTorcaVPPDoCentraAntKurNapr[lNapravlenieVPP]+
        lROtTochkiPrizDoTorcaVPP[lNapravlenieVPP])*tan(dUgolGlissady[lNapravlenieVPP]/180.0*M_PI+
                            dOtklonPoGlisPlus/540.0*M_PI));


   return 1;
}
