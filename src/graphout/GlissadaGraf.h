﻿//---------------------------------------------------------------------------
/*Модуль: GlissadaGraf
Автор: Матвеенко Е.А. ЧРЗ Полет ОКБ

Рисование сетки и отображение отметок в поле глиссады, рисуется в правой части области
отображения.
Класс TGlissadaDraw

*/


#ifndef GlissadaGrafH
#define GlissadaGrafH

#include <QPen>
#include <QBrush>
#include <QRect>
#include <QPainter>
#include <QRect>
#include <QSize>
#include <QPoint>
#include <QColor>
#include  <QImage>

//-----------
#include <list>


#include "qt_tools.h"



#ifdef INI_GROUP
#undef INI_GROUP
#endif
#define INI_GROUP  "GLISSADAGRAF"





using namespace std;


/*Класс TGlissadaDraw обеспечивает отображение сетки в поле глиссады, а также
вывод отметок положения воздушного судна в поле глиссады. Данный класс является
одним из родителей наряду с TKursDraw класса TRZPDraw (graphout/RZPLinGlaf).
*/
class TGlissadaDraw
{

  unsigned int uiFlashStateGlis; //Мерцание глиссады

  long lMaxDalnOut;     //Максимальная дальность в м
                        //Обычно

  long lDopDalnOut;       //Это значение добавляется к lMaxDalnOut в

  long lDopDalnOutOtsch;  //Начало координат, откуда начинается дальность
                             //Это дальность торца ВПП

  long lDopVysotaGlis;    //Высота начала глиссады. Может быть больше нуля и меньше


//Угломерные линии отличаются друг от друга на градус
  long lUglomerProcDaln; //Процент с дальности на которой начинаются угломерные линии
                         //обычно 30
  long lUglRazmer10;     //Число 0.1 градуса... Например 1гр, тогда lUglRazmer10=10
  long lMinUgol10;         //Минимальное значение угла умн на 10 - предположительно -10
  long lMaxUgol10;         //Максимальное значение угла умн на 10 - предположительно 100
  long lOpornUglRazmer10;    //Размер опорного угла - обчыно 50
  long lLiniiVertUdalRazmer;      //Размер линий горизонтального удаления в м
  long lOporLiniiVertUdalRazmer;      //Размер линий опорных горизонтального удаления в м
  long lMaxDalnostLiniiGlissady;  //Максимальная дальность глисады в м
  long lUgolLiniiGlissady10;         //Значение угла глиссады
  long lMinusOtUgolLiniiGlissady10; //На сколько отличается угол нижний части сектора
  long lPlusOtUgolLiniiGlissady10; //На сколько отличается угол нижний части сектора
  long lMaxDalnostSektorGlissady;  //Максимальная дальность сектора глиссад
  //Линии сектора сканирования
  long lMinUgolSektoraSkan10;    //Минимальный угол сектора сканирования
  long lMaxUgolSektoraSkan10;    //Максимальный угол сектора сканирования
  long lMinUgolKurs10;           //Минимальный угол
  long lMaxUgolKurs10;           //Максимальный угол


//Курса... Для  вывода отметок
   long lMinUgolForY10;          //Минимальный угол по Y - если выводить
   long lMaxUgolForY10;          //Максимальный угол по Y - если выводить




//Положение БПРМ от торца ВПП
  long lPolojenieBPRM;  //Положение от торца в метрах

//Положение ДПРМ от торца ВПП
  long lPolojenieDPRM;  //значение в метрах



public:
   bool bSpravaNaLevo;     //Вывод справа на лево

   QPen LpUglomerPen;   //Все параметры линии глиссадр - тольщина линии цвет и прочее
   bool bVyvodUglomer;    //Выводить неопорные угломеры
   QColor cVyvodUglomerText;//Цвет выводимого уголомера
   long szVyvodUglomerText; //Значение размера выводимого текста
   bool bVyvodUglomerText; //Выводить значения опорных угломеров
   QPen LpOpornUglPen;   //Все параметры линии опорного угла - цвет и прочее
   QColor cVyvodOpornUglText;//Цвет выводимого опорного уголомера
   long szVyvodOpornUglText; //Значение размера выводимого текста
   QPen LpLiniiVertUdal;    //Описания цветов линий удаления
   bool bVyvodLiniiVertUdal;  //Вывод значений линий горизонтального удаления
   QColor cTextVertUdal;  //Цвет для вывода текста значений линий горизонтального удаления
   long   szTextVertUdal;   //Размер для текста горизонтального удаления
   bool bVyvodTextVertUdal; //Выводить текст линий горизонтального удаления
   QPen LpOporLiniiVertUdal;          //Перо линий горизонтального удаления
   QColor cTextOporVertUdal;  //Цвет для вывода текста значений линий горизонтального удаления
   long   szTextOporVertUdal;   //Размер для текста горизонтального удаления
   QPen LpLiniiGlissady;          //Линии глиссады
   QBrush LbSektorGlissady;

   bool bVyvodSektoraSkanGlis;
   QPen   LpSektoraSkan;        //Цвета сектора сканирования
   QBrush LbSektoraSkan;        //Brush для сектора сканирования

   bool bVyvodUgolKursVGlis;
   QPen LpUgolKurs;   //Параметры для пера линий
   QBrush LbUgolKurs; //Для закраски угла курса


   /*Описание внешнего вида и размера БПРМ*/
  bool     bBPRMGlis;        //Выводить ли БПРМ в поле глиссады
  QPen   lpBPRMGlis;       //Логическое перо БПРМ

/*Описание внешнего вида и размера ДПРМ*/
  bool     bDPRMGlis;        //Выводить ли ДПРМ в поле глиссажы
  QPen   lpDPRMGlis;       //Логическое перо ДПРМ



//Значения равных высот типа public
  long lSposobVyvodaRavnyhVysot;   //0 - равномерно по всему вертикальному масштабу
                                  //1 Начальная высота и шаг
  long lPervayaVysotaRavnyhVysot;  //Первая высота равных высот
  long lShagVysotyRavnyhVysot;     //Шаг высоты равных высот


  long N_of_RavnyhVysot;        //Сколько линий равных высот
  long lRavnyhVysotZnach[100];    //Значения высот для линий равных высот
  QPen LpRavnyhVysotPen;   //Описания цветов линий равных высот
  QColor cRavnyhVysotText; //Цвет выводимого текста
  long szRavnyhVysotText;    //Значение размера выводимого текста
  bool bVyvodRavnyhVysot;    //
  bool bVyvodTextRavnyhVysot;


protected:


//Рисование одной точки -- промежуточная процедура
//  1 цель
     long RisovatOdnuCelGlis(
                      QPainter &pntr,    //Контекст устройства
                      const QRect &Rect, //Область куда рисуется цель
                      TDannyeObCeli &DannyeObCeli,
                      TVidCeli *VidCeli, //Начинаются рассматриваться с индекса 1, чтобы удобнее было
                      long lChisloVidovCeley, //Число видов целей
                      int isSledOtmetka        //Эта отметка в следе
                    );

     //Промежуточная процедура для опреления расстояния цели до глисадной линии в метрах
     //Возвращает: > -10000 < 10000  - если цель лежит в секторе глиссады
     // -20000 - вне сектора глисады
     //20000 - вне сектора глисады
     // - координата вне пределов оси 30000
            int getRastoyanieCeliIGlis( TDannyeObCeli &DannyeObCeli);




//Получение координаты окна из координат мировых
    int getXscrFromXwrld(int x)
    {
        return x*dfKoefWrldXToScrX+pScrFor0.x();
    }


    int getYscrFromYwrld(int y)
    {
        return y*dfKoefWrldYToScrY+pScrFor0.y();
    }


    int textOutWrld(QPainter &pntr,          //Дескриптер
                   int x_wrld, int y_wrld,    //Координаты в системе окна
                   const QFont &font,       //фонте
                   const QColor &col,
                   const QString &str,
                   enum QTT_GdeText qtt,       //0 - с
                   int otstup
                  )
    {
        return textOutScr(pntr,
                  getXscrFromXwrld(x_wrld), getYscrFromYwrld(y_wrld),
                  font, col,str,qtt,otstup
                          );
    }


    void drawPolygonWrld(QPainter &pntr,       //Куда выводить
                          const QPoint p[],     //Сами точки
                          int  n_of_p)          //ЧИсло точек
    {
        QPoint p1[100];
        for(int i=0;i<n_of_p;i++)
        {
            p1[i].setX(getXscrFromXwrld(p[i].x()));
            p1[i].setY(getYscrFromYwrld(p[i].y()));
        }
        pntr.drawPolygon(p1,n_of_p);
    }



   void  updateFlashStateGlis()
    {
        uiFlashStateGlis=(uiFlashStateGlis+1)%2;
    };


   int getFlashStateGlis()
   {
         return   uiFlashStateGlis;
   };

private:

/*Дополнительные данные для вывода. Задают соответствие всех логических и физических координат*/
  QRect RectOut;    //Область вывода.  Область куда выводятся данные

  int min_vysota_m, max_vysota_m;   //Минимальное максимальное значение высоты

  double dfKoefScrXToWrldX, //Коэффициент перехода из устройства в мировую систему координат
         dfKoefScrYToWrldY; //Коэффициент перехода из устройства в мировую систему координат


  double dfKoefWrldXToScrX, //Коэффициент перехода из мир.сист. коорд в систему координат устрйоства
         dfKoefWrldYToScrY; //Коэффициент перехода из мир.сист. коорд в систему координат устройства


  QPoint pScrFor0;          //Нулевая точка начала координат






  long ZadanieDopDataOut(const QRect &Rect);

//Вывести сектор глиссады
  long VyvestiSektorGlissady(QPainter &pntr);


//Вывести 1 наклонную линию - вывод в рамку
  long VyvestiUglomer(QPainter &pntr, long Gradus10);

//Вевести вертикальную
  long VyvestiLinVertUdalenia(QPainter &pntr, long Daln_m);

//Вывести Линию Глиссады
  long VyvestiLiniuGlissady(QPainter &pntr);


//Вывести линии диапазон сканирования
  long VyvestiSekrorSkanirovaniya(QPainter &pntr);

//Вывести вначале сектор сканинирования
  long VyvestiSekrorSkanirovaniyaSekt(QPainter &pntr);


//Вывесьт линии Курса
  long VyvestiDNKursa(QPainter &pntr);


//Вывести сектор курса
  long VyvestiDNKursaSekt(QPainter &pntr);


//Вывести линии равных равных высот
  long VyvestiLiniiRavnyhVysot(QPainter &pntr);

//Вывести вертикальные линии сответствующие глиссаде
  long VyvestiBPRMandDPRMGlis(QPainter &pntr);


//Рисовать линии. Функция рисует линии как накланненный брус
  long RisovatLiniuDlyaGlissady(QPainter &pntr,
      long X1, long Y1, long X2, long Y2,
      const QPen &lp);


public:


  TGlissadaDraw();    //А в

//Рисовать глисадную сетку
  long PaintGlis(QPainter &pntr,     //Конектс устройства
             const QRect &Rect); //Ограниченная область куда должна вписаться


//Рисовать цели
  long RisovatCeliGlis(QPainter &pntr,    //Контекст устройства
                   const QRect &Rect, //Область куда рисуется цель
                    list<TDannyeObCeli> &ldoc,
                    list<TDannyeObCeli> &ldoc_tail,
                   TVidCeli *VidCeli, //Начинаются рассматриваться с индекса 1, чтобы удобнее было
                   long lChisloVidovCeley //Число видов целей
                 );


//Загрузка данных из Ини файла
  long SaveDataToINI(const char* lpszINIFileName);

//Загрузка данных из ИНИ
  long LoadDataFromINI(const char* lpszINIFileName);


/*NovyyMasshtabDalnosti и NovyyMasshtabPoVertikali сможет вызывать обычный
пользователь. Это для изменения масштаба*/
  long NovyyMasshtabDalnosti(long NewMaxDaln)
  {
    if(NewMaxDaln<2000||NewMaxDaln>80000)return 0;

    if(NewMaxDaln<=5000)
    {
      lLiniiVertUdalRazmer=200;
      lOporLiniiVertUdalRazmer=1000;
    }else if(NewMaxDaln<=15000)
    {
      lLiniiVertUdalRazmer=500;
      lOporLiniiVertUdalRazmer=2000;
    }else if(NewMaxDaln<=30000)
    {
      lLiniiVertUdalRazmer=1000;
      lOporLiniiVertUdalRazmer=5000;
    }else if(NewMaxDaln<=50000)
    {
      lLiniiVertUdalRazmer=2000;
      lOporLiniiVertUdalRazmer=10000;
    }else{
      lLiniiVertUdalRazmer=5000;
      lOporLiniiVertUdalRazmer=10000;
    }
    lMaxDalnOut=NewMaxDaln;
    return 1;
  };

//Получить масштаб по дальности
   long GetMasshtabDalnosti(void)
   {
     return lMaxDalnOut;
   };



//Установка масштаба по углу
  long NovyyMasshtabPoVertikali(double PervUgol, double VtorUgol)
  {
    long Okrugl(double Chislo1);
    long dUgol10;
    long PervUgol10=Okrugl(PervUgol*10),
         VtorUgol10=Okrugl(VtorUgol*10);
    if(PervUgol10>0||VtorUgol10<0||PervUgol10>VtorUgol10||VtorUgol10-PervUgol10<30)return 0;
    lMinUgol10=PervUgol10;
    lMaxUgol10=VtorUgol10;
    if(lMinUgol10<lMinUgolSektoraSkan10-10)lMinUgol10=lMinUgolSektoraSkan10-10;
    if(lMaxUgol10>lMaxUgolSektoraSkan10+20)lMaxUgol10=lMaxUgolSektoraSkan10+20;

    dUgol10=lMaxUgol10-lMinUgol10;
    if(dUgol10<40)
    {
       lOpornUglRazmer10=20;
       lUglRazmer10=5;
    }else{
       lOpornUglRazmer10=50;
       lUglRazmer10=10;
    }
    return 1;
  };

//Получить масштаб по вертикали
  long GetMasshtabPoVertikali(double *PervUgol, double *VtorUgol)
  {
    *PervUgol=lMinUgol10/10.0;
    *VtorUgol=lMaxUgol10/10.0;
    return 1;
  };


/*Дальнейшие процедуры только для изменения администратором*/
/*Растояние места посадки от РСП*/
  long NovoeRasstoyanieRSPOtMestaPosadki(long NewDopDaln)
  {
    if(NewDopDaln<0||NewDopDaln>2000)return 0;
    lDopDalnOut=NewDopDaln;
    return 1;
  };

  long GetRasstoyanieRSPOtMestaPosadki(void)
  {
     return lDopDalnOut;
  };


/*Новое расстояние конца ВПП от РСП*/
  long NovoeRasstoyanieRSPOtTorcaVPP(long NewD)
  {
     if(NewD<0||NewD>2000)return 0;
     lDopDalnOutOtsch=NewD;
     return 1;
  };

  long GetRasstoyanieRSPOtTorcaVPP(void)
  {
     return lDopDalnOutOtsch;
  };



//Установить процент
  long NovyyProcentOtsekaemyhNeopornyhUglomerov(long NewProc)
  {
    if(NewProc>=0&&NewProc<100)
    {
      lUglomerProcDaln=NewProc;
      return 1;
    }
    return 0;
  };

  long GetProcentOtsekaemyhNeopornyhUglomerov(void)
  {
    return lUglomerProcDaln;
  };

/*Диапазон сектора сканирования*/
  long NovyySektorSkanirovania(double Ugol1, double Ugol2)
  {
    long Okrugl(double Chislo1);

    long lMinUgolSektoraSkan10a,lMaxUgolSektoraSkan10a;
    lMinUgolSektoraSkan10a=Okrugl(10*Ugol1);
    lMaxUgolSektoraSkan10a=Okrugl(10*Ugol2);
    if(lMinUgolSektoraSkan10a>lMaxUgolSektoraSkan10a)return 0;
    lMinUgolSektoraSkan10=lMinUgolSektoraSkan10a;
    lMaxUgolSektoraSkan10=lMaxUgolSektoraSkan10a;
    return 1;
  };

  long GetSektorSkanirovania(double *Ugol1, double *Ugol2)
  {
     *Ugol1=lMinUgolSektoraSkan10/10.0;
     *Ugol2=lMaxUgolSektoraSkan10/10.0;
     return 1;
  };

/*Диапазон сектора ДН азимута*/

  long NovyyDNKursa(double Ugol1, double Ugol2)
  {
    long Okrugl(double Chislo1);

    long lMinUgolKurs10a,lMaxUgolKurs10a;
    lMinUgolKurs10a=Okrugl(10*Ugol1);
    lMaxUgolKurs10a=Okrugl(10*Ugol2);
    if(lMinUgolKurs10a>lMaxUgolKurs10a)return 0;
    lMinUgolKurs10=lMinUgolKurs10a;
    lMaxUgolKurs10=lMaxUgolKurs10a;
    return 1;
  };

  long GetDNKursa(double *Ugol1, double *Ugol2)
  {
     *Ugol1=lMinUgolKurs10/10.0;
     *Ugol2=lMaxUgolKurs10/10.0;
     return 1;
  };


/*Сектор линии глиссады*/
  long NovayLiniyaGlissady(double UgolGlissady,
                            long MaxDalnostPryamoy,
                            long SmesheniePoVysote, //Смещение глиссады по выосоте в метрах
                            double OtstupMenshe,
                            double OtstupBolshe,
                            long MaxDalnostSektora
                            )
  {
      long Okrugl(double Chislo1);
      if(UgolGlissady>-100)lUgolLiniiGlissady10=Okrugl(UgolGlissady*10.0);
      if(MaxDalnostPryamoy>=0)lMaxDalnostLiniiGlissady=MaxDalnostPryamoy;
      if(SmesheniePoVysote>-1000)lDopVysotaGlis=SmesheniePoVysote; //Значение высоте

      if(OtstupMenshe>=0)lMinusOtUgolLiniiGlissady10=Okrugl(10.0*OtstupMenshe);
      if(OtstupBolshe>=0)lPlusOtUgolLiniiGlissady10=Okrugl(10.0*OtstupBolshe);
      if(MaxDalnostSektora>=0)lMaxDalnostSektorGlissady=MaxDalnostSektora;

      return 1;
  };

  double GetUgolGlissady(void)
  {
    return lUgolLiniiGlissady10/10.0;
  };
  long GetMaxDalnostGlissady(void)
  {
    return lUgolLiniiGlissady10;
  };
  double GetDopustMenshiyUgolOtGlissady(void)
  {
    return lMinusOtUgolLiniiGlissady10/10.0;
  };

  double GetDopustBolshiyUgolOtGlissady(void)
  {
    return lPlusOtUgolLiniiGlissady10/10.0;
  };

  long GetMaxDalnostSektoraGlissady(void)
  {
    return lMaxDalnostSektorGlissady;
  };

/*Допутимый диапазон по Y, для которого отметки будут выводиться.

*/
  long NovyyDopustimyyDiapazonPoY(double Ugl1, double Ugl2)
  {
    long Okrugl(double Chislo1);
    long lMinUgolForY10a,lMaxUgolForY10a;
    lMinUgolForY10a=Okrugl(Ugl1*10.0);
    lMaxUgolForY10a=Okrugl(Ugl2*10.0);
    if(lMinUgolForY10a>=lMaxUgolForY10a)
    {
       return 0;
    }


    lMinUgolForY10=lMinUgolForY10a;
    lMaxUgolForY10=lMaxUgolForY10a;
    return 1;
  };



};


#endif
