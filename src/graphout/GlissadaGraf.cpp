﻿//---------------------------------------------------------------------------




#include <QSettings>

#include <math.h>
#include <stdio.h>
#include <string.h>
#include "OtmetkaDannye.h"
#include "AnyTools.h"
#include "GlissadaGraf.h"
#include "safe_function.h"
#include "temp_work_with_ini.h"


//---------------------------------------------------------------------------
#define IS_FLT_NOL(x) (((x)>-0.01&&(x)<0.01)?true:false)
#define IS_NEBOLSHE_NOL(x) ((x)<0.01?true:false)

#define ABS(X) (((X) > 0) ? (X) : (-(X)))
#define MAX(a, b)  (((a) > (b)) ? (a) : (b))
#define MIN(a, b)  (((a) < (b)) ? (a) : (b))













TGlissadaDraw::TGlissadaDraw()
{
  uiFlashStateGlis=0;
  lUglomerProcDaln=30;
  lUglRazmer10=10;       //Значение шага угла 1 градус
  LpUglomerPen.setStyle(Qt::SolidLine);
  LpUglomerPen.setWidth(1);
  LpUglomerPen.setColor(QColor(100,100,100));
  lMinUgol10=-10;      //-1 градус
  lMaxUgol10=110;      //10 градусов
  bVyvodUglomer=true;  //Выводим линии углов
  cVyvodUglomerText.setRgb(0,200,200);
  szVyvodUglomerText=20;
  bVyvodUglomerText=true;  //Выведем текст для каждого угла

  lOpornUglRazmer10=50;    //5 градусов
  LpOpornUglPen.setStyle(Qt::SolidLine);
  LpOpornUglPen.setWidth(1);
  LpOpornUglPen.setColor(QColor(170,170,170));
  cVyvodOpornUglText.setRgb(0,200,200);

  szVyvodOpornUglText=20;
  lLiniiVertUdalRazmer=2000;
  LpLiniiVertUdal.setStyle(Qt::SolidLine);
  LpLiniiVertUdal.setWidth(1);
  LpLiniiVertUdal.setColor(QColor(100,100,100));
  bVyvodLiniiVertUdal=true;

  cTextVertUdal.setRgb(0,150,150);
  szTextVertUdal=16;
  bVyvodTextVertUdal=true;

  lOporLiniiVertUdalRazmer=10000;
  LpOporLiniiVertUdal.setStyle(Qt::SolidLine);
  LpOporLiniiVertUdal.setWidth(1);
  LpOporLiniiVertUdal.setColor(QColor(170,170,170));
  cTextOporVertUdal.setRgb(0,200,200);
  szTextOporVertUdal=20;

  lMaxDalnOut=30000; //
  lDopDalnOut=800;
  lDopDalnOutOtsch=1500;
  lDopVysotaGlis=50;


  lMaxDalnostLiniiGlissady=200000;
  lUgolLiniiGlissady10=28; //2.8 градуса
  LpLiniiGlissady.setStyle(Qt::SolidLine);
  LpLiniiGlissady.setWidth(1);
  LpLiniiGlissady.setColor(QColor(0,200,0));

  lMinusOtUgolLiniiGlissady10=5;  //Полградуса
  lPlusOtUgolLiniiGlissady10=5;
  lMaxDalnostSektorGlissady=20000;
  LbSektorGlissady.setStyle(Qt::SolidPattern);
  LbSektorGlissady.setColor(QColor(0,50,0));


  lMinUgolSektoraSkan10=-10;
  lMaxUgolSektoraSkan10=80;
  LpSektoraSkan.setStyle(Qt::DashLine);
  LpSektoraSkan.setWidth(1);
  LpSektoraSkan.setColor(QColor(0,0,180));
  LbSektoraSkan.setStyle(Qt::SolidPattern);
  LbSektoraSkan.setColor(QColor(0,0,40));

  lMinUgolKurs10=-5;
  lMaxUgolKurs10=65;
  LpUgolKurs.setStyle(Qt::DashLine);
  LpUgolKurs.setWidth(1);
  LpUgolKurs.setColor(QColor(220,0,0));
  LbUgolKurs.setStyle(Qt::SolidPattern);
  LbUgolKurs.setColor(QColor(40,0,0));


  lSposobVyvodaRavnyhVysot=0;
  lPervayaVysotaRavnyhVysot=200;
  lShagVysotyRavnyhVysot=200;


//Число равных высот
  N_of_RavnyhVysot=7;
//Определим максимальную высоту

  bVyvodRavnyhVysot=true;
  bVyvodTextRavnyhVysot=true;
  LpRavnyhVysotPen.setStyle(Qt::SolidLine);
  LpRavnyhVysotPen.setWidth(1);
  LpRavnyhVysotPen.setColor(QColor(0,150,0));
  cRavnyhVysotText.setRgb(0,200,0);
  szRavnyhVysotText=18;


  for(long i=0;i<N_of_RavnyhVysot;i++)
  {

    lRavnyhVysotZnach[i]=(i+1)*500;
  }


  lPolojenieBPRM=4200;
  bBPRMGlis=true;
  lpBPRMGlis.setStyle(Qt::SolidLine);
  lpBPRMGlis.setWidth(2);
  lpBPRMGlis.setColor(QColor(255,20,100));


  lPolojenieDPRM=6200;
  bDPRMGlis=true;
  lpDPRMGlis.setStyle(Qt::SolidLine);
  lpDPRMGlis.setWidth(2);
  lpDPRMGlis.setColor(QColor(200,20,200));


  bSpravaNaLevo=false;
}




//Рисовать глисадную сетку
long TGlissadaDraw::PaintGlis(QPainter &pntr,     //Конектс устройства
                              const QRect &Rect) //Ограниченная область куда должна вписаться
{
  long TekUgol10;  //Текущее значение угла
  long lRet;   //Возвращаемое значение типа long
  long TekDaln_m; //Текущая дальность в метрах

//Этап 0. Надо определить диапазон в пикеселях рисования сетки
//Опроделим - отступ сверху для вывода текста.
  ZadanieDopDataOut(Rect);



//Этап 1. Вывод секторов
//а) вывод сектора сканирования
  lRet=VyvestiSekrorSkanirovaniyaSekt(pntr);

//б) вывод сектора диаграммы направленности в вертикальной плоскости
  lRet=VyvestiDNKursaSekt(pntr);


//в) вывод сектора глиссады
  lRet=VyvestiSektorGlissady(pntr);

//Этап 2. Вывести линии маяков
  lRet=VyvestiBPRMandDPRMGlis(pntr);



//Этап 3. Вывод наклонных линий
  if(lMinUgol10<=0)
  {
    TekUgol10=0;
  }else{
    TekUgol10=lMinUgol10;
  }

  for(;TekUgol10<=lMaxUgol10;TekUgol10+=lUglRazmer10)
  {
     lRet=VyvestiUglomer(pntr,TekUgol10);
  }

  if(lMinUgol10<0)
  {
    for(TekUgol10=-lUglRazmer10;TekUgol10>=lMinUgol10;TekUgol10-=lUglRazmer10)
    {
      lRet=VyvestiUglomer(pntr,TekUgol10);
    }
  }



//Этап 4. Вывод линий горизонтального удаления
  for(TekDaln_m=0;TekDaln_m<=lMaxDalnOut;TekDaln_m+=lLiniiVertUdalRazmer)
  {
     lRet=VyvestiLinVertUdalenia(pntr,TekDaln_m);
  }

//Этап 5. Выведем глиссаду
  lRet=VyvestiLiniuGlissady(pntr);


//Этап 6. Вывести сектор сканирования
  lRet=VyvestiSekrorSkanirovaniya(pntr);

//Этап 7. Вывести Диаграмму направленности курса
  lRet=VyvestiDNKursa(pntr);

//Этап 8. Вывести линии равных высот
  lRet=VyvestiLiniiRavnyhVysot(pntr);





  return 1;
}



//Рисовать цели
long TGlissadaDraw::RisovatCeliGlis(
                 QPainter &pntr,    //Контекст устройства
                 const QRect &Rect, //Область куда рисуется цель
                 list<TDannyeObCeli> &ldoc,
                 list<TDannyeObCeli> &ldoc_tail,
                 TVidCeli *VidCeli, //Начинаются рассматриваться с индекса 1, чтобы удобнее было
                 long lChisloVidovCeley //Число видов целей
                )
{


  Qt::BGMode OldBkMode;
  char Strka[100];
  long szStrka;   //Длина строки Strka
  long Xt,Zt;     //Начало положения текста
  QFont font;




  bool IsNSled0;   //Если номер равен 0
  list<TDannyeObCeli>::iterator it;


//Этап 0. Надо определить диапазон в пикеселях рисования сетки
//Опроделим - отступ сверху для вывода текста.
  ZadanieDopDataOut(Rect);



  //Установим режим рисования без зарисовки пикселей
   OldBkMode=pntr.backgroundMode(); pntr.setBackgroundMode(Qt::TransparentMode);

  it=ldoc_tail.begin();
  while(it!=ldoc_tail.end())
  {
     RisovatOdnuCelGlis(pntr,Rect,(*it),VidCeli,lChisloVidovCeley,1);
      ++it;
  }

//Выведем новые отметки, которые не хвост
  it=ldoc.begin();
  while(it!=ldoc.end())
  {
      RisovatOdnuCelGlis(pntr,Rect,(*it),VidCeli,lChisloVidovCeley,0);
      ++it;
  }


  IsNSled0=false;

//А тут вывести текст
  it=ldoc.begin();
  for(it=ldoc.begin();it!=ldoc.end();)
  {
    if(
       (*it).X<0|| //Дальность слишком маленькая
       (*it).X>lMaxDalnOut+lDopDalnOutOtsch||//Или за пределы выходит отображения
       !VidCeli[(*it).Tip].IsVyvodTextGlis|| //Не надо выводить текст в поле глиссады
       (*it).Z==-100000|| //Не определена высота, что выводить-то?
       !(*it).VyhodOtklGlis_3 //Нет выхода за пределы сектора глиссады
      )
      {
         ++it;
         continue;  //Тогда переходим к след элементу
      }

 //Выведем теперь текст.
    if(VidCeli[(*it).Tip].IsVyvodTextGlis)
    {
      if((*it).XvpGlis>=0&&(*it).YvpGlis>=0)
      {
        //Сформируем строку с текстом, которую надо вывести
         sfSPRINTF(Strka,"%ld",(*it).lOtklGlis);


         font=createFontFromFamalyAndSize(DEFAULT_FAMILY_FONT,
                                          VidCeli[(*it).Tip].szTextGlis,false);

         textOutScr(pntr,(*it).XvpGlis,(*it).YvpGlis,font,
                 VidCeli[(*it).Tip].cTextGlis,QString(Strka),QTT_pravee,
                 VidCeli[(*it).Tip].lRazmerCeliGlis/2+4);
//Выведем строку
      }
    }
    ++it;
  }
  pntr.setBackgroundMode(OldBkMode);
  return 1;
}




/*Вывод всех данных в ИНИ файл*/
long TGlissadaDraw:: SaveDataToINI(const char* lpszINIFileName)
{
  QSettings *stng;
     stng=new QSettings(QString(lpszINIFileName),QSettings::IniFormat);

//Сохраненим сначала все данные которые не будут меняться в результате действий
//оператора.

    SAVE_UGOL_TO_INI("MinVisibleAngle",lMinUgol10/10.0)
    SAVE_UGOL_TO_INI("MaxVisibleAngle",lMaxUgol10/10.0)

    SAVE_LOGPEN_TO_INI("LOGPEN_Angle",LpUglomerPen)
    SAVE_BOOL_TO_INI("VisibleAngle",bVyvodUglomer)
    SAVE_COLORREF_TO_INI("ColorAngleText",cVyvodUglomerText)

    stng->setValue("GLISSADAGRAF/SizeAngleText",(int)szVyvodUglomerText);


    SAVE_BOOL_TO_INI("VisibleAngleText",bVyvodUglomerText)
    SAVE_LOGPEN_TO_INI("LOGPEN_OAngle",LpOpornUglPen)
    SAVE_COLORREF_TO_INI("ColorOAngleText",cVyvodOpornUglText)
    stng->setValue("GLISSADAGRAF/SizeOAngleText",(int)szVyvodOpornUglText);

    SAVE_LOGPEN_TO_INI("LOGPEN_VerLines",LpLiniiVertUdal)
    SAVE_BOOL_TO_INI("VisibleVerLines",bVyvodLiniiVertUdal)
    SAVE_COLORREF_TO_INI("ColorVerLinesText",cTextVertUdal)
    stng->setValue("GLISSADAGRAF/SizeVerLinesText",(int)szTextVertUdal);

    SAVE_BOOL_TO_INI("VisibleVerLinesText",bVyvodTextVertUdal)
    SAVE_LOGPEN_TO_INI("LOGPEN_OVerLines",LpOporLiniiVertUdal)
    SAVE_COLORREF_TO_INI("ColorOVerLinesText",cTextOporVertUdal)

    stng->setValue("GLISSADAGRAF/SizeOVerLinesText",(int)szTextOporVertUdal);

    SAVE_LOGPEN_TO_INI("LOGPEN_GlisLine",LpLiniiGlissady)
    SAVE_LOGBRUSH_TO_INI("LOGBRUSH_GlisSector",LbSektorGlissady)
    SAVE_BOOL_TO_INI("VisibleSektoraSkanGlis",bVyvodSektoraSkanGlis)
    SAVE_LOGPEN_TO_INI("LOGPEN_ScanSector",LpSektoraSkan)
    SAVE_LOGBRUSH_TO_INI("LOGBRUSH_ScanSector",LbSektoraSkan)
    SAVE_BOOL_TO_INI("VisibleUgolKursVGlis",bVyvodUgolKursVGlis)
    SAVE_LOGPEN_TO_INI("LOGPEN_DiagramCourse",LpUgolKurs)
    SAVE_LOGBRUSH_TO_INI("LOGBRUSH_DiagramCourse",LbUgolKurs)



    stng->setValue("GLISSADAGRAF/NumberOfHeightLines",(int)N_of_RavnyhVysot);


    SAVE_LOGPEN_TO_INI("LOGPEN_HeightLines", LpRavnyhVysotPen)
    stng->setValue("GLISSADAGRAF/HowDrawOutHeightLines",(int)lSposobVyvodaRavnyhVysot);

    stng->setValue("GLISSADAGRAF/FirstDrawOutHeightLines",(int)lPervayaVysotaRavnyhVysot);

    stng->setValue("GLISSADAGRAF/StepDrawOutHeightLines",(int)lShagVysotyRavnyhVysot);

    SAVE_BOOL_TO_INI("VisibleHeightLines",bVyvodRavnyhVysot)
    stng->setValue("GLISSADAGRAF/SizeHeightLinesText", (int)szRavnyhVysotText);

    SAVE_COLORREF_TO_INI("ColorHeightLinesText",cRavnyhVysotText)
    SAVE_BOOL_TO_INI("VisibleHeightLinesText",bVyvodTextRavnyhVysot)

    delete stng;

  return 1;
}




//Загрузка данных из ИНИ
long TGlissadaDraw::LoadDataFromINI(const char* lpszINIFileName)
{
  double ugl1, ugl2;
  QSettings *stng;
  stng=new QSettings(QString(lpszINIFileName),QSettings::IniFormat);


  LOAD_UGOL_FROM_INI("MinVisibleAngle",ugl1,"-1")
  LOAD_UGOL_FROM_INI("MaxVisibleAngle",ugl2, "9")



  NovyyMasshtabPoVertikali(ugl1, ugl2);

  LOAD_LOGPEN_FROM_INI("LOGPEN_Angle", LpUglomerPen,PS_SOLID,1,1,RGB(100,100,100))
  LOAD_BOOL_FROM_INI("VisibleAngle", bVyvodUglomer,true)
  LOAD_COLORREF_FROM_INI("ColorAngleText",cVyvodUglomerText,RGB(0,150,150))

  szVyvodUglomerText=stng->value("GLISSADAGRAF/SizeAngleText",20).toInt();

  LOAD_BOOL_FROM_INI("VisibleAngleText",bVyvodUglomerText,true)
  LOAD_LOGPEN_FROM_INI("LOGPEN_OAngle",LpOpornUglPen,PS_SOLID,1,1,RGB(170,170,170))
  LOAD_COLORREF_FROM_INI("ColorOAngleText",cVyvodOpornUglText,RGB(0,200,200));

  szVyvodOpornUglText=stng->value("GLISSADAGRAF/SizeOAngleText",20).toInt();


  LOAD_LOGPEN_FROM_INI("LOGPEN_VerLines",LpLiniiVertUdal,PS_SOLID,1,1,RGB(100,100,100))
  LOAD_BOOL_FROM_INI("VisibleVerLines",bVyvodLiniiVertUdal,true)
  LOAD_COLORREF_FROM_INI("ColorVerLinesText",cTextVertUdal,RGB(0,150,150))


  szTextVertUdal=stng->value("GLISSADAGRAF/SizeVerLinesText",18).toInt();

  LOAD_BOOL_FROM_INI("VisibleVerLinesText",bVyvodTextVertUdal,false)
  LOAD_LOGPEN_FROM_INI("LOGPEN_OVerLines",LpOporLiniiVertUdal,PS_SOLID,1,1,RGB(170,170,170))
  LOAD_COLORREF_FROM_INI("ColorOVerLinesText",cTextOporVertUdal,RGB(0,200,200))


  szTextOporVertUdal=stng->value("GLISSADAGRAF/SizeOVerLinesText",18).toInt();


  LOAD_LOGPEN_FROM_INI("LOGPEN_GlisLine",LpLiniiGlissady,PS_SOLID,1,1,RGB(0,200,0))
  LOAD_LOGBRUSH_FROM_INI("LOGBRUSH_GlisSector",LbSektorGlissady, BS_SOLID, RGB(0,50,0),1)
  LOAD_BOOL_FROM_INI("VisibleSektoraSkanGlis",bVyvodSektoraSkanGlis,true)
  LOAD_LOGPEN_FROM_INI("LOGPEN_ScanSector",LpSektoraSkan,PS_DASH,1,1,RGB(0,0,180))
  LOAD_LOGBRUSH_FROM_INI("LOGBRUSH_ScanSector",LbSektoraSkan,BS_SOLID,RGB(0,0,50),0)
  LOAD_BOOL_FROM_INI("VisibleUgolKursVGlis",bVyvodUgolKursVGlis,false)
  LOAD_LOGPEN_FROM_INI("LOGPEN_DiagramCourse",LpUgolKurs,PS_DASH,1,1,RGB(220,0,0))
  LOAD_LOGBRUSH_FROM_INI("LOGBRUSH_DiagramCourse",LbUgolKurs,BS_SOLID,RGB(40,0,0),1)



  N_of_RavnyhVysot=stng->value("GLISSADAGRAF/NumberOfHeightLines",18).toInt();


  LOAD_LOGPEN_FROM_INI("LOGPEN_HeightLines", LpRavnyhVysotPen, PS_SOLID,1,1,RGB(0,150,0))

  lSposobVyvodaRavnyhVysot=stng->value("GLISSADAGRAF/HowDrawOutHeightLines",0).toInt();

  lPervayaVysotaRavnyhVysot=stng->value("GLISSADAGRAF/FirstDrawOutHeightLines",200).toInt();

  lShagVysotyRavnyhVysot=stng->value("GLISSADAGRAF/StepDrawOutHeightLines",200).toInt();

  LOAD_BOOL_FROM_INI("VisibleHeightLines",bVyvodRavnyhVysot,true)
  szRavnyhVysotText=stng->value("GLISSADAGRAF/SizeHeightLinesText",18).toInt();

  LOAD_COLORREF_FROM_INI("ColorHeightLinesText",cRavnyhVysotText,RGB(0,200,0))
  LOAD_BOOL_FROM_INI("VisibleHeightLinesText",bVyvodTextRavnyhVysot,true)

  delete stng;


  return 1;
}





/**Частные процедуры**/
//Rect - область, куда выводится сетка
//Причем угол 0 проходит строго по низу
long TGlissadaDraw::ZadanieDopDataOut(const QRect &Rect)
{

//Зададим выводимую область (без учета надписей)
  RectOut=Rect;

  pScrFor0.setX(RectOut.left());

  min_vysota_m=Okrugl((lDopDalnOutOtsch+lMaxDalnOut)*tan(lMinUgol10/1800.0*M_PI));
  max_vysota_m=Okrugl((lDopDalnOutOtsch+lMaxDalnOut)*tan(lMaxUgol10/1800.0*M_PI));

  dfKoefWrldYToScrY=RectOut.height()/((double)(min_vysota_m-max_vysota_m));
  pScrFor0.setY(Okrugl(RectOut.top()-max_vysota_m*dfKoefWrldYToScrY));

  dfKoefWrldXToScrX=RectOut.width()/((double)(lDopDalnOutOtsch+lMaxDalnOut));


  dfKoefScrXToWrldX=1.0/dfKoefWrldXToScrX;
  dfKoefScrYToWrldY=1.0/dfKoefWrldYToScrY;

  return 1;
}


//Вывести вначале сектор сканинирования
long TGlissadaDraw::VyvestiSekrorSkanirovaniyaSekt(QPainter &pntr)
{
  QPoint Pnts[3];
  QPen LP, oldLP;
  QBrush oldLB;
  if(LbSektoraSkan.style()==Qt::NoBrush|| !bVyvodSektoraSkanGlis)
  {
      return 1;
  }
//Начальная вершина
  Pnts[0].setX(0);
  Pnts[0].setY(0);
  Pnts[2].setX(lMaxDalnOut+lDopDalnOutOtsch);
  Pnts[1].setX(lMaxDalnOut+lDopDalnOutOtsch);
  if( lMinUgolSektoraSkan10<lMinUgol10)
  {
    Pnts[1].setY((long)(Pnts[1].x()*tan(lMinUgol10/1800.0*M_PI)));
  }else if(lMinUgolSektoraSkan10<=lMaxUgol10)
  {
    Pnts[1].setY((long)(Pnts[1].x()*tan(lMinUgolSektoraSkan10/1800.0*M_PI)));
  }else{
//Тут ощибка
    return 0;
  }
  if( lMaxUgolSektoraSkan10>lMaxUgol10)
  {
    Pnts[2].setY((long)(Pnts[2].x()*tan(lMaxUgol10/1800.0*M_PI)));
  }else if(lMaxUgolSektoraSkan10>=lMinUgol10)
  {
    Pnts[2].setY((long)(Pnts[2].x()*tan(lMaxUgolSektoraSkan10/1800.0*M_PI)));
  }else{
//Тут ощибка
    return 0;
  }

  LP.setStyle(Qt::SolidLine);
  LP.setWidth(1);
  LP.setColor(LbSektoraSkan.color());

  oldLP=pntr.pen();  pntr.setPen(LP);
  oldLB=pntr.brush(); pntr.setBrush(LbSektoraSkan);
  drawPolygonWrld(pntr,Pnts,3);
  pntr.setPen(oldLP);
  pntr.setBrush(oldLB);

  return 1;
}



//Вывести вначале сектор сканинирования
long TGlissadaDraw::VyvestiDNKursaSekt(QPainter &pntr)
{

  QPoint Pnts[3];
  QPen LP, oldLP;
  QBrush oldLB;
  if(LbUgolKurs.style()==Qt::NoBrush||
          !bVyvodUgolKursVGlis)
  {
      return 1;
  }
//Начальная вершина
  Pnts[0].setX(0);
  Pnts[0].setY(0);
  Pnts[2].setX(lMaxDalnOut+lDopDalnOutOtsch);
  Pnts[1].setX(lMaxDalnOut+lDopDalnOutOtsch);
  if( lMinUgolKurs10<lMinUgol10)
  {
    Pnts[1].setY((long)(Pnts[1].x()*tan(lMinUgol10/1800.0*M_PI)));
  }else if(lMinUgolKurs10<=lMaxUgol10)
  {
    Pnts[1].setY((long)(Pnts[1].x()*tan(lMinUgolKurs10/1800.0*M_PI)));
  }else{
//Тут ощибка
    return 0;
  }
  if( lMaxUgolKurs10>lMaxUgol10)
  {
    Pnts[2].setY((long)(Pnts[2].x()*tan(lMaxUgol10/1800.0*M_PI)));
  }else if(lMaxUgolKurs10>=lMinUgol10)
  {
    Pnts[2].setY((long)(Pnts[2].x()*tan(lMaxUgolKurs10/1800.0*M_PI)));
  }else{
//Тут ощибка
    return 0;
  }

  LP.setStyle(Qt::SolidLine);
  LP.setWidth(1);
  LP.setColor(LbUgolKurs.color());


  oldLP=pntr.pen(); pntr.setPen(LP);
  oldLB=pntr.brush(); pntr.setBrush(LbUgolKurs);
  drawPolygonWrld(pntr,Pnts,3);

  pntr.setPen(oldLP);
  pntr.setBrush(oldLB);

  return 1;
}




long TGlissadaDraw::VyvestiSektorGlissady(QPainter &pntr)
{
  QPoint Pnts[3];

  double dX,dY;
  long Ugol10;
  double Rast;  //Наклонная дальность
  QPen LP, oldLP;
  QBrush oldLB;

//Начальная вершина
  Pnts[0].setX(lDopDalnOut);
  Pnts[0].setY(lDopVysotaGlis);
  if(lMaxDalnOut>lMaxDalnostSektorGlissady)
  {
    Pnts[1].setX(lDopDalnOutOtsch+lMaxDalnostSektorGlissady);
  }else{
    Pnts[1].setX(lDopDalnOutOtsch+lMaxDalnOut);
  }
  Ugol10=lUgolLiniiGlissady10+lPlusOtUgolLiniiGlissady10;
  Pnts[1].setY((long)(lDopVysotaGlis+(Pnts[1].x()-Pnts[0].x()))*tan(Ugol10/1800.0*M_PI));
  dX=(Pnts[1].x()-Pnts[0].x())/100.0;
  dY=(Pnts[1].y()-Pnts[0].y())/100.0;
  Rast=100*sqrt(dX*dX+dY*dY);
  Ugol10=lUgolLiniiGlissady10-lMinusOtUgolLiniiGlissady10;
  Pnts[2].setX((long)(lDopDalnOut+Rast*cos(Ugol10/1800.0*M_PI)));
  Pnts[2].setY((long)(lDopVysotaGlis+Rast*sin(Ugol10/1800.0*M_PI)));
  LP.setStyle(Qt::SolidLine);
  LP.setWidth(1);
  LP.setColor(LbSektorGlissady.color());

  oldLP=pntr.pen(); pntr.setPen(LP);
  oldLB=pntr.brush(); pntr.setBrush(LbSektorGlissady);

  drawPolygonWrld(pntr,Pnts,3);
  pntr.setPen(oldLP);
  pntr.setBrush(oldLB);
  return 1;
}



long TGlissadaDraw::VyvestiUglomer(QPainter &pntr, long Gradus10)
{
  QPen lp;
  long X,Y,X0,Y0;
  QFont Font;
  char Strka[10];  //Строка с выводимым текстом

  if(Gradus10%lOpornUglRazmer10==0)
  {
//Это опорный угол.
    lp=LpOpornUglPen;

//Рассчитаем начальную координату
    X=lMaxDalnOut+lDopDalnOutOtsch;
    Y=(long)(X*tan(Gradus10/1800.0*M_PI));
//Создадим фонт

    Font=createFontFromFamalyAndSize(DEFAULT_FAMILY_FONT,szVyvodOpornUglText,false);
    //Сформируем строку с текстом
    sfSPRINTF(Strka,"%g%s",Gradus10/10.0,"°");

    textOutWrld(pntr,X,Y,Font,cVyvodOpornUglText,QString(Strka),QTT_pravee,4);
    X0=0;
    Y0=0;

  }else{
    if(!bVyvodUglomer)return 1;
    lp=LpUglomerPen;
    X=lMaxDalnOut+lDopDalnOutOtsch;
    Y=(long)(X*tan(Gradus10/1800.0*M_PI));
    X0=(long)(lUglomerProcDaln/100.0*X);
    Y0=(long)(lUglomerProcDaln/100.0*Y);

    if(bVyvodUglomerText) //Выводить ли опорный текст
    {
//Вывести текст
//Создадим фонт

       Font=createFontFromFamalyAndSize(DEFAULT_FAMILY_FONT,szVyvodUglomerText,false);

//Сформируем строку с текстом
       sfSPRINTF(Strka,"%g%s",Gradus10/10.0,"°");
//Выведем этот текст в это место

       textOutWrld(pntr,X,Y,Font,cVyvodOpornUglText,QString(Strka),QTT_pravee,4);
    }
  }

  RisovatLiniuDlyaGlissady(pntr,X,Y,X0,Y0,lp);

  return 1;
}




long TGlissadaDraw::VyvestiLinVertUdalenia(QPainter &pntr, long Daln_m)
{
  QPen lp;
  long X1,Y1,X2,Y2;
  QFont Font;
  char Strka[10];  //Строка с выводимым текстом
  if(Daln_m%lOporLiniiVertUdalRazmer==0)
  {
//Опорная линий горизонтального удаления
    lp=LpOporLiniiVertUdal;
  //Определим координаты начала линии
    X1=lDopDalnOutOtsch+Daln_m;
    Y1=(long)(X1*tan(lMaxUgol10/1800.0*M_PI))-10*dfKoefScrYToWrldY;

//Расчитаем координаты второй точки
    X2=X1;
    Y2=(long)(X2*tan(lMinUgol10/1800.0*M_PI))+10*dfKoefScrYToWrldY;

    Font=createFontFromFamalyAndSize(DEFAULT_FAMILY_FONT,szTextOporVertUdal,false);

//Сформируем строку с текстом
    sfSPRINTF(Strka,"%g",Daln_m/1000.0);
    textOutWrld(pntr,X1,Y1,Font,cTextOporVertUdal,QString(Strka),QTT_vyshe,3);
    textOutWrld(pntr,X2,Y2,Font,cTextOporVertUdal,QString(Strka),QTT_nije,3);

  }else{
//Не опорная линия равного удаления
    if(!bVyvodLiniiVertUdal)return 1;

    lp=LpLiniiVertUdal;
  //Определим координаты начала линии

    X1=lDopDalnOutOtsch+Daln_m;
    Y1=(long)(X1*tan(lMaxUgol10/1800.0*M_PI))-10*dfKoefScrYToWrldY;
    X2=X1;
    Y2=(long)(X2*tan(lMinUgol10/1800.0*M_PI))+10*dfKoefScrYToWrldY;

    if(bVyvodTextVertUdal)
    {


       Font=createFontFromFamalyAndSize(DEFAULT_FAMILY_FONT,szTextVertUdal,false);


//Сформируем строку с текстом
       sfSPRINTF(Strka,"%g",Daln_m/1000.0);
       textOutWrld(pntr,X1,Y1,Font,cTextVertUdal,QString(Strka),QTT_vyshe,3);
       textOutWrld(pntr,X2,Y2,Font,cTextVertUdal,QString(Strka),QTT_nije,3);
    }
  }

  RisovatLiniuDlyaGlissady(pntr,X1,Y1,
                           X2,Y2,lp);
  return 1;

}


//Вывести Линию Глиссады
long TGlissadaDraw::VyvestiLiniuGlissady(QPainter &pntr)
{
   long X1,Y1,X2,Y2;
   Qt::BGMode OldBkMode;
   OldBkMode=pntr.backgroundMode(); pntr.setBackgroundMode(Qt::TransparentMode);

   if(lUgolLiniiGlissady10<lMinUgol10||
      lUgolLiniiGlissady10>lMaxUgol10)
   {
       pntr.setBackgroundMode(OldBkMode);
       return 1;
   }
   X1=lDopDalnOut;
   Y1=lDopVysotaGlis;
   if(lMaxDalnOut>lMaxDalnostLiniiGlissady)
   {
     X2=lDopDalnOutOtsch+lMaxDalnostLiniiGlissady;
   }else{
     X2=lDopDalnOutOtsch+lMaxDalnOut;
   }
   Y2=(long)(lDopVysotaGlis+(X2-X1)*tan(lUgolLiniiGlissady10/1800.0*M_PI));


//Выведем саму линию
   RisovatLiniuDlyaGlissady(pntr,X1,Y1,X2,Y2,LpLiniiGlissady);
   pntr.setBackgroundMode(OldBkMode);
   return 1;
}



//Вывести линии диапазон сканирования
long TGlissadaDraw::VyvestiSekrorSkanirovaniya(QPainter &pntr)
{
  long X1,Y1,X2,Y2;
  Qt::BGMode OldBkMode;

  if(!bVyvodSektoraSkanGlis)
  {
    return 1;
  }

  OldBkMode=pntr.backgroundMode(); pntr.setBackgroundMode(Qt::TransparentMode);
  X1=Y1=0;
  if(lMinUgolSektoraSkan10>=lMinUgol10&&
     lMinUgolSektoraSkan10<=lMaxUgol10)
  {
     X2=lMaxDalnOut+lDopDalnOutOtsch;
     Y2=(long)(X2*tan(lMinUgolSektoraSkan10/1800.0*M_PI));
     RisovatLiniuDlyaGlissady(pntr,X1,Y1,X2,Y2,LpSektoraSkan);
  }

  if(lMaxUgolSektoraSkan10>=lMinUgol10&&
     lMaxUgolSektoraSkan10<=lMaxUgol10)
  {
     X2=lMaxDalnOut+lDopDalnOutOtsch;
     Y2=(long)(X2*tan(lMaxUgolSektoraSkan10/1800.0*M_PI));
     RisovatLiniuDlyaGlissady(pntr,X1,Y1,X2,Y2,LpSektoraSkan);
  }
  pntr.setBackgroundMode(OldBkMode);
  return 1;
}


//Вывести линии диапазон сканирования
long TGlissadaDraw::VyvestiDNKursa(QPainter &pntr)
{
  QPen Pen,OldPen;
  long X1,Y1,X2,Y2,X3,Y3,X4;
  QBrush Brush, OldBrush;
  QPen LPOkr, oldLP;
  QBrush LBOkr, oldLB;
  Qt::BGMode OldBkMode;   //Старый режим вывода

  if(!bVyvodUgolKursVGlis)
  {
      return 1;
  }

  OldBkMode=pntr.backgroundMode(); pntr.setBackgroundMode(Qt::TransparentMode);
  X1=Y1=0;
  X2=lMaxDalnOut+lDopDalnOutOtsch;
  Y2=(long)(X2*tan(lMinUgolKurs10/1800.0*M_PI));

  if(lMinUgolKurs10>=lMinUgol10&&
     lMinUgolKurs10<=lMaxUgol10)
  {
    RisovatLiniuDlyaGlissady(pntr,X1,Y1,X2,Y2,LpUgolKurs);
  }

  X3=lMaxDalnOut+lDopDalnOutOtsch;
  Y3=(long)(X2*tan(lMaxUgolKurs10/1800.0*M_PI));

  if(lMaxUgolKurs10>=lMinUgol10&&
     lMaxUgolKurs10<=lMaxUgol10)
  {
    RisovatLiniuDlyaGlissady(pntr,X1,Y1,X3,Y3,LpUgolKurs);
  }

  LPOkr.setStyle(Qt::SolidLine);
  LPOkr.setWidth(1);
  LPOkr.setColor(LpUgolKurs.color());
  LBOkr.setStyle(Qt::NoBrush);

  oldLP=pntr.pen();  pntr.setPen(LPOkr);

  oldLB=pntr.brush(); pntr.setBrush(LBOkr);

//Отсчитаем пять логичесчих симолова справо.
  X4=(long)(lMaxDalnOut+lDopDalnOutOtsch-2*dfKoefScrXToWrldX);
  X2=(long)(X4-5*dfKoefScrXToWrldX); X3=(long)(X4+5*dfKoefScrXToWrldX);

  if(lMaxUgolKurs10>=lMinUgol10&&lMaxUgolKurs10<=lMaxUgol10)
  {
    Y2=(long)(X2*tan(lMaxUgolKurs10/1800.0*M_PI));
  }else if(lMaxUgolKurs10>lMaxUgol10)
  {
    Y2=(long)(X2*tan(lMaxUgol10/1800.0*M_PI));
  }else{
    Y2=-1;
  }

  if(lMinUgolKurs10>=lMinUgol10&&lMinUgolKurs10<=lMaxUgol10)
  {
     Y3=(long)(X3*tan(lMinUgolKurs10/1800.0*M_PI));
  }else if(lMinUgolKurs10<lMinUgol10)
  {
     Y3=(long)(X3*tan(lMinUgol10/1800.0*M_PI));
  }else{
     Y3=-1;   //Иначе значение Y=-1
  }

  if(Y2!=-1&&Y3!=-1)
  {
//вопрос    pntr.drawArc(X2,Y2,X3,Y3,X2,Y2,X2,Y2);
  }

  pntr.setPen(oldLP);
  pntr.setBrush(oldLB);
  pntr.setBackgroundMode(OldBkMode);
  return 1;
}

//Вывести линии равных равных высот
long TGlissadaDraw::VyvestiLiniiRavnyhVysot(QPainter &pntr)
{
  long i;
  QFont Font;
  long X1,Y1,X2,Y2;
  char Strka[10];
  QColor OldCol;
  long dVys;
  long lMaxVysota=(long)((lMaxDalnOut+lDopDalnOutOtsch)*tan(lMaxUgol10/1800.0*M_PI));

//Посчитаем число линий равных высот
  if (N_of_RavnyhVysot==0)return 0;
  dVys=lMaxVysota/(N_of_RavnyhVysot+1);
  if (dVys<10)return 0;
/*
  if (dVys<50)
  {
    dVys=25;
  }else if(dVys<100)
  {
    dVys=50;
  }else if(dVys<200)
  {
    dVys=100;
  }else if(dVys<250)
  {
    dVys=200;
  }else if(dVys<500)
  {
    dVys=250;
  }else if(dVys<1000)
  {
    dVys=500;
  }else if(dVys<2000)
  {
    dVys=1000;
  }else{
    dVys=2000;
  }


*/
  if(lSposobVyvodaRavnyhVysot)
  {
//Установим начальную высоту
    lRavnyhVysotZnach[0]=lPervayaVysotaRavnyhVysot;
    for(i=1;i<N_of_RavnyhVysot;i++)
    {
      lRavnyhVysotZnach[i]=lRavnyhVysotZnach[i-1]+lShagVysotyRavnyhVysot;
    }
  }else{
    if (dVys<100)
    {
      dVys=(dVys/10)*10;
    }else if(dVys<500)
    {
      dVys=(dVys/50)*50;
    }else if(dVys<1000)
    {
      dVys=(dVys/100)*100;
    }else if(dVys/5000)
    {
     dVys=(dVys/500)*500;
    }else{
     dVys=(dVys/1000)*1000;
    }




    for(i=0;i<N_of_RavnyhVysot;i++)
    {
      lRavnyhVysotZnach[i]=i*dVys+dVys;
    }
  }

  if(bVyvodRavnyhVysot)
  {
    for(i=0;i<N_of_RavnyhVysot;i++)
    {

        if(lRavnyhVysotZnach[i]>=lMaxVysota)continue;



        if(bVyvodTextRavnyhVysot)
        {
            Font=createFontFromFamalyAndSize(DEFAULT_FAMILY_FONT,szRavnyhVysotText,false);

//Сформируем текст
            sfSPRINTF(Strka,"%ld",lRavnyhVysotZnach[i]);

 //Найдем координаты откуда можно начать рисовать точку
            Y1=Y2=lRavnyhVysotZnach[i];
            X1=(long)(Y1/tan(lMaxUgol10/1800.0*M_PI));
            X2= (long)(lMaxDalnOut+lDopDalnOutOtsch-20*dfKoefScrXToWrldX);

            textOutWrld(pntr,X1,Y1,Font,cRavnyhVysotText,QString(Strka),QTT_levee,5);
        }
//Восстановить данные
        //Вывести прямую
        RisovatLiniuDlyaGlissady(pntr,X1,Y1,X2,Y2,LpRavnyhVysotPen);
     }
  }
  return 1;
}


//Вывести маяки
long TGlissadaDraw::VyvestiBPRMandDPRMGlis(QPainter &pntr)
{

//Разберемся с маяком в ближней зоне
   if(bBPRMGlis&&lPolojenieBPRM>0&&lPolojenieBPRM<=lMaxDalnOut)
   {

     RisovatLiniuDlyaGlissady(pntr,lPolojenieBPRM+lDopDalnOutOtsch,
          (long)((lDopDalnOutOtsch+lMaxDalnOut)*tan(lMinUgol10/1800.0*M_PI)),
          lPolojenieBPRM+lDopDalnOutOtsch,
         (long)((lDopDalnOutOtsch+lMaxDalnOut)*tan(lMaxUgol10/1800.0*M_PI)),
         lpBPRMGlis);
   }

   if(bDPRMGlis&&lPolojenieDPRM>0&&lPolojenieDPRM<=lMaxDalnOut)
   {


     RisovatLiniuDlyaGlissady(pntr,lPolojenieDPRM+lDopDalnOutOtsch,
          (long)((lDopDalnOutOtsch+lMaxDalnOut)*tan(lMinUgol10/1800.0*M_PI)),
          lPolojenieDPRM+lDopDalnOutOtsch,
         (long)((lDopDalnOutOtsch+lMaxDalnOut)*tan(lMaxUgol10/1800.0*M_PI)),
         lpDPRMGlis);

   }
   return 1;
}




/*Вспомогательная процедура для рисования линий*/
long TGlissadaDraw::RisovatLiniuDlyaGlissady(QPainter &pntr,
      long X1, long Y1, long X2, long Y2,   //Координаты мировые
      const QPen &lp)
{ 
   QPen  oldLP;

   oldLP=pntr.pen(); pntr.setPen(lp);
   pntr.drawLine(
                getXscrFromXwrld(X1), getYscrFromYwrld(Y1),
                getXscrFromXwrld(X2), getYscrFromYwrld(Y2)
               );

   pntr.setPen(oldLP);
   return 1;
}






//Рисование одной цели -- промежуточная процедура
//  1 цель
     long TGlissadaDraw::RisovatOdnuCelGlis(QPainter &pntr,    //Контекст устройства
                      const QRect &Rect, //Область куда рисуется цель
                      TDannyeObCeli &DannyeObCeli,
                      TVidCeli *VidCeli, //Начинаются рассматриваться с индекса 1, чтобы удобнее было
                      long lChisloVidovCeley, //Число видов целей
                      int isSledOtmetka        //Эта отметка в следе
                    )
{

         QPen oldLP,oldLP1;
         QBrush oldLB;
         QPen lpVektor;     //Вектор
         long  PromZ1, PromZ2;  //Промежуточное значение Y и Z
         int KodAvarii;

        int X2,Z2;
        double Skor,SinSk,CosSk;  //Синус скорости, косинус скорости
        double Radius;
        bool isPaintCel=false;  //Рисовать цель


         //Надо определить,  а выводить ли эту цель вообще за пределы границы
             if(DannyeObCeli.X<=0||DannyeObCeli.X>=lMaxDalnOut+lDopDalnOutOtsch)
               return 1;

         //Разбиремся с высотой




             PromZ1=Okrugl(DannyeObCeli.X*tan(lMinUgol10/1800.0*M_PI));
             PromZ2=Okrugl(DannyeObCeli.X*tan(lMaxUgol10/1800.0*M_PI));
             if (DannyeObCeli.Z==-100000)
             {
                 DannyeObCeli.XvpGlis=getXscrFromXwrld(DannyeObCeli.X);
                 DannyeObCeli.YvpGlis=-100000;
             }else
             if(DannyeObCeli.Z>PromZ2)
             {
                 DannyeObCeli.XvpGlis=getXscrFromXwrld(DannyeObCeli.X);
                 DannyeObCeli.YvpGlis=getYscrFromYwrld(PromZ2)-10;
             }else
             if(DannyeObCeli.Z<PromZ1)
             {
                 DannyeObCeli.XvpGlis=getXscrFromXwrld(DannyeObCeli.X);
                 DannyeObCeli.YvpGlis=getYscrFromYwrld(PromZ1)+10;
             }else{
                DannyeObCeli.XvpGlis=getXscrFromXwrld(DannyeObCeli.X);
                DannyeObCeli.YvpGlis=getYscrFromYwrld(DannyeObCeli.Z);
                isPaintCel=true;
             }


             if(DannyeObCeli.XvpGlis<0)
             {
                  return 1;
             }


             if(isSledOtmetka&&!isPaintCel)
             {
                 return 1;
             }


             if((DannyeObCeli.lHazardTwo&0xFF00)==0x0200 ||
                DannyeObCeli.lHazardGlis) KodAvarii=2;
             else if((DannyeObCeli.lHazardTwo&0xFF00)==0x0100) KodAvarii=1;
             else KodAvarii=0;



             if((KodAvarii==0)||(uiFlashStateGlis==0)||isSledOtmetka!=0)
             {          
                 oldLP=pntr.pen(); pntr.setPen(VidCeli[DannyeObCeli.Tip].lpCeliGlis[0][isSledOtmetka%2]);
                 oldLB=pntr.brush(); pntr.setBrush(VidCeli[DannyeObCeli.Tip].lbCeliGlis[0][isSledOtmetka%2]);
              }else{
                 oldLP=pntr.pen(); pntr.setPen(VidCeli[DannyeObCeli.Tip].lpCeliGlis[KodAvarii][0]);
                 oldLB=pntr.brush(); pntr.setBrush(VidCeli[DannyeObCeli.Tip].lbCeliGlis[KodAvarii][0]);
              }

         //Расчитаем коодинаты прямоугольника, чтобы вписать туда круг



         //Проверим, надо рисовать вектор скорости (он определяется)
             if(isPaintCel&&
               VidCeli[DannyeObCeli.Tip].IsVyvodVektorSkorostiGlis&&
               !isSledOtmetka   //Только для текущего отсчета(не для следа)
               )
             {
         //Надо нариовать вектор скорости?
                if(DannyeObCeli.Vz!=-100000&&   //Определено направление скорости вдоль высоты
         //И хотя бы одна из составляющих не обращается в ноль
                  (DannyeObCeli.Vx!=0||DannyeObCeli.Vz!=0))
                {

                  lpVektor.setStyle(Qt::SolidLine);
                  lpVektor.setWidth(1);
                  lpVektor.setColor(VidCeli[DannyeObCeli.Tip].lbCeliGlis[0][0].color());
                  oldLP1=pntr.pen();  pntr.setPen(lpVektor);

                  Skor=sqrt(DannyeObCeli.Vx/1000.0*DannyeObCeli.Vx/1000.0+
                            DannyeObCeli.Vz/1000.0*DannyeObCeli.Vz/1000.0);

                  CosSk=DannyeObCeli.Vx/1000.0/Skor;
                  SinSk=DannyeObCeli.Vz/1000.0/Skor;
         //Расчитаем длину скорости в логическом пространстве, если она задана в физическом
                  Radius=VidCeli[DannyeObCeli.Tip].lDlinaVektoraSkorosti/
                         sqrt(SinSk/dfKoefScrYToWrldY*SinSk/dfKoefScrYToWrldY+
                              CosSk/dfKoefScrXToWrldX*CosSk/dfKoefScrXToWrldX);

                  X2=DannyeObCeli.X+Okrugl(Radius*CosSk);
                  Z2=DannyeObCeli.Z+Okrugl(Radius*SinSk);

                  pntr.drawLine(DannyeObCeli.XvpGlis,DannyeObCeli.YvpGlis,
                                getXscrFromXwrld(X2),getYscrFromYwrld(Z2));

                  pntr.setPen(oldLP1);
                }
             }

         //Теперь надо нарисовать цель
             if(isPaintCel)
             {
                pntr.drawEllipse(QPoint(DannyeObCeli.XvpGlis,DannyeObCeli.YvpGlis),
                              Okrugl(VidCeli[DannyeObCeli.Tip].lRazmerCeliGlis/2),
                              Okrugl(VidCeli[DannyeObCeli.Tip].lRazmerCeliGlis/2));
             }
            pntr.setPen(oldLP);
            pntr.setBrush(oldLB);

            return 1;
}



     //Промежуточная процедура для опреления расстояния цели до глисадной линии в метрах
     //Возвращает: > -10000 < 10000  - если цель лежит в секторе глиссады
     // -20000 - вне сектора глисады
     //20000 - вне сектора глисады
     // - координата вне пределов оси 30000
int TGlissadaDraw::getRastoyanieCeliIGlis( TDannyeObCeli &DannyeObCeli)
{
    long z_temp, z_temp1, z_temp2;
    if(DannyeObCeli.X<lDopDalnOut||DannyeObCeli.X>(lMaxDalnostSektorGlissady+lDopDalnOutOtsch))
    {
        return 30000;
    }
    //Определим значение z_temp
    z_temp=(DannyeObCeli.X-lDopDalnOut)*tan(lUgolLiniiGlissady10/1800.0*M_PI)+lDopVysotaGlis;
    z_temp1=(DannyeObCeli.X-lDopDalnOut)*
                 tan((lUgolLiniiGlissady10-lMinusOtUgolLiniiGlissady10)/1800.0*M_PI)+lDopVysotaGlis;
    z_temp2=(DannyeObCeli.X-lDopDalnOut)*
                 tan((lUgolLiniiGlissady10+lPlusOtUgolLiniiGlissady10)/1800.0*M_PI)+lDopVysotaGlis;
    if(DannyeObCeli.Z<z_temp1)return -20000;
    if(DannyeObCeli.Z>z_temp2)return 20000;
    return (DannyeObCeli.Z-z_temp);
}

