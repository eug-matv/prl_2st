﻿//---------------------------------------------------------------------------

//#include "stdafx.h"

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <QSettings>
#include "OtmetkaDannye.h"
#include "AnyTools.h"
#include "KursGraf.h"
#include "temp_work_with_ini.h"
#include "safe_function.h"

//---------------------------------------------------------------------------
#define ABS(X) (((X) > 0) ? (X) : (-(X)))
#define MAX(a, b)  (((a) > (b)) ? (a) : (b))
#define MIN(a, b)  (((a) < (b)) ? (a) : (b))








TKursDraw::TKursDraw()
{

  uiFlashStateKurs=0;
  lMaxDalnOut=20000;
  lDopDalnOut=800;
  lDopDalnOutOtsch=1500;

  lAzmtProcDaln=30;   //30 процентов
  lAzmtRazmer10=10;   //1 градус
  lpAzmt.setStyle(Qt::SolidLine);
  lpAzmt.setWidth(1);
  lpAzmt.setColor(QColor(120,120,120));
  lMinAzmt10=-200;
  lMaxAzmt10=200;
  bVyvodAzmt=true;
  cAzmtText.setRgb(0,150,150);
  szAzmtText=18;
  bVyvodAzmtText=false;

  lOporAzmtRazmer10=50; //5 градусов
  lpOporAzmt.setStyle(Qt::SolidLine);
  lpOporAzmt.setWidth(1);
  lpOporAzmt.setColor(QColor(200,200,200));
  cOporAzmtText.setRgb(0,200,200);
  szOporAzmtText=20;

  lGorUdalRazmer=1000;   //1 км
  lpGorUdal.setStyle(Qt::SolidLine);
  lpGorUdal.setWidth(1);
  lpGorUdal.setColor(QColor(120,120,120));
  bVyvodGorUdal=true;

  cGorUdalText.setRgb(0,150,150);
  szGorUdalText=18;
  bVyvodGorUdalText=true;

  lOporUdalRazmer=5000; //5 км
  lpOporGorUdal.setStyle(Qt::SolidLine);
  lpOporGorUdal.setWidth(1);
  lpOporGorUdal.setColor(QColor(200,200,200));


  lLiniaPosadKursUgol10=10;
  lpLiniaPosadKurs.setStyle(Qt::SolidLine);
  lpLiniaPosadKurs.setWidth(1);
  lpLiniaPosadKurs.setColor(QColor(0,200,0));

  lMinusOtPosadKursUgol10=20;
  lPlusOtPosadKursUgol10=20;
  lMaxDalnostZonaOtkl=20000;
  lbZonaOtkl.setStyle(Qt::SolidPattern);
  lbZonaOtkl.setColor(QColor(0,70,0));


  lSektorSkanMin10=-175;
  lSektorSkanMax10=175;
  lpSektorSkan.setStyle(Qt::DashLine);
  lpSektorSkan.setWidth(1);
  lpSektorSkan.setColor(QColor(0,0,180));
  lbSektorSkan.setStyle(Qt::SolidPattern);
  lbSektorSkan.setColor(QColor(0,0,30));

  lDNMin10=-60;
  lDNMax10=45;
  lpLDN.setStyle(Qt::DashLine);;
  lpLDN.setWidth(1);
  lpLDN.setColor(QColor(180,0,0));
  lbLDN.setStyle(Qt::SolidPattern);
  lbLDN.setColor(QColor(30,0,0));

  lPolojenieBPRM=4200;
  bBPRMKurs=true;
  lpBPRMKurs.setStyle(Qt::SolidLine);
  lpBPRMKurs.setWidth(3);
  lpBPRMKurs.setColor(QColor(255,20,100));
  lbBPRMKurs.setStyle(Qt::SolidPattern);
  lbBPRMKurs.setColor(QColor(0,10,50));
  szBPRMKurs.setWidth(15);   szBPRMKurs.setHeight(15);

  lPolojenieDPRM=6200;
  bDPRMKurs=true;
  lpDPRMKurs.setStyle(Qt::SolidLine);
  lpDPRMKurs.setWidth(3);
  lpDPRMKurs.setColor(QColor(200,0,200));
  lbDPRMKurs.setStyle(Qt::SolidPattern);
  lbDPRMKurs.setColor(QColor(50,0,100));
  szDPRMKurs.setWidth(15);   szDPRMKurs.setHeight(15);

  cOporGorUdalText.setRgb(0,200,200);
  szOporGorUdalText=20;

}



long TKursDraw::PaintKurs(QPainter &pntr,     //Конектс устройства
                  const QRect &Rect) //Ограниченная область куда должна вписаться
{
  int bRet;   //Возвращаемое значение
  long TekUgol10;  //Текущее значение угла
  long TekDaln_m; //Текущая дальность в метрах
  long lRet;

//Этап 0. Надо определить диапазон в пикеселях рисования сетки
//Опроделим - отступ сверху для вывода текста.
  ZadanieDopDataOut(Rect);






//Этап 1. Нарисовать сектора
//Этап 1а Вывести сектор сканирования
   lRet=VyvestiSektorSkanSekt(pntr);

//Этап 1б. Вывести сектор диаграммы направленности
   lRet=VyvestiLiniuDNSekt(pntr);

//Этап 1в
    lRet=VyvestiZonuDopustOtkl(pntr);



//Этап 2. Рисовать азимутальные линии
  if(lMinAzmt10<0)
  {
//Рисуем азимут.линии справа
    TekUgol10=0;
    for(;TekUgol10>=lMinAzmt10;TekUgol10-=lAzmtRazmer10)
    {
      lRet=VyvestiLiniuAzimuta(pntr,TekUgol10);
    }
  }
  if(lMaxAzmt10>0)
  {
//Рисуем азимут.линии слева
    TekUgol10=lAzmtRazmer10;
    for(;TekUgol10<=lMaxAzmt10;TekUgol10+=lAzmtRazmer10)
    {
      lRet=VyvestiLiniuAzimuta(pntr,TekUgol10);
    }
  }

//Этап 3. Рисовать  линии горизонтального удаления
  for(TekDaln_m=0;TekDaln_m<=lMaxDalnOut;TekDaln_m+=lGorUdalRazmer)
  {
     lRet=VyvestiLiniuGorUdal(pntr,TekDaln_m);
  }


//Этап 4. Рисовать линию посадного
   lRet=VyvestiLiniuPosadKursa(pntr);

//Этап 5. Значки маяков БПРМ и ДПРМ
   lRet=VyvestiBPRMandDPRMKurs(pntr);

//Этап 6. Рисовать границы сектора сканирования
   lRet=VyvestiSektorSkanLin(pntr);


//Этап 7. Сектор k направленности
   lRet=VyvestiLiniuDNLin(pntr);
  return 1;

}



//Рисовать цели
long TKursDraw::RisovatCeliKurs(
                 QPainter &pntr,    //Контекст устройства
                 const QRect &Rect, //Область куда рисуется цель
                 list<TDannyeObCeli> &ldoc,
                 list<TDannyeObCeli> &ldoc_tail,
                 TVidCeli *VidCeli, //Начинаются рассматриваться с индекса 1, чтобы удобнее было
                 long lChisloVidovCeley //Число видов целей
                )
{
  int bRet;   //Возвращаемое значение
  QFont font;
  long i;      //Индекс для счетчика
  char Strka[100];
  long szStrka;   //Длина строки Strka
  long Xt,Yt;     //Начало положения текста

  list<TDannyeObCeli>::iterator it;

//Этап 0. Надо определить диапазон в пикеселях рисования сетки
//Опроделим - отступ сверху для вывода текста.
  ZadanieDopDataOut(Rect);



  it=ldoc_tail.begin();
  while(it!=ldoc_tail.end())
  {
      RisovatOdnuCelKurs(pntr,Rect,(*it),VidCeli,lChisloVidovCeley,1);
      ++it;
  }

  it=ldoc.begin();
  while(it!=ldoc.end())
  {
      RisovatOdnuCelKurs(pntr,Rect,(*it),VidCeli,lChisloVidovCeley,0);
      ++it;
  }

  //А теперь выведем текстовую информацию, если есть необходимость
  //А тут вывести текст
  it=ldoc.begin();
  for(it=ldoc.begin();it!=ldoc.end();)
  {

    if(
       (*it).X<0|| //Дальность слишком маленькая
       (*it).X>(lMaxDalnOut+lDopDalnOutOtsch)||//Или за пределы выходит отображения
       !VidCeli[(*it).Tip].IsVyvodTextKurs|| //Не надо выводить текст в поле глиссады
       (*it).Y==-100000 //|| //Не определена высота, что выводить-то?
     //  !(*it).VyhodOtklKurs_3 //Нет выхода за пределы сектора глиссады
      )
    {
        ++it;
        continue;  //Тогда переходим к след элементу
    }
//Установим цвет текста

 //Выведем теперь текст.
    if(VidCeli[(*it).Tip].IsVyvodTextKurs)
    {
      if((*it).XvpGlis>=0&&(*it).YvpGlis>=0)
      {

//Сформируем строку с текстом, которую надо вывести
        if((*it).lOtklKurs<=-10)
        {
            sfSPRINTF(Strka,"П%03ld",-(*it).lOtklKurs/10*10);
        }else
        if((*it).lOtklKurs>=10)
        {
            sfSPRINTF(Strka,"Л%03ld",(*it).lOtklKurs/10*10);
        }else{
            sfSPRINTF(Strka,"000");
        }

        font=createFontFromFamalyAndSize(DEFAULT_FAMILY_FONT,VidCeli[(*it).Tip].szTextKurs,false);

         textOutScr(pntr,(*it).XvpKurs,(*it).YvpKurs,font, VidCeli[(*it).Tip].cTextKurs,
                    QString(Strka),QTT_levee, VidCeli[(*it).Tip].lRazmerCeliKurs/2+4);

      }
    }
    ++it;
  }
  return 1;
}





long TKursDraw::SaveDataToINI(const char* lpszINIFileName)
{
    QSettings *stng;
    stng=new QSettings(QString(lpszINIFileName),QSettings::IniFormat);

    SAVE_UGOL_TO_INI("VizDiapazonUglKurs",lMaxAzmt10/10.0);

    SAVE_LOGPEN_TO_INI("LOGPEN_AzimutLines",lpAzmt)
    SAVE_BOOL_TO_INI("VisibleAzimutLines",bVyvodAzmt)
    SAVE_COLORREF_TO_INI("ColorAzimutText",cAzmtText)
    SAVE_TO_INI("SizeAzimutText",szAzmtText)
    SAVE_BOOL_TO_INI("VisibleAzimutText",bVyvodAzmtText)
    SAVE_LOGPEN_TO_INI("LOGPEN_OAzimutLines",lpOporAzmt)
    SAVE_COLORREF_TO_INI("ColorOAzimutText",cOporAzmtText)
    SAVE_TO_INI("SizeOAzimutText",szOporAzmtText)
    SAVE_LOGPEN_TO_INI("LOGPEN_HorLines",lpGorUdal)
    SAVE_BOOL_TO_INI("VisibleHorLines",bVyvodGorUdal)
    SAVE_COLORREF_TO_INI("ColorHorLinesText",cGorUdalText)
    SAVE_TO_INI("SizeHorLinesText",szGorUdalText)
    SAVE_BOOL_TO_INI("VisibleHorLinesText",bVyvodGorUdalText)
    SAVE_LOGPEN_TO_INI("LOGPEN_OHorLines",lpOporGorUdal)
    SAVE_COLORREF_TO_INI("ColorOHorLinesText",cOporGorUdalText)
    SAVE_TO_INI("SizeOHorLinesText",szOporGorUdalText)
    SAVE_LOGPEN_TO_INI("LOGPEN_CourseLine",lpLiniaPosadKurs)
    SAVE_LOGBRUSH_TO_INI("LOGBRUSH_CourseSector",lbZonaOtkl)
    SAVE_BOOL_TO_INI("VisisbleSektoraSkanKurs",bVyvodSektoraSkanKurs)
    SAVE_LOGPEN_TO_INI("LOGPEN_ScanSector",lpSektorSkan)
    SAVE_LOGBRUSH_TO_INI("LOGBRUSH_ScanSector",lbSektorSkan)
    SAVE_BOOL_TO_INI("VisibleUgolGlisVKurs",bVyvodUgolGlisVKurs)
    SAVE_LOGPEN_TO_INI("LOGPEN_DiagramGlis",lpLDN)
    SAVE_LOGBRUSH_TO_INI("LOGBRUSH_DiagramGlis",lbLDN)
    delete stng;
    return 1;
}



long TKursDraw::LoadDataFromINI(const char* lpszINIFileName)
{
    char  Strka[80], DefStrka[80];
    long lRet;
    double ugl1,ugl2;

    QSettings *stng;
    stng=new QSettings(QString(lpszINIFileName),QSettings::IniFormat);

    LOAD_UGOL_FROM_INI("VizDiapazonUglKurs",ugl2,"5");


    NovyyMasshtabAzimuta(ugl2);

    LOAD_LOGPEN_FROM_INI("LOGPEN_AzimutLines",lpAzmt,PS_SOLID,1,1,RGB(100,100,100))
    LOAD_BOOL_FROM_INI("VisibleAzimutLines",bVyvodAzmt,false)
    LOAD_COLORREF_FROM_INI("ColorAzimutText",cAzmtText,RGB(0,150,150))
    LOAD_FROM_INI("SizeAzimutText",szAzmtText,"18")
    LOAD_BOOL_FROM_INI("VisibleAzimutText",bVyvodAzmtText,false)
    LOAD_LOGPEN_FROM_INI("LOGPEN_OAzimutLines",lpOporAzmt,PS_SOLID,1,1,RGB(170,170,170))
    LOAD_COLORREF_FROM_INI("ColorOAzimutText",cOporAzmtText,RGB(0,200,200))
    LOAD_FROM_INI("SizeOAzimutText",szOporAzmtText,"20")
    LOAD_LOGPEN_FROM_INI("LOGPEN_HorLines",lpGorUdal,PS_SOLID,1,1,RGB(100,100,100))
    LOAD_BOOL_FROM_INI("VisibleHorLines",bVyvodGorUdal,true)
    LOAD_COLORREF_FROM_INI("ColorHorLinesText",cGorUdalText,RGB(0,150,150))
    LOAD_FROM_INI("SizeHorLinesText",szGorUdalText,"18")
    LOAD_BOOL_FROM_INI("VisibleHorLinesText",bVyvodGorUdalText,false)
    LOAD_LOGPEN_FROM_INI("LOGPEN_OHorLines",lpOporGorUdal,PS_SOLID,1,1,RGB(170,170,170))
    LOAD_COLORREF_FROM_INI("ColorOHorLinesText",cOporGorUdalText,RGB(0,200,200))
    LOAD_FROM_INI("SizeOHorLinesText",szOporGorUdalText,"20")
    LOAD_LOGPEN_FROM_INI("LOGPEN_CourseLine",lpLiniaPosadKurs,PS_SOLID,1,1,RGB(0,200,0))
    LOAD_LOGBRUSH_FROM_INI("LOGBRUSH_CourseSector",lbZonaOtkl, BS_SOLID, RGB(0,50,0),1)
    LOAD_BOOL_FROM_INI("VisisbleSektoraSkanKurs",bVyvodSektoraSkanKurs,true)
    LOAD_LOGPEN_FROM_INI("LOGPEN_ScanSector",lpSektorSkan,PS_DASH,1,1,RGB(0,0,180))
    LOAD_LOGBRUSH_FROM_INI("LOGBRUSH_ScanSector",lbSektorSkan,BS_SOLID,RGB(0,0,50),1)        
    LOAD_BOOL_FROM_INI("VisibleUgolGlisVKurs",bVyvodUgolGlisVKurs,false)
    LOAD_LOGPEN_FROM_INI("LOGPEN_DiagramGlis",lpLDN,PS_DASH,1,1,RGB(220,0,0))
    LOAD_LOGBRUSH_FROM_INI("LOGBRUSH_DiagramGlis",lbLDN,BS_SOLID,RGB(40,0,0),1)

    delete stng;

    return 1;
}



/*ЧАСТНЫЕ ПРОЦЕДУРЫ*/
long TKursDraw::ZadanieDopDataOut(const QRect &Rect)
{

  RectOut=Rect;

  min_otkl_kurs_m=Okrugl((lMaxDalnOut+lDopDalnOutOtsch)*tan(lMinAzmt10/1800.0*M_PI));
  max_otkl_kurs_m=Okrugl((lMaxDalnOut+lDopDalnOutOtsch)*tan(lMaxAzmt10/1800.0*M_PI));

  dfKoefWrldXToScrY=RectOut.height()/((double)(lMaxDalnOut+lDopDalnOutOtsch));
  dfKoefWrldYToScrX=RectOut.width()/(double)((min_otkl_kurs_m-max_otkl_kurs_m)); //Этот знак меньше 0
  dfKoefScrXToWrldY=1.0/dfKoefWrldYToScrX;
  dfKoefScrYToWrldX=1.0/dfKoefWrldXToScrY;

  pScrFor0.setY(RectOut.top());
  pScrFor0.setX(Okrugl(RectOut.right()-dfKoefWrldYToScrX*(min_otkl_kurs_m)));



  return 1;
}


long TKursDraw::VyvestiLiniu(QPainter &pntr, //Дескриптор устройства
                    long Azmt10,  //Значение угла азимутов в градусах умноженных на 10
                    const QPen &lp,  //Описание логического пера
                    long FirstDalnost, //Обычно равно или 0, или lDopDalnOut
                    long OstupOtKursa, //Если >0, то будет левее, если больше, то правее НК
                    long *Xlast, long *Ylast //Куда помещается помещается последняя выделенная точка
                    )
{

  long X1,Y1,X2,Y2;  

  X1=FirstDalnost;
  Y1=OstupOtKursa;
  X2=lMaxDalnOut+lDopDalnOutOtsch;
  if(Azmt10<lMinAzmt10)
  {
    if(Xlast)(*Xlast)=(long)(lMaxDalnOut+lDopDalnOutOtsch);
    if(Ylast)*Ylast=(long)(OstupOtKursa+(X2-X1)*tan(lMinAzmt10/1800.0*M_PI));
//Точка сама правее - вывода не делаем
    return 2;
  }
  if(Azmt10>lMaxAzmt10)
  {
    if(Xlast)(*Xlast)=(long)(lMaxDalnOut+lDopDalnOutOtsch);
    if(Ylast)(*Ylast)=(long)(OstupOtKursa+(X2-X1)*tan(lMaxAzmt10/1800.0*M_PI));
    return 3;
  }

  Y2=(long)(OstupOtKursa+(X2-X1)*tan(Azmt10/1800.0*M_PI));
  if(Xlast)*Xlast=X2;
  if(Ylast)*Ylast=Y2;
  RisovatLiniuDlyaKursa(pntr,X1,Y1,X2,Y2,lp);
  return 1;
}


long TKursDraw::VyvodSektora(QPainter &pntr,
                    long MinSAzmt10,
                    long MaxSAzmt10,
                    const QBrush &lb,
                    long FirstDalnost,
                    long OstupOtKursa, //Если <0, то будет правее, если больше, то левее
                    long MaxSDalnost)
{

  QPoint Pnt[3];
  QPen  lp, oldLp;
  QBrush  oldLb;
  if(!FirstDalnost)
  {
    MinSAzmt10=MAX(MinSAzmt10, lMinAzmt10);
    MaxSAzmt10=MIN(MaxSAzmt10, lMaxAzmt10);
  }

  Pnt[0].setY(OstupOtKursa);
  Pnt[0].setX(FirstDalnost);

//Определим дальность
  Pnt[2].setX(MIN(MaxSDalnost+FirstDalnost,lMaxDalnOut+lDopDalnOutOtsch));
  Pnt[1].setX(MIN(MaxSDalnost+FirstDalnost,lMaxDalnOut+lDopDalnOutOtsch));

  Pnt[2].setY((long)(OstupOtKursa+(Pnt[2].x()-Pnt[0].x())*tan(MinSAzmt10/1800.0*M_PI)));
  Pnt[1].setY((long)(OstupOtKursa+(Pnt[1].x()-Pnt[0].x())*tan(MaxSAzmt10/1800.0*M_PI)));
  lp.setStyle(Qt::SolidLine);
  lp.setWidth(1);
  lp.setColor(lp.color());


  oldLp=pntr.pen(); pntr.setPen(lp);
  oldLb=pntr.brush(); pntr.setBrush(lb);


  drawPolygonWrld(pntr, Pnt, 3);
  pntr.setPen(oldLp);
  pntr.setBrush(oldLb);

  return 1;
}





/*Вывод линии азимута: Azmt10 - значение угла в градусах умноженное на 10*/
long TKursDraw::VyvestiLiniuAzimuta(QPainter &pntr, long Azmt10)
{
  QPen lp;
  long X,Y,X0,Y0;
  QFont Font;
  char Strka[10];  //Строка с выводимым текстом


//Проверим, а не опорный ли это азимут
  if((Azmt10%lOporAzmtRazmer10)==0)
  {
//Опорный азимут


    X=lMaxDalnOut+lDopDalnOutOtsch;
    Y=(long)(X*tan(Azmt10/1800.0*M_PI)); //Так как положительные значения азимутов слева,
                                  //А отрицательные справа

    lp=lpOporAzmt;

//Создадим шрифт  
    Font=createFontFromFamalyAndSize(DEFAULT_FAMILY_FONT, szOporAzmtText, false);

//Сформируем строку с текстом
    sfSPRINTF(Strka,"%g%s",Azmt10/10.0,"°");

//Выведем этот текст в это место
    textOutWrld(pntr,X,Y,Font,cOporAzmtText,QString(Strka),QTT_nije,5);
    X0=Y0=0;
  }else{
  //Если не надо выводить не опорную линию, то не будем выводить ее
    if(!bVyvodAzmt)return 1;

    X=lMaxDalnOut+lDopDalnOutOtsch;
    Y=(long)(X*tan(Azmt10/1800.0*M_PI));
    X0=(long)(lAzmtProcDaln/100.0*X);
    Y0=(long)(lAzmtProcDaln/100.0*Y);
    lp=lpAzmt;
    if(bVyvodAzmtText)
    {

//Создадим шрифт
       Font=createFontFromFamalyAndSize(DEFAULT_FAMILY_FONT, szAzmtText,false);

//Сформируем строку с текстом
       sfSPRINTF(Strka,"%g%s",Azmt10/10.0,"°");


//Выведем этот текст в это место
       textOutWrld(pntr,X,Y,Font,cAzmtText,QString(Strka),QTT_nije,5);
     }//if bVyvodAzmtText
  }

  RisovatLiniuDlyaKursa(pntr,X,Y,X0,Y0,lp);
  return 1;
}


//Нарисовать линию горизонтального удаления
long TKursDraw::VyvestiLiniuGorUdal(QPainter &pntr, long Udal_m)
{
  QPen lp;
  long X1,Y1,X2,Y2;
  QFont Font;
  char Strka[10];  //Строка с выводимым текстом


//Проверим опорная ли линия горизонтального удаления
  if(Udal_m%lOporUdalRazmer==0)
  {
//Опорная линия
     lp=lpOporGorUdal;
//Определить координаты начала и конца линии

     X1=lDopDalnOutOtsch+Udal_m;             //Внизу чтобы линия выходила за пределы азимутов
     Y1=(long)(X1*tan(lMaxAzmt10/1800.0*M_PI)-10*dfKoefScrXToWrldY);
     X2=X1;
     Y2=(long)(X2*tan(lMinAzmt10/1800.0*M_PI)+10*dfKoefScrXToWrldY);
     Font=createFontFromFamalyAndSize(DEFAULT_FAMILY_FONT,szOporGorUdalText,false);

//Сформируем текст
     sfSPRINTF(Strka,"%g",Udal_m/1000.0);

 //Вывести текст
     textOutWrld(pntr,X1,Y1,Font,cOporGorUdalText,QString(Strka),QTT_levee,3);
     textOutWrld(pntr,X2,Y2,Font,cOporGorUdalText,QString(Strka),QTT_pravee,3);

  }else{
//Не опорная линия
     if(!bVyvodGorUdal)return 1;  //Если не опорная линия не выводится то выйти из процедуры

     lp=lpGorUdal;
//Определить координаты начала и конца линии

     X1=lDopDalnOutOtsch+Udal_m;             //Внизу чтобы линия выходила за пределы азимутов
     Y1=(long)(X1*tan(lMaxAzmt10/1800.0*M_PI)-10*dfKoefScrXToWrldY);
     X2=X1;
     Y2=(long)(X2*tan(lMinAzmt10/1800.0*M_PI)+10*dfKoefScrXToWrldY);

     if(bVyvodGorUdalText)
     {
//Сформируем шрифт для текста
        Font=createFontFromFamalyAndSize(DEFAULT_FAMILY_FONT,szGorUdalText,false);
//Сформируем текст
        sfSPRINTF(Strka,"%g",Udal_m/1000.0);

        textOutWrld(pntr,X1,Y1,Font,cGorUdalText,QString(Strka),QTT_levee,3);
        textOutWrld(pntr,X2,Y2,Font,cGorUdalText,QString(Strka),QTT_pravee,3);
     }
  }

//Вывести саму линию
  RisovatLiniuDlyaKursa(pntr,X1,Y1,X2,Y2,lp);
  return 1;
}


//Вывести линию посадочного курса
long TKursDraw::VyvestiLiniuPosadKursa(QPainter &pntr)
{
  long X1,Y1,X2,Y2;
//  lRet=VyvestiLiniu(DC,lLiniaPosadKursUgol10,lpLiniaPosadKurs,lDopDalnOut,lRastOtOsiVPPDoKursa,0,0);
   X1=lDopDalnOut;
   Y1=lRastOtOsiVPPDoKursa;
   if(lMaxDalnOut<=lMaxDalnostZonaOtkl)
   {
     X2=lDopDalnOutOtsch+lMaxDalnOut;
   }else{
     X2=lDopDalnOutOtsch+lMaxDalnostZonaOtkl;
   }
   Y2=lRastOtOsiVPPDoKursa+
      (long)((X2-X1)*tan(lLiniaPosadKursUgol10/1800.0*M_PI));

//Выведем угол курса
   RisovatLiniuDlyaKursa(pntr,X1,Y1,X2,Y2,lpLiniaPosadKurs);

  return 1;
}


//ВЫвести зону допустимых отклонений
long TKursDraw::VyvestiZonuDopustOtkl(QPainter &pntr)
{
  long lRet;
  long Ugl10a,Ugl10b;
  Ugl10a=lLiniaPosadKursUgol10-lMinusOtPosadKursUgol10;
  Ugl10b=lLiniaPosadKursUgol10+lPlusOtPosadKursUgol10;

  lRet=VyvodSektora(pntr,Ugl10a,Ugl10b,
     lbZonaOtkl,lDopDalnOut,lRastOtOsiVPPDoKursa,
     lMaxDalnostZonaOtkl+lDopDalnOutOtsch-lDopDalnOut);
  return lRet;
}



long TKursDraw::VyvestiSektorSkanSekt(QPainter &pntr)
{
  long lRet=1;
  if(lbSektorSkan.style()!=Qt::NoBrush&&bVyvodSektoraSkanKurs)
  {
    lRet=VyvodSektora(pntr,lSektorSkanMin10,lSektorSkanMax10,
                    lbSektorSkan,0,0,lMaxDalnOut+lDopDalnOutOtsch);
  }
  return lRet;
}


long TKursDraw::VyvestiSektorSkanLin(QPainter &pntr)
{
  long lRet;
  if(!bVyvodSektoraSkanKurs)
  {
      return 1;
  }
  lRet=VyvestiLiniu(pntr,lSektorSkanMin10,lpSektorSkan,0,0);
  if(lRet!=1)
  {

  }
  lRet=VyvestiLiniu(pntr,lSektorSkanMax10,lpSektorSkan,0,0);
  if(lRet!=1)
  {

  }
  return 1;
}


//Вывести сектор положения диаграммы направленности
long TKursDraw::VyvestiLiniuDNSekt(QPainter &pntr)
{
   long lRet=1;
   if(bVyvodUgolGlisVKurs)
   {
     lRet=VyvodSektora(pntr,lDNMin10,lDNMax10,lbLDN,0,0,lMaxDalnOut+lDopDalnOutOtsch);
   }
   return lRet;
}


//Вывести границы положения диаграммы направленности
long TKursDraw::VyvestiLiniuDNLin(QPainter &pntr)
{
  QPen lp, oldLp;
  QRectF rect_f;
  long lRet=1;
  long X1,Y1,X2,Y2;
  long x1,y1,x2,y2;
  if(!bVyvodUgolGlisVKurs)
  {
      return 1;
  }


  lRet=VyvestiLiniu(pntr,lDNMin10,lpLDN,0,0,&X2,&Y2);
  if(lRet!=1&&lRet!=2)
  {
//Обработать ощибку

  }

  if(lRet==2)
  {
//Означает, что сектор выхоходит за пределы масштаба азимутов и следует брать точку
//Соответствующую границе масштаба
//    X1=Y1*tan(-lMinAzmt10/1800.0*M_PI);
  }else{
//    X1=Y1*tan(-lDNMin10/1800.0*M_PI);
  }


  lRet=VyvestiLiniu(pntr,lDNMax10,lpLDN,0,0,&X1,&Y1);
  if(lRet!=1&&lRet!=3)
  {
//Обработать ошибку

  }

  lp=lpLDN;
  lp.setStyle(Qt::SolidLine);



 //Нарисуем эллипс
  x1=getXscrFromYwrld(Y1);
  y1=(long)(getYscrFromXwrld(X1)-5);
  x2=getXscrFromYwrld(Y2);
  y2=(long)(getYscrFromXwrld(X1)+5);


  rect_f.setLeft(x1);
  rect_f.setTop(y1);
  rect_f.setRight(x2);
  rect_f.setBottom(y2);

  oldLp=pntr.pen(); pntr.setPen(lp);
  pntr.drawArc(rect_f,0,16*360);

  pntr.setPen(oldLp);

  return 1;
}


//Вывести маяки
long TKursDraw::VyvestiBPRMandDPRMKurs(QPainter &pntr)
{
   QPen oldLP;
   int x,y,x1,y1,x2,y2;

//Разберемся с маяком в ближней зоне
   if(bBPRMKurs&&lPolojenieBPRM>0&&lPolojenieBPRM<=lMaxDalnOut)
   {
            y=getYscrFromXwrld(lPolojenieBPRM+lDopDalnOutOtsch);
            x=getXscrFromYwrld(Okrugl(lRastOtOsiVPPDoKursa+
                                    lPolojenieBPRM*tan(lLiniaPosadKursUgol10/1800.0*M_PI)));

            oldLP=pntr.pen(); pntr.setPen(lpBPRMKurs);
            x1=x-szBPRMKurs.width()/2;
            y1=y-szBPRMKurs.height()/2;
            x2=x+szBPRMKurs.width()/2;
            y2=y+szBPRMKurs.height()/2;
            pntr.drawLine(x1,y1,x2,y2);

            y1=y+szBPRMKurs.height()/2;
            y2=y-szBPRMKurs.height()/2;
            pntr.drawLine(x1,y1,x2,y2);
            pntr.setPen(oldLP);
   }


   if(bDPRMKurs&&lPolojenieDPRM>0&&lPolojenieDPRM<=lMaxDalnOut)
   {
            y=getYscrFromXwrld(lPolojenieDPRM+lDopDalnOutOtsch);
            x=getXscrFromYwrld(Okrugl(lRastOtOsiVPPDoKursa+
                               lPolojenieDPRM*tan(lLiniaPosadKursUgol10/1800.0*M_PI)));

            oldLP=pntr.pen(); pntr.setPen(lpDPRMKurs);
            x1=x-szDPRMKurs.width()/2;
            y1=y-szDPRMKurs.height()/2;
            x2=x+szDPRMKurs.width()/2;
            y2=y+szDPRMKurs.height()/2;
            pntr.drawLine(x1,y1,x2,y2);

            y1=y+szDPRMKurs.height()/2;
            y2=y-szDPRMKurs.height()/2;
            pntr.drawLine(x1,y1,x2,y2);
            pntr.setPen(oldLP);
   }
   return 1;
}

/*Вспомогательная процедура для рисования линий*/
long TKursDraw::RisovatLiniuDlyaKursa(QPainter &pntr,
      long X1, long Y1, long X2, long Y2,
      const QPen &lp)
{
   QPen oldLp;
   oldLp=pntr.pen(); pntr.setPen(lp);
   pntr.drawLine(getXscrFromYwrld(Y1),getYscrFromXwrld(X1), getXscrFromYwrld(Y2),getYscrFromXwrld(X2));
   pntr.setPen(oldLp);
   return 1;
}




long TKursDraw::RisovatOdnuCelKurs(QPainter &pntr,    //Контекст устройства
                 const QRect &Rect, //Область куда рисуется цель
                 TDannyeObCeli &DannyeObCeli,
                 TVidCeli *VidCeli, //Начинаются рассматриваться с индекса 1, чтобы удобнее было
                 long lChisloVidovCeley, //Число видов целей
                 int isSledOtmetka        //Эта отметка в следе
               )
{
    long lRet;
    QPen oldLP, oldLP1;  //Старый пен
    QBrush OldLB;  //Старый бруш
    QPen lpVektor;     //Вектор
    long  PromY1, PromY2;  //Промежуточное значение Y и Z
    int KodAvarii;
   int X11,X22,Y11,Y22;
   int X2,Y2;
   double Skor,SinSk,CosSk;  //Синус скорости, косинус скорости
   double Radius;
   bool isPaintCel=false;     //Рисовать ли цель


  //Надо определить,  а выводить ли эту цель вообще

  if(DannyeObCeli.X<0||DannyeObCeli.X>lMaxDalnOut+lDopDalnOutOtsch)
  {
         return 1;
  }
  //Оценим предельные значения Y-координат для текущей X-координаты
      PromY2=Okrugl(DannyeObCeli.X*tan(lMaxAzmt10/1800.0*M_PI));
      PromY1=Okrugl(DannyeObCeli.X*tan(lMinAzmt10/1800.0*M_PI));

      if(DannyeObCeli.Y==-100000)
      {
          DannyeObCeli.XvpKurs=-100000;
          DannyeObCeli.YvpKurs=getYscrFromXwrld(DannyeObCeli.X);

      }
      else if(DannyeObCeli.Y>PromY2)
      {
          DannyeObCeli.XvpKurs=getXscrFromYwrld(PromY2)-40;
          DannyeObCeli.YvpKurs=getYscrFromXwrld(DannyeObCeli.X);
      }
      else if(DannyeObCeli.Y<PromY1)
      {
          DannyeObCeli.XvpKurs=getXscrFromYwrld(PromY1)+40;
          DannyeObCeli.YvpKurs=getYscrFromXwrld(DannyeObCeli.X);
      }else{
          DannyeObCeli.XvpKurs=getXscrFromYwrld(DannyeObCeli.Y);
          DannyeObCeli.YvpKurs=getYscrFromXwrld(DannyeObCeli.X);
          isPaintCel=true;
      }

      if(DannyeObCeli.XvpKurs<0)
      {
          return 1;
      }

      if(!isPaintCel&&isSledOtmetka)
      {
          return 1;
      }

      if((DannyeObCeli.lHazardTwo&0x00FF)==0x0002 ||
          DannyeObCeli.lHazardKurs) KodAvarii=2;
      else if((DannyeObCeli.lHazardTwo&0x00FF)==0x0001) KodAvarii=1;
      else KodAvarii=0;


      if(KodAvarii==0||uiFlashStateKurs==0||isSledOtmetka!=0)
      {
          oldLP=pntr.pen(); pntr.setPen(VidCeli[DannyeObCeli.Tip].lpCeliKurs[0][isSledOtmetka%2]);
          OldLB=pntr.brush(); pntr.setBrush(VidCeli[DannyeObCeli.Tip].lbCeliKurs[0][isSledOtmetka%2]);
      }else{
          oldLP=pntr.pen(); pntr.setPen(VidCeli[DannyeObCeli.Tip].lpCeliKurs[KodAvarii][0]);
          OldLB=pntr.brush(); pntr.setBrush(VidCeli[DannyeObCeli.Tip].lbCeliKurs[KodAvarii][0]);
      }


  //Проверим, надо рисовать вектор скорости (он определяется)
      if(isPaintCel&&VidCeli[DannyeObCeli.Tip].IsVyvodVektorSkorostiKurs&&
        !isSledOtmetka)   //Только для текущего отсчета(не для следа))
      {

  //Надо нариовать вектор скорости?
         if(DannyeObCeli.Vy!=-100000&&   //Определено направление скорости вдоль линии гор.удаления
  //И хотя бы одна из составляющих не обращается в ноль
           (DannyeObCeli.Vx!=0||DannyeObCeli.Vy!=0))
         {

           lpVektor.setStyle(Qt::SolidLine);
           lpVektor.setWidth(1);
           lpVektor.setColor(VidCeli[DannyeObCeli.Tip].lbCeliKurs[0][0].color());
           oldLP1=pntr.pen();   pntr.setPen(lpVektor);


           Skor=sqrt(DannyeObCeli.Vx/1000.0*DannyeObCeli.Vx/1000.0+
                     DannyeObCeli.Vy/1000.0*DannyeObCeli.Vy/1000.0);
           SinSk=DannyeObCeli.Vy/1000.0/Skor;
           CosSk=DannyeObCeli.Vx/1000.0/Skor;
  //Расчитаем длину скорости в логическом пространстве, если она задана в пространству
  //устройства
           Radius=VidCeli[DannyeObCeli.Tip].lDlinaVektoraSkorosti/
                  sqrt(SinSk/dfKoefScrYToWrldX*SinSk/dfKoefScrYToWrldX+
                       CosSk/dfKoefScrXToWrldY*CosSk/dfKoefScrXToWrldY);

           X2=DannyeObCeli.X+Okrugl(Radius*CosSk);
           Y2=DannyeObCeli.Y+Okrugl(Radius*SinSk);

           pntr.drawLine(DannyeObCeli.XvpKurs,DannyeObCeli.YvpKurs,
                        getXscrFromYwrld(Y2),getYscrFromXwrld(X2));
           pntr.setPen(oldLP1);
         }
      }


  //Теперь надо нарисовать цель
  //Теперь надо нарисовать цель
      //Теперь надо нарисовать цель
      if(isPaintCel)
      {
            pntr.drawEllipse(QPoint(DannyeObCeli.XvpKurs,DannyeObCeli.YvpKurs),
                           Okrugl(VidCeli[DannyeObCeli.Tip].lRazmerCeliKurs/2),
                           Okrugl(VidCeli[DannyeObCeli.Tip].lRazmerCeliKurs/2));
      }
      pntr.setPen(oldLP);
      pntr.setBrush(OldLB);
      return 1;
}


int TKursDraw::getRastoyanieCeliIKursa( TDannyeObCeli &DannyeObCeli)
{
    long y_temp, y_temp1, y_temp2;
    if(DannyeObCeli.X<lDopDalnOut||DannyeObCeli.X>(lMaxDalnostZonaOtkl+lDopDalnOutOtsch))
    {
        return 30000;
    }

 //Определим значение y_temp
    y_temp=(DannyeObCeli.X-lDopDalnOut)*tan(lLiniaPosadKursUgol10/1800.0*M_PI)+lRastOtOsiVPPDoKursa;

    y_temp1=(DannyeObCeli.X-lDopDalnOut)*
              tan((lLiniaPosadKursUgol10-lMinusOtPosadKursUgol10)/1800.0*M_PI)+lRastOtOsiVPPDoKursa;

    y_temp2=(DannyeObCeli.X-lDopDalnOut)*
              tan((lLiniaPosadKursUgol10+lPlusOtPosadKursUgol10)/1800.0*M_PI)+lRastOtOsiVPPDoKursa;

    if(DannyeObCeli.Y<y_temp1)return -20000;
    if(DannyeObCeli.Y>y_temp2)return 20000;

    return (DannyeObCeli.Y-y_temp);


}

