﻿//---------------------------------------------------------------------------



#include <math.h>
#include <stdlib.h>
#include "AnyTools.h"

//---------------------------------------------------------------------------
#define ABS(X) (((X) > 0) ? (X) : (-(X)))
#define MAX(X,Y) (((X) > (Y)) ? (X) : (Y))
#define MIN(X,Y) (((X) > (Y)) ? (Y) : (X))

long Okrugl(double Chislo1)
{
 long Chislo2;
 double Chislo3;
 Chislo2=(long)Chislo1;
 Chislo3=Chislo1-Chislo2;
 if(Chislo3<0.5)return Chislo2;
 return ((long)Chislo2+1);
}

