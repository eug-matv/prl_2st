﻿/*Модуль графического вывода типа поле курса и поле глиссды данных РЗП
(руководителя ближней зоны.
Автор: Матвеенко Е.А. нач КБ-29
Дата комментария: 17.04.07.


TRZPDraw - класс обеспечивающий задание параметров (масштаб вывода), отсчеты и
вывод данных. Данный класс наследуется от двух классов TKursDraw и TGlissadaDraw.



1. СИСТЕМА КООРДИНАТ

Ось X направлена вдоль луча антены - паралельно взлетной полосе.
Ось Z направлена вверх.
Ось Y - так что организует провостороннюю тройку
Координаты даны в метрах.
Взлетная полоса расположена паралельно или почти паралелльно
Координаты взлетное центра взлетной полосы около РЛС
при lNapravlenieVPP=0:
 (0, lROtOsiVPPDoCentraAntKursa,lRaznicaVPPIAntena),
 где lROtOsiVPPDoCentraAntKursa и lRaznicaVPPIAntena может быть больше,
 а может быть и меньше 0.

при lNapravlenieVPP=1:
 (0, -lROtOsiVPPDoCentraAntKursa,lRaznicaVPPIAntena).


 Посадка и взлет должны происходить на координате
 (lROtTorcaVPPDoCentraAntKurNapr[0]-lROtTochkiPrizDoTorcaVPP[0],
  lROtOsiVPPDoCentraAntKursa,lRaznicaVPPIAntena) или близкой. при направлении lNapravlenieVPP=0

 или
 (lROtTorcaVPPDoCentraAntKurNapr[1]-lROtTochkiPrizDoTorcaVPP[1],
  -lROtOsiVPPDoCentraAntKursa,lRaznicaVPPIAntena)
  если lNapravlenieVPP!=0



 lROtTorcaVPPDoCentraAntKurNapr[i] - расстояние от РЛС до торца ВПП;
 lROtOsiVPPDoCentraAntKursa[i] - расстояние от торца ВПП до места посадки вдоль взлетно посадочной полосы.



2. ЛИНИЯ ГЛИССАДЫ

 Линия глиссады направлена вдоль ВПП.  Начинается с координат
 (lROtTorcaVPPDoCentraAntKurNapr-lROtTochkiPrizDoTorcaVPP,
  lROtOsiVPPDoCentraAntKursa,lRaznicaVPPIAntena). Отображается
 в поле глиссады, которая в правой части всей области отображения.
  Угол глиссады равен dUgolGlissady в градусах над горизонтом. Глиссада заканчивается
на дальности lUdalenie. Также задаются отклонения по глиссаде
dOtklonPoGlisMinus и dOtklonPoGlisPlus. И рисуется сектор:
с градусами: dUgolGlissady-dOtklonPoGlisMinus до dUgolGlissady+dOtklonPoGlisPlus.
Дальность сектора: lUdalenie. Линия глиссады рисуется внутри сектора.

3. ЛИНИЯ КУРСА
  Линия курса направлена вдоль ВПП.
  Начинается с координат
 (lROtTorcaVPPDoCentraAntKurNapr-lROtTochkiPrizDoTorcaVPP,
  lROtOsiVPPDoCentraAntKursa,lRaznicaVPPIAntena)
Рисуется в поле курса, которое в левой части всей области отображения.
Угол отклонения равен dOtklNormaliKursaOtVPP.







*/

//---------------------------------------------------------------------------

#ifndef RZPLinGlafH
#define RZPLinGlafH


#include <list>


#include <QPen>
#include <QBrush>
#include <QMutex>
#include <QImage>
#include <QString>
#include <QPainter>
#include <QSound>

#include "OtmetkaDannye.h"
#include "KursGraf.h"
#include "GlissadaGraf.h"
#include "Formulyary.h"


#ifdef INI_GROUP
#undef INI_GROUP
#endif
#define INI_GROUP  "COMMON"
using namespace std;

//---------------------------------------------------------------------------
/**/
struct RECT_DOUBLE
{
  double left;
  double top;
  double right;
  double bottom;

};



enum RZPLG_Formulyar_String
{
    RFS_None=0,     //Ничего не отображать
    RFS_Nomer,      //Номер трассы
    RFS_VysM,       //Высота в метрах и признак изменения высоты
    RFS_SpeedGround,    //Скорость относительно землли
    RFS_SpeedVertical,  //Вертикальная скорость
    RFS_DalnostKM      //Дальность
};





class TRZPDraw: public TKursDraw, public TGlissadaDraw
{

//Описание верхних нижних координат
  RECT_DOUBLE rdGlissada; //Описание прямоугольника поля глиссады
                          //В долях от размера DCBack.
                          //то есть значение(0,0) соответствует левому верхнему углу
                          //а (1,1) - правому нижнему

  RECT_DOUBLE rdKurs;    //Описание прямоугольника поля курса
                          //В долях от размера DCBack.
                          //то есть значение(0,0) соответствует левому верхнему углу
                          //а (1,1) - правому нижнему

  RECT_DOUBLE rdInfoText;   //Область куда выводится текстовая информация

  QImage *imgSetka;          // - туда постоянно выведена сетка
                  //Сетка рисуется 1 раз на 1 изменение параметров отображения




  QImage  *imgBack;        //Туда каждый раз копируется из DCSetka и выводятся данные


  QRect RectDraw;       //Вся область отображения в системе координат окна
                       //Именно туда будет копироваться память из DCBack
  QRect RectGlissada;  //Прямоугольник глиссады в системе координат DCSetka и DCBack
                      //Вычисляется по значениям rdGlissada
  QRect RectKurs;      //Прямоугольник курса  в системе координат DCSetka и DCBack

  QRect RectInfoText;      //Область куда будет выводиться информация

  bool bInit;         //Инициализирована ли работа
  long lNapravlenieVPP; //0 или 1.


/*Следующие данные заполняются из файла PreSetup.INI*/
  long lROtTochkiPrizDoTorcaVPP[2]; //Расстояние от точки приземления до торца ВПП
                                    //Всегда больше 0

//Расстояние от оси ВПП до центра полотна антенны курса;
  long lROtOsiVPPDoCentraAntKursa; //Значение в метрах.
                     //Интерпретация данной величины зависит от lNapravlenieVPP
                     //Если lNapravlenieVPP=0, то
                     //при lROtOsiVPPDoCentraAntKursa<0, полоса левее РСП
                     //прри lROtOsiVPPDoCentraAntKursa>0, ВПП правее РСП

                     //Если lNapravlenieVPP!=0 (lNapravlenieVPP=1), то
                     //при lROtOsiVPPDoCentraAntKursa<0, полоса правее РСП
                     //при  lROtOsiVPPDoCentraAntKursa>0, ВПП левее РСП

//Расстояние от торца ВПП до центра полотнп антенны курса
//в направлении паралельном оси ВПП
  long lROtTorcaVPPDoCentraAntKurNapr[2];
                  //Значение в метрах
                  //Значение около 1500

//Превышение и приуменьшение точки приземления на поверхности ВПП над центром полотна аннтены глиссады
//Значение может быть отрицательным
  long lRaznicaVPPIAntena;
                 //Значение в метрах
                 //Если точка призимление выше антенны глиссады, то значение больше 0,
                 //Если ниже, то значение меньше 0


//Дальность до ДПРМ от торца ВПП
  long lDalnDoDPRMOtTorcaVPP[2];
                //Значение в метрах


//Дальность до БПРМ от торца ВПП
  long lDalnDoBPRMOtTorcaVPP[2];
                //Значение в метрах


//Угол глиссады.
  double dUgolGlissady[2];
           //Угол глиссады


//Угол курса - отклонение курса от вертикальной линии 
  double dUgolKursa[2];


//Максимальная дальность глиссады и курса (на которое он действует)
  long lUdalenie;  //Удаление глиссады и курса
           



//Допустимые отклонения по курсу (по умолчению +-2 градуса)
//Абсолютное значение на сколько может быть меньше
  double dOtklonPoKursuMinus;
  double dOtklonPoKursuPlus;

//Отклонение по глиссаде (обычно +-0.5 градусов)
  double dOtklonPoGlisMinus;
  double dOtklonPoGlisPlus;

//Диаграмма направленности курса в поле глиссаде (в верт плоскости)
  double dMinUgolDNKursaVGlis;  //Минимальный угол курса
  double dMaxUgolDNKursaVGlis;  //Максимальный угол курса

//Диаграмма направленности курса в поле курса (в горизонтальной плоскости)
  double dMinUgolDNGlisVKurs;   //Минимальный угол глиссады
  double dMaxUgolDNGlisVKurs;   //Максимальный угол глиссады




//Магнитный курс посадки на ВПП (слева, если смотреть от РСП на ВПП) (применительно к направлению 0)
//При lNapravlenieVPP=0. Это угол поворота от в текущем направлении ВПП на взлет
//При lNapravlenieVPP!=0 (lNapravlenieVPP=1), то
//К этоик значению добавляется 180 градусоыв
  double dMagnKursPosadkiNaVPP;  //(от 0 до 360)


//Отклонение нормали антены курса к антене курса от направления, паралельного оси ВПП.
  double dOtklNormaliKursaOtVPP;       //Пока под вопросом



//Отклонение нормали антены глиссады от горизонта
  double dOtklNormaliGlisOtGoriz;     //Пока под вопросом


//Размер сектора сканирования в поле глиссады в градусах
  double dMinSektorSkanGlis;   //Отклонения от сектора сканирования
  double dMaxSektorSkanGlis;    //Полный размер, а граничные определяются


//Размер сектора сканирования в поле курса в градусах
  double dMinSektorSkanKurs;   //Отклонения от сектора сканирования
  double dMaxSektorSkanKurs;    //Полный размер, а граничные определяются


//Допустимое расстояние пар целей. Если это расстояние меньше,
//то значит ситуация критическая.
  double dRmin;        //Значение в метрах
  double dTimeForRmin; //Значение времени в секундах для оценки времени

//Координаты РСП в км в системе МПСТК
  int dXofRSP_m;
  int dYofRSP_m;


//Путь к каталогу, где хранятся файлы LUA
  QString csLUA_Directory;


//Массив отметок, которые описаны в модуле OtmetkaDannye
  list<TDannyeObCeli> ldoc;

  long lHazardTwoBorts;               //0 - не было опасных ситуаций с двумя бортами
                                      //1 - борта могут в течении  времени сблизиться на недопустимое расстояние
                                      //2 -  борта уже находяься наопасном расстоянии
//Массив отметок, которые в хвосте
  list<TDannyeObCeli> ldoc_tail;




//Массив отметок, который добавляется изначально
  list<TDannyeObCeli> ldoc_new;





  long lHazardKurs;                   //0 - нормально
                                      //1 - опасность
  long lHazardGlis;                   //0 - нормально
                                      //1 - опасность

//Была ли опасная ситуация
  bool bHazardSituation;              //Опасная ситуация



/*Указатели на LOGFILE*/
  char *LogFileError;   //Лог файл ошибки программы
  char *LogFile;        //Адрес лог файла для записи данных
  QMutex csLogFileError;  //Критичиская секция для записи в Log-файл
  QMutex csLogFile;    //Критическая секция для записи в Log-файл

/*Ини файл, куда-потом будут занесены данные*/
  char *IniFileForUser;

/*Область выделения*/
  bool bOblastVydeleniya;    //Рисовать область выделения
  QRect rOblastVydeleniya;    //Область выделения


  int iTipDalnosti; //Фиксированные значения максимальной дальности


  int isNeedPereraschet;     //Были ли добавлены данные после последнего вывода их на экран


/*Организация формуляров*/
  int isReadyForOutput;


/*Мьютекс для согласования ввода данных и отображения*/
  QMutex mutex_dobav_cel;

/*Мьютекс для согласования вывода*/
  QMutex mutex_paint;

/*Мьютекс для согласования очистки и сформирования отчереди данных*/
  QMutex mutex_napr;

/*Максимальное отклонение азимута по курсу при аварии*/
  double dfHazardMaxUgolKurs;


/*Минимальное отклонение глиссады по углу*/
  double dfHazardMinUgolGlis;

/*Максимальное отклонение глиссады по углу*/
  double dfHazardMaxUgolGlis;


/*Максимальная дальность*/
  long lHazardMaxDalnost;


/*Файл звукового сигнала для извещении об опасной ситуации*/
  QString fileNameAlarmWAV;


/*Для запуска звукового файла при наличии опасности*/
  QSound *snd_plr;

/*Играет ли уже звук*/
  bool isPlayingSound;


protected:

//Загрузить параметр lNapravlenie из файла UserINI
//По умолчанию ставится 0
   void zagruzitNapravlenie(const char *IniFile);



//Установить все параметры
   long UstanovitParametry();


//Рисовать сетку в DCSkreen
   long RisovatSetku(void);








//Преобразовать координаты для всех новых отсчетов
//Возврашает 1, если все успешно иначе 0
  long RaschetKoordinatXZYIOcenkaPar(bool isTail);


//Вывести отсчеты в поле курса
  long VyvestiOtschetyVPolyaKursaIGlissady(void);


//Полный путь к имени файла
//Возвращает 1, если успешно
//0 - если не успешно
  long ZagruzitPreSetupIni(const char *IniFile, const char *ErrorPar);


//Полный путь к имени файла IniFile - эо файл спользовательскими настройками
//Возвращает 1, если успешно
//0 - если не успешно
  long ZagruzitIUstanovitDannye(const char *IniFile);

//Сохранение файла в ини-файла
  long ZapisatDannyeVIniFile(const char *IniFile);



//Промежуточная процедура - поиск элемента с таким же номером
//поиск элемента с номером в ldoc_tail
  int addDataIntoLdoc_tail(const TDannyeObCeli &doc);

//Добавление данных в ldoc
  int addDataIntoLdoc(const TDannyeObCeli &doc);

//Проредить по времени и    ldoc и ldoc_tail
  int removeDataFromLdocAndLdoc_tail();

//Добавление новой цели
  long DobavitNovuyuCelProtected(const TDannyeObCeli &NovayCel);

//Создание строки для соответствующей цели
  QString getStringInfo();

//Следующая функция определяет, отображается ли  данная цель в окне. И если не отображается
//То она меняет следующие параметры dfHazardMaxUgolKurs, dfHazardMinUgolGlis,dfHazardMaxUgolGlis
//lHazardMaxDalnost
  void testParametersWithHazard(const TDannyeObCeli &doc);


//Следующая процедура устанавливает новый масштаб, если были отметки с признаками аварии, не попадающие
//  в диапазон. Возвращает 0, если масштаб не  изменился. 1 - если изменился
  int  trySetNewMasshtabWithHazard();


/*Очистить всю информацию об отметках, которые только были добавлены*/
  int clearAllData();

public:
  long ErrorKod1,

       ErrorKod2;


  QPen lpOblastVydeleniya; //цвет области выделения

//Размеры формуляра
  long szTextFormulyara;    //Размер текста формуляра
  QColor cTextFormulyara;  //Цвет текста формуляра


//Цвет текста выводимого в левой части окна.
   QColor cTextInfo;

//Параметры шрифта
   QString sFamTextInfo;
   int szTextInfo;
   bool bBoldTextInfo;


  RZPLG_Formulyar_String rfs_options[6];    //Отображаемые параметры


  TVidCeli VidyCeley[8];              //Виды целей

//Индексы:
//                                    0 - не используется
//  1 - обнаружена МОС но не сопровождается ни глисадным ни курсовым каналом
//  2 - не обнаружена МОС, сопровождается курсовым каналом
//  3 - обнаружена МОС, сопровождается курсовым каналом
//  4 - не обнаружена МОС, сопровождается глиссадным каналом
//  5 - обнаружена МОС, сопровождается глиссадным каналом
//  6 - не обнаружена МОС, сопровождается и курсовым и глиссадным каналами
//  7 - обнаружена МОС, сопровождается и курсовым и глисадным каналами






//Конструктор класса. Основная информация берется из ини-файла
  TRZPDraw(
           const char *LogFileOut,      //Лог-файл для журналирования
           const char *LogFileOutForErr//Лог-файл для записи ошибок
          );

  ~TRZPDraw();

//Начать работу с данным пользователем


///Начать работу   - возвращает 0, если уже запущена.
//1 - если запущена успешно. <0 - если ощибка
  long BeginWork(
            const QRect &rDraw,            //Область отображения окна
            const char *PreSetupIni,     //Файл инициализации. Этот файл заполняется вручную - или отдельной программой
            const char *UserIni         //Файл инициализации.
                );

//Остановка режима работы
//0 - если уже остановлен
//1 - если остановка прошла успешно
  long EndWork(void);




/*Предварительный расчет координат и подготовка данных к выводу.
Если что-то поменялось, то вернуть 1, иначе вернуть 0*/
  int RaschetKoordinatIPodgotovkaDannyhKVyvodu();


   /*Вывод объекта на экран не передается параметр HDC DC - по идее должно происходить периодический. Допустим 2 раза в секнду.
   Тут не проводится ни какой обработки.
   */
      long VyvodNaEkranForPeriodDC(QPainter &pntr);


//Ввести сами данные командой
//Добавить новый элемент
//Возвращает 1, если все успешно
//2 - если успешно но пришлось вытолкнуть элемент, так как элементов  2000
//0, если не успешно
  long DobavitNovuyuCel(const TDannyeObCeli &NovayCel);



/*Методы, которые реализуют действия текущего оператора,
не требуюшие прав администратора*/

//Методы изменения данных режима.
 //Установить масштаб по дальности (максимальная дальность)


   long UstanovitMaxVizDalnost(long TipDalnost);  //Установка максимальной дальности
                                       //TipDalnost=0, -> 5000 м
                                       //TipDalnost=1, -> 10000 м
                                       //TipDalnost=2, -> 20000 м
                                       //3 - > 40000 м
                                       //4 -> 60000 м


//Получение максимальной дальности
   long PoluchitMaxVizDalnost(void)
   {
     return TKursDraw::GetMasshtabDalnosti();
   };

//Получение типа максимальной дальности
   int PoluchitTipDalnost(void)
   {
       return iTipDalnosti;
   };

 //Установить массштаб по горизонтали в поле курса
 //Должны лежать в -dUgol +dUgol
//Возвращает 1, если успешно установлено.
//0 если нет
//Максимально допустимое
   long UstanovitVizDiapazonUglKurs(double dUgol);

//Вернуть значения углов отображаемого углов курса
   long PoluchitVizDiapazonUglKurs(double *dUgol)
   {
     GetMasshtabAzimuta(dUgol);
     return 1;
   };

//Установить масштаб по вертикали в поле глиссады
//Возращает 1, если нормально
//0, если нет
   long UstanovitVizDiapazonGlis(double Ugol1,
                                 double Ugol2);

//Вернуть значения углов отображаемого углов курса
   long PoluchitVizDiapazonGlis( double *Ugol1,
                                 double *Ugol2)
   {
     GetMasshtabPoVertikali(Ugol1,Ugol2);
     return 1;
   };


//Установить свойства отображеная сетки курса
   long UstanovitVisibleSetkaKurs(bool VisAzmt,
                              bool VisAzmtText,
                              bool VisUdal,
                              bool VisUdalText
                              );

//Установить свойства отображения сетки глиссады
   long UstanovitVisibleSetkaGlis(bool VisUgol,
                                  bool VisUgolText,
                                  bool VisUdal,
                                  bool VisUdalText
                                 );

//Отображать линии равных высот
   long UstanovitVisibleLiniiRavnyhVysot(
              bool VyvodLinii,
              bool VyvodLiniiText);


//Изменить размер и положение отображаемой области
   long UstanovitOblastVyvoda(const QRect  &rNewOblst);


/*Процедуры работы с LOG-файлами*/
//Запись в лог файл типа ошибка
  long DataToLogFileError( const char *Strka, bool MustOutData); //Запись строки

//Запись в лог файл типа ошибка
  long DataToLogFile(char *Strka, bool MustOutData);







/*Для вывода отсчета на передний план*/
  long VyvodOtmetkiNaPeredniyPlan(list<TDannyeObCeli>::iterator &it); //Индекс в списке отсчетов

/*Проверка: был ли выбран отсчет по координатам в пикселях, относительно области вывода*/
//Возвращает истинное значение, если отсчет был выбран
//
  bool VyborOtschetaPoTochke(long X, long Y); //точки


/*Обновить данные сетки, после изменения параметров*/
  int updateForRepaint();


/*Задать второе направление*/
  int ustanovitNapravlenie(int napr);


/*Получить данные о направлении*/
  int poluchitNapravlenie()
  {
        return lNapravlenieVPP;
  };

/*ДЛЯ ТЕСТИРОВАНИЯ РЗП*/
  long GetKoordinatyForTestNaKurseIGlissade(
  //Входной параметр
                            long X,
  //Выходные паараметры
                            double *Azmt,
                            long *Dlnst,
                            long *H,
                            long *Y,
                            long *Z,
                            long *MinY,
                            long *MaxY,
                            long *MinZ,
                            long *MaxZ,
                            long *MinY_3,
                            long *MaxY_3,
                            long *MinZ_3,
                            long *MaxZ_3);



   void updateFlashState()
  {
        TKursDraw::updateFlashStateKurs();
        TGlissadaDraw::updateFlashStateGlis();
        if(isNeedPereraschet==0)isNeedPereraschet=1;
  };






};


#endif
