#include <QFontMetrics>
#include "qt_tools.h"



//Вывод текста в поле глиссады
int textOutScr(QPainter &pntr,          //Дескриптер
               int x_scr, int y_scr,    //Координаты в системе окна
               const QFont &font,       //фонте
               const QColor &col,
               const QString &str,
               enum QTT_GdeText qtt,       //0 - с
               int otstup
              )
{
    QFontMetrics fm(font);
    int h,w;
    int x_out, y_out;
    QFont oldFont;
    QPen oldPen;
    QRect outRect;
    Qt::BGMode oldBkMode;

    w=fm.width(str);
    h=fm.height();


    oldBkMode=pntr.backgroundMode(); pntr.setBackgroundMode(Qt::TransparentMode);
    oldFont=pntr.font();  pntr.setFont(font);
    oldPen=pntr.pen(); pntr.setPen(Qt::SolidLine); pntr.setPen(col);
    if(qtt==QTT_levee)
    {
        x_out=x_scr-w-otstup;
        y_out=y_scr-h/2;
    }else
    if(qtt==QTT_pravee)
    {
        x_out=x_scr+otstup;
        y_out=y_scr-h/2;
    }else
    if(qtt==QTT_nije)
    {
        x_out=x_scr-w/2;
        y_out=y_scr+otstup;
    }else{
        x_out=x_scr-w/2;
        y_out=y_scr-h-otstup;
    }

    outRect.setLeft(x_out);
    outRect.setTop(y_out);
    outRect.setRight(x_out+w);
    outRect.setBottom(y_out+h);

    pntr.drawText(outRect,Qt::AlignLeft,str);
    pntr.setBackgroundMode(oldBkMode);
    pntr.setPen(oldPen);
    pntr.setFont(oldFont);
    return 1;
}



int heightOfTextScr(const QFont &font)
{
    QFontMetrics fm(font);
    return fm.height();
}


int widthOfTextScr(const QFont &font, const QString &str)
{
    QFontMetrics fm(font);
    return fm.width(str);
}



QFont createFontFromFamalyAndSize(const QString &fam, int size, bool bBold)
{
    QFont font;
    font.setFamily(fam);
    font.setPointSize(size);
    font.setBold(bBold);
    return font;
}

