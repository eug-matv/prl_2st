﻿//---------------------------------------------------------------------------


#include <string.h>
#include "Formulyary.h"
#include "qt_tools.h"
#include "safe_function.h"
//---------------------------------------------------------------------------


long TFormulyar::UstanovitParametry(long X1, long Y1,
                                    long X2, long Y2,
                                    char **sTextOut,
                                    long N_of_sTextOut,
                                    QPen &lp, QBrush &lb,
                                    long szText, QColor cText,
                                    QRect *Rect
                                    )
{
  int i;
  QFont font;
  long tX,tY; //Временные координаты
  long szShir, szVys;
  font=createFontFromFamalyAndSize(DEFAULT_FAMILY_FONT,szText,false);
  szVys=N_of_sTextOut*heightOfTextScr(font)+6;
  szShir=0;
  for(i=0;i<N_of_sTextOut;i++)
  {
    if(szShir<(5+widthOfTextScr(font,QString(sTextOut[i]))))
    {
        szShir=5+widthOfTextScr(font,QString(sTextOut[i]));
    }
  }
//Рассчитаем координыты формуляра
  tX=X2; tY=Y1;
  Left=tX-szShir/2;
  Right=tX+szShir/2;
  Top=tY-szVys/2;
  Bottom=tY+szVys/2;

//Запомним область
  Rect->setLeft(Left);
  Rect->setRight(Right);
  Rect->setTop(Top);
  Rect->setBottom(Bottom);


//Расчитаем координаты соединяющих линий
  X11=X1; Y11=Y1;
  X12=Left; Y12=tY;
  X21=X2; Y21=Y2;
  X22=tX;  Y22=Top;

//Скопируем текст
  for(i=0;i<N_of_sTextOut&&i<5;i++)
  {
     sfSTRCPY(Stroki[i],sTextOut[i]);
  }
  ChisloStrok=N_of_sTextOut;

  lpOut=lp;
  lbOut=lb;

//Скопируем данные о параметрах текста
  szTextOut=szText;
  cTextOut=cText;
  IsOut=true;


   return 1;
}

bool TFormulyar::Vyvod(QPainter &pntr)   //Вывести формуляр
{

  QFont Font;      //шрифт
  QPen oldLp;
  QBrush oldLb;
  Qt::BGMode OldBkMode;
  long i;
  int tY; //Временный y
  long Lngth;

  Font=createFontFromFamalyAndSize(DEFAULT_FAMILY_FONT,szTextOut,false);

//Создадим перо
  oldLp=pntr.pen(); pntr.setPen(lpOut);

//Создадим кисть
  oldLb=pntr.brush(); pntr.setBrush(lbOut);

  OldBkMode=pntr.backgroundMode(); pntr.setBackgroundMode(Qt::TransparentMode);




  if(X11>-10000)
  {
    pntr.drawLine(X11,Y11,X12,Y12);
  }

  if(Y21>-10000)
  {
    pntr.drawLine(X21,Y21,X22,Y22);
  }

//Выведем прямоугольник
  pntr.drawRect(Left,Top,Right-Left+1, Bottom-Top+1);


//Выведем текст внутри прямоугольника
//Проверим размеры
  tY=Top+heightOfTextScr(Font)/2+3;

  for(i=0;i<ChisloStrok;i++)
  {
      textOutScr(pntr,Left,tY,Font,cTextOut,QString(Stroki[i]),QTT_pravee,3);
      tY+=heightOfTextScr(Font);
  }

//Восстанавливаем все предыдущие настройки
  pntr.setBackgroundMode(OldBkMode);
  pntr.setPen(oldLp);
  pntr.setBrush(oldLb);

  return true;
}


//Проверка, что внутри рамки
bool TFormulyar::VnutriFormulyara(long X, long Y)
{
  if(!IsOut)return false;
  if(X>=Left&&X<=Right&&Y>=Top&&Y<=Bottom)return true;
  return false;

}



