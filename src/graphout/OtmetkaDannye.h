﻿//---------------------------------------------------------------------------
/*Модуль OtmetkaDannyh.
Реализована Матвеенко Е.А. 10.09.07.
Данные отсчетов, подготовленные для графического вывода.
Используется в модулях: KursGraf и GlisGraf для вывода
в процедурах: RisovatCeliKurs и  RisovatCeliGlis, соответственно.
Класс: TVidCeli описивает внешний вид для типа цели. Должен использоваться так:
  выделяется массив объектов TVidCeli,  размер массива 8 элементов.
  Используются элементы с индексами от 1 до 7, объект с индексом 0
  не используется. На соответствующий индекс указывает свойство Tip
  класса TDannyeObCeli. Индексы соответствуют след целям:
  0 - не используется
  1 - обнаружена МОС но не сопровождается ни глисадным ни курсовым каналом
  2 - не обнаружена МОС, сопровождается курсовым каналом
  3 - обнаружена МОС, сопровождается курсовым каналом
  4 - не обнаружена МОС, сопровождается глиссадным каналом
  5 - обнаружена МОС, сопровождается глиссадным каналом
  6 - не обнаружена МОС, сопровождается и курсовым и глиссадным каналами
  7 - обнаружена МОС, сопровождается и курсовым и глисадным каналами
  Прочие описание смотреть в текста определения класса TVidCeli

Класс TDannyeObCeli описывает конкретную цель.
Содержит угловые координаты цели определенные МОС, если цель была определна МОС.
Координаты цели в режиме ПРЛ, если цель сопровождается ПРЛФС.
Координаты  X, Y, Z и Vx, Vy, Vz(скорости), эти координаты и будут отображаться,
рассчитываются в зависимости от параметров Tip по МОС координатам или
координатам сопровождения.
Используется массив данных TDannyeObCeli.
Данный массив определен как элемент класса
После заполнения массива, которые были получены и от МОС и от ПРЛФС
1) Должен вызвать для каждого элемента метод RasschetXYZ - убрано, значение получается в ЛУА
2) попарно для всех отсчетов вызвать ControlForKrit_AvariaTwo
3) вызвать функцию для проверки близости к курсу
   ControlForKrit_AvariaKurs
4) вызвать функцию для проверки близости к глиссаде
   ControlForKrit_AvariaGlis

Выше описанные действия выполнить для всех отметок.

Далее массивы типа TDannyeObCeli и TVidCeli используются в процедурах
RisovatCeliKurs и RisovatCeliGlis класса TRZPDraw
(наследуеься от TKursDraw и TGlissadaDraw, соответственно )



*/

#ifndef OtmetkaDannyeH
#define OtmetkaDannyeH

#include <QPen>
#include <QBrush>
#include <QFont>
#include <QRect>
#include <list>
//---------------------------------------------------------------------------
//Описание одной цели
struct TDannyeObCeli;




struct TDannyeObCeli
{
//Позывной  в виде строки
  char sPozyvnoy[20];  //Позывной
  long lNomerVozdushnogoSudna;         //Номер воздушного судна от 1 до 1000


//Координаты, которые будут отображаться
  long X,Y,Z;     //Вектор координат относительно ОРЛ в декартовых координт
  long Vx,Vy,Vz;  //Вектор скорости



//Определяется тип отсчета: используется как индекс в массиве TVidCeli.
//Используется так же для формирования координат X,Y,Z.
  unsigned short Tip;     //Структура слова: Tip.
                  
  long timeOfCel;       //Время миллисекундах

  long timeOfLife1_ms;  //Время жизни в миллисекундах
  long timeOfLife2_ms;  //Время жизни до полного изчезновения
  long timeOfLifeWithOut_ms;    //Время жизни если данные данной трассы не поступают

  long timeForDie1_ms;   //Время ликвидации
  long timeForDie2_ms;   //Время ликвидации



/*Физические координаты цели в системе координат дисплея*/
  long XvpGlis,YvpGlis; //Координаты в системе координат дисплея (пикселя монитора) в поле глиссады
                        //Данное значение определяется после вывода функцией вывода

  long XvpKurs,YvpKurs; //Координаты в систему координат дисплея (пиксели) в поле курса
                        //Данное значение определяется после вывода функцией вывода


//Значения lOtklKurs и VyhodOtklKurs_3 рассчитываются и устанавливаются
//процедурой ControlForKrit_AvariaKurs
//Рассчитывается в методе ControlForKrit_AvariaGlis
  long lOtklKurs;        //Отклонение  от курсу в метрах. Выводить минус или плюс, а не право или лево


  bool VyhodOtklKurs_3;  //Выходит ли за пределы 1/3 и надо ли выводить текст по курсу
                         //Если да, то значения высоты надо писать
                         //Рассчитывается в методе ControlForKrit_AvariaKurs

//Значения lOtklGlis и VyhodOtklGlis_3 рассчитываются и устанавливаются
//процедурой ControlForKrit_AvariaGlis
//Рассчитывается в методе ControlForKrit_AvariaGlis
  long lOtklGlis;       //Отклонение  от глиссады в метрах.
                        //Отображается
                                            //Рассчитывается в методе ControlForKrit_AvariaKurs



  bool VyhodOtklGlis_3; //Выходит ли за пределы 1/3
  //и при этом можно вывести текст по глиссаде
  //Рассчитывается в методе ControlForKrit_AvariaKurs



/*Есть ли опасность двух бортов*/
  long lHazardTwo;     //Опасность для двух  бортов
                       //Определяется в методе ControlForKrit_AvariaTwo
                       //Если борта расположены близко
                       //0 - нет опасности

                       //0x01 - предвидется опасность в курсе
                       //0x02 - критическая ситуация в поле курса
                       //0x0100 - предвидется опасность в поле глиссады
                       //0x0200 - критическая ситуация в поле глиссады
                       //0x0101 - опасность и там и там
                       //0x0202 - критическая ситуация и там  и там


/*Какова опасность в поле курса. Определяется и устанавливается
процедурой ControlForKrit_AvariaKurs
*/
  long lHazardKurs;    //Отклонение по курсу - выход за пределы сектора курса
                       //0 - нет опасности
                       //1 - борт выходит за пределы.
                       //Определяется в методе ControlForKrit_AvariaKurs


/*Какова опасность в поле глиссады. Определяется и устанавливается
процедурой ControlForKrit_AvariaGlis*/
  long lHazardGlis;    //Отклонение по глиссаде - выход за пределы сектора глиссады
                       //0 - нет опасности
                       //1 - борт выходит за пределы.
                       //Определяется в методе ControlForKrit_AvariaGlis




 QRect FormulyarRect;        //Координаты формуляра (только для отсчетов текущего вывода)
                            //в координатах окна



//Определение с другой отметкой расстояние. И будут ли они находится в ограниченной области
//Возвращает: 0 - не будут,
//         старший байт для поля глиссады: 1 - есть опасность, что встретятся. 2 - уже на растоянии
//         младший - для поля курса: 1 - могут оказаться на расстоянии в этот период, 2 - уже на этом расстоянии
//         мигать соответственно должны в соответствующем поле
//
//Проверка на критическое положение или положение по дальности
  long ControlForKrit_AvariaTwo(TDannyeObCeli *Dan,  //Данные о цели
                              double Period,       //Период в секундах
                              double lDopRast);       //Допустимое расстояние в метрах


  long ControlForKrit_AvariaKurs(
                           long lPervDalnostVMetrah,  //Первая дальность в метрах
                           long lDalnostVzleta,       //Дальность места взлета
                           long lRastOtOsiVPPDoKursa, //Расстояние от ОСИ ВПП до курса
                           long lMaxDalnostOtRLS,           //Максимальная дальность относительно РЛС
                           double lfTan,      //Значение тангенса
                           double lfTanMinus,  //Тангенс максим отклонения вниз
                           double lfTanPlus,   //Значение тангенса отклонения плюс
                           double lfTanMinus_3,//Значение тангенса угла отклонения угла/3
                           double lfTanPlus_3  //Значение тангенса угла/3
                            );


  long  ControlForKrit_AvariaGlis(
                           long lPervDalnostVMetrah,  //Первая дальность в метрах
                           long lDalnostVzleta,       //Дальность места взлета
                           long lDopVysotaGlis,       //Начальная высота глиссады
                           long lMaxDalnostOtRLS,     //Максимальная дальность относительно РЛС
                           double lfTan,      //Значение тангенса глиссалы
                           double lfTanMinus,  //Тангенс максим отклонения вниз
                           double lfTanPlus,   //Значение тангенса отклонения плюс
                           double lfTanMinus_3,//Значение тангенса угла отклонения угла/3
                           double lfTanPlus_3  //Значение тангенса угла/3
                            );


  QString getStringInfo();


//Определим тут оператор
  TDannyeObCeli& operator += (TDannyeObCeli &C)
  {
     if(this==&C)return *this;
     for(int i=0;i<20;i++)sPozyvnoy[i]=C.sPozyvnoy[i];
     lNomerVozdushnogoSudna=C.lNomerVozdushnogoSudna;
     Tip=C.Tip;
     X=-100000; Y=-100000; Z=-100000;
     Vx=-100000; Vy=-100000; Z=-100000;
     lHazardGlis=0;
     lHazardKurs=0;
     lHazardTwo=0;
     lOtklGlis=0;
     lOtklKurs=0;
     VyhodOtklGlis_3=false;
     VyhodOtklKurs_3=false;
     XvpGlis=100000;
     YvpGlis=100000;
     XvpKurs=100000;
     YvpKurs=100000;
     FormulyarRect=C.FormulyarRect;
     return *this;
  };

  //А тут просто присвоить
  TDannyeObCeli& operator = (TDannyeObCeli &C)
  {
     if(this==&C)return *this;
     for(int i=0;i<20;i++)sPozyvnoy[i]=C.sPozyvnoy[i];
     lNomerVozdushnogoSudna=C.lNomerVozdushnogoSudna;

     Tip=C.Tip;
     X=C.X; Y=C.Y; Z=C.Z;
     Vx=C.Vx; Vy=C.Vy; Vz=C.Vz;
     lHazardGlis=0;
     lHazardKurs=0;
     lHazardTwo=0;
     lOtklGlis=0;
     lOtklKurs=0;
     VyhodOtklGlis_3=false;
     VyhodOtklKurs_3=false;
     XvpGlis=C.XvpGlis;
     YvpGlis=C.YvpGlis;
     XvpKurs=C.XvpKurs;
     YvpKurs=C.YvpKurs;
     timeOfCel=C.timeOfCel;
     timeOfLife1_ms=C.timeOfLife1_ms;
     timeOfLife2_ms=C.timeOfLife2_ms;
     timeForDie1_ms=C.timeForDie1_ms;
     timeForDie2_ms=C.timeForDie2_ms;
     timeOfLifeWithOut_ms=C.timeOfLifeWithOut_ms;

     FormulyarRect=C.FormulyarRect;
     return *this;
  };



  TDannyeObCeli& operator = (const TDannyeObCeli &C)
  {
     if(this==&C)return *this;
     for(int i=0;i<20;i++)sPozyvnoy[i]=C.sPozyvnoy[i];
     lNomerVozdushnogoSudna=C.lNomerVozdushnogoSudna;

     Tip=C.Tip;
     X=C.X; Y=C.Y; Z=C.Z;
     Vx=C.Vx; Vy=C.Vy; Vz=C.Vz;
     lHazardGlis=0;
     lHazardKurs=0;
     lHazardTwo=0;
     lOtklGlis=0;
     lOtklKurs=0;
     VyhodOtklGlis_3=false;
     VyhodOtklKurs_3=false;
     XvpGlis=C.XvpGlis;
     YvpGlis=C.YvpGlis;
     XvpKurs=C.XvpKurs;
     YvpKurs=C.YvpKurs;
     timeOfCel=C.timeOfCel;
     timeOfLife1_ms=C.timeOfLife1_ms;
     timeOfLife2_ms=C.timeOfLife2_ms;
     timeForDie1_ms=C.timeForDie1_ms;
     timeForDie2_ms=C.timeForDie2_ms;
     timeOfLifeWithOut_ms=C.timeOfLifeWithOut_ms;


      FormulyarRect=C.FormulyarRect;
     return *this;
  };


  TDannyeObCeli& operator ++()
  {
    return *this;
  };

  //Получение информации находится ли точка клика внутри
  bool isVnutryOtmetki(int x_scr, int y_scr,struct TVidCeli &vid_celi);

};



//Вид цели.
struct TVidCeli
{

//Все три параметра для случаев
//Первый индекс определяет
//индекс 0, если всё в норме ...
//инлекс 1, для опасной ситуации
//инддекс 2, для критической ситуации.

    QPen lpCeliKurs[3][2];    //Задание обрамления цели в поле курса
    QBrush lbCeliKurs[3][2];  //Закраска цели в поле курса
    QPen lpCeliGlis[3][2];    //Задание обрамления цели в поле глиссады
    QBrush lbCeliGlis[3][2];  //Закраска цели в поле глиссады


  long lRazmerCeliKurs;  //Размер цели в пикселях для курса
  long lRazmerCeliGlis;  //Размер цели в пикселях для глиссады


                     //Размер квадрата, куда будет вписанацель
                     //Размер цели одинаков и в поле курса и в поле глиссады



  long szTextKurs;          //Размер текста цели в поле курса
  QColor cTextKurs;       //Цвет текста текста в поле курса
  long szTextGlis;          //Размер текста цели в поле курса
  QColor cTextGlis;       //Цвет текста текста в поле курса




  bool IsVyvodVektorSkorostiGlis; //Выводи скорости в поле курса
  bool IsVyvodVektorSkorostiKurs; //Выводи скорости в поле глиссады
  long lDlinaVektoraSkorosti;  //Длина вектора скорости в пикселях
  bool IsVyvodTextGlis;     //Выводить ли текст в поле глиссады
  bool IsVyvodTextKurs;     //Выводить ли текст в поле курса

  char LftText[10];         //Текст влево на кириллице    (это либо по-русски, либо по французски)
  char RghtText[10];        //Текст вправо

};




bool odCompareByNomerVozdSudna(const TDannyeObCeli &doc1, const TDannyeObCeli &doc2);






#endif
