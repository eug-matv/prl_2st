﻿/*Модуль: KursGraf
Автор: Матвеенко Е.А. ЧРЗ Полет ОКБ

Рисование сетки и отображение отметок в поле курса, рисуется в нижней части области
отображения.
Класс TKursDraw

*/

#ifndef KursGrafH
#define KursGrafH
//---------------------------------------------------------------------------

#include <math.h>

#include <QPen>
#include <QBrush>
#include <QRect>
#include <QPainter>
#include <QRect>
#include <QSize>
#include <QPoint>
#include <QColor>

#include <list>


#include "qt_tools.h"


#ifdef INI_GROUP
#undef INI_GROUP
#endif
#define INI_GROUP  "KURSGRAF"

using namespace std;

/*Класс TKursDraw обеспечивает отображение сетки в поле курса, а также
вывод отметок положения воздушного судна в поле курса. Данный класс является
одним из родителей наряду с TGlissadaDraw класса TRZPDraw (graphout/RZPLinGlaf).
*/
class TKursDraw
{


  unsigned int uiFlashStateKurs;   //Состояние используемое для организации мерцания

  long lMaxDalnOut;       //Максимальная дальность в м
                          //Отсчитывается от кромки взлетной полосы

  long lDopDalnOut;       //Точка посадки относительно РСП


  long lDopDalnOutOtsch;  //Начала точки отсчета координат - соответствует
                          //кромке взлетной полосы


  long lRastOtOsiVPPDoKursa; //Расстояние от оси ВПП до середины антенны курса


//Угломерные линии отличаются друг от друга на градус
  long lAzmtProcDaln;    //Процент с дальности на которой начинаются угломерные линии
                         //обычно 30


//Для не опорных линий азимутов
  long lAzmtRazmer10;   //Шаг азимутов в градусах умноженных на 10

  long lDopustMinAzmt10;    //Допустимое значение минимального значения азимута
  long lDopustMaxAzmt10;    //Допустимое значение максимального значения азимута

  long lMinAzmt10;   //Минимальное значение азимута (значение справа)
  long lMaxAzmt10;   //Максимальное значение азимута (значение слева)
  long lOporAzmtRazmer10; //Шаг опорных азимутов в градусах на 10
  long lGorUdalRazmer;   //Размер (Шаг) линий горизонтального удаления в м
//Опорные линии горизонтального удаления
  long lOporUdalRazmer;   //Шаг линий горизонтального удаления в м
//Линия посадочного курса
  long lLiniaPosadKursUgol10; //Значение угла линии посадочного курса на 10
//Зона допустимых отклонений
  long lMinusOtPosadKursUgol10;  //Минимальный угол зоны отклонения
  long lPlusOtPosadKursUgol10;  //Максимальный угол зоны отклонения
  long lMaxDalnostZonaOtkl; //Максимальная дальность зоны отклонения

//Сектор сканирования антены курса
  long lSektorSkanMin10;    //Минимальный угол сектора сканирования
  long lSektorSkanMax10;    //Максимальное значение угла сектора сканирования

//Диаграмма направленности
  long lDNMin10;          //Диагмрамма направленности - минимальный угол
  long lDNMax10;          //Диаграмма направленности - максимальный угол


//Допустимые пределы для углов в направлении поля глиссады, то есть
//паралельно Z
  long lMinUgolForZ10;  //Минимальный угол
  long lMaxUgolForZ10;  //Максимальный угол


//Положение БПРМ от торца ВПП
  long lPolojenieBPRM;  //Положение от торца в метрах

//Положение ДПРМ от торца ВПП
  long lPolojenieDPRM;  //значение в метрах




public:
  QPen lpAzmt;   //Перо линий
  bool bVyvodAzmt;    //Выводить неопорные азимутальные линии
  QColor cAzmtText;  //Цвет текста неопорных азимутальных линий
  long szAzmtText;     //Размер текста неопорных азимутальных линий
  bool bVyvodAzmtText; //Выводить текст для неопорных азимутов


  QPen lpOporAzmt;      //Перо линий опорных азимутов
  QColor cOporAzmtText; //Цвет текста опорных азимутов
  long szOporAzmtText;    //Размер текста опорных азимутов
//Не опорные линии горизонтального удаления
  QPen lpGorUdal;      //Перо линий горизонтального удаления
  bool bVyvodGorUdal;    //Выводить линии горизонтального удаления
  QColor cGorUdalText;  //Цвет для вывода текста значений линий горизонтального удаления
  long   szGorUdalText;   //Размер для текста горизонтального удаления
  bool bVyvodGorUdalText; //Выводить текст линий горизонтального удаления
  QPen lpOporGorUdal;   //Перо линии горизонтального удаления
  QColor cOporGorUdalText;  //Цвет линии горизонтального удаления
  long szOporGorUdalText;   //Размер линий горизонтального удаления
  QPen lpLiniaPosadKurs;


  QBrush lbZonaOtkl;      //Зоны отклонений

  bool bVyvodSektoraSkanKurs;
  QPen lpSektorSkan;      //Сектор сканирования
  QBrush lbSektorSkan;    //Закраска сектора сканирования


  bool bVyvodUgolGlisVKurs;
  QPen lpLDN;           //Линия диаграммы направленности
  QBrush lbLDN;         //Линия диаграммы направленности

/*Описание внешнего вида и размера БПРМ*/
  bool     bBPRMKurs;        //Отображать БРПМ
  QPen   lpBPRMKurs;       //Логическое перо БПРМ
  QBrush lbBPRMKurs;       //Логическая кисть БПРМ
  QSize     szBPRMKurs;       //Размер в пикселях экрана

/*Описание внешнего вида и размера ДПРМ*/
  bool     bDPRMKurs;        //Отображать ДРПМ
  QPen   lpDPRMKurs;       //Логическое перо ДПРМ
  QBrush lbDPRMKurs;       //Логическая кисть ДПРМ
  QSize     szDPRMKurs;       //Размер в пикселях экрана

protected:
  //Рисование одной точки -- промежуточная процедура
  //  1 цель
       long RisovatOdnuCelKurs(QPainter &pntr,    //Контекст устройства
                        const QRect &Rect, //Область куда рисуется цель
                        TDannyeObCeli &DannyeObCeli,
                        TVidCeli *VidCeli, //Начинаются рассматриваться с индекса 1, чтобы удобнее было
                        long lChisloVidovCeley, //Число видов целей
                        int isSledOtmetka        //Эта отметка в следе
                      );



//Промежуточная процедура для опреления расстояния цели до курсовой линии в метрах
//Возвращает: > -10000 < 10000  - если цель лежит в секторе курса
// -20000 - вне сектора курса
//20000 - вне сектора курса
// - координата вне пределов оси 30000
       int getRastoyanieCeliIKursa( TDannyeObCeli &DannyeObCeli);



       //Получение координаты окна из координат мировых
           int getXscrFromYwrld(int y)
           {
               return y*dfKoefWrldYToScrX+pScrFor0.x();
           }


           int getYscrFromXwrld(int x)
           {
               return x*dfKoefWrldXToScrY+pScrFor0.y();
           }



           int textOutWrld(QPainter &pntr,          //Дескриптер
                          int x_wrld, int y_wrld,    //Координаты в системе окна
                          const QFont &font,       //фонте
                          const QColor &col,
                          const QString &str,
                          enum QTT_GdeText qtt,       //0 - с
                          int otstup
                         )
           {
               return textOutScr(pntr,
                         getXscrFromYwrld(y_wrld), getYscrFromXwrld(x_wrld),
                         font, col,str,qtt,otstup
                                 );
           }


           void drawPolygonWrld(QPainter &pntr,       //Куда выводить
                                 const QPoint p[],     //Сами точки
                                 int  n_of_p)          //ЧИсло точек
           {
               QPoint p1[100];
               for(int i=0;i<n_of_p;i++)
               {
                   p1[i].setX(getXscrFromYwrld(p[i].y()));
                   p1[i].setY(getYscrFromXwrld(p[i].x()));
               }
               pntr.drawPolygon(p1,n_of_p);
           }


   void  updateFlashStateKurs()
  {
        uiFlashStateKurs=(uiFlashStateKurs+1)%2;
  };


   int getFlashStateKurs()
  {
        return   uiFlashStateKurs;
  };

private:
  /*Дополнительные данные для вывода. Задают соответствие всех логических и физических координат*/
  QRect RectOut;    //Область вывода

  int min_otkl_kurs_m, max_otkl_kurs_m;     //Минимальные и максимальные отклонения


  double dfKoefScrXToWrldY, //Коэффициент перехода из устройства в мировую систему координат
         dfKoefScrYToWrldX; //Коэффициент перехода из устройства в мировую систему координат


  double dfKoefWrldXToScrY, //Коэффициент перехода из мир.сист. коорд в систему координат устрйоства
         dfKoefWrldYToScrX; //Коэффициент перехода из мир.сист. коорд в систему координат устройства


  QPoint pScrFor0;          //Нулевая точка начала координат




  long ZadanieDopDataOut(const QRect &Rect);



//Нарисовать линию азимута
  long VyvestiLiniuAzimuta(QPainter &pntr, long Azmt10);


//Нарисовать линию линии горизонтального удаления
  long VyvestiLiniuGorUdal(QPainter &pntr, long Udal_m);


//Вывести линию посадочного курса
  long VyvestiLiniuPosadKursa(QPainter &pntr);


//ВЫвести зону допустимых отклонений
  long VyvestiZonuDopustOtkl(QPainter &pntr);


//Вывод сектора сканирования внутри
  long VyvestiSektorSkanSekt(QPainter &pntr);

//Вывод сектора сканирования но границ
  long VyvestiSektorSkanLin(QPainter &pntr);


//Вывести сектор положения диаграммы направленности
  long VyvestiLiniuDNSekt(QPainter &pntr);

//Вывести границы положения диаграммы направленности
  long VyvestiLiniuDNLin(QPainter &pntr);


//Вывод линии
  long VyvestiLiniu(QPainter &pntr,
                    long Azmt10,
                    const QPen &lp,
                    long FirstDalnost, //Обычно равно или 0, или lDopDalnOut
                    long OstupOtKursa, //Если <0, то будет правее, если больше, то левее
                    long *Xlast=NULL, long *Ylast=NULL //Куда помещается помещается последняя выделенная точка
                    );

//Вывод Сектора
  long VyvodSektora(QPainter &pntr,
                    long MinSAzmt10,
                    long MaxSAzmt10,
                    const QBrush &lb,
                    long FirstDalnost,
                    long OstupOtKursa, //Если <0, то будет правее, если больше, то левее
                    long MaxSDalnost);

//Вывести маяки
  long VyvestiBPRMandDPRMKurs(QPainter &pntr);

//Рисовать линии. Функция рисует линии со временным переводом в предыдущую систему координат
  long RisovatLiniuDlyaKursa(QPainter &pntr,
      long X1, long Y1, long X2, long Y2,
      const QPen  &lp);


//Рисовать линии. Функция рисует линии со временным переводом в предыдущую систему координат
  long RisovatLiniuDlyaKursa(QPainter &pntr,
      double X1, double Y1, double X2, double Y2,
      const QPen &lp)
  {
        return RisovatLiniuDlyaKursa(pntr, (long)X1, (long)Y1, (long)X2, (long)Y2, lp);
  };

  public:
  TKursDraw();

//Рисовать  сетку курса
  long PaintKurs(QPainter &pntr,     //Конектс устройства
             const QRect &Rect); //Ограниченная область куда должна вписаться

//Рисовать цели
  long RisovatCeliKurs(
                 QPainter &pntr,    //Контекст устройства
                 const QRect &Rect, //Область куда рисуется цель
                 list<TDannyeObCeli> &ldoc,
                 list<TDannyeObCeli> &ldoc_tail,
                 TVidCeli *VidCeli, //Начинаются рассматриваться с индекса 1, чтобы удобнее было
                 long lChisloVidovCeley //Число видов целей
                );

//Загрузка данных из Ини файла
  long SaveDataToINI(const char* lpszINIFileName);

//Загрузка данных из ИНИ
  long LoadDataFromINI(const char* lpszINIFileName);


/*Установка новых масштабов по дальности и по горизонтали*/
  long NovyyMasshtabDalnosti(long NewMaxDaln)
  {
    if(NewMaxDaln<2000||NewMaxDaln>80000)return 0;
    if(NewMaxDaln<=5000)
    {
      lGorUdalRazmer=200;
      lOporUdalRazmer=1000;
    }else if(NewMaxDaln<=15000)
    {
      lGorUdalRazmer=500;
      lOporUdalRazmer=2000;
    }else if(NewMaxDaln<=30000)
    {
      lGorUdalRazmer=1000;
      lOporUdalRazmer=5000;
    }else if(NewMaxDaln<=50000)
    {
      lGorUdalRazmer=2000;
      lOporUdalRazmer=10000;
    }else{
      lGorUdalRazmer=5000;
      lOporUdalRazmer=10000;
    }
    lMaxDalnOut=NewMaxDaln;
    return 1;
  };

  long GetMasshtabDalnosti(void)
  {
    return lMaxDalnOut;
  };

/*Масштаб по углу*/
  long NovyyMasshtabAzimuta(double dUgl)
  {
    long Okrugl(double Chislo1); //Реализована в AnyTools.cpp
    long lMinAzmt10a, lMaxAzmt10a;
    lMinAzmt10a=-Okrugl(10.0*dUgl);
    lMaxAzmt10a= Okrugl(10.0*dUgl);
    if(lMinAzmt10a>0 || lMaxAzmt10a<0 || lMaxAzmt10a-lMinAzmt10a<30)return 0;
    lMinAzmt10=lMinAzmt10a; lMaxAzmt10=lMaxAzmt10a;
    if(lMinAzmt10>lDopustMinAzmt10)lMinAzmt10=lDopustMinAzmt10;
    if(lMaxAzmt10<lDopustMaxAzmt10)lMaxAzmt10=lDopustMaxAzmt10;
    if(lMaxAzmt10>(-lMinAzmt10))
    {
        lMinAzmt10=-lMaxAzmt10;
    }else{
        lMaxAzmt10=-lMinAzmt10;
    }
    if(lMaxAzmt10-lMinAzmt10<100)
    {
      lAzmtRazmer10=5;
      lOporAzmtRazmer10=20;
    }else{
      lAzmtRazmer10=10;
      lOporAzmtRazmer10=50;
    }
    return 1;
  };

  long GetMasshtabAzimuta(double *dUgl)
  {
    *dUgl=lMaxAzmt10/10.0;
    return 1;
  }


/*Далее просто доступ к приватным данным*/
   long NovoeRasstoyanieRSPOtMestaPosadki(long NewDopDaln)
  {
    long ugl10;
    if(NewDopDaln<0||NewDopDaln>1500)return 0;
    lDopDalnOut=NewDopDaln;


    //Расчитаем допустимые значения углов
          ugl10=(long)(atan2(lRastOtOsiVPPDoKursa,lDopDalnOut)/M_PI*1800.0);
          if(ugl10<=-10)
          {
            lDopustMinAzmt10=ugl10-1;
            lDopustMaxAzmt10=10;
          }else
          if(ugl10>=10)
          {
            lDopustMaxAzmt10=ugl10+1;
            lDopustMinAzmt10=-10;
          }else{
            lDopustMinAzmt10=-10;
            lDopustMaxAzmt10=10;
          }


    return 1;
  };
  long GetRasstoyanieRSPOtMestaPosadki(void)
  {
   return lDopDalnOut;
  };

/**/
/*Новое расстояние конца ВПП от РСП*/
  long NovoeRasstoyanieRSPOtTorcaVPP(long NewD)
  {
     if(NewD<0||NewD>2000)return 0;
     lDopDalnOutOtsch=NewD;
     return 1;
  };
  long GetRasstoyanieRSPOtTorcaVPP(void)
  {
     return lDopDalnOutOtsch;
  };



//Установить процент
  long NovyyProcentOtsekaemyhNeopornyhAzimutov(long NewProc)
  {
    if(NewProc>=0&&NewProc<100)
    {
      lAzmtProcDaln=NewProc;
      return 1;
    }
    return 0;
  };

  long GetProcentOtsekaemyhNeopornyhAzimutov(void)
  {
    return lAzmtProcDaln;
  };


  /*Диапазон сектора сканирования*/
  long NovyySektorSkanirovania(double Ugol1, double Ugol2)
  {
    long Okrugl(double Chislo1);

    long lSektorSkanMin10a,lSektorSkanMax10a;
    lSektorSkanMin10a=Okrugl(10*Ugol1);
    lSektorSkanMax10a=Okrugl(10*Ugol2);
    if(lSektorSkanMin10>lSektorSkanMax10)return 0;
    lSektorSkanMin10=lSektorSkanMin10a;
    lSektorSkanMax10=lSektorSkanMax10a;
    return 1;
  };

  long GetSektorSkanirovania(double *Ugol1, double *Ugol2)
  {
     *Ugol1=lSektorSkanMin10/10.0;
     *Ugol2=lSektorSkanMax10/10.0;
     return 1;
  };

/*Диапазон сектора ДН азимута*/
  long NovyyDNGlis(double Ugol1, double Ugol2)
  {
    long Okrugl(double Chislo1);

    long lDNMin10a,lDNMax10a;
    lDNMin10a=Okrugl(10*Ugol1);
    lDNMax10a=Okrugl(10*Ugol2);
    if(lDNMin10a>lDNMax10a)return 0;
    lDNMin10=lDNMin10a;
    lDNMax10=lDNMax10a;
    return 1;
  };

  long GetDNGlis(double *Ugol1, double *Ugol2)
  {
     *Ugol1=lDNMin10/10.0;
     *Ugol2=lDNMax10/10.0;
     return 1;
  };

/*Сектор линии глиссады*/
  long NovayLiniyaPosadKursa(
                            long RasstoyanieVPPDoRSP,
                            double UgolKursa,
                            double OtstupMenshe,
                            double OtstupBolshe,
                            long MaxDalnostSektora
                            )
  {
      long Okrugl(double Chislo1);
      long ugl10;
      lRastOtOsiVPPDoKursa=RasstoyanieVPPDoRSP;
      lLiniaPosadKursUgol10=Okrugl(UgolKursa*10.0);
      lMinusOtPosadKursUgol10=Okrugl(OtstupMenshe*10.0);
      lPlusOtPosadKursUgol10=Okrugl(OtstupBolshe*10.0);
      lMaxDalnostZonaOtkl=MaxDalnostSektora;

//Расчитаем допустимые значения углов
      ugl10=(long)(atan2(lRastOtOsiVPPDoKursa,lDopDalnOut)/M_PI*1800.0);
      if(ugl10<=-10)
      {
        lDopustMinAzmt10=ugl10-1;
        lDopustMaxAzmt10=10;
      }else
      if(ugl10>=10)
      {
        lDopustMaxAzmt10=ugl10+1;
        lDopustMinAzmt10=-10;
      }else{
        lDopustMinAzmt10=-10;
        lDopustMaxAzmt10=10;
      }
      return 1;
  };

  double GetUgolKursa(void)
  {
    return lLiniaPosadKursUgol10/10.0;
  };

  long GetMaxDalnostSektoraKursa(void)
  {
    return lMaxDalnostZonaOtkl;
  };

  long NovyyDopustimyyDiapazonPoZ(double Ugl1, double Ugl2)
  {
    long Okrugl(double Chislo1);
    long lMinUgolForZ10a,lMaxUgolForZ10a;
    lMinUgolForZ10a=Okrugl(Ugl1*10.0);
    lMaxUgolForZ10a=Okrugl(Ugl2*10.0);
    if(lMinUgolForZ10a>=lMaxUgolForZ10a)
    {
       return 0;
    }
    lMinUgolForZ10=lMinUgolForZ10a;
    lMaxUgolForZ10=lMaxUgolForZ10a;
    return 1;
  };



};
#endif
