﻿/*Модуль: tgettingdatathread
Автор: Матвеенко Е.А. ЧРЗ Полет ОКБ

Реализован вторичный поток TGettingDataThread чтения данных с последовательного порта или
через сетевой протокол UDP в зависимости от настроек.

Каждый полученный байт передается функции luAddByteToFIFOInLUA (модуль lua_util/lua_utils),
которая обеспечивает обработку входных данных средствами языка LUA.

*/

#ifndef TGETTINGDATATHREAD_H
#define TGETTINGDATATHREAD_H

#include <QThread>
#include <QMutex>
#include <QUdpSocket>
#include <QObject>

#include "RZPLinGlaf.h"
#include "tools_win_unix.h"


/*Тип источника*/
enum TGDTTypeOfSource
{
    TGDTUnknown=0,
    TGDTComPort,   //Через последовательный порт (или имитирующие работу порта файлы)
    TGDT_UDP       //через сетевой протокол UDP
};


struct TGIDTComPortOptions
{
    char port_name[100];    //имя порта
    int baudrate;       //скорость передачи
    int stopbits;   //0 - 1 стоп бит, 1 - 1.5 стопбита, 2 - 2 стопбита
};

struct TGID_UDP_IPGettingData
{
    char address[255];  //адрес интерфейса сетевого протокола UDP
    int port;           // номер входящего порта

};


/*Общая информация о параметрах получения*/
struct TGDITOptions
{
    TGDTTypeOfSource tos;  //Тип источника
    union
    {
        TGIDTComPortOptions com;    //Параметры последовательного порта
        TGID_UDP_IPGettingData udp; //Параметры сетевого протокола UDP
    } opt;
    long tmOfCurVO;
    long tmOfViewAfterDieVO;
    long tmOfCurTrs;
    long tmOfHvstTrs;
    long tmOfViewAfterDieTrs;
};


/*Основной класс модуля, реализующий вторичный поток для чтения данных из порта
 * или по сетевому протоколу UDP.
Обеспечивает чтение с последовательного порта или по сетевому протоколу UDP.

*/
class TGettingDataThread : public QThread
{
    Q_OBJECT
public:
    explicit TGettingDataThread(QObject *parent = 0);
    int setOptions(const TGDITOptions &_opt);   //Установка опций (описание выше)
    int setRZP_DRAW( TRZPDraw *_rzp_draw);  //Установка указателя объекта обеспечивающего вывод данных
    int setMutexForSynchr( QMutex *_mutex); //Мьютек для синхронизации
    void slot_GetDataFromComport();
    void slot_GetDataFromUDP();
    int errCode;        //0 - еще не запускалось.
                        //1 - всё успешно.
                        //-1 - не были установлены свойства чтения данных, то есть не запускалось
                        //setOptions
                        //-2 - ошибка работы

protected:
    virtual void run ();
    int run_com();  //Вызывается из run(), если тип источника - последовательный порт
    int run_udp();  //Вызывается из rub(), если тип источника - сеть по UDP

/*Обработка пакета полученногог байта  данных.
Данные передаются функции luAddByteToFIFOInLUA, которая обеспечивает разбор на языке lua
а затем если был получен очередной пакет с данными, он передается _rzp_draw для отображеняи
параметры:
data - массив байт
sz_data - число байт
Возвращает: 1
*/
    int workWithGettingData(unsigned char *data, int sz_data);

private:
    TGDITOptions opt;   //Настройки источника данных
   TRZPDraw *rzp_draw;  //Объект для отображения данных - реализовн в модуле
                        //graphout/RZPLinGlaf
   QMutex *mutex;
   TWUFileDescr *twuFile;   //дескриптор для чтения с посл.порта
   QUdpSocket *udpSocket;   //сокет UDP
};


/*Объект полупустышка, чтобы быть созданным во вторичном потоке и ловить сигналы
при получении байт из сети по протоколу UDP*/
class TUdpReader : public QObject
{
    Q_OBJECT
public:
     TUdpReader(class TGettingDataThread *parent):QObject()
    {
        gdt=parent;
    };

public slots:
        void slot_GetDataFromUDP()
        {
            gdt->slot_GetDataFromUDP();
        };

private:
   class TGettingDataThread *gdt;

};

/*Объект полупустышка, чтобы быть созданным во вторичном потоке и ловить сигналы
при получении байт из сети с последовательного порта*/

class TComReader : public QObject
{
    Q_OBJECT
public:
    TComReader(class TGettingDataThread *parent):QObject()
    {
        gdt=parent;
    };

public slots:
        void slot_GetDataFromComport()
        {
           gdt->slot_GetDataFromComport();
        };

private:
   class TGettingDataThread *gdt;


};


#endif // TGETTINGDATATHREAD_H
