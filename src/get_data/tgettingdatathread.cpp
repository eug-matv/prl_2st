﻿#include <string.h>

#include <QTimer>
#include <QHostAddress>
#include <QString>
#include <QByteArray>
#include "tgettingdatathread.h"
#include "lua_utils.h"



TGettingDataThread::TGettingDataThread(QObject *parent) :
    QThread(parent)
{
    opt.tos=TGDTUnknown;
    rzp_draw=NULL;
    mutex=NULL;
    twuFile=NULL;

}




void TGettingDataThread::run()
{
    if(!rzp_draw||!mutex||opt.tos==TGDTUnknown)
	{
		errCode=-1;
		return;
	}
	errCode=0;
	if(opt.tos==TGDTComPort)
	{
        errCode=run_com();
	}else{
        errCode=run_udp();
	}
	
		
}


int TGettingDataThread::setOptions(const TGDITOptions &_opt)
{
    if(isRunning())
    {
        return 0;
    }

    memcpy(&opt,&_opt,sizeof(opt));
    return 1;
}


int TGettingDataThread::setRZP_DRAW( TRZPDraw *_rzp_draw)
{
    if(isRunning())
    {
        return 0;
    }
    rzp_draw=_rzp_draw;
    return 1;
}


int TGettingDataThread::setMutexForSynchr( QMutex *_mutex)
{
    if(isRunning())
    {
        return 0;
    }
    mutex=_mutex;
    return 1;
}



int TGettingDataThread::run_com()
{


    QTimer *timer;
    TComReader *comreader;
    int err;

    twuFile=twuOpenFileWork(opt.opt.com.port_name,opt.opt.com.baudrate,opt.opt.com.stopbits);
    if(!twuFile)return 0;
    if(twuFile->iError)
    {
        errCode=err=twuFile->iError;
        twuCloseFile(twuFile);
        return err;
    }
    timer = new QTimer;
    timer->setInterval(100);
    comreader=new TComReader(this);
    connect(timer, SIGNAL(timeout()), comreader, SLOT(slot_GetDataFromComport()));
    timer->start();
    exec();
    timer->stop();
    delete timer;
    twuCloseFile(twuFile);
    twuFile=NULL;

    delete comreader;

    return 0;
}


int TGettingDataThread::run_udp()
{
    int ret;
    class TUdpReader *udpreader;


    udpreader=new TUdpReader(this);

    udpSocket=new QUdpSocket;
    ret=udpSocket->bind(QHostAddress::Any,opt.opt.udp.port);

    connect(udpSocket, SIGNAL(readyRead()), udpreader, SLOT(slot_GetDataFromUDP()));



    exec();

    delete udpSocket;
    udpSocket=NULL;	

    delete udpreader;

    return 1;	
}


int TGettingDataThread::workWithGettingData(unsigned char *data, int sz_data)
{

    int i;
    int iRet;
    TDannyeObCeli doc;
    mutex->lock();

    for(i=0;i<sz_data;i++)
    {
        iRet=luAddByteToFIFOInLUA(data[i]);
        if(iRet==KOORD_INFO_CONST||iRet==TRASS_INFO_CONST)
        {
            iRet=luGetPRLInfo(doc);
            if(iRet==1)
            {
                rzp_draw->DobavitNovuyuCel(doc);
            }
        }

    }

    mutex->unlock();

    return 1;
}


//СЛОТЫ
 void TGettingDataThread::slot_GetDataFromComport()
 {
     int n,iRet;
     unsigned char data[1000];
     n=twuReadFile(twuFile,data,1000);
     iRet=workWithGettingData(data,n);
     if(iRet!=1)
     {
//Обработать

     }

 }


void TGettingDataThread::slot_GetDataFromUDP()
{
    int iRet;
	while (udpSocket->hasPendingDatagrams()) 
	{
         	QByteArray datagram;
         	datagram.resize(udpSocket->pendingDatagramSize());
         	QHostAddress sender;
         	quint16 senderPort;

         	udpSocket->readDatagram(datagram.data(), datagram.size(),
	                                 &sender, &senderPort);
		
        iRet=workWithGettingData((unsigned char*)(datagram.data()),
                datagram.size());
     		if(iRet!=1)
    		{
//Обработать
     		}
		
	}
}





