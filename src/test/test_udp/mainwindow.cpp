#include <QString>
#include <QPushButton>
#include <QFileDialog>
#include <QMessageBox>

#include <time.h>
#include <stdio.h>
#include "lua.hpp"
#include "lua_utils.h"

#include "mainwindow.h"
#include "ui_mainwindow.h"




static struct
{
    char address[200];
    quint32 remote_port;
    quint32 local_port;
    char start_data[4000];
    char next_com[4000];
    char luaPath[1000];

}tmpOption;



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    timer=NULL;
    udpSocket=NULL;





    connect(ui->pushButton, SIGNAL(clicked()),this, SLOT(startStopCommand()));

    connect(ui->mnuOpen, SIGNAL(triggered()),this,SLOT(openOptionFile()));
    connect(ui->mnuSave, SIGNAL(triggered()),this,SLOT(saveOptionFile()));
    connect(ui->mnuQuit, SIGNAL(triggered()),this,SLOT(close()));


    loadFromFile(QApplication::applicationDirPath()+QString("/lastconfig.cfg"));

}

MainWindow::~MainWindow()
{
    saveToFile(QApplication::applicationDirPath()+QString("/lastconfig.cfg"));
    delete ui;
}




void MainWindow::timerForOutput()
{
    int iRet;

    iRet=luDoString(ui->teNext->toPlainText());

    if(iRet!=1)
    {
        ui->teDataOut->setPlainText("Error of Next Code");
        return;
    }

    ui->teDataOut->setPlainText(luTestGetPackageInfo());

    iRet=luTestGetBytes(bytes_out);
    if(iRet<=0)
    {
        ui->teDataOutBytes->setPlainText("Error of Making package!");
        return;
    }


    sz_bytes_out=iRet;

    ui->teDataOutBytes->setPlainText(luTestGetBytesOut());


    //Пошлем пакет UDP
       iRet=udpSocket->writeDatagram((const char*)(bytes_out),sz_bytes_out,
                                QHostAddress(ui->lineEdit->text()),ui->sbPort->value());


       if(iRet>0)
       {
           ui->lPeredanoBytes->setText(QString("Передано ")+QString::number(iRet)+
                                       QString(" байт"));
       }else{
           ui->lPeredanoBytes->setText(QString("Байты не переданы "));
       }


}



void MainWindow::startStopCommand()
{
    time_t tm_t;
    struct tm* m_tm;
    char str_time[100];
    int iRet;
    if(timer)
    {

            ui->pushButton->setText(QString("старт передачи информации о трассе ВО"));
            timer->stop();
            delete timer;
            timer=NULL;
            delete udpSocket;
            udpSocket=NULL;
            luCloseLua();
    }else{

        iRet=luLoadLuaFilesFromDirectory(ui->leLUAPath->text());
        if(iRet<0)
        {
            ui->teDataOut->setPlainText("Не найден каталог с файлами LUA");
            return;
        }

        if(iRet>1000)
        {
            ui->teDataOut->setPlainText("Не все файлы LUA загружены");
            return;
        }

        iRet=luDoString(ui->teStarting->toPlainText());

        if(iRet!=1)
        {
            ui->teDataOut->setPlainText("Error of Starting");
            luCloseLua();
            return;
        }

        ui->teDataOut->setPlainText(luTestGetPackageInfo());

        iRet=luTestGetBytes(bytes_out);
        if(iRet<=0)
        {
            ui->teDataOutBytes->setPlainText("Error of Making package!");
            luCloseLua();
            return;
        }


        sz_bytes_out=iRet;



        ui->teDataOutBytes->setPlainText(luTestGetBytesOut());


        ui->pushButton->setText(QString("Стоп"));
        timer=new QTimer(this);
        timer->setInterval(1000);
        connect(timer,SIGNAL(timeout()),this, SLOT(timerForOutput()));

        udpSocket=new QUdpSocket();
        udpSocket->bind(QHostAddress::Any,ui->sbLocalPort->value());


     //Пошлем пакет UDP
        iRet=udpSocket->writeDatagram((const char*)(bytes_out),sz_bytes_out,
                                 QHostAddress(ui->lineEdit->text()),ui->sbPort->value());


        if(iRet>0)
        {
            ui->lPeredanoBytes->setText(QString("Передано ")+QString::number(iRet)+
                                        QString(" байт"));
        }else{
            ui->lPeredanoBytes->setText(QString("Байты не переданы "));
        }


       timer->start();
    }
}




void MainWindow::sendDataForKoordInfo()
{
    time_t tm_t;
    struct tm* m_tm;
    int iRet;
    bool noUdpSocket=false;
    if(!udpSocket)
    {
        udpSocket=new QUdpSocket();
        udpSocket->bind(QHostAddress::Any,ui->sbLocalPort->value());
        noUdpSocket=true;
    }





/*    iRet=udpSocket->writeDatagram((const char*)(tcci2.data),18,
                             QHostAddress(ui->lineEdit->text()),ui->sbPort->value());
*/


    if(iRet==18)
    {
    }else{
    }
    if(noUdpSocket)
    {
        delete udpSocket;
        udpSocket=NULL;
    }
}


void MainWindow::openOptionFile()
{
    int iRet;
    QString fileName;
    fileName=QFileDialog::getOpenFileName(this,tr("Опции трека"),QString(),
                tr("Config (*.cfg);;All files (*.*)"));

    if(!fileName.isNull())
    {
        iRet=loadFromFile(fileName);
        if(!iRet)
        {
            QMessageBox::warning(this,tr("Ошибка открытия файла"),tr("Ошибка открытия файла с данными"));
        }
    }
}


void MainWindow::saveOptionFile()
{
    int iRet;
    QString fileName;
    fileName=QFileDialog::getSaveFileName(this,tr("Опции трека"),QString(),
                tr("Config (*.cfg);;All files (*.*)"));

    if(!fileName.isNull())
    {
        iRet=saveToFile(fileName);
        if(!iRet)
        {
            QMessageBox::warning(this,tr("Ошибка открытия файла"),tr("Ошибка сохранения данных"));
        }
    }

}





int MainWindow::loadFromFile(const QString &fileName)
{

    QFile file(fileName);
    if(file.exists())
    {

        if(file.open(QIODevice::ReadOnly))
        {
              if(file.read((char*)(&tmpOption),sizeof(tmpOption))==sizeof(tmpOption))
              {
                  ui->lineEdit->setText(tr(tmpOption.address));
                  ui->sbPort->setValue(tmpOption.remote_port);
                  ui->sbLocalPort->setValue(tmpOption.local_port);
                  ui->teStarting->setPlainText(tr(tmpOption.start_data));
                  ui->teNext->setPlainText(tr(tmpOption.next_com));
                  ui->leLUAPath->setText(tr(tmpOption.luaPath));
                  file.close();
                  return 1;
              }
              file.close();
        }
    }
    return 0;
}


int MainWindow::saveToFile(const QString &fileName)
{
        QFile file(fileName);
        if(file.open(QIODevice::ReadWrite|QIODevice::Truncate))
        {
            tmpOption.remote_port=ui->sbPort->value();
            tmpOption.local_port=ui->sbLocalPort->value();
            strcpy(tmpOption.address,ui->lineEdit->text().toUtf8().data());
            strcpy(tmpOption.start_data,ui->teStarting->toPlainText().toUtf8().data());
            strcpy(tmpOption.next_com,ui->teNext->toPlainText().toUtf8().data());
            strcpy(tmpOption.luaPath,ui->leLUAPath->text().toUtf8().data());

            file.write((char*)(&tmpOption),sizeof(tmpOption));
            return 1;
        }
        return 0;
}
