#-------------------------------------------------
#
# Project created by QtCreator 2014-05-30T11:36:16
#
#-------------------------------------------------

QT       += core  network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test_udp
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    ../../lua_src/lapi.c \
    ../../lua_src/lauxlib.c \
    ../../lua_src/lbaselib.c \
    ../../lua_src/lbitlib.c \
    ../../lua_src/lcode.c \
    ../../lua_src/lcorolib.c \
    ../../lua_src/lctype.c \
    ../../lua_src/ldblib.c \
    ../../lua_src/ldebug.c \
    ../../lua_src/ldo.c \
    ../../lua_src/ldump.c \
    ../../lua_src/lfunc.c \
    ../../lua_src/lgc.c \
    ../../lua_src/linit.c \
    ../../lua_src/liolib.c \
    ../../lua_src/llex.c \
    ../../lua_src/lmathlib.c \
    ../../lua_src/lmem.c \
    ../../lua_src/loadlib.c \
    ../../lua_src/lobject.c \
    ../../lua_src/lopcodes.c \
    ../../lua_src/loslib.c \
    ../../lua_src/lparser.c \
    ../../lua_src/lstate.c \
    ../../lua_src/lstring.c \
    ../../lua_src/lstrlib.c \
    ../../lua_src/ltable.c \
    ../../lua_src/ltablib.c \
    ../../lua_src/ltm.c \
    ../../lua_src/lundump.c \
    ../../lua_src/lutf8lib.c \
    ../../lua_src/lvm.c \
    ../../lua_src/lzio.c \
    ../../lua_util/lua_utils.cpp \
    ../../tools/safe_function.cpp \
    ../../tools/time_tools.cpp \
    ../../graphout/AnyTools.cpp

HEADERS  += mainwindow.h \
    ../../lua_src/lapi.h \
    ../../lua_src/lauxlib.h \
    ../../lua_src/lcode.h \
    ../../lua_src/lctype.h \
    ../../lua_src/ldebug.h \
    ../../lua_src/ldo.h \
    ../../lua_src/lfunc.h \
    ../../lua_src/lgc.h \
    ../../lua_src/llex.h \
    ../../lua_src/llimits.h \
    ../../lua_src/lmem.h \
    ../../lua_src/lobject.h \
    ../../lua_src/lopcodes.h \
    ../../lua_src/lparser.h \
    ../../lua_src/lprefix.h \
    ../../lua_src/lstate.h \
    ../../lua_src/lstring.h \
    ../../lua_src/ltable.h \
    ../../lua_src/ltm.h \
    ../../lua_src/lua.h \
    ../../lua_src/lua.hpp \
    ../../lua_src/luaconf.h \
    ../../lua_src/lualib.h \
    ../../lua_src/lundump.h \
    ../../lua_src/lvm.h \
    ../../lua_src/lzio.h \
    ../../lua_util/lua_utils.h \
    ../../tools/my_stdafx.h \
    ../../tools/my_stdlib.h \
    ../../tools/safe_fucntion.h \
    ../../tools/time_tools.h \
    ../../graphout/AnyTools.h

FORMS    += mainwindow.ui

INCLUDEPATH += ../../graphout \
                ../../lua_src \
                ../../lua_util \
                ../../tools
