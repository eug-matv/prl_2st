#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QUdpSocket>
#include <QSystemSemaphore>
#include "lua.hpp"
#include "lua_utils.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QTimer *timer;
    QUdpSocket *udpSocket;
    double xcur,ycur,zcur;




    unsigned char bytes_out[100];
    int sz_bytes_out;

protected:
    int loadFromFile(const QString &fileName);
    int saveToFile(const QString &fileName);

    bool isCanSendData();

protected slots:
    void timerForOutput();      //Обработка ежесекундной посылки информации
    void startStopCommand();    //Обработка нажатия кнопки - запустить или остановить поссылку информации

    void sendDataForKoordInfo();    //Посылка координатной информации

    void openOptionFile();
    void saveOptionFile();
};

#endif // MAINWINDOW_H
