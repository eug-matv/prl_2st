/*Модуль: tools_unix
Автор: Матвеенко Е.А. ЧРЗ Полет ОКБ

Реализована процедура findProcessesOpenFile для поиска дескрипторов процессов, владеющих
файлом file_name. Данная процедура предназначена для использования в ОС Linux, вызывается
в модуле comport_work/tools_win_unix в процедуре twuCloseFile.
*/


#ifndef TOOLS_UNIX_H_INCLUDED
#define TOOLS_UNIX_H_INCLUDED

/*findProcessesOpenFile - определяет, сколько процессов владеют файлом file_name
 * in: file_name  -- имя (с путем) проверяемого файла;
 * out: proc_id --- массив для получения дескрипторов, которые владеют файлом.
 * out: max_n_of_proc_id -- размер массива proc_id, если дескрипторов боль
 *      то их заполняется только max_n_of_proc_id
 * возвращает: число процессов, владеющих файлом file_name.
 * */
int findProcessesOpenFile(const char *file_name, int *proc_id, int max_n_of_proc_id);



/*findProcessesOpenFile - определяет, сколько процессов владеют файлом file_name
 * in: file_name  -- имя (с путем) проверяемого файла;
 *  * возвращает: число процессов, владеющих файлом file_name.
 * */
int findProcessesOpenFile(const char *file_name);
#endif // TOOLS_UNIX_H_INCLUDED
