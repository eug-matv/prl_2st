
#include "my_stdafx.h"

#include "my_stdlib.h"
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>



#ifdef linux
	#include <termios.h>
        #include <unistd.h>
        #include <sys/stat.h>
        #include <semaphore.h>
        #include "tools_unix.h"
#else
	#include <windows.h>
        #include <io.h>
#endif




#include "tools_win_unix.h"
#include "safe_function.h"


#define MIN(x,y)  (((x)>(y))?(y):(x))
/*Создание каталога. MKDIR (имя каталога с путем в виде ANSI). Если успешно, возвращает 0, иначе возвращает 1.*/
#ifdef _WIN32
        #define twuMKDIR(dir) (!CreateDirectoryA((dir),NULL))
#else
        #define twuMKDIR(dir) mkdir(dir, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP)
#endif




TWUFileDescr* twuOpenFileTemp(
                    char *file_name,
                    int speed,
                    int stop_bits, //0 -1, 1 - 1.5, 2 -2
			     	int isTest)
{
        int i;
 	TWUFileDescr* twuF;
        char *file_name1;
	int iRet;
	int max_size_fifo, in_fifo,out_fifo;

#ifdef linux
	struct termios options;

#else
	DCB dcb;
        COMMTIMEOUTS cto;
#endif

//1 создадим объект типа
	twuF=(TWUFileDescr*)malloc(sizeof(TWUFileDescr));
	if(!twuF)return twuF;
        memset(twuF,0,sizeof(TWUFileDescr));
        i=0;

//Удалим на всякий случай пробелы в начале строки
	while(file_name[i]==' '||file_name[i]=='\t')i++;
	file_name1=file_name+i;
	i=strlen(file_name1);
        if(file_name[0]==0)
        {
            free(twuF);
            return NULL;
        }
#ifdef linux
//Определим - это имя устройства или имя канала
        if(file_name1[0]=='/'&&file_name1[1]=='d'&&file_name1[2]=='e'&&file_name1[3]=='v'&&file_name1[4]=='/')
	{
		twuF->isSerial=1;
		twuF->i_fd2=-1;



//Открыть порт на чтение и запись
                twuF->i_fd1=sfOpen(file_name1, O_RDWR |  O_NOCTTY | O_NDELAY);
		if(twuF->i_fd1<0)
		{
                        twuF->iError=-1;
		}

//Установить состояние порта, чтобы он возвращал управление
                if(twuF->iError==0)
                {
                    fcntl(twuF->i_fd1, F_SETFL, FNDELAY);

//Установить скорость
                    tcgetattr(twuF->i_fd1,&options);

                    switch(speed)
                    {
                        case 1200:
                                cfsetispeed(&options, B1200);
                                cfsetospeed(&options, B1200);
                        break;
                        case 9600:
                                cfsetispeed(&options, B9600);
                                cfsetospeed(&options, B9600);
                        break;
                        case 19200:
                                cfsetispeed(&options, B19200);
                                cfsetospeed(&options, B19200);
                            break;
                        case 57600:
                                cfsetispeed(&options, B57600);
                                cfsetospeed(&options, B57600);
                        break;
                        case 115200:
                              cfsetispeed(&options, B115200);
                              cfsetospeed(&options, B115200);
                        break;
                        default:
                            sfCLOSE(twuF->i_fd1);
                            twuF->i_fd1=-1;
                            twuF->iError=-2;
                    };
                };
                if(twuF->iError==0)
                {
                    options.c_cflag |= (CLOCAL | CREAD);

		/*Нет проверки бита четности.  1 стоповый бит*/
                    options.c_cflag &= ~PARENB;	//No parity check
                    if(stop_bits==0)
                    {
                        options.c_cflag &= ~CSTOPB;	//1 stop bit
                    }else{
                    }
                    options.c_cflag &= ~CSIZE;
                    options.c_cflag |= CS8; 	//Size is 8 bits
                    options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
                    tcsetattr(twuF->i_fd1, TCSANOW, &options);
                }

//Установить скорость и прочие параметры - если что-то не удастся, то всё закрыть
	}else{
//        if( not file_name1[0]=='/'&&file_name1[1]=='d'&&file_name1[2]=='e'&&file_name1[3]=='v'&&file_name1[4]=='/')
            twuF->isSerial=0;
        }

#else
        if((file_name1[0]=='C'&&file_name1[1]=='O'&&file_name1[2]=='M'&&file_name1[3]>='1'&&file_name1[3]<='9')||
          (file_name1[0]=='\\'&&file_name1[1]=='\\'&&file_name1[2]=='.'&&file_name1[3]=='\\'&&
          file_name1[4]=='C'&&file_name1[5]=='O'&&file_name1[6]=='M'&&file_name1[7]>='1'&&file_name1[7]<='9'))
	{
                twuF->isSerial=1;
                twuF->h_fd1=CreateFileA(file_name1,GENERIC_READ | GENERIC_WRITE, 0,0,
	                OPEN_EXISTING,         //Открывать порт если он существует
              	  0, 0);
                if(twuF->h_fd1==NULL ||twuF->h_fd1==INVALID_HANDLE_VALUE)
		{
                        twuF->h_fd1=NULL;
                        twuF->iError=-1;
                }
                if(twuF->iError==0)
                {
                    twuF->h_fd2=NULL;
                    memset(&dcb,0,sizeof(dcb));
                    dcb.DCBlength=sizeof(DCB);
                    iRet=GetCommState(twuF->h_fd1,&dcb);
                    if(iRet)
                    {
       			dcb.BaudRate=speed;
                        dcb.fParity=0;
                        dcb.StopBits=stop_bits;
                        dcb.ByteSize=8;
                        dcb.Parity=NOPARITY;
                        iRet=SetCommState(twuF->h_fd1,&dcb);
                    }
                    if(!iRet)
                    {
			CloseHandle(twuF->h_fd1);
                        twuF->h_fd1=NULL;
                        twuF->iError=-2;
                    }
                }
                if(twuF->iError==0)
                {
                    GetCommTimeouts(twuF->h_fd1,&cto);         //Получение всех таймаутов ком порта
                    cto.ReadIntervalTimeout=MAXDWORD;
                    cto.ReadTotalTimeoutMultiplier=0;   //
                    cto.ReadTotalTimeoutConstant=0;
                    cto.WriteTotalTimeoutMultiplier=10;   //
                    cto.WriteTotalTimeoutConstant=100;
                    SetCommTimeouts(twuF->h_fd1,&cto);
                }
	}else{
            twuF->isSerial=0;
	}
#endif

        if(twuF->isSerial==0)
        {

            twuF->i_fd1=twuF->i_fd2=-1;
            twuF->file_name1=(char*)malloc(i+2);
            twuF->file_name2=(char*)malloc(i+2);
            memcpy(twuF->file_name1,file_name1,i);
            memcpy(twuF->file_name2,file_name1,i);
            if(isTest)
            {
                    twuF->file_name1[i]='1';
                    twuF->file_name1[i+1]=0;
                    twuF->file_name2[i]='2';
                    twuF->file_name2[i+1]=0;
            }else{
                    twuF->file_name1[i]='2';
                    twuF->file_name1[i+1]=0;
                    twuF->file_name2[i]='1';
                    twuF->file_name2[i+1]=0;
            }
//Откроем оба файла
#ifdef linux
            if(sfACCESS(twuF->file_name1,F_OK))
#else
            if(sfACCESS(twuF->file_name1,00))
#endif
            {
#ifdef linux
                twuF->i_fd1=sfOpen(twuF->file_name1,O_CREAT|O_RDWR,0666);
#else
                twuF->i_fd1=sfOpen(twuF->file_name1,O_CREAT|O_RDWR|O_BINARY,S_IREAD|S_IWRITE);
#endif
                if(twuF->i_fd1<0)
                {
                    twuF->iError=-11;
                    return twuF;
                }
                max_size_fifo=100000;

                sfWRITE(twuF->i_fd1,&max_size_fifo,4);
                in_fifo=0;
                out_fifo=0;
                sfWRITE(twuF->i_fd1,&in_fifo,4);
                sfWRITE(twuF->i_fd1,&out_fifo,4);
                sfLSEEK(twuF->i_fd1,0,SEEK_SET);


            }else{
//Проверим соответствие размеров, указателей и реальное значение размеров файла
#ifdef linux
                twuF->i_fd1=sfOpen(twuF->file_name1,O_RDWR);
#else
                twuF->i_fd1=sfOpen(twuF->file_name1,O_RDWR|O_BINARY);
#endif
                sfLSEEK(twuF->i_fd1,0,SEEK_SET);
                iRet=sfREAD(twuF->i_fd1,&max_size_fifo,4);
                if(iRet!=4)
                {
                    max_size_fifo=100000;

                    sfWRITE(twuF->i_fd1,&max_size_fifo,4);
                    in_fifo=0;
                    out_fifo=0;
                    sfWRITE(twuF->i_fd1,&in_fifo,4);
                    sfWRITE(twuF->i_fd1,&out_fifo,4);
                    sfLSEEK(twuF->i_fd1,0,SEEK_SET);
                    sfLSEEK(twuF->i_fd1,4,SEEK_SET);
                }
                iRet=sfREAD(twuF->i_fd1,&in_fifo,4);
                if(iRet!=4)
                {
                    sfLSEEK(twuF->i_fd1,0,SEEK_SET);
                    max_size_fifo=100000;
                    sfWRITE(twuF->i_fd1,&max_size_fifo,4);
                    in_fifo=0;
                    out_fifo=0;
                    sfWRITE(twuF->i_fd1,&in_fifo,4);
                    sfWRITE(twuF->i_fd1,&out_fifo,4);
                    sfLSEEK(twuF->i_fd1,0,SEEK_SET);
                    sfLSEEK(twuF->i_fd1,8,SEEK_SET);
                }
                iRet=sfREAD(twuF->i_fd1,&out_fifo,4);
                if(iRet!=4)
                {
                    sfLSEEK(twuF->i_fd1,0,SEEK_SET);
                    max_size_fifo=100000;
                    sfWRITE(twuF->i_fd1,&max_size_fifo,4);
                    in_fifo=0;
                    out_fifo=0;
                    sfWRITE(twuF->i_fd1,&in_fifo,4);
                    sfWRITE(twuF->i_fd1,&out_fifo,4);
                    sfLSEEK(twuF->i_fd1,0,SEEK_SET);
                }
                iRet=0;
                sfLSEEK(twuF->i_fd1,0,SEEK_SET);
#ifdef linux
                fsync(twuF->i_fd1);
#endif
            }

#ifdef linux
            if(sfACCESS(twuF->file_name2, F_OK))
#else
            if(sfACCESS(twuF->file_name2, 00))
#endif
            {
#ifdef linux
                twuF->i_fd2=sfOpen(twuF->file_name2,O_CREAT|O_RDWR,0666);
#else
                twuF->i_fd2=sfOpen(twuF->file_name2,O_CREAT|O_RDWR|O_BINARY,S_IREAD|S_IWRITE);
#endif
                if(twuF->i_fd2<0)
                {
                    twuF->iError=-21;
                    sfCLOSE(twuF->i_fd1);
                    twuF->i_fd1=-1;
                    return twuF;
                }
                max_size_fifo=100000;
                sfWRITE(twuF->i_fd2,&max_size_fifo,4);
                in_fifo=0;
                out_fifo=0;
                sfWRITE(twuF->i_fd2,&in_fifo,4);
                sfWRITE(twuF->i_fd2,&out_fifo,4);
                sfLSEEK(twuF->i_fd2,0,SEEK_SET);

            }else{
//Проверим соответствие размеров, указателей и реальное значение размеров файла
#ifdef linux
                twuF->i_fd2=sfOpen(twuF->file_name2,O_RDWR);
#else
                twuF->i_fd2=sfOpen(twuF->file_name2,O_RDWR|O_BINARY);
#endif
                sfLSEEK(twuF->i_fd2,0,SEEK_SET);
                iRet=sfREAD(twuF->i_fd2,&max_size_fifo,4);
                if(iRet!=4)
                {
                    max_size_fifo=100000;
                    sfWRITE(twuF->i_fd2,&max_size_fifo,4);
                    in_fifo=0;
                    out_fifo=0;
                    sfWRITE(twuF->i_fd2,&in_fifo,4);
                    sfWRITE(twuF->i_fd2,&out_fifo,4);
                    sfLSEEK(twuF->i_fd2,0,SEEK_SET);
                    sfLSEEK(twuF->i_fd2,4,SEEK_SET);
                }
                iRet=sfREAD(twuF->i_fd2,&in_fifo,4);
                if(iRet!=4)
                {
                    max_size_fifo=100000;
                    sfWRITE(twuF->i_fd2,&max_size_fifo,4);
                    in_fifo=0;
                    out_fifo=0;
                    sfWRITE(twuF->i_fd2,&in_fifo,4);
                    sfWRITE(twuF->i_fd2,&out_fifo,4);
                    sfLSEEK(twuF->i_fd2,0,SEEK_SET);
                    sfLSEEK(twuF->i_fd2,8,SEEK_SET);
                }
                iRet=sfREAD(twuF->i_fd2,&out_fifo,4);
                if(iRet!=4)
                {
                    max_size_fifo=100000;
                    sfWRITE(twuF->i_fd2,&max_size_fifo,4);
                    in_fifo=0;
                    out_fifo=0;
                    sfWRITE(twuF->i_fd2,&in_fifo,4);
                    sfWRITE(twuF->i_fd2,&out_fifo,4);
                    sfLSEEK(twuF->i_fd2,0,SEEK_SET);
                    sfLSEEK(twuF->i_fd2,12,SEEK_SET);
                }

                iRet=0;
                sfLSEEK(twuF->i_fd2,0,SEEK_SET);
#ifdef linux
                fsync(twuF->i_fd2);
#endif
            }
        }

        twuF->file_name=(char*)malloc(i+1);
        sfSTRCPY(twuF->file_name,file_name1);
        twuF->speed=speed;
        twuF->stop_bits=stop_bits;
        twuF->isTest=isTest;
        return twuF;
}



 TWUFileDescr* twuOpenFileWork(char *file_name,
				   int speed,
				   int stop_bits)
{
	return twuOpenFileTemp(file_name,speed, stop_bits,0);
}

 TWUFileDescr* twuOpenFileTest(char *file_name,
				   int speed,
				   int stop_bits)
{
	return twuOpenFileTemp(file_name,speed, stop_bits,1);
}


 TWUFileDescr* twuReopenFile(TWUFileDescr* twuFile)
 {
    char *tempFileName;
    int speed,stop_bits,isTest;
    TWUFileDescr* twuRetFile;
    if(!twuFile)return NULL;
    if(!twuFile->file_name)return NULL;
    tempFileName=(char*)malloc(strlen(twuFile->file_name)+1);
    sfSTRCPY(tempFileName,twuFile->file_name);
    speed=twuFile->speed;
    isTest=twuFile->isTest;
    stop_bits=twuFile->stop_bits;
    twuCloseFile(twuFile);
    twuRetFile=twuOpenFileTemp(tempFileName,speed,stop_bits,isTest);
    free(tempFileName);
    return twuRetFile;
}




 int twuReadFile(TWUFileDescr* twuFile,
			void *data,
			int size)
{


    int iRet;
    off_t offRet;
	int max_size_fifo, in_fifo,out_fifo,size_read;
#ifndef linux
	DWORD n_of_b1;

#endif

	if(twuFile==NULL)return 0;
        if(twuFile->iError)return (twuFile->iError);
#ifdef linux
    if(twuFile->i_fd1<0)return 0;
    if(twuFile->isSerial)
    {
        return sfREAD(twuFile->i_fd1,data,size);
    }
#else
    if(twuFile->isSerial)
    {
        if(twuFile->h_fd1==NULL|| twuFile->h_fd1==INVALID_HANDLE_VALUE)return 0;
        iRet=ReadFile(twuFile->h_fd1,data,size,&n_of_b1,NULL);
        if(iRet&&n_of_b1>0)return (int)n_of_b1;
        return 0;
    }
#endif
    else{
        sfLSEEK(twuFile->i_fd1,0,SEEK_SET);
        iRet=sfREAD(twuFile->i_fd1,&max_size_fifo,4);
        if(iRet!=4)return (-101);
        iRet=sfREAD(twuFile->i_fd1,&in_fifo,4);
        if(iRet!=4)return (-102);
        iRet=sfREAD(twuFile->i_fd1,&out_fifo,4);
        if(iRet!=4)return (-103);
        if(in_fifo==out_fifo)return 0;
        if(in_fifo>out_fifo)
        {
            size_read=(unsigned int)MIN(size,(in_fifo-out_fifo));
            offRet=sfLSEEK(twuFile->i_fd1,12+out_fifo,SEEK_SET);
            if(offRet==(off_t)(-1))
            {
                return (-104);
            }
            iRet=sfREAD(twuFile->i_fd1,data,size_read);
            out_fifo+=size_read;
            sfLSEEK(twuFile->i_fd1,8,SEEK_SET);
            sfWRITE(twuFile->i_fd1,&out_fifo,4);
#ifdef linux
            fsync(twuFile->i_fd1);
#endif
            return iRet;
        }else{
            size_read=max_size_fifo-out_fifo+in_fifo;
            size_read=(unsigned int)MIN(size_read,size);
            if(out_fifo+size_read<max_size_fifo)
            {
                offRet=sfLSEEK(twuFile->i_fd1,12+out_fifo,SEEK_SET);
                if(offRet==(off_t)(-1))
                {
                    return (-104);
                }
                iRet=sfREAD(twuFile->i_fd1,data,size_read);
                out_fifo+=size_read;
                sfLSEEK(twuFile->i_fd1,8,SEEK_SET);
                sfWRITE(twuFile->i_fd1,&out_fifo,4);
#ifdef linux
                fsync(twuFile->i_fd1);
#endif
                return iRet;
            }else{
                offRet=sfLSEEK(twuFile->i_fd1,12+out_fifo,SEEK_SET);
                if(offRet==(off_t)(-1))
                {
                    return (-104);
                }
                iRet=sfREAD(twuFile->i_fd1,data,max_size_fifo-out_fifo);
                if(size_read-(max_size_fifo-out_fifo)>0)
                {
                    iRet+=sfREAD(twuFile->i_fd2,(unsigned char*)data+(max_size_fifo-out_fifo),size_read-(max_size_fifo-out_fifo));
                }
                out_fifo=size_read-(max_size_fifo-out_fifo);
                sfLSEEK(twuFile->i_fd1,8,SEEK_SET);
                sfWRITE(twuFile->i_fd1,&out_fifo,4);
#ifdef linux
                fsync(twuFile->i_fd1);
#endif
                return iRet;
            }
        }
    }

}


 int twuWriteFile(TWUFileDescr* twuFile,
			void *data,
			int size)
{
	int iRet;

    off_t offRet;
   	int max_size_fifo, in_fifo,out_fifo,size_wrote;
#ifndef linux
	DWORD n_of_b1;
#endif

	if(twuFile==NULL)return -100;
        if(twuFile->iError)return (twuFile->iError);
#ifdef linux
	if(twuFile->isSerial)
	{
		if(twuFile->i_fd1<0)return 0;
        return sfWRITE(twuFile->i_fd1,data,size);
        }
#else
        if(twuFile->isSerial)
        {
                if(twuFile->h_fd1==NULL|| twuFile->h_fd1==INVALID_HANDLE_VALUE)
                {
                    return 0;
                }
                iRet=WriteFile(twuFile->h_fd1,data,size,&n_of_b1,NULL);


                if(iRet&&n_of_b1>0)return (int)n_of_b1;
        }
#endif
        else{


//Определим сколько места мы можем записать
            sfLSEEK(twuFile->i_fd2,0,SEEK_SET);
            iRet=sfREAD(twuFile->i_fd2,&max_size_fifo,4);
            if(iRet!=4)return (-101);
            iRet=sfREAD(twuFile->i_fd2,&in_fifo,4);
            if(iRet!=4)return (-102);
            iRet=sfREAD(twuFile->i_fd2,&out_fifo,4);
            if(iRet!=4)return (-103);
            if(in_fifo==out_fifo-1||(out_fifo==0&&in_fifo==max_size_fifo-1))return 0;
            size_wrote=MIN(size,(int)((out_fifo-in_fifo+max_size_fifo-1)%max_size_fifo));
            if(in_fifo+size_wrote<max_size_fifo)
            {
                offRet=sfLSEEK(twuFile->i_fd2,12+in_fifo,SEEK_SET);
                if(offRet==(off_t)(-1))
                {
                    return (-105);
                }
                iRet=sfWRITE(twuFile->i_fd2,data,size_wrote);
                in_fifo+=size_wrote;
            }else{
                offRet=sfLSEEK(twuFile->i_fd2,12+in_fifo,SEEK_SET);
                if(offRet==(off_t)(-1))
                {
                    return (-105);
                }
                iRet=sfWRITE(twuFile->i_fd2,data,max_size_fifo-in_fifo);
                if(size_wrote-(max_size_fifo-in_fifo)==0)
                {
                    iRet+=sfWRITE(twuFile->i_fd2,(unsigned char*)data+max_size_fifo-in_fifo,size_wrote-(max_size_fifo-in_fifo));
                }
                in_fifo=size_wrote-(max_size_fifo-in_fifo);
            }
            offRet=sfLSEEK(twuFile->i_fd2,4,SEEK_SET);
            if(offRet==(off_t)(-1))
            {
                    return (-105);
            }
            sfWRITE(twuFile->i_fd2,&in_fifo,4);
#ifdef linux
            fsync(twuFile->i_fd2);
#endif
            return iRet;
        }
    return 0;
}

int twuPurgeFile(TWUFileDescr* twuFile, unsigned int whats)
{
    int iRet;
    int out_fifo,in_fifo,max_size_fifo;
    if(!twuFile)return 0;
    if(twuFile->iError)return (twuFile->iError);
#ifdef linux
    if(twuFile->isSerial)
    {
          if(whats&1)
          {
//Приосановка передачи
              if(whats&2)
              {
//Приостановка и передачи и приема
              }else{

              }
          }else if(whats&2)
          {
//Приостановка приема
          }

          if(whats&4)
          {
              if(whats&8)
              {
                 tcflush(twuFile->i_fd1, TCIOFLUSH);
              }else{
                  tcflush(twuFile->i_fd1, TCOFLUSH);
              }
          }else if(whats&8)
          {
              tcflush(twuFile->i_fd1, TCIFLUSH);
          }
      }
#else
    DWORD  flg=0;
    if(twuFile->isSerial)
    {
        if(whats&1)flg|=PURGE_TXABORT;
        if(whats&2)flg|=PURGE_RXABORT;
        if(whats&4)flg|=PURGE_TXCLEAR;
        if(whats&8)flg|=PURGE_RXCLEAR;
        PurgeComm(twuFile->h_fd1,flg);

    }
#endif
      else{
         if(whats&1)
         {

         }
         if(whats&2)
         {

         }
         if(whats&4)
         {
             sfLSEEK(twuFile->i_fd2,0,SEEK_SET);
             iRet=sfREAD(twuFile->i_fd2,&max_size_fifo,4);
             if(iRet!=4)return (-101);
             iRet=sfREAD(twuFile->i_fd2,&in_fifo,4);
             if(iRet!=4)return (-102);
             iRet=sfREAD(twuFile->i_fd2,&out_fifo,4);
             if(iRet!=4)return (-103);
             in_fifo=out_fifo;
             sfLSEEK(twuFile->i_fd2,4,SEEK_SET);
             sfWRITE(twuFile->i_fd2,&in_fifo,4);
         }

         if(whats&8)
         {
             sfLSEEK(twuFile->i_fd1,0,SEEK_SET);
             iRet=sfREAD(twuFile->i_fd1,&max_size_fifo,4);
             if(iRet!=4)return (-101);
             iRet=sfREAD(twuFile->i_fd1,&in_fifo,4);
             if(iRet!=4)return (-102);
             iRet=sfREAD(twuFile->i_fd1,&out_fifo,4);
             if(iRet!=4)return (-103);
             out_fifo=in_fifo;
             sfLSEEK(twuFile->i_fd1,8,SEEK_SET);
             sfWRITE(twuFile->i_fd1,&out_fifo,4);
         }
    }
    return 1;
}


int twuCloseFile(TWUFileDescr* twuFile)
{
	if(twuFile==NULL)return 0;
#ifdef linux
        if(twuFile->i_fd1>0)sfCLOSE(twuFile->i_fd1);
        if(twuFile->i_fd2>0)sfCLOSE(twuFile->i_fd2);
        if(twuFile->isSerial==0&&
           twuFile->file_name1&&!sfACCESS(twuFile->file_name1,F_OK))
        {
            if(findProcessesOpenFile(twuFile->file_name1)==0)
                remove(twuFile->file_name1);
        }

        if(twuFile->isSerial==0&&
           twuFile->file_name2&&!sfACCESS(twuFile->file_name2,F_OK))
        {
            if(findProcessesOpenFile(twuFile->file_name2)==0)
                remove(twuFile->file_name2);
        }

#else
        if(twuFile->isSerial)
        {
            if(twuFile->h_fd1!=NULL&&twuFile->h_fd1!=INVALID_HANDLE_VALUE)CloseHandle(twuFile->h_fd1);
            if(twuFile->h_fd2!=NULL&&twuFile->h_fd2!=INVALID_HANDLE_VALUE)CloseHandle(twuFile->h_fd2);
        }else{
            if(twuFile->i_fd1>0)sfCLOSE(twuFile->i_fd1);
            if(twuFile->i_fd2>0)sfCLOSE(twuFile->i_fd2);
        }
#endif
	if(twuFile->file_name1)free(twuFile->file_name1);
	if(twuFile->file_name2)free(twuFile->file_name2);
        if(twuFile->file_name)free(twuFile->file_name);
	free(twuFile);
	return 1;
}

/*Проверка существование каталога и возможности записи туда.
Возвращает 1, если каталог сушествует и туда можно писать и читать.
0 - если каталога не существует.
-1 - если писать или читать-писать в/в-из данный каталог нельзя.
-2 - файл не является каталогом
*/
int twuTestDirReadWrite(char *path)
{
    int i,iRet;
    char path1[2000];
#ifdef linux
    char slesh='/';
    struct stat f_stat;
#else
    char slesh='\\';
#endif

    for(i=0;i<1999&&path[i]!=0;i++)
    {
        if(path[i]=='/'||path[i]=='\\')
        {
            path1[i]=slesh;
        }else{
            path1[i]=path[i];
        }
    }
    path1[i]=0;


#ifdef linux
//Проверим существование файла(каталога)
    iRet=sfACCESS(path1,F_OK);
   if(iRet)
   {
       return 0;
   }
//Проверим права данного каталога
   iRet=sfACCESS(path1,X_OK|W_OK|R_OK);
   if(iRet)
   {
       return (-1);
   }

   memset(&f_stat,0,sizeof(f_stat));
   stat(path1,&f_stat);
//Проверим
   if(!(f_stat.st_mode&S_IFDIR))
   {
            return (-2);
   }
//Это каталог

#else
   iRet=sfACCESS(path1,00);
   if(iRet)return 0;

   iRet=sfACCESS(path1,06);
   if(iRet)return (-1);

//Проверим теперь, а каталог ли это
   if(!(GetFileAttributesA(path1)&FILE_ATTRIBUTE_DIRECTORY))
   {
       return (-2);
   }
#endif
    return 1;
}

//Создание каталогов
int twuMakeDirReadWrite(char *path)
{
    int i,iRet;
    char path1[2000];
#ifdef linux
    char slesh='/';
#else
    char slesh='\\';
#endif

    for(i=0;i<1999;i++)
    {
        if(path[i]=='/'||path[i]=='\\'||path[i]==0)
        {

            if(i==0)
            {
                if(path[i]==0)return 0;
                path1[i]=slesh;
                continue;
            }
            path1[i]=0;
            iRet=twuTestDirReadWrite(path1);
            if(iRet==0)
            {
//Не существует - попробуем создать каталог
                iRet=twuMKDIR(path1);
                if(iRet)return -1;
            }else
            if(iRet==-2)return -2;
            if(path[i]==0)break;
            else path1[i]=slesh;
        }else{
            path1[i]=path[i];
        }
    }
    path1[i]=0;
    return 1;
}


