

#ifndef __WIN32

#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


#include "tools_unix.h"
#include "safe_function.h"



/*findProcessesOpenFile - определяет, сколько процессов владеют файлом file_name
 * in: file_name  -- имя (с путем) проверяемого файла;
 * out: proc_id --- массив для получения дескрипторов, которые владеют файлом.
 * out: max_n_of_proc_id -- размер массива proc_id, если дескрипторов боль
 *      то их заполняется только max_n_of_proc_id
 * возвращает: число процессов, владеющих файлом file_name.
 * */
int findProcessesOpenFile(const char *file_name, int *proc_id, int max_n_of_proc_id)
{

    DIR *pDir1, *pDir2;
    int n_of_files=0;
    int iRet;
    char dname[2000],fname[2000],realfile[2000];
    struct dirent *dr1, *dr2;
    pDir1=opendir("/proc");
    if(!pDir1)return (-1);
    do
    {
         dr1=readdir(pDir1);
         if(dr1&&strcmp(dr1->d_name,"")&&dr1->d_type==DT_DIR&&dr1->d_name[0]>=0x30&&dr1->d_name[0]<=0x39)
         {
             sfSPRINTF(dname,"/proc/%s/fd",dr1->d_name);
             pDir2=opendir(dname);
             if(!pDir2)continue;
             do
             {
                 dr2=readdir(pDir2);
                 if(dr2&&dr2->d_name[0]>=0x30&&dr2->d_name[0]<=0x39)
                 {
                     sfSPRINTF(fname,"%s/%s",dname,dr2->d_name);
                     iRet=readlink(fname,realfile,1999);
                     if(iRet>0)
                     {
                        realfile[iRet]=0;
                        if(strcmp(realfile,file_name)==0)
                        {
                            if(n_of_files<max_n_of_proc_id)
                            {
                                if(proc_id)
                                {
                                    proc_id[n_of_files]=atoi(dr2->d_name);
                                }
                            }
                            n_of_files++;
                        }
                    }

                 }

             }while(dr2);
             closedir(pDir2);
         }
    }while(dr1);
    closedir(pDir1);
    return n_of_files;
}

/*findProcessesOpenFile - определяет, сколько процессов владеют файлом file_name
 * in: file_name  -- имя (с путем) проверяемого файла;
 *  * возвращает: число процессов, владеющих файлом file_name.
 * */
int findProcessesOpenFile(const char *file_name)
{
    return findProcessesOpenFile(file_name,NULL,0);

}

#endif  //#ifndef __WIN32
