#include <windows.h>
#include "StdAfx.h"
#include "m_radiobuttonsgroup.h"


TRadioButtonsGroup::TRadioButtonsGroup(
	const struct TMyRBGroupDescr &_rbgd		
								   )
{
	int x,y;
	RECT rect;
//��������� ������
	CopyMemory(&rbgd, &_rbgd, sizeof(struct TMyRBGroupDescr));



//�������� ����
//��� ����� ������� ���������� ����������
	if(rbgd.isFromRight||rbgd.isFromBottom)
	{
		GetClientRect((HWND)(rbgd.parent_hwnd),&rect);
	}

	if(rbgd.isFromRight)
	{
		x=(rect.right-rect.left)-rbgd.x;
	}else{
		x=rbgd.x;
	}

	if(rbgd.isFromBottom)
	{
		y=(rect.bottom-rect.top)-rbgd.y;
	}else{
		y=rbgd.y;
	}

//�������� ����
	rbgd.hwnd = 
		(void*)CreateWindow (L"button", rbgd.text, 
			WS_CHILD | WS_VISIBLE | BS_GROUPBOX, x, y, rbgd.dx, rbgd.dy,
			(HWND)(rbgd.parent_hwnd), (HMENU)(rbgd.code), (HINSTANCE)(rbgd.hinstance), 0);


//������� ����
	rbs.clear();

	min_code=max_code=rbgd.code;

}

TRadioButtonsGroup::~TRadioButtonsGroup(void)
{

	vector<TMyRadioButtonDescr>::iterator it;
	for(it=rbs.begin();it!=rbs.end();it++)
	{
		if((*it).hwnd)
		{
			DestroyWindow((HWND)((*it).hwnd));
			(*it).hwnd=NULL;
		}
	}
	
	if(rbgd.hwnd)
	{
		DestroyWindow((HWND)(rbgd.hwnd));
		rbgd.hwnd=NULL;
	}

}


int TRadioButtonsGroup::addRadioButton(const struct TMyRadioButtonDescr &_rb)
{


	vector<TMyRadioButtonDescr>::iterator it;
	if(!rbgd.hwnd)return (-1);
	
	if(_rb.code<=min_code||_rb.code>min_code+100)
	{
		return (-2);
	}

	if(!(rbs.empty()))
	{
		for(it=rbs.begin(); it!=rbs.end();it++)
		{
			if(_rb.code==(*it).code)
			{
				return 0;
			}
		}
	}




//������� ������
	rbs.push_back(_rb);
	rbs.back().hwnd=CreateWindow(_T("button"), rbs.back().text, 
WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON, 
		rbs.back().x, rbs.back().y, rbs.back().dx,  rbs.back().dy, 
		(HWND)(rbgd.hwnd), (HMENU)(rbs.back().code), 
		(HINSTANCE)(rbgd.hinstance), 0);

	if(rbs.back().code>max_code)
	{
		max_code=rbs.back().code;
	}



	return 1;
}

int TRadioButtonsGroup::testAndMake(int _code)
{

	vector<TMyRadioButtonDescr>::iterator it;

	if(_code<=min_code||_code>max_code)
	{
		return 0;
	}
	if(rbs.empty())
	{
		return (-1);
	}

	for(it=rbs.begin();it!=rbs.end();it++)
	{
		if((*it).code==_code)
		{
			if((*it).f)
			{
				((*it).f)();
				return 1;
			}else{
				return (-2);
			}
		}
	}
	
	return (-1);	
}


int TRadioButtonsGroup::renew(void)
{
	int x, y;
	RECT rect;

	if(rbgd.isFromRight||rbgd.isFromBottom)
	{
		GetClientRect((HWND)(rbgd.parent_hwnd),&rect);
	}

	if(rbgd.isFromRight)
	{
		x=(rect.right-rect.left)-rbgd.x;
	}else{
		x=rbgd.x;
	}

	if(rbgd.isFromBottom)
	{
		y=(rect.bottom-rect.top)-rbgd.y;
	}else{
		y=rbgd.y;
	}
	
	if(rbgd.isFromRight||rbgd.isFromBottom)
	{
		MoveWindow((HWND)(rbgd.hwnd), x, y,rbgd.dx, rbgd.dy,TRUE);
	}
	return 1;
}