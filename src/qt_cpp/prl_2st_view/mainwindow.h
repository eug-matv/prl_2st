/*Модуль: mainwindow -главное окно программы
Автор: Матвеенко Е.А. ЧРЗ Полет ОКБ

Реализация главного окна прграммы средствами qt
Используется форма в формате UI: mainwindow.ui


*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QMutex>
#include <QSound>
#include "RZPLinGlaf.h"
#include "rsp_outputwidget.h"
#include "tgettingdatathread.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    bool isError;

private:
    Ui::MainWindow *ui;
    TRZPDraw *rzp_draw;
    RSP_OutputWidget *rsp_w;
    QTimer *timer;
    QTimer *timerFlashing;
    TGettingDataThread *gthrd;
    TGDITOptions gdito;
    QMutex *m_mutex;

    int k_for_flash;

protected:



virtual void resizeEvent(QResizeEvent * );



    void getOptionFromIniFile(char *input_ini);
    int setDataForBegin();

    void synchParametersWithMasshtab();

protected slots:
    void openOptionWindow();
    void setMashtab5000();
    void setMashtab10000();
    void setMashtab20000();
    void setMashtab30000();
    void setMashtab40000();
    void setMashtab50000();
    void setMashtab60000();
    void setMashtab70000();
    void setMashtab80000();


    void changeDiapazonUglKurs();

    void changeMinUgolGlis();
    void changeMaxUgolGlis();


    void timeoutForFlashing();


    void slot_timeout_for_output();

    void setNapravlenie0();

    void setNapravlenie1();
};

#endif // MAINWINDOW_H
