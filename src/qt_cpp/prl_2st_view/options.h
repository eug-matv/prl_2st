/*Модуль: options - окно с опциями
Автор: Матвеенко Е.А. ЧРЗ Полет ОКБ

Реализация главного окна прграммы средствами qt
Используется форма в формате UI: options.ui
*/



#ifndef OPTIONS_H
#define OPTIONS_H


#include <QDialog>
#include <QAbstractButton>
#include <QCheckBox>
#include <QComboBox>
#include <QSpinBox>
#include "RZPLinGlaf.h"

namespace Ui {
class Options;
}






struct TOptionPenData
{
    QPen *pen;

    QPushButton *pbCol;
    QSpinBox *sbWidth;
    QComboBox *cbStyle;
};


struct TOptionBrushData
{
    QBrush *brush;
    QPushButton *pbCol;
};

struct TOptionColorData
{
    QColor *col;
    QPushButton *pbCol;
};


struct TOptionChkBoxState
{
    QCheckBox *chBox;
    bool *st;
};

struct TOptionLongData
{
    long *lData;
    QSpinBox *sbData;

};







class Options : public QDialog
{
    Q_OBJECT

public:
    explicit Options(TRZPDraw *_rzp_draw, QWidget *parent = 0);
    ~Options();

private:



    TOptionChkBoxState ocbs[100];
    int n_of_ocbs;



    TOptionPenData  opd[100];
    int n_of_opd;


    TOptionBrushData obd[100];
    int n_of_obd;


    TOptionColorData ocd[100];
    int n_of_ocd;


    TOptionLongData old[100];
    int n_of_old;

    Ui::Options *ui;
    TRZPDraw *rzp_draw;








protected:


    void setPen(  QPen *pen,
                QPushButton *pbCol,
                QSpinBox *sbWidth,
                QComboBox *cbStyle
                );

    void setPenFromRZP_DRAW();

    void setPenToRZP_DRAW();


    void setBrush(  QBrush *brush,
                  QPushButton *pbCol);
    void setBrushFromRZP_DRAW();
    void setBrushToRZP_DRAW();


    void setColor(  QColor *color,
                  QPushButton *pbCol);
    void setColorFromRZP_DRAW();
    void setColorToRZP_DRAW();


    void setLong(  long *lData,
                 QSpinBox *sbLong
                 );
    void setLongFromRZP_DRAW();
    void setLongToRZP_DRAW();


    void setColorButton(QPushButton *pb,
                          QColor &col);





    void getColorButton(  QPushButton *pb,
                       QColor &col);




void setParameterFromRZP_DRAW();



void setParameterToRZP_DRAW();

void setPushButtonColorToRZP_DRAW();


void setCheckedButtonState( QCheckBox *cb,
                           bool *state);

void setCheckedButtonStatesFromRZP_DRAW();
void setCheckedButtonsToRZP_DRAW();





void setComboBoxFromRZP_DRAW();

void setComboBoxToRZP_DRAW();


protected slots:
    void psChangeColor();

    void psButtonOkOrCancel(QAbstractButton *btn);
};

#endif // OPTIONS_H
