﻿#include <QPushButton>
#include <QObjectList>
#include <QColorDialog>
#include <QColor>
#include <QPalette>
#include <QSpinBox>
#include "options.h"
#include "ui_options.h"




Options::Options(TRZPDraw *_rzp_draw, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Options)
{

    n_of_obd=0;
    n_of_opd=0;


    ui->setupUi(this);
    rzp_draw=_rzp_draw;

    setParameterFromRZP_DRAW();

    connect(ui->buttonBox,SIGNAL(clicked(QAbstractButton*)),this,SLOT(psButtonOkOrCancel(QAbstractButton*)));

}

Options::~Options()
{
    delete ui;
}




void Options::setPen(  QPen *pen,
            QPushButton *pbCol,
            QSpinBox *sbWidth,
            QComboBox *cbStyle
            )
{
    QColor col;


    if(n_of_opd==100)return;

    opd[n_of_opd].pen=pen;
    opd[n_of_opd].pbCol=pbCol;
    opd[n_of_opd].sbWidth=sbWidth;
    opd[n_of_opd].cbStyle=cbStyle;


    col=pen->color();

    if(pbCol)
    {
       setColorButton(pbCol,col);
       connect(pbCol, SIGNAL(clicked()),this,SLOT(psChangeColor()));
    }

    if(sbWidth)
    {
        sbWidth->setValue(pen->width());
    }

    if(cbStyle)
    {
        switch(pen->style())
        {
            case Qt::SolidLine:
                cbStyle->setCurrentIndex(0);
            break;

            case Qt::DashLine:
                cbStyle->setCurrentIndex(1);
            break;

            case Qt::DotLine:
                cbStyle->setCurrentIndex(2);
            break;

            case Qt::DashDotLine:
                cbStyle->setCurrentIndex(3);
            break;

            case Qt::DashDotDotLine:
                cbStyle->setCurrentIndex(4);
            break;

            default:
                 cbStyle->setCurrentIndex(5);
            break;
        }
    }
    n_of_opd++;
}



void Options::setBrush(  QBrush *brush,
            QPushButton *pbCol)
{
    QColor col;
    if(n_of_obd==100)return;
    obd[n_of_obd].brush=brush;
    obd[n_of_obd].pbCol=pbCol;
    col=brush->color();
    if(pbCol)
    {
        setColorButton(pbCol,col);
        connect(pbCol, SIGNAL(clicked()),this,SLOT(psChangeColor()));
    }
    n_of_obd++;
}


void Options::setColor(  QColor *color,
              QPushButton *pbCol)
{
    if(n_of_ocd==100)return;
    ocd[n_of_ocd].col=color;
    ocd[n_of_ocd].pbCol=pbCol;
    if(pbCol)
    {
        setColorButton(pbCol,*color);
        connect(pbCol, SIGNAL(clicked()),this,SLOT(psChangeColor()));
    }
    n_of_ocd++;
}



void Options::setLong(  long *lData,
             QSpinBox *sbLong
             )
{
   if(n_of_old==100)
   {
       return;
   }
   old[n_of_old].lData=lData;
   old[n_of_old].sbData=sbLong;
   if(sbLong)
   {
       sbLong->setValue((int)(*lData));
   }
   n_of_old++;
}



void Options::setColorButton(QPushButton *pb,
                             QColor &col)
{
    QString s="background-color: ";
    pb->setStyleSheet(s+col.name());
}







void Options::getColorButton(  QPushButton *pb,
                           QColor &col)
{

    QString s="background-color: ",stl, s_col;
    stl=pb->styleSheet();
    s_col=stl.mid(s.length());
    if(s_col.isEmpty())
    {
       col=QColor(0,0,0);
    }else{
        col=QColor(s_col);
    }
}






void Options::psChangeColor()
{
    QColor col;
    QPushButton *pb;


    pb=qobject_cast<QPushButton *>(sender());
    if(!pb)
    {
        return;
    }

    getColorButton(pb,col);

    col=QColorDialog::getColor(col,this);
    if(col.isValid())
    {
        setColorButton(pb,col);
    }
}


void Options::psButtonOkOrCancel(QAbstractButton *btn)
{
   if(ui->buttonBox->buttonRole(btn)==QDialogButtonBox::AcceptRole)
   {
        setParameterToRZP_DRAW();
   }
   hide();
}





void Options::setPenFromRZP_DRAW()
{
    n_of_opd=0;
    setPen(&(rzp_draw->lpLiniaPosadKurs),ui->pbLiniaKursColor,ui->sbLiniaKursWidth,ui->cbStlLiniaKursa);
    setPen(&(rzp_draw->lpSektorSkan), ui->pbScanSectorBorderKursColor, ui->sbScanSectorBorderKursWidth,
           ui->cbStlSectorKursBorder);
    setPen(&(rzp_draw->lpLDN), ui->pbDNBorderGlissadyKursColor, ui->sbDNBorderGlissadyKursWidth,
           ui->cbStlDNGlisVKursBorder);
    setPen(&(rzp_draw->lpOporAzmt), ui->pbOpornAzmtColor, ui->sbOpornAzmtWidth,
           ui->cbStlOpornLiniiAzmtBorder);
    setPen(&(rzp_draw->lpOporGorUdal),ui->pbOpornHorLiniiKursColor, ui->sbOpornHorLiniiKursWidth,
           ui->cbStlOpornHorLinii);
    setPen(&(rzp_draw->lpAzmt),ui->pbAzmtColor,ui->sbAzmtWidth,ui->cbStlLiniiAzmt);
    setPen(&(rzp_draw->lpGorUdal),ui->pbHorLiniiKursColor,ui->sbHorLiniiKursWidth,ui->cbStlHorLinii);


    setPen(&(rzp_draw->LpLiniiGlissady), ui->pbLiniaGlisColor,ui->sbLiniaGlisWidth,ui->cbStlLiniaGlis);
    setPen(&(rzp_draw->LpSektoraSkan), ui->pbSectorBorderGlisColor, ui->sbSectorBorderGlisWidth,
           ui->cbStlSectorGlisBorder);
    setPen(&(rzp_draw->LpUgolKurs),ui->pbDNBorderGlisColor,ui->sbDNBorderGlisWidth,
           ui->cbStlDNKursVGlisBorder);
    setPen(&(rzp_draw->LpOpornUglPen),ui->pbOpornUglLiniiGlisColor,ui->sbOpornUglLiniiGlisWidth,
           ui->cbStlOpornUglLinii);
    setPen(&(rzp_draw->LpOporLiniiVertUdal),ui->pbOpornVerLiniiGlisColor,ui->sbOpornVerLiniiGlis,
           ui->cbStlOpornVertLinii);
    setPen(&(rzp_draw->LpUglomerPen), ui->pbUglLiniiGlisColor,ui->sbUglLiniiGlisWidth,ui->cbStlUglLinii);
    setPen(&(rzp_draw->LpLiniiVertUdal), ui->pbVerLiniiGlisColor, ui->sbVerLiniiGlisWidth,
           ui->cbStlVertLinii);

    setPen(&(rzp_draw->LpRavnyhVysotPen),ui->pbLiniiRavnyhVysotGlisColor,
           ui->sbLiniiRavnyhVysotGlisWidth,ui->cbStlLiniiRavnyhVysot);

}




void Options::setPenToRZP_DRAW()
{
    QColor col;
    for(int i=0;i<n_of_opd;i++)
    {
        if(opd[i].pbCol)
        {
            getColorButton(opd[i].pbCol,col);
            opd[i].pen->setColor(col);
        }
        if(opd[i].cbStyle)
        {
            switch(opd[i].cbStyle->currentIndex())
            {
                case 0:
                    opd[i].pen->setStyle(Qt::SolidLine);
                break;

                case 1:
                    opd[i].pen->setStyle(Qt::DashLine);
                break;

                case 2:
                    opd[i].pen->setStyle(Qt::DotLine);
                break;

                case 3:
                    opd[i].pen->setStyle(Qt::DashDotLine);
                break;

                case 4:
                    opd[i].pen->setStyle(Qt::DashDotDotLine);
                break;
                default:
                    opd[i].pen->setStyle(Qt::NoPen);
                break;
            }
        }
        if(opd[i].sbWidth)
        {
            opd[i].pen->setWidth(opd[i].sbWidth->value());
        }
    }
}

void Options::setBrushFromRZP_DRAW()
{
       n_of_obd=0;
       setBrush(&(rzp_draw->lbZonaOtkl),ui->pbSektorLiniaKursColor);
       setBrush(&(rzp_draw->lbSektorSkan),ui->pbScanSectorKursColor);
       setBrush(&(rzp_draw->lbLDN),ui->pbDNKursColor);
       setBrush(&(rzp_draw->LbSektorGlissady),ui->pbSectorLiniaGlisColor);
       setBrush(&(rzp_draw->LbSektoraSkan),ui->pbScanSectorGlisColor);
       setBrush(&(rzp_draw->LbUgolKurs),ui->pbDNSectorGlisColor);
}



void Options::setBrushToRZP_DRAW()
{
    QColor col;
    for(int i=0;i<n_of_obd;i++)
    {
        if(obd[i].pbCol)
        {
            getColorButton(obd[i].pbCol,col);
            obd[i].brush->setColor(col);
        }
        obd[i].brush->setStyle(Qt::SolidPattern);
    }
}


void Options::setColorFromRZP_DRAW()
{
        n_of_ocd=0;
        setColor(&(rzp_draw->cOporAzmtText),ui->pbOpornLiniiTextKursColor);
        setColor(&(rzp_draw->cOporGorUdalText),ui->pbOpornHorLiniiTextKursColor);
        setColor(&(rzp_draw->cAzmtText), ui->pbLiniiTextKursColor);
        setColor(&(rzp_draw->cGorUdalText), ui->pbHorLiniiTextKursColor);
        setColor(&(rzp_draw->cVyvodOpornUglText),ui->pbOpornUglLiniiTextGlisColor);
        setColor(&(rzp_draw->cTextOporVertUdal),ui->pbOpornVerLiniiTextGlisColor);
        setColor(&(rzp_draw->cVyvodUglomerText),ui->pbUglLiniiTextGlisColor);
        setColor(&(rzp_draw->cTextVertUdal),ui->pbVerLiniiTextGlisColor);
        setColor(&(rzp_draw->cRavnyhVysotText),ui->pbLiniiRavnyhVysotTextGlisColor);
}


void Options::setColorToRZP_DRAW()
{
    QColor col;
    for(int i=0;i<n_of_ocd;i++)
    {
        if(ocd[i].pbCol)
        {
            getColorButton(ocd[i].pbCol,col);
            *(ocd[i].col)=col;

        }
    }
}



void Options::setLongFromRZP_DRAW()
{
    n_of_old=0;
    setLong(&(rzp_draw->szOporAzmtText),ui->sbOpornLiniiTextKursSize);
    setLong(&(rzp_draw->szOporGorUdalText),ui->sbOpornHorLiniiTextKursSize);
    setLong(&(rzp_draw->szAzmtText), ui->sbLiniiTextKursSize);
    setLong(&(rzp_draw->szGorUdalText), ui->sbHorLiniiTextKursSize);
    setLong(&(rzp_draw->szVyvodOpornUglText),ui->sbOpornUglLiniiTextGlisSize);
    setLong(&(rzp_draw->szTextOporVertUdal),ui->sbOpornVerLiniiTextGlisSize);
    setLong(&(rzp_draw->szVyvodUglomerText),ui->sbUglLiniiTextGlisSize);
    setLong(&(rzp_draw->szTextVertUdal),ui->sbVerLiniiTextGlisSize);
    setLong(&(rzp_draw->szRavnyhVysotText),ui->sbLiniiRavnyhVysotTextGlisSize);
    setLong(&(rzp_draw->N_of_RavnyhVysot),ui->sbChisloLiniiRavnyhVysot);
    setLong(&(rzp_draw->lPervayaVysotaRavnyhVysot), ui->sbNachVysotaLiniiRavnyhVysot);
    setLong(&(rzp_draw->lShagVysotyRavnyhVysot),ui->sbShagIsmenenizVysotyLiniiRavnyhVysot);

}

void Options::setLongToRZP_DRAW()
{
    for(int i=0;i<n_of_old;i++)
    {
        if(old[i].sbData)
        {
            *(old[i].lData)=(long)(old[i].sbData->value());
        }
    }
}




void Options::setParameterFromRZP_DRAW()
{
    setPenFromRZP_DRAW();
    setBrushFromRZP_DRAW();
    setColorFromRZP_DRAW();
    setLongFromRZP_DRAW();
    setCheckedButtonStatesFromRZP_DRAW();
}







void Options::setParameterToRZP_DRAW()
{
    setPenToRZP_DRAW();
    setBrushToRZP_DRAW();
    setColorToRZP_DRAW();
    setLongToRZP_DRAW();
    setCheckedButtonsToRZP_DRAW();
}



void Options::setCheckedButtonState( QCheckBox *cb,
                           bool *state)
{
    if(n_of_ocbs<100)
    {
        ocbs[n_of_ocbs].chBox=cb;
        ocbs[n_of_ocbs].st=state;
        ocbs[n_of_ocbs].chBox->setChecked(*(ocbs[n_of_ocbs].st));
        n_of_ocbs++;
    }
}


void Options::setCheckedButtonStatesFromRZP_DRAW()
{
    n_of_ocbs=0;
    setCheckedButtonState(ui->cbIsViewSacnSektorKurs, &(rzp_draw->bVyvodSektoraSkanKurs));
    setCheckedButtonState(ui->cbIsViewDNGlisKurs, &(rzp_draw->bVyvodUgolGlisVKurs));
    setCheckedButtonState(ui->cbIsViewLiniiAzimKurs,  &(rzp_draw->bVyvodAzmt));
    setCheckedButtonState(ui->cbIsViewHorLiniiKurs, &(rzp_draw->bVyvodGorUdal));
    setCheckedButtonState(ui->cbIsViewLiniiAzimTextKurs, &(rzp_draw->bVyvodAzmtText));
    setCheckedButtonState(ui->cbIsViewHorLiniiTextKurs, &(rzp_draw->bVyvodGorUdalText));

    setCheckedButtonState(ui->cbIsViewScanSectorGlis, &(rzp_draw->bVyvodSektoraSkanGlis));
    setCheckedButtonState(ui->cbIsViewDNKursGlis, &(rzp_draw->bVyvodUgolKursVGlis));
    setCheckedButtonState(ui->cbIsViewUglLiniiGlis, &(rzp_draw->bVyvodUglomer));
    setCheckedButtonState(ui->cbVerLiniiGlis, &(rzp_draw->bVyvodLiniiVertUdal));
    setCheckedButtonState(ui->cbIsViewUglLiniiTextGlis, &(rzp_draw->bVyvodUglomerText));
    setCheckedButtonState(ui->cbVerLiniiTextGlis, &(rzp_draw->bVyvodTextVertUdal));
    setCheckedButtonState(ui->cbLiniiRavnyhVysotGlis, &(rzp_draw->bVyvodRavnyhVysot));
}


void Options::setCheckedButtonsToRZP_DRAW()
{
    int i;
    for(i=0;i<n_of_ocbs;i++)
    {
        *(ocbs[i].st)=ocbs[i].chBox->isChecked();
    }

}





