#-------------------------------------------------
#
# Project created by QtCreator 2014-01-15T14:48:10
#
#-------------------------------------------------

QT       += core  network multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = prl_2st_view
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    options.cpp \
    ../../graphout/AnyTools.cpp \
    ../../graphout/Formulyary.cpp \
    ../../graphout/GlissadaGraf.cpp \
    ../../graphout/KursGraf.cpp \
    ../../graphout/OtmetkaDannye.cpp \
    ../../graphout/RZPLinGlaf.cpp \
    stdafx.cpp \
    rsp_outputwidget.cpp \
    ../../get_data/tgettingdatathread.cpp \
    ../../tools/time_tools.cpp \
    ../../tools/safe_function.cpp \
    ../../comport_work/tools_win_unix.cpp \
    ../../graphout/qt_tools.cpp \
    ../../comport_work/tools_unix.cpp \
    ../../lua_src/lapi.c \
    ../../lua_src/lauxlib.c \
    ../../lua_src/lbaselib.c \
    ../../lua_src/lbitlib.c \
    ../../lua_src/lcode.c \
    ../../lua_src/lcorolib.c \
    ../../lua_src/lctype.c \
    ../../lua_src/ldblib.c \
    ../../lua_src/ldebug.c \
    ../../lua_src/ldo.c \
    ../../lua_src/ldump.c \
    ../../lua_src/lfunc.c \
    ../../lua_src/lgc.c \
    ../../lua_src/linit.c \
    ../../lua_src/liolib.c \
    ../../lua_src/llex.c \
    ../../lua_src/lmathlib.c \
    ../../lua_src/lmem.c \
    ../../lua_src/loadlib.c \
    ../../lua_src/lobject.c \
    ../../lua_src/lopcodes.c \
    ../../lua_src/loslib.c \
    ../../lua_src/lparser.c \
    ../../lua_src/lstate.c \
    ../../lua_src/lstring.c \
    ../../lua_src/lstrlib.c \
    ../../lua_src/ltable.c \
    ../../lua_src/ltablib.c \
    ../../lua_src/ltm.c \
    ../../lua_src/lundump.c \
    ../../lua_src/lutf8lib.c \
    ../../lua_src/lvm.c \
    ../../lua_src/lzio.c \
    ../../lua_util/lua_utils.cpp


HEADERS  += mainwindow.h \
    options.h \
    ../../graphout/AnyTools.h \
    ../../graphout/Formulyary.h \
    ../../graphout/GlissadaGraf.h \
    ../../graphout/KursGraf.h \
    ../../graphout/OtmetkaDannye.h \
    ../../graphout/RZPLinGlaf.h \
    stdafx.h \
    targetver.h \
    rsp_outputwidget.h \
    ../../get_data/tgettingdatathread.h \
    ../../tools/time_tools.h \
    ../../comport_work/my_stdlib.h \
    ../../tools/my_stdlib.h \
    ../../tools/my_stdafx.h \
    ../../comport_work/tools_win_unix.h \
    ../../graphout/qt_tools.h \
    ../../graphout/temp_work_with_ini.h \
    ../../comport_work/tools_unix.h \
    ../../lua_src/lzio.h \
    ../../lua_src/lapi.h \
    ../../lua_src/lauxlib.h \
    ../../lua_src/lcode.h \
    ../../lua_src/lctype.h \
    ../../lua_src/ldebug.h \
    ../../lua_src/ldo.h \
    ../../lua_src/lfunc.h \
    ../../lua_src/lgc.h \
    ../../lua_src/llex.h \
    ../../lua_src/llimits.h \
    ../../lua_src/lmem.h \
    ../../lua_src/lobject.h \
    ../../lua_src/lopcodes.h \
    ../../lua_src/lparser.h \
    ../../lua_src/lprefix.h \
    ../../lua_src/lstate.h \
    ../../lua_src/lstring.h \
    ../../lua_src/ltable.h \
    ../../lua_src/ltm.h \
    ../../lua_src/lua.h \
    ../../lua_src/lua.hpp \
    ../../lua_src/luaconf.h \
    ../../lua_src/lualib.h \
    ../../lua_src/lundump.h \
    ../../lua_src/lvm.h \
    ../../lua_util/lua_utils.h \
    ../../tools/safe_function.h

FORMS    += mainwindow.ui \
    options.ui

INCLUDEPATH += ../../graphout \
        ../../get_data \
        ../../tools \
        ../../comport_work \
        ../../lua_src \
        ../../lua_util

DISTFILES += \
    ../../../ini/PreSetup.ini



