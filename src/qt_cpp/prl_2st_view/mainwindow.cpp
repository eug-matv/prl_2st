
#include <QString>
#include <QPushButton>
#include <QObjectList>
#include <QApplication>
#include <QRect>
#include <QMessageBox>
#include <QPainter>
#include <QSettings>
#include <QTextStream>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "options.h"
#include "tools_win_unix.h"
#include "safe_function.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    QObjectList obj_list;
    QString str,str_log,str_log_err;
    timer=NULL;
    rsp_w=NULL;
    gthrd=NULL;
    m_mutex=NULL;
    k_for_flash=0;
    timerFlashing=NULL;
    isError=false;

   // this->setWindowState(Qt::WindowFullScreen);

    timer=new QTimer(this);

    timer->setInterval(100);


 //   setAttribute(Qt::WA_PaintOnScreen);
    ui->setupUi(this);
    ui->mainToolBar->addWidget(ui->gbMaxDalnost);
    ui->mainToolBar->addWidget(ui->gbUgolKurs);
    ui->mainToolBar->addWidget(ui->gbUgolGlis);
    ui->mainToolBar->addWidget(ui->gbNumberNapr);
    str=QApplication::applicationDirPath();
#ifdef __WIN32
    for(int i=0;i<str.length();i++)
    {
          if(str[i]==QChar('/'))
          {
              str.replace(i,1,QString("\\"));
          }
    }
    str_log=str+QString("\\log");
    str_log_err=str+QString("\\err_log");
#else
    str_log=str+QString("/log");
    str_log_err=str+QString("/err_log");
#endif

    rzp_draw=new TRZPDraw(str_log.toLatin1().data(),str_log_err.toLatin1().data());

    rsp_w=new RSP_OutputWidget(rzp_draw,this);


    if(setDataForBegin()<0)
    {
        delete rzp_draw;
        rzp_draw=NULL;
        delete rsp_w;
        rsp_w=NULL;
        delete timer;
        timer=NULL;
        isError=true;

         return;
    }

    m_mutex=new QMutex();


    gthrd=new TGettingDataThread(NULL);
    str=QApplication::applicationDirPath()+QString("/input.ini");
    getOptionFromIniFile(str.toLatin1().data());
    gthrd->setOptions(gdito);
    gthrd->setRZP_DRAW(rzp_draw);
    gthrd->setMutexForSynchr(m_mutex);


    timerFlashing=new QTimer(this);
    timerFlashing->setInterval(500);





        connect(ui->mnuOption,SIGNAL(triggered()),this,SLOT(openOptionWindow()));
        connect(ui->rb5000,SIGNAL(toggled(bool)),this,SLOT(setMashtab5000()));
        connect(ui->rb10000,SIGNAL(toggled(bool)),this,SLOT(setMashtab10000()));
        connect(ui->rb20000,SIGNAL(toggled(bool)),this,SLOT(setMashtab20000()));
        connect(ui->rb30000,SIGNAL(toggled(bool)),this,SLOT(setMashtab30000()));
        connect(ui->rb40000,SIGNAL(toggled(bool)),this,SLOT(setMashtab40000()));
        connect(ui->rb50000,SIGNAL(toggled(bool)),this,SLOT(setMashtab50000()));
        connect(ui->rb60000,SIGNAL(toggled(bool)),this,SLOT(setMashtab60000()));
        connect(ui->rb70000,SIGNAL(toggled(bool)),this,SLOT(setMashtab70000()));
        connect(ui->rb80000,SIGNAL(toggled(bool)),this,SLOT(setMashtab80000()));
        connect(ui->rbNapr0,SIGNAL(toggled(bool)),this,SLOT(setNapravlenie0()));
        connect(ui->rbNapr1,SIGNAL(toggled(bool)),this,SLOT(setNapravlenie1()));

    /*    connect(ui->dsbDiapazonUglKurs,SIGNAL(editingFinished()),
                this, SLOT(changeDiapazonUglKurs()));

        connect(ui->dsbMinUgolGlis,SIGNAL(editingFinished()),
                 this, SLOT(changeMinUgolGlis()));
        connect(ui->dsbMaxUgolGlis,SIGNAL(editingFinished()),
                 this, SLOT(changeMaxUgolGlis()));

*/
        connect(timer,SIGNAL(timeout()),this, SLOT(slot_timeout_for_output()));
        timer->start();



        gthrd->start();

        twuSLEEP(1000);
        if(gthrd->errCode<0)
        {
            QMessageBox::warning(NULL,QString("Error of input"),
                                         QString("Error of input"));
        }



        connect(timerFlashing, SIGNAL(timeout()),this,SLOT(timeoutForFlashing()));

        timerFlashing->start();


    setWindowState(Qt::WindowMaximized);





}

MainWindow::~MainWindow()
{


    if(timerFlashing)
    {
        timerFlashing->stop();
        delete timerFlashing;
        timerFlashing=NULL;
    }

    if(timer)
    {
           timer->stop();
           delete timer;
           timer=NULL;
    }

    if(gthrd)
    {
        gthrd->exit();
        gthrd->wait();
        delete gthrd;
        gthrd=NULL;
    }
    if(rzp_draw)
    {
         delete rzp_draw;
        rzp_draw=NULL;
    }
    if(m_mutex)
    {
        delete m_mutex;
        m_mutex=NULL;
    }
    if(ui)
    {
        delete ui;
    }
}



/*СЛОТЫ*/
void MainWindow::openOptionWindow()
{
    Options *opt;
    if(rzp_draw)
    {
        //rzp_draw->UstanovitMaxVizDalnost(0);
         opt=new Options(rzp_draw,this);
        opt->exec();


        delete opt;
    }
    rzp_draw->updateForRepaint();
     rsp_w->update();
}

void MainWindow::setMashtab5000()
{
    if(rzp_draw)
    {
        rzp_draw->UstanovitMaxVizDalnost(0);
        rsp_w->update();
    }
}

void MainWindow::setMashtab10000()
{
    if(rzp_draw)
    {
        rzp_draw->UstanovitMaxVizDalnost(1);
        slot_timeout_for_output();
    }
}

void MainWindow::setMashtab20000()
{
    if(rzp_draw)
    {
        rzp_draw->UstanovitMaxVizDalnost(2);
        slot_timeout_for_output();
    }
}

void MainWindow::setMashtab30000()
{
    if(rzp_draw)
    {
        rzp_draw->UstanovitMaxVizDalnost(3);
        slot_timeout_for_output();
    }
}

void MainWindow::setMashtab40000()
{
    if(rzp_draw)
    {
        rzp_draw->UstanovitMaxVizDalnost(4);
        slot_timeout_for_output();
    }
}

void MainWindow::setMashtab50000()
{
    if(rzp_draw)
    {
        rzp_draw->UstanovitMaxVizDalnost(5);
        slot_timeout_for_output();
    }
}



void MainWindow::setMashtab60000()
{
    if(rzp_draw)
    {
        rzp_draw->UstanovitMaxVizDalnost(6);
        slot_timeout_for_output();
    }
}

void MainWindow::setMashtab70000()
{
    if(rzp_draw)
    {
        rzp_draw->UstanovitMaxVizDalnost(7);
        slot_timeout_for_output();
    }
}

void MainWindow::setMashtab80000()
{
    if(rzp_draw)
    {
        rzp_draw->UstanovitMaxVizDalnost(8);
        slot_timeout_for_output();
    }
}



void MainWindow::setNapravlenie0()
{
    int iRet;
    if(rzp_draw)
    {
        if(rzp_draw->poluchitNapravlenie()==0)return;


//Остановим поток получения данных
        gthrd->exit();
        gthrd->wait();

        iRet=rzp_draw->ustanovitNapravlenie(0);
        if(iRet<0)
        {
            return;
        }



        gthrd->start();

        twuSLEEP(1000);
        if(gthrd->errCode<0)
        {
            QMessageBox::warning(NULL,QString("Error of input"),
                                         QString("Error of input"));
            return;
        }



        slot_timeout_for_output();
    }

}


void MainWindow::setNapravlenie1()
{
    int iRet;
    if(rzp_draw)
    {
        if(rzp_draw->poluchitNapravlenie()!=0)return;


//Остановим поток получения данных
        gthrd->exit();
        gthrd->wait();

        iRet=rzp_draw->ustanovitNapravlenie(1);
        if(iRet<0)
        {
            return;
        }


        gthrd->start();

        twuSLEEP(1000);
        if(gthrd->errCode<0)
        {
            QMessageBox::warning(NULL,QString("Error of input"),
                                         QString("Error of input"));
        }


        slot_timeout_for_output();
    }

}



int MainWindow::setDataForBegin()
{
    QString str, str_presetup_ini,str_user_ini;
    QRect rct;
    QSize sz;

    long lRet;



    str=QApplication::applicationDirPath();


    rsp_w->setGeometry(0,ui->menuBar->size().height(),
                       ui->mainToolBar->pos().x()-1,
                       ui->statusBar->pos().y()-1-ui->menuBar->size().height());


    if(rzp_draw)
    {

        int iTipDalnosti;
        rct.setLeft(0);
        rct.setTop(ui->menuBar->size().height());
        rct.setRight(ui->mainToolBar->pos().x()-1);
        rct.setBottom(ui->statusBar->pos().y()-1);


#ifdef __WIN32
        for(int i=0;i<str.length();i++)
        {
              if(str[i]==QChar('/'))
              {
                  str.replace(i,1,QString("\\"));
              }
        }
        str_presetup_ini=str+QString("\\PreSetup.ini");
        str_user_ini=str+QString("\\User.ini");
#else
        str_presetup_ini=str+QString("/PreSetup.ini");
        str_user_ini=str+QString("/User.ini");
#endif

         lRet=rzp_draw->BeginWork(rct,str_presetup_ini.toLatin1().data(),
                             str_user_ini.toLatin1().data());
         if(lRet<0)
         {
            QMessageBox::critical(this,QString("Ошибка запуска rzp_draw"),
                                  QString("Что-то в параметрах не правильно"));

            return lRet;
         }
         if(lRet>0)
         {
             double ugl1,ugl2;


             rzp_draw->PoluchitVizDiapazonUglKurs(&ugl1);
             ui->dsbDiapazonUglKurs->setValue(ugl1);


             rzp_draw->PoluchitVizDiapazonGlis(&ugl1,&ugl2);
             ui->dsbMinUgolGlis->setValue(ugl1);
             ui->dsbMaxUgolGlis->setValue(ugl2);



         }
      //   rzp_draw->VyvodNaEkranSSetkoy();
         iTipDalnosti=rzp_draw->PoluchitTipDalnost();
         switch(iTipDalnosti)
         {
            case 0:
             ui->rb5000->setChecked(true);
            break;

            case 1:
              ui->rb10000->setChecked(true);
            break;

            case 2:
               ui->rb20000->setChecked(true);
            break;

             case 3:
                ui->rb30000->setChecked(true);
             break;

            case 4:
                ui->rb40000->setChecked(true);
            break;

             case 5:
                 ui->rb50000->setChecked(true);
             break;


             case 6:
                 ui->rb60000->setChecked(true);
             break;

             case 7:
                 ui->rb70000->setChecked(true);
             break;

             case 8:
                 ui->rb80000->setChecked(true);
             break;

         };

         if(rzp_draw->poluchitNapravlenie())
         {
            ui->rbNapr1->setChecked(true);
         }else{
            ui->rbNapr0->setChecked(true);
         }

         return 1;
    }
    return 0;
}




void MainWindow::synchParametersWithMasshtab()
{
    int iTipDalnosti;
    double diap_ugl, diap_ugl1, diap_ugl2;
    if(rzp_draw)
    {
//Установим дальность
        iTipDalnosti=rzp_draw->PoluchitTipDalnost();
        switch(iTipDalnosti)
        {
            case 0:
                disconnect(ui->rb5000,SIGNAL(toggled(bool)));
                ui->rb5000->setChecked(true);
                connect(ui->rb5000,SIGNAL(toggled(bool)),this,SLOT(setMashtab5000()));
            break;
            case 1:
                disconnect(ui->rb10000,SIGNAL(toggled(bool)));
                ui->rb10000->setChecked(true);
                connect(ui->rb10000,SIGNAL(toggled(bool)),this,SLOT(setMashtab10000()));
            break;
            case 2:
                disconnect(ui->rb20000,SIGNAL(toggled(bool)));
                ui->rb20000->setChecked(true);
                connect(ui->rb20000,SIGNAL(toggled(bool)),this,SLOT(setMashtab20000()));
            break;
            case 3:
                disconnect(ui->rb30000,SIGNAL(toggled(bool)));
                ui->rb30000->setChecked(true);
                connect(ui->rb30000,SIGNAL(toggled(bool)),this,SLOT(setMashtab30000()));
            break;
            case 4:
                disconnect(ui->rb40000,SIGNAL(toggled(bool)));
                ui->rb40000->setChecked(true);
                connect(ui->rb40000,SIGNAL(toggled(bool)),this,SLOT(setMashtab40000()));
            break;
            case 5:
                disconnect(ui->rb50000,SIGNAL(toggled(bool)));
                ui->rb50000->setChecked(true);
                connect(ui->rb50000,SIGNAL(toggled(bool)),this,SLOT(setMashtab50000()));
            break;
            case 6:
                disconnect(ui->rb60000,SIGNAL(toggled(bool)));
                ui->rb60000->setChecked(true);
                connect(ui->rb60000,SIGNAL(toggled(bool)),this,SLOT(setMashtab60000()));
            break;
            case 7:
                disconnect(ui->rb70000,SIGNAL(toggled(bool)));
                ui->rb70000->setChecked(true);
                connect(ui->rb70000,SIGNAL(toggled(bool)),this,SLOT(setMashtab70000()));
            break;
            default:
                disconnect(ui->rb80000,SIGNAL(toggled(bool)));
                ui->rb80000->setChecked(true);
                connect(ui->rb80000,SIGNAL(toggled(bool)),this,SLOT(setMashtab80000()));
        }

//Установим углы
        rzp_draw->PoluchitVizDiapazonUglKurs(&diap_ugl);
        ui->dsbDiapazonUglKurs->setValue(diap_ugl);

        rzp_draw->PoluchitVizDiapazonGlis(&diap_ugl1, &diap_ugl2);
        ui->dsbMinUgolGlis->setValue(diap_ugl1);
        ui->dsbMaxUgolGlis->setValue(diap_ugl2);

    }
}



void MainWindow::resizeEvent(QResizeEvent * )
{
    if(rsp_w)
    {
        rsp_w->setGeometry(0,  ui->menuBar->size().height(),
                       ui->mainToolBar->pos().x()-1,
                       ui->statusBar->pos().y()-1-ui->menuBar->size().height());


        if(rzp_draw)
        {
            rzp_draw->UstanovitOblastVyvoda(rsp_w->rect());
            slot_timeout_for_output();
        }
    }


}





void MainWindow::getOptionFromIniFile(char *input_ini)
{
    int isUDP;
    QSettings *stng;
    QString str;

    stng=new QSettings(QString(input_ini),QSettings::IniFormat);

    isUDP=stng->value(QString("INPUT/isUDP"),0).toInt();

    if(isUDP)
    {
        gdito.tos=TGDT_UDP;
        str=stng->value(QString("INPUT/ip_name"), QString("127.0.0.1")).toString();
        sfSTRCPY(gdito.opt.udp.address,str.toLatin1().data());

        gdito.opt.udp.port=stng->value(QString("INPUT/port"),7777).toInt();
    }else{
        gdito.tos=TGDTComPort;

        str=stng->value(QString("INPUT/com_name"), QString("\\\\.\\COM1")).toString();
        sfSTRCPY(gdito.opt.com.port_name,str.toLatin1().data());
        gdito.opt.com.baudrate=stng->value(QString("INPUT/baud_rate"),9600).toInt();
        gdito.opt.com.stopbits=0;
    }

    gdito.tmOfCurVO=stng->value(QString("TIMEOFLIVE/tmOfCurVO"),1100).toInt();
    gdito.tmOfViewAfterDieVO=stng->value(QString("TIMEOFLIVE/tmOfViewAfterDieVO"),2100).toInt();
    gdito.tmOfCurTrs=stng->value(QString("TIMEOFLIVE/tmOfCurTrs"),1100).toInt();
    gdito.tmOfHvstTrs=stng->value(QString("TIMEOFLIVE/tmOfHvstTrs"),100100).toInt();
    gdito.tmOfViewAfterDieTrs=stng->value(QString("TIMEOFLIVE/tmOfViewAfterDieTrs"),5100).toInt();



    delete stng;

}




void MainWindow::changeDiapazonUglKurs()
{
    double ugl;
    if(rzp_draw)
    {
            ugl=ui->dsbDiapazonUglKurs->value();
            rzp_draw->UstanovitVizDiapazonUglKurs(ugl);
            rzp_draw->updateForRepaint();
            rzp_draw->PoluchitVizDiapazonUglKurs(&ugl);
    }
    ui->dsbDiapazonUglKurs->setValue(ugl);
    rsp_w->update();
}

void MainWindow::changeMinUgolGlis()
{
    double ugl1,ugl2;
    ugl1=ui->dsbMinUgolGlis->value();
    ugl2=ui->dsbMaxUgolGlis->value();
    if(rzp_draw)
    {
        rzp_draw->UstanovitVizDiapazonGlis(ugl1,
                                           ugl2);
        rzp_draw->PoluchitVizDiapazonGlis(&ugl1,&ugl2);
        rzp_draw->updateForRepaint();
    }
    ui->dsbMinUgolGlis->setValue(ugl1);
    ui->dsbMaxUgolGlis->setValue(ugl2);
    rsp_w->update();
}

void MainWindow::changeMaxUgolGlis()
{
    double ugl1,ugl2;
    ugl1=ui->dsbMinUgolGlis->value();;
    ugl2=ui->dsbMaxUgolGlis->value();
    if(rzp_draw)
    {
        rzp_draw->UstanovitVizDiapazonGlis(ugl1,
                                           ugl2);
        rzp_draw->PoluchitVizDiapazonGlis(&ugl1,&ugl2);
        rzp_draw->updateForRepaint();
    }
    ui->dsbMinUgolGlis->setValue(ugl1);
    ui->dsbMaxUgolGlis->setValue(ugl2);
    rsp_w->update();
}

void MainWindow::timeoutForFlashing()
{
    if(rzp_draw)
    {
        rzp_draw->updateFlashState();
    }
}

void MainWindow::slot_timeout_for_output()
{
    int iRet;

    if(rzp_draw)
    {
        iRet=rzp_draw->RaschetKoordinatIPodgotovkaDannyhKVyvodu();
        if(iRet)
        {
            rsp_w->update();
            if(iRet>=1000)
            {
                synchParametersWithMasshtab();
            }
        }
    }
}
