#include "rsp_outputwidget.h"

RSP_OutputWidget::RSP_OutputWidget(TRZPDraw *_rzp_draw, QWidget *parent) :
    QWidget(parent)
{
    rzp_draw=_rzp_draw;
   }




void RSP_OutputWidget::slot_timeout_for_output()
{
    int iRet;

    if(rzp_draw)
    {
        iRet=rzp_draw->RaschetKoordinatIPodgotovkaDannyhKVyvodu();
        if(iRet)
        {
            update();
        }
    }
}





void RSP_OutputWidget::paintEvent(QPaintEvent * event)
{

    QPainter p;
    p.begin(this);


    if(rzp_draw)
    {

   //    rzp_draw->VyvodNaEkranForPeriodDC(DC);
        rzp_draw->VyvodNaEkranForPeriodDC(p);
    }


    p.end();
}
