/*Модуль: rsp_outputwidget.h - область отображения данных
Автор: Матвеенко Е.А. ЧРЗ Полет ОКБ

Реализована графическая область отображения данных.
Главный класс RSP_OutputWidget
*/


#ifndef RSP_OUTPUTWIDGET_H
#define RSP_OUTPUTWIDGET_H

#include <QWidget>
#include <QPainter>
#include <QPaintEngine>
#include <QTimer>
#include "RZPLinGlaf.h"

class RSP_OutputWidget : public QWidget
{
    Q_OBJECT
public:
    explicit RSP_OutputWidget(TRZPDraw *_rzp_draw, QWidget *parent = 0);
virtual QPaintEngine *	paintEngine(){return 0;};

protected:
virtual void paintEvent(QPaintEvent * event);

public slots:
 void   slot_timeout_for_output(); //Обратка события таймера. В данном случае перерисовка

private:
    TRZPDraw *rzp_draw; //Основной объект для отображения данных. Реализован в модуле
                        //graphout/RZPLinGlaf
    QTimer *timer;  //Таймер для перидического обновления экрана.
};

#endif // RSP_OUTPUTWIDGET_H
