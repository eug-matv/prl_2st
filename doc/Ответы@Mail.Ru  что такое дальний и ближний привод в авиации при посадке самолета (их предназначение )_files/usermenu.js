var iframeanchor;
var iframeperse;

function screenSize()
{
  var w, h; // ��������� ����������, w - �����, h - ������
  
  w = (window.innerWidth)  ? window.innerWidth  : window.document.body.clientWidth;
  h = (window.innerHeight) ? window.innerHeight : window.document.body.clientHeight;

  return {w:w, h:h};
}

function userMenuShow(_link, evt, short)
{
  // ������� ���������� iframe � �����������
  // ����������� ���������� �������
  evt = evt || window.event;
  evt.cancelBubble = true;

  // ��������� e-mail ������������
  var url;

  if(short)
  {
    url = "http://help.mail.ru/js/arrowshort.html?mail=";
  }

  else
  {
    url = "http://help.mail.ru/js/arrow.html?mail=";
  }

  var mail = _link.href.substring(_link.href.indexOf("to=") + 3, _link.href.length);

  iframeanchor = _link;

  // ������� iframe
  var iframe = document.getElementById(short ? "userDropDownIdshort" : "userDropDownId");

  iframeperse          = iframe;
  iframe.src           = url + mail;
  iframe.style.display = "";

  // ������ ��������� iframe-�
  iframe.style.top = (absPosition(_link).y + _link.offsetHeight) + "px";
  iframe.style.left = ((screenSize().w - absPosition(_link).x - iframe.offsetWidth > 0) ?
    absPosition(_link).x : absPosition(_link).x - iframe.offsetWidth + _link.offsetWidth) + "px";

  return false;
}

function iframerepositiononresize()
{
  if(!iframeperse)
  {
    return;
  }

  var _link = iframeanchor;
  var iframe = iframeperse;

  // ������ ��������� iframe-�
  iframe.style.top = absPosition(_link).y + _link.offsetHeight;
  iframe.style.left = (screenSize().w - absPosition(_link).x - iframe.offsetWidth > 0) ?
    absPosition(_link).x : absPosition(_link).x - iframe.offsetWidth + _link.offsetWidth;
}

// ������� ��� ���������� ����������� �������
function addHandler(object, event, handler)
{
  if(typeof object.addEventListener != 'undefined')
    object.addEventListener(event, handler, false);

  else if(typeof object.attachEvent != 'undefined')
    object.attachEvent('on' + event, handler);

  else
    throw "Incompatible browser";
}

// ������� ��� ������� iframe-a �� ����� �� ��������
function userMenuHide()
{
  var iframe = document.getElementById("userDropDownId");

  iframe.style.display = "none";

  document.getElementById("userDropDownIdshort").style.display = "none";
}

// ������� ��������� ����� 
addHandler(document, "click", userMenuHide);
addHandler(window, "resize", iframerepositiononresize);


// New dropdown
var friendEmails = new Object();
var linksArray = [
					{'type': 'link', 'short': '1', 'cls': 'add', 'href': 'http://www.mail.ru/agent?message&to=@&from=otvet', 'text': '�������� � �����'},
					{'type': 'link', 'short': '1', 'cls': 'let', 'href': 'http://e.mail.ru/cgi-bin/sentmsg?To=@&from=otvet', 'text': '�������� ������'},
					{'type': 'line', 'short': '0', 'cls': '',	 'href': '', 'text': '<img src="http://otvet.mail.ru/img/0.gif" alt="" />'},
					{'type': 'link', 'short': '0', 'cls': 'myw', 'href': 'http://my.mail.ru/@/?from=otvet', 'text': '��� ������������'},
					{'type': 'link', 'short': '0', 'cls': 'pht', 'href': 'http://foto.mail.ru/@/?from=otvet', 'text': '���� ������������'},
					{'type': 'link', 'short': '0', 'cls': 'vid', 'href': 'http://video.mail.ru/@/?from=otvet', 'text': '����� ������������'},
					//{'type': 'link', 'short': '0', 'cls': 'blg', 'href': 'http://blogs.mail.ru/@/?from=otvet', 'text': '����� ������������'}
					];
var friendsLinks = [
					{'type': 'link', 'short': '1', 'cls': 'plusfr', 'href': 'http://my.mail.ru/my/invitation?single=1&email=@', 'text': '�������� � ������'},
					{'type': 'link', 'short': '1', 'cls': 'minusfr', 'href': 'http://my.mail.ru/my/addfriend?broke=@', 'text': '������� �� ������'}
					];

document.onclick = function() {
  if ( document.getElementById("userMenu") ){
    document.getElementById("userMenu").style.display = "none";
  }
}

function addLinkToDropDown(short, cls, href, div, email, parsedEMail){
  var temp;
	
  if ( short == 1 ){
    temp = email;
  } else if ( short == 0 ){
    temp = parsedEMail.domain + '/' + parsedEMail.box;
  }
  
  href = href.replace(/@/, temp);
  elem = document.createElement("A");
  
  if ( div.getAttribute("className") ){
    elem.setAttribute('className', cls);
  } else {
    elem.setAttribute('class', cls);
  }
  
  elem.setAttribute('target', '_blank');
  elem.setAttribute('href', href);

  return elem;
}

function dropDownUserMenu(link, evt, rgt, long, needFriends){
  var rightFlag, parsedEMail, email, re, elem, count, i, j, href, temp;
  evt = evt || window.event;
  evt.cancelBubble = true;

  email = link.getAttribute("href");
  re = new RegExp("to=(.+)$", "");
  re.exec(email);
  email = RegExp.$1;
  parsedEMail = parseEMail(email);
	
  div = document.getElementById("userMenu");
  div .innerHTML = '';
  count = linksArray.length;
  for (i=0; i<count; i++){
    if ( long || linksArray[i].short == 1 ){
      if ( linksArray[i].type == 'link' ){
        elem = addLinkToDropDown(linksArray[i].short, linksArray[i].cls, linksArray[i].href, div, email, parsedEMail);
      } else if ( linksArray[i].type == 'line' ){
        elem = document.createElement("DIV");
      }
      elem.innerHTML = linksArray[i].text;
      div.appendChild(elem);
    }
    if ( linksArray[i+1] && linksArray[i].short != linksArray[i+1].short && needFriends ){
      j = friendEmails[email] || 0;
      elem = addLinkToDropDown(friendsLinks[j].short, friendsLinks[j].cls, friendsLinks[j].href, div, email, parsedEMail);
      elem.innerHTML = friendsLinks[j].text;
      div.appendChild(elem);
    }
  }
  div.style.top  = absPosition(link).y + link.offsetHeight + "px";
  div.style.zIndex = -10000;
  div.style.display = "";
  div.style.left = ((screenSize().w - absPosition(link).x - div.offsetWidth - 10 > 0) ? absPosition(link).x : absPosition(link).x - div.offsetWidth + link.offsetWidth) + "px";
  div.style.zIndex = 10000;
  return false;
}
