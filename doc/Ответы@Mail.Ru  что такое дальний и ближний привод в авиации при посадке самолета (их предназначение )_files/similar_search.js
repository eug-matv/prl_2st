function wordCount(){
	var str = " " + qtext;
	pattern = str.match( / [a-zA-Z�-��-߸�]{2}[a-zA-Z�-��-߸�0-9-]+/g, str );
	return (pattern ? pattern.length : 0);
}

function getWords(){
	var str, retval="";
	str = " " + qtext;
	pattern = str.match( / [a-zA-Z�-��-߸�0-9-]+/g, str );
	if ( pattern ) {
		for ( i=0; i<pattern.length; i++){
			retval += pattern[i];
		}
	}
	return retval;
}


var wordStart   = 0;  //  ����, ��� ���� ����� �����
var reqWord 	= 0;  //  ����� ����, ��� ������� ��������� ��� ������� ������
var minWordCont = 4;  //  ����������� ����� ����, ��� ������� ������ ������, ���� ����� ��������� ������������
var minWordStop = 3;  //  ����������� ����� ����, ��� ������� ������ ������, ���� ���� ��������� �������� �����
var curReqCount = 0;  //  ����� �������� � ������� � ������� �������
var maxReqCount = 5;  //  ������������ ����� �������� � �������
var lastWord 	= ""; //  ��������� ������
var resCount    = 3;  //  ����� ��������� ��������
var qtext 		= ""; //  ����� �������
var qid			= 0;  //  ������������� �������

function keypress(e) {
	var key, chr, count;
	key = getkey(e);

	if ( key ){
		chr = String.fromCharCode( key );
		if ( chr.match(/[A-Z-]/)  ){
			if ( ! wordStart ) wordStart = 1;
		}else{
			if ( wordStart ) {
				if ( !chr.match(/[0-9]/) ){
					wordStart = 0;
					count = wordCount();
					if ( count != reqWord ){
						//  ������ ������
						reqWord = count;
						loadHint( count, minWordCont );
					}
				}
			}
		}
	}
}


function getkey(e){
	if (!e) e = window.event;
	return e.keyCode ? e.keyCode : e.which ?  e.which : 0;
}

function loadHint(count, mincount){ 
	count = parseInt(count);
	if ( isNaN(count) ) {
		count     = wordCount();
		mincount  = minWordStop;
	}
	//  �������� ��������� ����������
	if ( curReqCount < maxReqCount &&  count >= mincount ){
		var word = getWords();
		if ( word != lastWord ){
			document.getElementById('temp').innerHTML = '';
			lastWord = word;
			curReqCount++;
			fastSearch();
		}
	}
}

function init_search() {
	if ( !qtext ){
		var doc = document.getElementById("temp");
        qtext = doc.innerHTML;
        qid = doc.getAttribute("relid");
        doc.innerHTML = "";
	}
	if ( wordCount() ){
    	loadHint();
	}
}

function fastSearch(){
	var url = "/search/similar";
	var params = "n="+ resCount +"&q=" +  getWords() + "&zvstate=3";  //document.getElementById("qst").value;
	var method = "POST";
	var onload  = fastSearchParser;
	var onerror = fastSearchError;
	var contentType = "application/x-www-form-urlencoded; charset=windows-1251";

	return setAjaxRequest(method, url, params, onload, onerror, contentType, false, false);
}






function fastSearchParser(){
	var obj, hint, qst, id, snippet, text, len, nick, email, parsedEmail;
	len = this.req.responseXML.getElementsByTagName('id').length;

	if ( len>0 ) {
		hint    = '<h2 class="mb10">��� ����� ���� � ������� ��������</h2>'
				+ '<table class="Questions mb20">';

		for ( i=0; i<len; i++){
			id = this.req.responseXML.getElementsByTagName('id').item(i).firstChild.nodeValue;

			if ( id != qid ){
				nick    = this.req.responseXML.getElementsByTagName('nick').item(i).firstChild.nodeValue;
				email   = this.req.responseXML.getElementsByTagName('email').item(i).firstChild.nodeValue;
				text    = this.req.responseXML.getElementsByTagName('text').item(i).firstChild.nodeValue;
				text    = text.replace(/^[\r\n ]+/, "");
				parsedEmail = parseEMail(email);

				hint += "<tr><td class=\"data p0 pb5\">"
						+"<div class=\"det2\"><a href=\"/question/" + id + "/\" class=\"subj2\">"+ text + "</a>"
						+"<div class=\"add\"><a href=\"http://www.mail.ru/agent?message&to=" + email + "\"><img src=\"http://status.mail.ru/?" + email + "&9x9\" width=\"9\" height=\"9\" alt=\"\" /></a>"
						+"<a href=\"http://otvet.mail.ru/" + parsedEmail.domain + "/" + parsedEmail.box + "/\">" + nick + "</a>&nbsp;<a href=\"http://otvet.mail.ru/#url?to=" + email + "\" target=\"_blank\" onclick=\"return userMenuShow(this, event,0)\"><img src=\"http://otvet.mail.ru/img/ico_flips.gif\" width=\"9\" height=\"9\" alt=\"���� ������������\" title=\"���� ������������\" /></a>";
			}
		}
		document.getElementById('qst_open').innerHTML = hint;
	}
}


function fastSearchError(){
	return false;
}

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

