


function make_emul_trass_info(ti, prl)
	local ret_ti={}

	local tm
	local loc_tm1={}
	if type(ti)~="table" or type(prl) ~= "table" then return nil end
	
	
-- получение номеров отправителей получателей	
	ret_ti.n_sender=ti.n_sender
	ret_ti.n_receiver=ti.n_receiver
	
--Узнаем информацию о признаках источника радиолокационной информацию	
	ret_ti.kg_prl=ti.kg_prl	
	ret_ti.kk_prl=ti.kk_prl	


--получим признак сопровождения
    	ret_ti.sopr={}
	ret_ti.sopr.new_trass=ti.sopr.new_trass
	ret_ti.sopr.extropolir=ti.sopr.extropolir
	ret_ti.sopr.upd_xy=ti.sopr.upd_xy
	ret_ti.sopr.upd_xyh=ti.sopr.upd_xyh
	ret_ti.sopr.reset_trass=ti.sopr.reset_trass

--Получить тип донесения	
	ret_ti.type_i=TRASS_INFO_CONST


--получить признак системы координат. Должен быть 1
	ret_ti.psk=ti.psk

--получим номер трассы	
	ret_ti.t_num=ti.t_num	

--получим признаки маневра
	ret_ti.manevr={}
--маневр по курсу	
	ret_ti.manevr.k=ti.manevr.k
--маневр по высоте
	ret_ti.manevr.h=ti.manevr.h

	ret_ti.manevr.v=ti.manevr.v
		
--признак уровня отсчета H - пока пропустим

--признак части 1 - пока пропустим

   	ret_ti.x_km=prl.x_km*math.sin(option_prl.magnit_vpp/180.0*math.pi)+
				prl.y_km*math.cos(option_prl.magnit_vpp/180.0*math.pi)+option_prl.x_prl_km
					
	ret_ti.y_km=prl.x_km*math.cos(option_prl.magnit_vpp/180.0*math.pi)-
				prl.y_km*math.sin(option_prl.magnit_vpp/180.0*math.pi)+option_prl.y_prl_km
	
	ret_ti.h_km=prl.z_km+option_prl.lLevelOfVPP_m/1000.0
	

   	ret_ti.vx_m_s=prl.vx_m_s*math.sin(option_prl.magnit_vpp/180.0*math.pi)+
				  prl.vy_m_s*math.cos(option_prl.magnit_vpp/180.0*math.pi)
					
	ret_ti.vy_m_s=prl.vx_m_s*math.cos(option_prl.magnit_vpp/180.0*math.pi)-
				  prl.vy_m_s*math.sin(option_prl.magnit_vpp/180.0*math.pi)


	ret_ti.vh_m_s=prl.vz_m_s	

	
	if ret_ti.vx_m_s-math.floor(ret_ti.vx_m_s) >=0.5 then
		ret_ti.vx_m_s=math.floor(ret_ti.vx_m_s)+1 
	else
		ret_ti.vx_m_s=math.floor(ret_ti.vx_m_s)
	end

	if ret_ti.vy_m_s-math.floor(ret_ti.vy_m_s) >=0.5 then
		ret_ti.vy_m_s=math.floor(ret_ti.vy_m_s)+1 
	else
		ret_ti.vy_m_s=math.floor(ret_ti.vy_m_s)
	end



	if ret_ti.vh_m_s-math.floor(ret_ti.vh_m_s) >=0.5 then
		ret_ti.vh_m_s=math.floor(ret_ti.vh_m_s)+1 
	else
		ret_ti.vh_m_s=math.floor(ret_ti.vh_m_s)
	end


--теперь время введем
	tm=os.date("*t")
	loc_tm1.hour=tm.hour
	loc_tm1.min=tm.min
	loc_tm1.sec=tm.sec+math.random()
	ret_ti.loc_tm=visp_get_copy_time_location(loc_tm1)
	return ret_ti
end


function make_emul_koord_info(ki, prl)
	local ret_ki={}

	local tm
	local loc_tm1={}
	if type(ki)~="table" or type(prl) ~= "table" then return nil end
	
	
-- получение номеров отправителей получателей	
	ret_ki.n_sender=ki.n_sender
	ret_ki.n_receiver=ki.n_receiver
	
--Узнаем информацию о признаках источника радиолокационной информацию	
	ret_ki.kg_prl=ki.kg_prl	
	ret_ki.kk_prl=ki.kk_prl	


-- получим тип донесения
	ret_ki.type_i = KOORD_INFO_CONST
	
--получить признак системы координат. Должен быть 0	
	ret_ki.psk=0	
	
--отклонение возущного судна от осевой линии ВПП ко курсу в градусах
	ret_ki.ok_gradus = ki.ok_gradus
	
--получние отклонения по линии глиссады в градусах
	ret_ki.og_gradus = ki.og_gradus

	if ret_ki.kk_prl then
		if ret_ki.kg_prl then
			local tmp_dbl
			local tmp_ugl
			tmp_dbl=prl.x_km*prl.x_km+prl.y_km*prl.y_km+prl.z_km*prl.z_km
			ret_ki.d_km=tmp_dbl^0.5
			ret_ki.h_km=prl.z_km	
			ret_ki.a_gr=math.atan2(prl.y_km,prl.x_km)/math.pi*180.0+option_prl.magnit_vpp
			if ret_ki.a_gr<0 then ret_ki.a_gr=ret_ki.a_gr+360.0 end
			if ret_ki.a_gr>=360 then ret_ki.a_gr=ret_ki.a_gr-360.0 end
		else
-- высота			
			tmp_dbl=prl.x_km*prl.x_km+prl.y_km*prl.y_km
			ret_ki.d_km=tmp_dbl^0.5
			ret_ki.h_km=0
			ret_ki.a_gr=math.atan2(prl.y_km,prl.x_km)/math.pi*180.0+option_prl.magnit_vpp
			if ret_ki.a_gr<0 then ret_ki.a_gr=ret_ki.a_gr+360.0 end
			if ret_ki.a_gr>=360 then ret_ki.a_gr=ret_ki.a_gr-360.0 end
		end		
	else		
		if ret_ki.kg_prl then
--есть глисандня, но нет курсовой инфоримации			
			tmp_dbl=prl.x_km*prl.x_km+prl.z_km*prl.z_km
			ret_ki.d_km=tmp_dbl^0.5
			ret_ki.h_km=ret_ki.z_km
			ret_ki.a_gr=0
		else
			ret_ki.d_km=0
			ret_ki.a_gr=0
			ret_ki.h_km=0
		end	
	end		
	
--теперь время введем
	tm=os.date("*t")
	loc_tm1.hour=tm.hour
	loc_tm1.min=tm.min
	loc_tm1.sec=tm.sec+math.random()
	ret_ki.loc_tm=visp_get_copy_time_location(loc_tm1)
	return ret_ki
end


