--ѕредопреленные константы
KOORD_INFO_CONST=0x0A
TRASS_INFO_CONST=0x02
METEO_INFO_CONST=0x0D
ANTEN_INFO_CONST=0x1D
CONTR_INFO_CONST=0x01

NO_DATA_CONST=0xFF

SENDER_NUM=0x02
RECEIVER_NUM=0x01

--bytes in fifo
fifo_bytes={size=0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}


--данные полученного пакета
visp_package_data={}


--данные которые надо отразить
visp_data_for_view={}


function get_type_info()
	
	local kod
	if fifo_bytes.size ~= 18 or fifo_bytes[4] == nil then	
        return NO_DATA_CONST
	end		     
	
	kod=get_unsigned_diap_bits(fifo_bytes[4],2,7)
	if kod==KOORD_INFO_CONST or kod==TRASS_INFO_CONST or	
	   kod==METEO_INFO_CONST or kod==ANTEN_INFO_CONST or
	   kod==CONTR_INFO_CONST	   then
	   return kod
	end   
	
	return NO_DATA_CONST

end


--разбор координатной информации
function get_koord_info()
	local k_i={}
	if fifo_bytes.size ~= 18 then	
        		return 0, nil
	end		
	    	
			
end


function add_new_byte(b)
	local kod,j
	
	if type(b)~="number" or b<0 or b>255 then
		return -1
	end	
		
	if fifo_bytes.size==0 then
		if b~=SENDER_NUM then
			return 0
		end
	elseif 	fifo_bytes.size==1  then
		if b~=RECEIVER_NUM then
			if b==SENDER_NUM then
				return 0
			end
			fifo_bytes.size=0
			return 0
		end
	elseif fifo_bytes.size==3 then
		kod=get_unsigned_diap_bits(b,2,7)
		if kod~=KOORD_INFO_CONST and kod~=TRASS_INFO_CONST and	
		   kod~=METEO_INFO_CONST and kod~=ANTEN_INFO_CONST and
		   kod~=CONTR_INFO_CONST  then
			if b==RECEIVER_NUM and fifo_bytes[3]==SENDER_NUM then
				fifo_bytes[1]=SENDER_NUM
				fifo_bytes[2]=RECEIVER_NUM
				fifo_bytes.size=2
				return 0
			end
			if b==SENDER_NUM then
				fifo_bytes[1]=SENDER_NUM
				fifo_bytes.size=1
				return 0
			end
			fifo_bytes.size=0
			return 0
		end	
	end
		
	fifo_bytes.size=fifo_bytes.size+1
		
	fifo_bytes[fifo_bytes.size]=b	

	if fifo_bytes.size==18 then
		kod=get_type_info()
		if kod==KOORD_INFO_CONST then
			visp_package_data=read_koord_info(fifo_bytes)
			visp_data_for_view=get_data_prl(visp_package_data)
		elseif kod==TRASS_INFO_CONST then
			visp_package_data=read_trass_info(fifo_bytes)
			visp_data_for_view=get_data_prl(visp_package_data)
		elseif visp_package_data==METEO_INFO_CONST  then
			visp_package_data=read_meteo_info(fifo_bytes)
			visp_data_for_view={}
		elseif kod==ANTEN_INFO_CONST then
			visp_package_data=read_anten_info(fifo_bytes)
			visp_data_for_view={}	
		else
			visp_package_data={}
			visp_data_for_view={}			
		end	
		if  visp_package_data==nil then
			visp_package_data={}
		end 

		if  visp_data_for_view==nil then
			visp_data_for_view={}
		end	


		fifo_bytes.size=0
		return kod
	end
	return 0
end
