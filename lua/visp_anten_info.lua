
function read_anten_info(fb)
	local ai={}
	if  fb.size ~= 18 then
		return nil
	end
	
-- получение номеров отправителей получателей	
	ai.n_sender=get_unsigned_bits(fb[1],8)
	ai.n_receiver=get_unsigned_bits(fb[2],8)
	
--Получить тип донесения	
	ai.type_i=get_unsigned_diap_bits(fb[4],2,7)
	
--получить распоряжение для подтверждения
	ai.p=get_unsigned_bits(fb[6],0,6)
	
-- а теперь проверим, чтобы ai.p=0101010
	if ai.p ~= 0x2A  then
		ai=nil
		return ai
	end
	
--азимут центра сектора работы канала курса ПРЛ, град
	ai.azmt_kk=0.5*get_unsigned_diap_bits({fb[8], fb[7]}, {6,0},{7,7})	
	
--азимут положения максимума диаграммы направленности антены глиссады ПРЛ, 
	ai.azmt_max_akg=0.5*get_unsigned_diap_bits({fb[10], fb[9]}, {6,0},{7,7})	
	
	return ai
end	

