--Здесь объявлены данные для работы с программой test_udp



--Тестовая переменная, через нее будет общение
t_pkg={}


--координаты для отображения 
t_prl={}

--последовательность байт, которая посылается
t_bytes={}


--получить из t_pkg последовательность байт
function glbl_get_bytes()
	if t_pkg.type_i==KOORD_INFO_CONST then
		t_bytes=get_koord_package(t_pkg)
	elseif t_pkg.type_i==TRASS_INFO_CONST then
		t_bytes=get_trass_package(t_pkg)
	else
		t_bytes={}
		return -1	
	end
	
	if t_bytes==nil then
		t_bytes={}
		return (-2)
	end				
	
	return 1
end


function glbl_get_string_pkg()
	return get_string("t_pkg", t_pkg)	
end


function glbl_get_string_bytes()
	return get_string("t_bytes", t_bytes)	
end


