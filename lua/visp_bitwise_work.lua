-- Функция получения последовательности бит
function get_unsigned_bits (number, n_bits)
	local g=~(((-1)>>n_bits)<<n_bits)
	return (g&number)	
end


function get_unsigned_diap_bits (number, indxs1, indxs2)
	local rez=number>>indxs1
	return get_unsigned_bits(rez, indxs2-indxs1+1)
end




function get_signed_bits (number, n_bits)
	local g=(((-1)>>n_bits)<<n_bits)
	local rez=number&(~g)
	if  ((1<<(n_bits-1)) & rez) ==0 then
		return rez
	end
	return (rez | g)	
end




function get_unsigned_bytes_diap_bits(byts, indxs1, indxs2)
    local i
	local tmp_rez_glb=0
	local n_bits=0
    for i=1, 4 do
		if byts[i]==nil or indxs1[i]==nil or indxs2[i]==nil or 
	       indxs1[i]<0 or indxs1[i]>7 or
	       indxs2[i]<0 or indxs2[i]>7 or
	       indxs1[i]>indxs2[i]     then
	    	break
	    end
		tmp_rez_glb=tmp_rez_glb|(get_unsigned_diap_bits(byts[i], indxs1[i], indxs2[i])<<n_bits)
		n_bits=n_bits+indxs2[i]-indxs1[i]+1			
    end		
	return tmp_rez_glb 
end


function get_signed_bytes_diap_bits(byts, indxs1, indxs2)
    local i
	local tmp_rez_glb=0
	local n_bits=0
    for i=1, 4 do
		if byts[i]==nil or indxs1[i]==nil or indxs2[i]==nil or 
	      indxs1[i]<0 or indxs1[i]>7 or
	      indxs2[i]<0 or indxs2[i]>7 or
	      indxs1[i]>indxs2[i]     then
	    	break
	    end
		tmp_rez_glb=tmp_rez_glb|(get_unsigned_diap_bits(byts[i], indxs1[i], indxs2[i])<<n_bits)
		n_bits=n_bits+indxs2[i]-indxs1[i]+1			
    end		
	if  ((1<<(n_bits-1)) & tmp_rez_glb) ==0 then
		return tmp_rez_glb
	end
	return (((-1)>>n_bits)<<n_bits)|tmp_rez_glb
end




function get_copy_data(dt)
	local rt={size=dt.size,dt[1],dt[2],dt[3],dt[4], dt[5],dt[6],dt[7],dt[8],dt[9],dt[10], dt[11],dt[12],dt[13],dt[14],dt[15],dt[16], dt[17],dt[18]}
	return rt
end




function set_one_bit(o_byte, n_bit, znach)
	local ret_byte=o_byte


	if znach==1 then
--установим бит в 1
		ret_byte=(ret_byte|(1<<n_bit))&0xFF
	else
--обнулим бит	
		ret_byte=(ret_byte)&((~(1<<n_bit))&0xFF)
	end	

	return ret_byte	
end


function get_bytes_from_integer(dt, us, n_bytes, indxs1,indxs2)
	local rt=get_copy_data(dt)
	local n
	local i
	local j
	local bits={}
	local us1=us
--получим список бит	
	for i=0, 63 do
		if (us1%2) == 1 then
			bits[i]=1
		else
			bits[i]=0
		end
		us1=us1>>1
		
	end

	n=0
	for i=1, 4 do
		
		if n_bytes[i]==nil or indxs1[i]==nil or indxs2[i]==nil then break end
		
		for j=indxs1[i], indxs2[i] do
			rt[n_bytes[i]]=set_one_bit(rt[n_bytes[i]],j, bits[n])
			n=n+1	
		end
	end	
	return rt
end