--������ ������ ����������

function read_koord_info(fb)
	local ki={}
	if  fb.size ~= 18 then
		return nil
	end

-- ��������� ������� ������������ �����������	
	ki.n_sender=get_unsigned_bits(fb[1],8)
	ki.n_receiver=get_unsigned_bits(fb[2],8)
	
--������ ���������� � ��������� ��������� ���������������� ����������	
	if  get_unsigned_diap_bits(fb[3],0,0) == 1 then ki.kg_prl=true else ki.kg_prl=false end
	if  get_unsigned_diap_bits(fb[3],1,1) == 1 then ki.kk_prl=true else ki.kk_prl=false end
	
--�������� ��� ���������	
	ki.type_i=get_unsigned_diap_bits(fb[4],2,7)
	
--�������� ������� ������� ���������. ������ ���� 0	
	ki.psk=get_unsigned_diap_bits(fb[4],0,1)
	
	
--������� ���������� ��������� ����� �� ������ ����� ��� �� ����� � ��������
	ki.ok_gradus = 0.01*get_signed_bytes_diap_bits({fb[6],fb[5]},{4,0},{7,7})

--�������� ���������� �� ����� �������� � ��������
	ki.og_gradus = 0.01*get_signed_bytes_diap_bits({fb[18],fb[6]},{0,0},{7,3})

--��������� ��������� � ��
    ki.d_km = 0.001*get_unsigned_bytes_diap_bits({fb[8],fb[7]}, {0, 0}, {7,7})

--��������� �������
	ki.a_gr=0.01*get_unsigned_bytes_diap_bits({fb[10], fb[9]},{0, 0},{7, 7})
	
--������� ������������ �������� ��������
	if  get_unsigned_diap_bits(fb[11],6,6) == 1 then ki.mestnik=true end		
	
--�� --- �� ���������
  
-- ��������� ������  � ���������� 	
	ki.h_km = 0.001*get_unsigned_bytes_diap_bits({fb[12], fb[11]}, {0, 0}, {7, 4})
	
-- ��������� ������� ������� � �����, �������, �������� � ��������� �� ����� �������
   ki.loc_tm=visp_get_time_location(0.01*get_unsigned_bytes_diap_bits({fb[16],fb[15],fb[17]},{0,0,0},{7,7,7}))
   return ki
end




function get_koord_package(ki)
	local fb={size=18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
	local tmp_zn
	local tmp_zn2
	
	if type(ki) ~= "table" then
		return nil
	end
	
-- ��������� ������� ������������ �����������		
	if type(ki.n_sender) ~= "number" then
		return nil
	end
	fb=get_bytes_from_integer(fb,ki.n_sender, {1}, {0}, {7})
	
	if type(ki.n_receiver) ~= "number" then
		return nil
	end
	fb=get_bytes_from_integer(fb,ki.n_receiver, {2}, {0}, {7})
	
	
	
--������ ���������� � ��������� ��������� ���������������� ����������	
	if ki.kg_prl then
		fb=get_bytes_from_integer(fb, 1, {3},{0},{0})
	end
	

	if ki.kk_prl then
		fb=get_bytes_from_integer(fb, 1, {3},{1},{1})
	end
	
	
	--������� ��� ��������	
	fb=get_bytes_from_integer(fb, KOORD_INFO_CONST,{4},{2},{7})
	
--��� �������� �� ����� --- ��� ����
		
	
--���������� ��������� ����� �� ������ ����� ��� �� ����� � ��������
	if type(ki.ok_gradus) ~= "number" then return nil end
	tmp_zn=math.floor(ki.ok_gradus*100)
	if tmp_zn<-2047 then tmp_zn=-2047 end
	if tmp_zn>2047 then tmp_zn=2047	end
	fb=get_bytes_from_integer(fb,tmp_zn,{6,5}, {4,0}, {7,7})
	
--���������� �� ����� �������� � ��������
	if type(ki.og_gradus) ~= "number" then return nil end
	tmp_zn=math.floor(ki.og_gradus*100)
	if tmp_zn<-1023 then tmp_zn=-1023 end
	if tmp_zn>1023 then tmp_zn=1023 end
	fb=get_bytes_from_integer(fb,tmp_zn, {18,6}, {0,0}, {7,3})
	
--��������� � ��
	if type(ki.d_km) ~= "number" then return nil end
	tmp_zn=math.floor(ki.d_km*1000)
	if tmp_zn<0 then tmp_zn=0 end
	if tmp_zn>60000 then tmp_zn=60000 end
	fb=get_bytes_from_integer(fb,tmp_zn, {8,7}, {0,0}, {7,7})
	
	
--������
	if type(ki.a_gr) ~= "number" then return nil end
	tmp_zn=math.floor(ki.a_gr*100)
	if tmp_zn<0 then tmp_zn=0 end 
	if tmp_zn>35999 then tmp_zn=35999 end
	fb=get_bytes_from_integer(fb,tmp_zn, {10,9}, {0,0}, {7,7})
	
--������� ������������ �������� ��������
	if  get_unsigned_diap_bits(fb[11],6,6) == 1 then ki.mestnik=true end		
	if ki.mestnik then
		fb=get_bytes_from_integer(fb,1, {11}, {6}, {6})
	end	
	
-- ��� ������ ������ 0


-- ��������� ������  � ���������� 	
	if type(ki.h_km) ~= "number" then return nil end
	tmp_zn=math.floor(ki.h_km*1000)
	if tmp_zn<0 then tmp_zn=0 end 
	if tmp_zn>8191 then tmp_zn=8191 end
	fb=get_bytes_from_integer(fb,tmp_zn,{12,11}, {0,0},{7,4})
	
	
--���������� �� ��������. ��������� �������� ����� � �������� �� �������
	tmp_zn2=visp_get_sec_from_time_location(ki.loc_tm)
--���� ����� �� ���� ������, �� ������ ������� �����	
	if type(tmp_zn2) ~= number then
		local tm
		local loc_tm1={}
--os.date("*t") --- ���������� ���� � ����� � ��������� �� ������ 		
		tm=os.date("*t")
		loc_tm1.hour=tm.hour
		loc_tm1.min=tm.min
		loc_tm1.sec=tm.sec+math.random()
--��� ��� ��������� �������� ����� � ������ ����� � ��������		
		tmp_zn2=visp_get_sec_from_time_location(loc_tm1)
	end
	tmp_zn=math.floor(tmp_zn2*100)
--������������� ��������	
	if tmp_zn>8639999 then tmp_zn=8639999 end
	fb=get_bytes_from_integer(fb, tmp_zn, {16,15,17}, {0,0,0}, {7,7,7})	
	
	fb.size=18
	return fb
	
	
	
end	