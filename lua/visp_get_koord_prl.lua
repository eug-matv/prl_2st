

option_prl={}


option_prl.min_ugol_kurs=-17.5
option_prl.max_ugol_kurs=17.5 
option_prl.min_ugol_glis=-1.0 
option_prl.max_ugol_glis=9.0 

option_prl.min_ugol_scan_glis_in_kurs=-17.0
option_prl.max_ugol_scan_glis_in_kurs=17.0
option_prl.min_ugol_scan_kurs_in_glis=-1.0
option_prl.max_ugol_scan_kurs_in_glis=8.0



--Координаты РСП в координатах МПТСК
option_prl.x_prl_km=-10
option_prl.y_prl_km=10

--Уровень  взлетной полосы относительно антенны
option_prl.lLevelOfVPP_m=0

--Магнитный курс ВПП
option_prl.magnit_vpp=30.5

--Максимальная дальность
option_prl.max_d_km=80.0











function get_data_prl(dat)

	local data_prl={}
	local ugl

	if  type(dat) ~= "table"   then
		return nil
	end	
		


	if  dat.type_i == KOORD_INFO_CONST then

	
-- получаем данные от цели
		data_prl.type_i=KOORD_INFO_CONST
		
-- а тут информация чисто для вывода
		data_prl.tip=1	
		
		
		if type(dat.d_km) ~= "number" or type(dat.a_gr) ~= "number" or
		   type(dat.h_km) ~= "number" then return nil end
		
		
		if dat.kk_prl then
			if dat.kg_prl then
				local cos_z, sin_z;
				local ugl=dat.a_gr-option_prl.magnit_vpp
								
				data_prl.z_km=dat.h_km
				sin_z=data_prl.z_km/dat.d_km
				cos_z=(1.0-sin_z*sin_z)^0.5			
				data_prl.x_km=cos_z*dat.d_km*math.cos(ugl/180.0*math.pi)
				data_prl.y_km=cos_z*dat.d_km*math.sin(ugl/180.0*math.pi)
			else
--только курсовой данные
				data_prl.z_km=nil
				local ugl=dat.a_gr-option_prl.magnit_vpp
				data_prl.x_km=dat.d_km*math.cos(ugl/180.0*math.pi)
				data_prl.y_km=dat.d_km*math.sin(ugl/180.0*math.pi)

			end	
		else
			if dat.kg_prl then
--есть глисандня, но нет курсовой инфоримации			
				local cos_z, sin_z;
				data_prl.z_km=dat.h_km
				sin_z=data_prl.z_km/dat.d_km
				cos_z=(1.0-sin_z*sin_z)^0.5			
				data_prl.x_km=cos_z*dat.d_km			
				data_prl.y_km=nil
			else
				data_prl=nil 
				return nil
			end
		end
	elseif dat.type_i == TRASS_INFO_CONST then
		
	
-- Разберемся с координатами
		data_prl.type_i=TRASS_INFO_CONST
				
-- а тут информация чисто для вывода
		
		data_prl.tip=7				


		
		data_prl.y_km=(dat.x_km-option_prl.x_prl_km)*math.cos(option_prl.magnit_vpp/180.0*math.pi)-
				    		  (dat.y_km-option_prl.y_prl_km)*math.sin(option_prl.magnit_vpp/180.0*math.pi)

		data_prl.x_km=(dat.y_km-option_prl.y_prl_km)*math.cos(option_prl.magnit_vpp/180.0*math.pi)+
				  (dat.x_km-option_prl.x_prl_km)*math.sin(option_prl.magnit_vpp/180.0*math.pi)					
				
		data_prl.z_km=dat.h_km-option_prl.lLevelOfVPP_m/1000.0
				
--разберемся со скоростями
		data_prl.vy_m_s=dat.vx_m_s*math.cos(option_prl.magnit_vpp/180.0*math.pi)-
 				dat.vy_m_s*math.sin(option_prl.magnit_vpp/180.0*math.pi)
								
		data_prl.vx_m_s=dat.vy_m_s*math.cos(option_prl.magnit_vpp/180.0*math.pi)+
 				dat.vx_m_s*math.sin(option_prl.magnit_vpp/180.0*math.pi)
					
		data_prl.vz_m_s=dat.vh_m_s	


		
		if data_prl.vx_m_s-math.floor(data_prl.vx_m_s) >=0.5 then
			data_prl.vx_m_s=math.floor(data_prl.vx_m_s)+1 
		else
			data_prl.vx_m_s=math.floor(data_prl.vx_m_s)
		end

		if data_prl.vy_m_s-math.floor(data_prl.vy_m_s) >=0.5 then
			data_prl.vy_m_s=math.floor(data_prl.vy_m_s)+1 
		else
			data_prl.vy_m_s=math.floor(data_prl.vy_m_s)
		end



		if data_prl.vz_m_s-math.floor(data_prl.vz_m_s) >=0.5 then
			data_prl.vz_m_s=math.floor(data_prl.vz_m_s)+1 
		else
			data_prl.vz_m_s=math.floor(data_prl.vz_m_s)
		end
		

--присвоим номер трассы				
		data_prl.t_num=dat.t_num
				
				
				
	else
		return nil
	end
	
	
	
	

--Проверим координату на удаление от РПС попадание 
	

	if data_prl.x_km < 0 or data_prl.x_km>option_prl.max_d_km then return nil end

	
	
--Если точка выходит за сектор глиссады или сектор сканирование глиссаы в поле курса, то координату по глиссаде надо выкинуть
	if type(data_prl.z_km)=="number" then
		local z1,z2,y1,y2
		z1=data_prl.x_km*math.tan(option_prl.min_ugol_glis/180.0*math.pi)
		z2=data_prl.x_km*math.tan(option_prl.max_ugol_glis/180.0*math.pi)
		y1=data_prl.x_km*math.tan(option_prl.min_ugol_scan_glis_in_kurs/180.0*math.pi)
		y2=data_prl.x_km*math.tan(option_prl.max_ugol_scan_glis_in_kurs/180.0*math.pi)
		if data_prl.z_km<z1 or 	data_prl.z_km>z2 then 
			data_prl.z_km=nil
		else
			if type(data_prl.y_km)=="number" then
				if data_prl.y_km<y1 or 	data_prl.y_km>y2 then
					data_prl.z_km=nil				
				end
			else
				data_prl.z_km=nil
			end
		end

	end		
	
	if type(data_prl.y_km)=="number" then
		local z1,z2,y1,y2
		y1=data_prl.x_km*math.tan(option_prl.min_ugol_kurs/180.0*math.pi)
		y2=data_prl.x_km*math.tan(option_prl.max_ugol_kurs/180.0*math.pi)
		z1=data_prl.x_km*math.tan(option_prl.min_ugol_scan_kurs_in_glis/180.0*math.pi)
		z2=data_prl.x_km*math.tan(option_prl.max_ugol_scan_kurs_in_glis/180.0*math.pi)
		if data_prl.y_km<y1 or 	data_prl.y_km>y2 then 
			data_prl.y_km=nil
		else
			if type(data_prl.z_km)=="number" then
				if data_prl.z_km<z1 or 	data_prl.z_km>z2 then
					data_prl.y_km=nil				
				end
			else
				data_prl.y_km=nil
			end
		end

	end		
	
	if data_prl.x_km==nil and data_prl.y_km==nil then
		data_prl=nil
		return nil
	end


--скопируем данные	
	data_prl.loc_tm=visp_get_copy_time_location(dat.loc_tm)
	
	
	return data_prl

end






