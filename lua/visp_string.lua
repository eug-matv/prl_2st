




function get_string(sname,dt)
	if type(sname) ~="string" or type(dt) ~= "table" then return "not table" end
	if type(dt.size)=="number" then
		return get_string_bytes_package(sname,dt)
	end
	
	if type(dt.n_sender)=="number" and type(dt.n_receiver)=="number" and  type(dt.type_i)=="number" and
	   dt.type_i==KOORD_INFO_CONST then
		return get_string_koord_info(sname,dt)
	end

	
	if type(dt.n_sender)=="number" and type(dt.n_receiver)=="number" and  type(dt.type_i)=="number" and
	   dt.type_i==TRASS_INFO_CONST then
		return get_string_trass_info(sname,dt)
	end
	
	return get_string_koord_prl(sname,dt)   		


end


function get_string_koord_info(sname, ki)
	local s=""
	local was_time=false

	if type(sname) ~="string" or type(ki) ~= "table" then return "not table" end
	
	if type(ki.n_sender) ~= "number" then return "error" end
	if type(ki.n_receiver) ~= "number" then return "error" end
	s=s..sname..".n_sender="..string.format("%d", ki.n_sender).."  "
	s=s..sname..".n_receiver="..string.format("%d", ki.n_receiver).."  "
	if ki.kk_prl==true then 
		s=s..sname..".kk_prl=true "
	elseif ki.kk_prl==nil then
		s=s..sname..".kk_prl=nil "
	else	
		s=s..sname..".kk_prl=false "
	end

	if ki.kg_prl==true then 
		s=s..sname..".kg_prl=true "
 	elseif ki.kg_prl==nil then
		s=s..sname..".kg_prl=nil "
	else	
		s=s..sname..".kg_prl=false "
	end
	
	if type(ki.type_i) ~= "number" then return ("error "..sname..".type_i") end
	s=s..sname..".type_i="..string.format("%d",ki.type_i).."  "

	if type(ki.psk) ~= "number" then return ("error "..sname..".psk") end
	s=s..sname..".psk="..string.format("%d", ki.psk).."  "
	
	if type(ki.ok_gradus) ~= "number" then return ("error "..sname..".ok_gradus") end
	if type(ki.og_gradus) ~= "number" then return ("error "..sname..".og_gradus") end
	if type(ki.a_gr) ~= "number" then return ("error "..sname..".a_gr") end
	if type(ki.d_km) ~= "number" then return ("error "..sname..".d_km") end
	if type(ki.h_km) ~= "number" then return ("error "..sname..".h_km") end


	s=s..sname..".ok_gradus="..string.format("%5.2f", ki.ok_gradus).."  "
	s=s..sname..".og_gradus="..string.format("%5.2f", ki.og_gradus).."  "
	s=s..sname..".d_km="..string.format("%7.3f", ki.d_km).."  "
	s=s..sname..".a_gr="..string.format("%6.2f", ki.a_gr).."  "
	s=s..sname..".h_km="..string.format("%5.3f", ki.h_km).."  "
	
	if ki.mestnik==true then 
		s=s..sname..".mestnik=true  "
	elseif ki.mestnik==false then
		s=s..sname..".mestnik=false "
	else
		s=s..sname..".mestnik=nil "	
	end	
	
	if type(ki.loc_tm)=="table" then
		if type(ki.loc_tm.hour)=="number" and
		   type(ki.loc_tm.min)=="number" and
		   type(ki.loc_tm.sec)=="number" then
			s=s..sname..".loc_tm={hour="..string.format("%02d",ki.loc_tm.hour)..", min="..
			  string.format("%02d",ki.loc_tm.min)..", sec="..
                          string.format("%05.02f",ki.loc_tm.sec).."} "					
			was_time=true
		end
	end
	
	if was_time ~= true then
		s=s..sname..".loc_tm=visp_get_cur_time() "	
	end
	return s
end


function get_string_trass_info(sname,ti)
	local s=""
	local was_time=false

	if type(sname) ~="string" or type(ti) ~= "table" then return nil end
	
	if type(ti.n_sender) ~= "number" then return "error" end
	if type(ti.n_receiver) ~= "number" then return "error" end
	s=s..sname..".n_sender="..string.format("%d", ti.n_sender).."  "
	s=s..sname..".n_receiver="..string.format("%d", ti.n_receiver).."  "
	if ti.kk_prl==true then 
		s=s..sname..".kk_prl=true "
	elseif ti.kk_prl==nil then
		s=s..sname..".kk_prl=nil "
	else	
		s=s..sname..".kk_prl=false "
	end

	if ti.kg_prl==true then 
		s=s..sname..".kg_prl=true "
 	elseif ti.kg_prl==nil then
		s=s..sname..".kg_prl=nil "
	else	
		s=s..sname..".kg_prl=false "
	end
	
	if type(ti.sopr) ~= "table" then return ("error "..sname..".sopr") 
	else
		was_el=false
		s=s..sname..".sopr={"
		if  ti.sopr.new_trass==true then
			if was_el then s=s.."," end 
			s=s.." new_trass=true" 
			was_el=true 
		end
		
		if ti.sopr.extropolir==true then
			if was_el then s=s.."," end
			s=s.." extropolir=true" 
			was_el=true 
		end

		if ti.sopr.upd_xy==true then 
			if was_el then s=s.."," end
			s=s.." upd_xy=true" 
			was_el=true 
		end
		if ti.sopr.upd_xyh==true  then 
			if was_el then s=s.."," end
			s=s.." upd_xyh=true" 
			was_el=true 
		end

		if ti.sopr.reset_trass==true then 
			if was_el then s=s.."," end
			s=s.." reset_trass=true" 
		end
		s=s.." }  "	
	end	

	if type(ti.type_i) ~= "number" then return ("error "..sname..".type_i") end
	s=s..sname..".type_i="..string.format("%d",ti.type_i).."  "

	if type(ti.psk) ~= "number" then return ("error "..sname..".psk") end
	s=s..sname..".psk="..string.format("%d", ti.psk).."  "

	if type(ti.t_num) ~= "number" then return ("error "..sname..".t_num") end
	s=s..sname..".t_num="..string.format("%d", ti.t_num).."  "


	if type(ti.manevr) ~= "table" then return ("error "..sname..".manevr") 
	else
		was_el=false
		s=s..sname..".manevr={"	
		if ti.manevr.k == true then
			if was_el then s=s.."," end
			s=s.." k=true"
			was_el=true
		end
		if ti.manevr.h == true then
			if was_el then s=s.."," end
			s=s.." h=true"
			was_el=true
		end

		if ti.manevr.v == true then
			if was_el then s=s.."," end
			s=s.." h=true"
		end
		s=s.." }  "
	end
	if type(ti.x_km) ~= "number" then return ("error "..sname..".x_km") end 
	if type(ti.y_km) ~= "number" then return ("error "..sname..".y_km") end 
	if type(ti.h_km) ~= "number" then return ("error "..sname..".h_km") end 
	if type(ti.vx_m_s) ~= "number" then return ("error "..sname..".vx_m_s") end 
	if type(ti.vy_m_s) ~= "number" then return ("error "..sname..".vy_m_s") end 
	if type(ti.vh_m_s) ~= "number" then return ("error "..sname..".vh_m_s") end 

	s=s..sname..".x_km="..string.format("%5.02f", ti.x_km).."  "
	s=s..sname..".y_km="..string.format("%5.02f", ti.y_km).."  "
	s=s..sname..".h_km="..string.format("%5.03f", ti.h_km).."  "
	s=s..sname..".vx_m_s="..string.format("%d", ti.vx_m_s).."  "
	s=s..sname..".vy_m_s="..string.format("%d", ti.vy_m_s).."  "
	s=s..sname..".vh_m_s="..string.format("%d", ti.vh_m_s).."  "

	if type(ti.loc_tm)=="table" then
		if type(ti.loc_tm.hour)=="number" and
		   type(ti.loc_tm.min)=="number" and
		   type(ti.loc_tm.sec)=="number" then
			s=s..sname..".loc_tm={hour="..string.format("%02d",ti.loc_tm.hour)..", min="..
			  string.format("%02d",ti.loc_tm.min)..", sec="..
                          string.format("%05.02f",ti.loc_tm.sec).."} "					
			was_time=true
		end
	end
	
	if was_time ~= true then
		s=s..sname..".loc_tm=visp_get_cur_time() "	
	end
	return s
end

function get_string_koord_prl(sname,prl)
	local s=""		
	local was_time=false
	if type(sname) ~="string" or type(prl) ~= "table" then return nil end
	
	if type(prl.type_i) ~= "number" then s=s..sname..".type_i=nil  " 
	else
		s=s..sname..".type_i="..string.format("%d", prl.type_i).."  "
	end
	
	if type(prl.tip) ~= "number" then s=s..sname..".tip=nil  " 
	else
		s=s..sname..".tip="..string.format("%d", prl.tip).."  "
	end

	if type(prl.x_km)=="nil" then s=s..sname..".x_km=nil  " 
	elseif type(prl.x_km)=="number" then
		s=s..sname..".x_km="..string.format("%6.03f",prl.x_km).."  "
	else	return ("error "..sname..".x_km") end

	if type(prl.y_km)=="nil" then s=s..sname..".y_km=nil  " 
	elseif type(prl.y_km)=="number" then
		s=s..sname..".y_km="..string.format("%6.03f",prl.y_km).."  " 
	else	return ("error "..sname..".y_km") end

	if type(prl.z_km)=="nil" then s=s..sname..".z_km=nil  " 
	elseif type(prl.z_km)=="number" then
		s=s..sname..".z_km="..string.format("%6.03f",prl.z_km).."  "
	else	return ("error "..sname..".z_km") end

	if type(prl.vx_m_s)=="nil" then s=s..sname..".vx_m_s=nil  " 
	elseif type(prl.vx_m_s)=="number" then
		s=s..sname..".vx_m_s="..string.format("%6.03f",prl.vx_m_s).."  "
	else	return ("error "..sname..".prl.vx_m_s") end

	if type(prl.vy_m_s)=="nil" then s=s..sname..".vy_m_s=nil  " 
	elseif type(prl.vy_m_s)=="number" then
		s=s..sname..".vy_m_s="..string.format("%6.03f",prl.vy_m_s).."  " 
	else	return ("error "..sname..".vy_m_s") end

	if type(prl.vz_m_s)=="nil" then s=s..sname..".vz_m_s=nil  " 
	elseif type(prl.vz_m_s)=="number" then
		s=s..sname..".vz_m_s="..string.format("%6.03f",prl.vz_m_s).."  "
	else	return ("error "..sname..".vz_m_s") end

	if type(prl.t_num)=="nil" then s=s..sname..".t_num=nil  "
	elseif type(prl.t_num)=="number" then
		s=s..sname..".t_num="..string.format("%d", prl.t_num).."  "
	else return ("error "..sname.."t_num") end			
	
	if type(prl.loc_tm)=="table" then
		if type(prl.loc_tm.hour)=="number" and
		   type(prl.loc_tm.min)=="number" and
		   type(prl.loc_tm.sec)=="number" then
			s=s..sname..".loc_tm={hour="..string.format("%02d",prl.loc_tm.hour)..", min="..
			  string.format("%02d",prl.loc_tm.min)..", sec="..
                          string.format("%05.02f",prl.loc_tm.sec).."} "					
			was_time=true
		end
	end
	
	if was_time ~= true then
		s=s..sname..".loc_tm=nil "	
	end
	return s	
end

function get_string_bytes_package(sname,fb)
	
	local s=""
	local i	
	if type(sname) ~="string" or type(fb) ~= "table" then return nil end
	
	if type(fb.size) ~= "number" then return ("error "..sname..".size") end
	
	s=sname.."={ size="..string.format("%d",fb.size)
	
	for i=1,fb.size do
		if  type(fb[i])=="number" then
			s=s..", 0x"..string.format("%02X",fb[i])
		else
			s=s..", nil"
		end			
	end 
	s=s.." }"
	return s
	 		
	
end


