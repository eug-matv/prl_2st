--������ ������ ����������

function read_trass_info(fb)
	local ti={}
	if  fb.size ~= 18 then
		return nil
	end

-- ��������� ������� ������������ �����������	
	ti.n_sender=get_unsigned_bits(fb[1],8)
	ti.n_receiver=get_unsigned_bits(fb[2],8)
	
--������ ���������� � ��������� ��������� ���������������� ����������	
	if  get_unsigned_diap_bits(fb[3],0,0) == 1 then ti.kg_prl=true else ti.kg_prl=false end
	if  get_unsigned_diap_bits(fb[3],1,1) == 1 then ti.kk_prl=true else ti.kk_prl=false end
	
--������� ������� �������������
    ti.sopr={}
	if  get_unsigned_diap_bits(fb[3],5,7) == 1 then ti.sopr.new_trass = true 
	elseif 	get_unsigned_diap_bits(fb[3],5,7) == 3 then ti.sopr.extropolir = true
	elseif  get_unsigned_diap_bits(fb[3],5,7) == 4 then ti.sopr.upd_xy = true
	elseif  get_unsigned_diap_bits(fb[3],5,7) == 6 then ti.sopr.upd_xyh = true
	elseif  get_unsigned_diap_bits(fb[3],5,7) == 7 then ti.sopr.reset_trass = true
	end
	
	
--�������� ��� ���������	
	ti.type_i=get_unsigned_diap_bits(fb[4],2,7)
	
--�������� ������� ������� ���������. ������ ���� 1
	ti.psk=get_unsigned_diap_bits(fb[4],0,1)

--������� ����� ������	
	ti.t_num=get_unsigned_bytes_diap_bits({fb[6], fb[5]},{7,0},{7,7})
	
--������� �������� �������
	ti.manevr={}
--������ �� �����	
	if 	get_unsigned_diap_bits(fb[6], 4,4) == 1 then  ti.manevr.k=true end
	
--������ �� ������
	if 	get_unsigned_diap_bits(fb[6], 5,5) == 1 then  ti.manevr.h=true end

--������ �� ��������
	if 	get_unsigned_diap_bits(fb[6], 6,6) == 1 then  ti.manevr.v=true end
		
--������� ������ ������� H - ���� ���������

--������� ����� 1 - ���� ���������

--���������� x � ��
    ti.x_km=0.01*get_signed_bytes_diap_bits({fb[8],fb[7],fb[6]},{0,0,1},{7,7,1})
	
--���������� y � ��
    ti.y_km=0.01*get_signed_bytes_diap_bits({fb[10],fb[9],fb[6]},{0,0,0},{7,7,0})
	
--��� ������ ������ -- ���� ���������


--�������� ����� ��� � � �/�
	ti.vx_m_s=5*get_signed_bytes_diap_bits({fb[13],fb[11]},{0,6},{7,6})
	
--�������� ����� ��� y � �/�
	ti.vy_m_s=5*get_signed_bytes_diap_bits({fb[14],fb[11]},{0,5},{7,5})
	
--������ ������ � ��
	ti.h_km=0.001*get_unsigned_bytes_diap_bits({fb[12],fb[11]},{0,0},{7,4})
	
--�������� ��������� ������ ������ � �/�
	ti.vh_m_s=get_signed_bits(fb[18],8)
	
--����� ����� ������������ ������
   ti.loc_tm=visp_get_time_location(0.01*get_unsigned_bytes_diap_bits({fb[16],fb[15],fb[17]},{0,0,0},{7,7,7}))

	return ti
	
end



function get_trass_package(ti)
	local fb={size=18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
	local tmp_zn
	local tmp_zn2
	
	if type(ti) ~= "table" then
		return nil
	end
	
-- ��������� ������� ������������ �����������		
	if type(ti.n_sender) ~= "number" then
		return nil
	end
	fb=get_bytes_from_integer(fb,ti.n_sender, {1}, {0}, {7})
	
	if type(ti.n_receiver) ~= "number" then
		return nil
	end
	fb=get_bytes_from_integer(fb,ti.n_receiver, {2}, {0}, {7})
	
	
--������ ���������� � ��������� ��������� ���������������� ����������	
	if ti.kg_prl then
		fb=get_bytes_from_integer(fb, 1, {3},{0},{0})
	end
	
	
	if ti.kk_prl then
		fb=get_bytes_from_integer(fb, 1, {3},{1},{1})
	end
	
	
--������� ������� �������������
	if type(ti.sopr)=="table" then
		if  ti.sopr.new_trass then tmp_zn=1
		elseif ti.sopr.extropolir then tmp_zn=3
		elseif ti.sopr.upd_xy then tmp_zn=4
		elseif ti.sopr.upd_xyh  then tmp_zn=6
		elseif 	ti.sopr.reset_trass then tmp_zn=7
		else tmp_zn=0 end
	else
		tmp_zn=0
	end	
	fb=get_bytes_from_integer(fb,tmp_zn,{3},{5},{7})
	
--������� ��� ���������	
	fb=get_bytes_from_integer(fb, TRASS_INFO_CONST,{4},{2},{7})

--������� ������� ������� ���������. ����� 01
	fb=get_bytes_from_integer(fb,1,{4},{0},{1})
	
	
--������� ����� ������	
	if type(ti.t_num)~="number" then return nil end
	fb=get_bytes_from_integer(fb, ti.t_num%512, {6,5}, {7,0},{7,7})
	
--������� �������� �������	
	if type(ti.manevr) == "table" then
		if ti.manevr.k then fb=get_bytes_from_integer(fb, 1, {6}, {4}, {4}) end		
		if ti.manevr.h then fb=get_bytes_from_integer(fb, 1, {6}, {5}, {5}) end		
		if ti.manevr.v then fb=get_bytes_from_integer(fb, 1, {6}, {6}, {6}) end		
	end
	
--������� ������ ������� � ��� ����� 0. ������� ����� 1 � ��� ����� 0.

	if type(ti.x_km) ~= "number" then return nil end
	if type(ti.y_km) ~= "number" then return nil end
--���������� x � ��		
	tmp_zn=math.floor(ti.x_km*100)
	if tmp_zn<-40000 then tmp_zn=-40000 end
	if tmp_zn>40000 then tmp_zn=40000 end
	fb=get_bytes_from_integer(fb,tmp_zn,{8,7,6},{0,0,1},{7,7,1})
	
--���������� y � ��	
	tmp_zn=math.floor(ti.y_km*100)
	if tmp_zn<-40000 then tmp_zn=-40000 end
	if tmp_zn>40000 then tmp_zn=40000 end
	fb=get_bytes_from_integer(fb,tmp_zn,{10,9,6},{0,0,0},{7,7,0})
	
--��� ������ ������ ���� 11, ��� 7 ����� 0 --- ����������


	if type(ti.vx_m_s) ~= "number" then return nil end
	if type(ti.vy_m_s) ~= "number" then return nil end

--�������� ����� ��� � � �/�
	tmp_zn, tmp_zn2=math.modf(ti.vx_m_s/5.0)
	if tmp_zn2<=-0.5 then tmp_zn=tmp_zn-1 end
	if tmp_zn2>=0.5 then tmp_zn=tmp_zn+1 end
	if tmp_zn<-255 then tmp_zn=-255 end
	if tmp_zn>255 then tmp_zn=255 end	
	fb=get_bytes_from_integer(fb, tmp_zn, {13,11},{0,6}, {7,6})
	
--�������� ����� ��� y � �/�
	tmp_zn, tmp_zn2=math.modf(ti.vy_m_s/5.0)
	if tmp_zn2<=-0.5 then tmp_zn=tmp_zn-1 end
	if tmp_zn2>=0.5 then tmp_zn=tmp_zn+1 end
	if tmp_zn<-255 then tmp_zn=-255 end
	if tmp_zn>255 then tmp_zn=255 end	
	fb=get_bytes_from_integer(fb, tmp_zn, {14,11},{0,5}, {7,5})
	
--������ ������ � ��
	if type(ti.h_km) ~= "number" then return nil end
	tmp_zn=math.floor(ti.h_km*1000)
	if tmp_zn<0 then tmp_zn=0 end
	if tmp_zn>8191 then tmp_zn=8191 end
	fb=get_bytes_from_integer(fb,tmp_zn,{12,11},{0,0},{7,4})
	
--�������� ��������� ������ ������ � �/�
	if type(ti.vh_m_s) ~= "number" then return nil end		
	tmp_zn=math.floor(ti.vh_m_s)
	if tmp_zn<-127 then tmp_zn=-127 end
	if tmp_zn>127 then tmp_zn=127 end
	fb=get_bytes_from_integer(fb,tmp_zn, {18}, {0}, {7})
	
--���������� �� ��������. ��������� �������� ����� � �������� �� �������
	tmp_zn2=visp_get_sec_from_time_location(ti.loc_tm)
--���� ����� �� ���� ������, �� ������ ������� �����	
	if type(tmp_zn2) ~= "number" then
		local tm
		local loc_tm1={}
--os.date("*t") --- ���������� ���� � ����� � ��������� �� ������ 		
		tm=os.date("*t")
		loc_tm1.hour=tm.hour
		loc_tm1.min=tm.min
		loc_tm1.sec=tm.sec+math.random()
--��� ��� ��������� �������� ����� � ������ ����� � ��������		
		tmp_zn2=visp_get_sec_from_time_location(loc_tm1)
	end
	tmp_zn=math.floor(tmp_zn2*100)
--������������� ��������	
	if tmp_zn>8639999 then tmp_zn=8639999 end
	fb=get_bytes_from_integer(fb, tmp_zn, {16,15,17}, {0,0,0}, {7,7,7})
	
	fb.size=18
	return fb
	
end
