

function visp_get_time_location(sec)
   local loc_tm={}
   local t
   loc_tm.hour, t = math.modf(sec/3600.0)
   loc_tm.min, t = math.modf(t*60.0)
   loc_tm.sec = t*60.0 
   return loc_tm  
end


function visp_get_copy_time_location(loc_tm)
	local ret_loc_tm={}
	ret_loc_tm.hour=loc_tm.hour
    ret_loc_tm.min=loc_tm.min
    ret_loc_tm.sec = loc_tm.sec
    return ret_loc_tm  
end


function visp_get_sec_from_time_location(loc_tm)
	if type(loc_tm) ~= "table" or
	   type(loc_tm.hour) ~= "number" or 
	   type(loc_tm.min) ~= "number" or 
	   type(loc_tm.min) ~= "number" or
	   loc_tm.hour<0 or loc_tm.hour>23 or 
	   loc_tm.min<0 or loc_tm.min>59 or
	   loc_tm.sec<0 or loc_tm.sec>59.999999999999999999 then
			return nil
	end   

	return (3600*math.floor(loc_tm.hour)+60*math.floor(loc_tm.min)+loc_tm.sec)
end

function visp_get_cur_time()
	local ret_loc_tm={}
	local tm
	tm=os.date("*t")
	ret_loc_tm.hour=tm.hour
	ret_loc_tm.min=tm.min
	ret_loc_tm.sec=tm.sec+math.random()
	return ret_loc_tm	
		
end

