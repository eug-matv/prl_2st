ret=0
i=0
dofile("./lua/visp_anten_info.lua")
dofile("./lua/visp_bitwise_work.lua")
dofile("./lua/visp_get_koord_prl.lua")
dofile("./lua/visp_get_location_time.lua")
dofile("./lua/visp_get_package.lua")
dofile("./lua/visp_koord_info.lua")
dofile("./lua/visp_meteo_info.lua")
dofile("./lua/visp_trass_info.lua")
dofile("./lua/visp_get_package_from_koord_prl.lua")
dofile("./lua/visp_string.lua")
dofile("./lua/visp_global_data_for_test.lua")


option_prl={}
option_prl.min_ugol_kurs=-17.5
 option_prl.max_ugol_kurs=17.5 
option_prl.min_ugol_glis=-1.0 
option_prl.max_ugol_glis=9.0 
option_prl.min_ugol_scan_glis_in_kurs=-17.0 
option_prl.max_ugol_scan_glis_in_kurs=17.0 
option_prl.min_ugol_scan_kurs_in_glis=-1.0 
option_prl.max_ugol_scan_kurs_in_glis=8.0
option_prl.x_prl_km=0
option_prl.y_prl_km=0
option_prl.lLevelOfVPP_m=0
option_prl.magnit_vpp=30.5
option_prl.max_d_km=80.0


my_ti={}
my_prl={}

my_ti.n_sender=SENDER_NUM
my_ti.n_receiver=RECEIVER_NUM
my_ti.kg_prl=true
my_ti.kk_prl=true
my_ti.sopr={}
my_ti.sopr.upd_xyh=true
my_ti.type_i=TRASS_INFO_CONST
my_ti.psk=1

my_ti.t_num=321
my_ti.manevr={k=true, h=true}

my_ti.x_km=0
my_ti.y_km=0
my_ti.vx_m_s=0
my_ti.vy_m_s=0
my_ti.h_km=0
my_ti.vh_m_s=0


my_prl.x_km=20.0
my_prl.y_km=0.15
my_prl.z_km=my_prl.x_km*math.tan(2.56/180*math.pi)
my_prl.vx_m_s=110
my_prl.vy_m_s=0
my_prl.vz_m_s=my_prl.vx_m_s*math.tan(2.56/180*math.pi)

print(get_string_koord_prl("my_prl", my_prl))

my_ti=make_emul_trass_info(my_ti, my_prl)



print(get_string_trass_info("t_pkg", my_ti))

my_prl2=get_data_prl(my_ti)
print(get_string_koord_prl("my_prl2", my_prl2))


